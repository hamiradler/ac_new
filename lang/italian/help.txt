##### TRANSLATION STRINGS #####
##### Example: "original string" = "your-translation-here" #####

"(Optional) Specify how many emails you wish to send with this mail connection before looking for the next connection. If you only have one mail connection this setting does not matter." = "(Facoltativo) Specifica quante email vuoi inviare con questa connessione prima di cercare la prosima connessione. Se hai solo una connessione mail non considerare questa opzione."
"(Optional) You can specify a limit of the number of emails to send in a certain time period. (Example = Only allow 1,000 emails per hour)" = "(Facoltativo) Puoi specificare un limite di email da inviare in un dato periodo. (Ad es.: Permetti solo 1000 email all'ora)"
"Attachments will be disabled." = "Gli allegati saranno disabilitati."
"Brief Description for you to recognize. Does NOT affect your actual personalization tag." = "Breve descrizione per tuo riconoscimento. Non va a modificare i tag esistenti."
"Brief Description for you to recognize. Does NOT affect your actual template." = "Breve descrizione per tuo riconoscimento. Non va a modificare i template esistenti."
"Enter a brief description for this bounce code here." = "Inserisci qui una breve descrizione di questo codice di rifiuto."
"GD Library is needed to generate images from the application. Most common (and useful) use are CAPTCHA images." = "La libreria GD deve generare immagini dall'applicazione. Le pi� usate sono le immagini CAPTCHA"
"Header needs to be in format 'HEADER-NAME: HEADER VALUE'" = "L'intestazione deve essere nel formato 'NOME INTESTAZIONE: VALORE INTESTAZIONE'"
"If authentication is not required, do not enter a password." = "Se non � richiesta un'autenticazione non inserire una password."
"If authentication is not required, do not enter a username." = "Se non � richiesta un'autenticazione non inserire uno username."
"If selected, the software will disable most functions such as deleting, sending and more." = "Se selezionato, il software disabiliter� le maggiori funzionalit� come la cancellazione, l'invio, ecc."
"If the URL is used, it has to start with http:// or https:// . Otherwise, a file path will be assumed. If a path starts with './', the application folder will be assumed." = "Se usi un URL, deve cominciare con http:// o https://, altrimenti sar� calcolato il percorso del file, se il percorso comincia con '/' sar� calcolata la cartella delle apllicazioni."
"If this option is checked, the person filling the form will not be able to proceed with form submission unless the field has some value entered." = "Se questa opzione � selezionata, la persona che compila il modulo non potr� procedere con l'iscrizione se non inserisce un valore in questo campo."
"If your mail transfer agents does not include the code in form 5.X.X., enter a string that should be matched so this Bounce Code can be assigned.\n(example: ' returned: Unknown User')" = "Se il tuo server di posta on include il codice in formato 5.X.X, inserisci una stringa che possa corrispondere cos� che venga assegnato questo codice di rifiuto.\n(esempio: ' rifiutato: Utente Sconosciuto')"
"License agreement does not allow you to set your own copyright, but you may opt to have the copyright invisibe to public/non-code view. Copyrights may never be removed from the actual code." = "La licenza d'uso non ti permette di avere il tuo copyright, ma puoi scegliere di rendere invisibile al pubblico il copyright. I copyright non possono essere rimossi dal codice."
"Logs will be kept for 30 days." = "I log saranno salvati per 30 giorni."
"Most mail transfer agents include the code in form 5.X.X. If your server is not returning those, you can enter here some identifier for this Bounce Code." = "La maggior parte dei server di posta includono il codice in formato 5.X.X. Se il tuo server non ti restituisce ci�, puoi inserire qui gli identificativi per questo codice di rifiuto."
"Note: this overrides the 'day of the month' option" = "Nota: questo non terr� conto dell'opzione 'giorno del mese'"
"Notice: Hold CTRL to select multiple destinations." = "Avviso: Tieni premuto CTRL per selezionare destinazioni multiple."
"Notice: Hold CTRL to select multiple items." = "Avviso: Tieni premuto CTRL per selezionare diversi oggetti."
"Notice: Hold CTRL to select multiple lists." = "Avviso: Tieni premuto CTRL per selezionare pi� liste."
"Notice: Hold CTRL to select multiple statuses." = "Avviso: Tieni premuto CTRL per per selezionare pi� status."
"Notice: This action will be performed on each selected list! Hold CTRL to select multiple lists." = "Avviso: Questa azione sar� effettuata su ogni lista selezionata! Tieni premuto CTRL per selezionare pi� liste."
"Notice: This custom header will be a member of each selected list! Hold CTRL to select multiple lists." = "Avviso: Questa intestazione personalizzata sar� in ogni lista selezionata! Tieni premuto CTRL per selezionare pi� liste."
"Notice: This email account will be a member of each selected list! Hold CTRL to select multiple lists." = "Avviso: Questo account email sar� in ogni lista selezionata! Tieni premuto CTRL per selezionare pi� liste."
"Notice: This message will be a member of each selected group! Hold CTRL to select multiple group." = "Avviso: Questo messaggio sar� in ogni gruppo selezionato! Tieni premuto CTRL per selezionare pi� gruppi."
"Notice: This personalization will be a member of each selected list! Hold CTRL to select multiple lists." = "Avviso: Questa personalizzazione sar� in ogni lista selezionata! Tieni premuto CTRL per selezionare pi� liste."
"Please check if any vital function is listed here." = "Controlla se nella seguente lista � presente qualche funzione fondamentale."
"Sessions need to be enabled in order for this application to work." = "Le sessioni devono essere abilitate per far s� che questa applicazione funzioni."
"Set * for 'every hour'." = "Imposta * per 'ogni ora'. "
"Set * for 'every minute'." = "Imposta * per 'ogni minuto'."
"Soft bounces usually are emails that were not delivered (yet), but the problem appears to be temporal. Hard bounces are the ones that most likely permanent (such as 'invalid email address', etc.)" = "I rifiuti soft sono solitamente email che non sono (ancora) recapitate, ma si tratta di un problema temporaneo. I rifiuti hard sono quelli permanenti (come 'indirizzo email non valido', ecc.)"
"Some scripts might not finish the execution within the allowed timeframe, which is set to a value lower than a default PHP value (30 seconds)." = "Alcuni script potrebbero non finire l'esecuzione entro il tempo stabilito, impostato al di sotto del valore PHP di default (30 secondi)."
"Subscriber will receive an opt-in email with a link to confirm list subscription(s)." = "Gli iscritti riceveranno un'email opt-in con un link per confermare l'iscrizione alla lista."
"The Analytics account number should appear in the format UA-xxxxxxx-y. The '-y' towards the end refers to the profile number." = "Il numero dell'account Analytics dovrebbe apparire nel formato UA-xxxxxxx-y. La '-y' alla fine si riferisce al numero del profilo."
"The contents of this field will show up as a tooltip when visitor hovers over the field." = "Il contenuto di questo campo sar� mostrato come tooltip qndo il visitatore ci passer� sopra."
"This is just a label for your own internal use" = "Questa � un'etichetta solo per tuo uso."
"This is the character that separates the fields within the file. Usually this is a comma." = "Questo � il carattere che separa i campi nel file. Solitamente � una virgola."
"This is the internal name for you to use. Every cron job needs to have a unique identifier." = "Questo � il tuo nome interno che usi. Ogni cron job deve avere un identificatore unico."
"This value is too low, and this server imposed limit cannot be changed." = "Questo valore � troppo basso e questo server impone un limite che non pu� essere cambiato."
"This will turn this Cron Job on or off" = "Questo accender� o spegner� il cron job."
"Upon saving, the system will try to add this content to your .htaccess file if it doesn't have it already. If the file cannot be written to, you will have to put this into file %s" = "Subito dopo aver salvato, il sistema cercher� di aggiungere questo contenuto al tuo file .htaccess se non c'� gi�. Se il file non pu� essere sovrascritto dovrai inserire questo contenuto nel file %s"
"With safe mode on, the script will not be able to adjust the server configuration for optimal performance." = "Con la modalit� sicura attiva, lo script non potr� aggiustare la configurazione del server per una performance ottimale."
"You are responsible for escaping! Also, if constructing a LIST, then strings should be encapsulated with quotes." = ""
"Your server imposes the following limits: maximum file upload size is %s and maximum post size is %s." = "Il tuo sever impone i seguenti limiti: la massima dimensione del file da caricare � %s e la dimensione massima di ogni commento � %s."
"Your server is set to allow less memory than this script requires." = "Il tuo server � impostato per permettere meno memoria rispetto a quella richiesta da questo script."



##### 2009-05-28 #####

"%REPORTLINK% is shorthand for the shared report link that you see above." = "%REPORTLINK% � una scorciatoia per il link report condiviso che vedi sopra."



##### 2009-07-10 #####




##### 2009-07-14 #####

"(Optional) This option will automatically pause your mailing after a certain number of messages.  This helps reduce your mail servers load." = "(Facoltativo) Questa opzione metter� in pausa la tua attivit� di mailing dopo n certo numero di messaggi. Questo ti aiuter� a ridurre il carico per il tuo server."
"The label of this field (a title) can be shown both above the field, or on its left side." = "L'etichetta di questo campo (titolo) pu� essere mostrata sopra o sulla sinistra."



##### 2009-08-27 #####

"This feature will use Apache's mod_rewrite plugin to reformat its public URLs to appear more descriptive, mentioning for example the title of a list or form in the URL." = "Questa funzionalit� user� il plug-in mod_rewrite di Apache per riformattare l'URL pubblico e farlo apparire pi� descrittivo, menzionando ad esempio il titolo di una lista o di un modulo."



##### 2009-11-25 #####




##### 2009-12-21 #####

"Begin typing their name and live results will be displayed." = "Inizia a digitare il loro nome e ti saranno mostrati i risultati in tempo reale."



##### 2010-03-28 #####

"If you check this option all new subscribers will be sent a copy of the last campaign you sent immediately upon subscribing." = "Se selezioni questa opzione a tutti gli iscritti verr� inviata, subito dopo l'iscrizione, una copia dell'ultima campagna che hai inviato."



##### 2010-04-07 #####




##### 2010-07-02 #####

"In some cases, particularly Active Directory, the user name you normally log in with is not actually what the server expects through its normal LDAP interface.  This field is the name of the attribute that you want to use to log in with.  In most of those cases, the value which must go here would be 'samAccountName'." = "In alcuni casi, in particolare in Active Directory, lo username con cui effettui il log-in di solito non � quello che si aspetta il server con la sua normale interfaccia LDAP. Inserisci qui il nome con cui vuoi effettuare il log-in. Nella maggior parte dei casi il valore inserito � 'samAccountName'"
"In some cases, particularly with Active Directory, we must log in with an administrative account before we can truly log in with a user.  In that case, you would place here the full distinguished name of the LDAP server's administrative user (e.g. cn=Administrator,ou=Users,dc=domain,dc=com)." = "In alcuni casi, in particolare con Active Directory, bisogna efettuare il log-in con un account amministrativo prima di loggarsi come utente. In questo caso inserisci qui il nome completo dell'amministratore del server LDAP (ad es. cn=Administrator,ou=Users,dc=domain,dc=com)."
"The attribute that the LDAP server expects us to log in with.  In the vast majority of cases, this field should be 'cn' (short for 'common name')." = "I parametri con i quali il server LDAP si aspetta che noi effettuiamo il login. Nella maggior parte dei casi questo campo � 'cn' (abbreviazione di 'common name')"
"The base of the distinguished name that users would log in with; e.g., ou=Users,dc=domain,dc=com" = "La base del nome con cui gli utenti efettueranno il log-in; ad es. ou=Users,dc=domain,dc=com"
"The full distinguished name of an administrator account, necessary for Active Directory's login process.  Example: cn=Administrator,ou=Users,dc=domain,dc=com" = "Il nome completo dell'account amministratore, necessario per il login in Active Directory."
"The password for the administrator account, mentioned above." = "La password per l'account amministratore di cui sopra."
"The password of the administrative user mentioned above." = "La password per l'utente amministratore di cui sopra."
"With this checked, we will log in with a fully-formed distinguished name (cn=user,ou=Users,dc=domain,dc=com); without it, we'll log in with 'user' and let the server sort things out." = "Con questo selezionato, effettueremo il login con un nome completo (cn=user,ou=Users,dc=domain,dc=com); senza effettueremo il login con 'user' e lasceremo che il server si organizzi."



##### 2010-09-07 #####

"The location of this software on the web. To change this, run Updater from a new location." = "Questo software � localizzato nel web, se vuoi cambiare la sua posizione apri Updater."



##### 2010-09-30 #####

"To update a different Twitter account, uncheck Twitter above, save, then come back and re-enter your information." = "Per aggiornare un altro account Twitter deseleziona Twitter qui sopra, salva, poi torna indietro e reinserisci le informazioni."



##### 2010-12-01 #####




##### 2011-02-23 #####

"Campaigns sent to this list will be auto-posted to Facebook with a link to your campaign web copy." = "Le campagne inviate a questa lista saranno automaticamente pubblicate su Facebook con un link alla versione web."
"Campaigns sent to this list will be auto-posted to Twitter with a link to your campaign web copy." = "Le campagne inviate a questa lista saranno automaticamente pubblicate su Twitter con un link alla versione web."



##### 2011-03-17 #####




##### 2011-04-06 #####

"The user who owns the list will determine which branding settings the list uses." = "L'utente a cui questa lista appartiene decider� quali impostazioni usare per il branding."


##### 2011-07-13 #####

"This button will re-import all stock templates. PLEASE NOTE: This will only import global stock templates you are missing. If you already have some stock templates, but have modified them, it will NOT overwrite customizations made." = "Cliccando questo tasto verranno reimportati i templates. NOTA: Saranno importati solo i template che mancano. Se ne hai gi� alcuni e li hai modificati, quest'azione NON li sovrascriver�."
"This value will be used as a placeholder for this personalization field. If you enter 'MYTAG', then your content should have a placeholder %MYTAG% that would be replaced with a field value. NOTE: spaces will be replaced with a dash, and % characters are not allowed." = "Questo valore sar� di riferimento per questo campo. Se inserisci 'LAMIATAG' il tuo valore dovrebbe avere un riferimento %MYTAG% che sar� sostituito dal valore del campo. NOTA: lo spazio sar� sostituito dal trattino e i caratteri %s non sono ammessi."



##### 2011-08-01 #####




##### 2011-11-03 #####




##### 2011-12-12 #####

"After logging into Twitter, you will be asked to confirm that ActiveCampaign is allowed to update your Twitter account. You will then be redirected back to this page." = "Dopo aver effettuato il login in Twitter, ti verr� chiesto di confermare il permesso a ActiveCampaign di aggiornare il tuo account."
"See instructions for the appropriate field name to include here." = "Consulta le istruzioni per il nome appropriato da inserire qui."
