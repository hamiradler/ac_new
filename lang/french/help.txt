##### TRANSLATION STRINGS #####
##### Example: "original string" = "your-translation-here" #####

"(Optional) Specify how many emails you wish to send with this mail connection before looking for the next connection. If you only have one mail connection this setting does not matter." = "(Facultatif) Spécifiez combien de courriels vous désirez envoyer avec cette connexion courriel avant de chercher la connexion suivante. Si vous avez seulement une connexion courriel ce paramètre n'importe pas."
"(Optional) You can specify a limit of the number of emails to send in a certain time period. (Example = Only allow 1,000 emails per hour)" = "(Facultatif) Vous pouvez spécifier une limite du nombre de courriels à envoyer pour une certaine période de temps. (Exemple = permettre uniquement l'envoi de 1000 courriel par heure)"
"Attachments will be disabled." = "Les pièces jointes seront désactivées."
"Brief Description for you to recognize. Does NOT affect your actual personalization tag." = "Brève description à titre indicatif. N'affecte pas l'étiquette de personnalisation actuelle."
"Brief Description for you to recognize. Does NOT affect your actual template." = "Brève description à titre indicatif. N'affecte pas le gabarit actuel"
"Enter a brief description for this bounce code here." = "Écrire une courte description pour ce code de rebond ici."
"Extra users that were not part of the sync will be removed." = "Des utilisateurs supplémentaires qui ne faisait pas une partie de la synchronisation seront enlevés."
"GD Library is needed to generate images from the application. Most common (and useful) use are CAPTCHA images." = "La librairie GD est nécessaire pour générer des images pour l'application. L'utilisation la plus commune (et la plus utile) sont les images des CAPTCHA"
"Header needs to be in format 'HEADER-NAME: HEADER VALUE'" = "Les entêtes doivent être dans le format 'NOM DE L'ENTÊTE: VALEUR DE L'ENTÊTE'"
"If authentication is not required, do not enter a password." = "Si l'authentification n'est pas exigée, n'entrez pas de mot de passe."
"If authentication is not required, do not enter a username." = "Si l'authentification n'est pas exigée, n'entrez pas de nom d'utilisateur."
"If selected, the software will disable most functions such as deleting, sending and more." = "Si cette option est sélectionnée, le logiciel rendra désactivera la plupart des fonctions comme la suppression, l'envoi et plus."
"If the URL is used, it has to start with http:// or https:// . Otherwise, a file path will be assumed. If a path starts with './', the application folder will be assumed." = "Si l'URL est utilisé, il doit commencer par http:// ou https://. Autrement, un chemin vers un fichier sera assumé. Si le chemin commence par './', le dossier de l'application sera assumé."
"If this option is checked, the person filling the form will not be able to proceed with form submission unless the field has some value entered." = "Si cette option est sélectionnée, la personne qui rempli le formulaire ne pourra pas soumettre le formulaire à moins que le champ ait une valeur inscrite"
"If your mail transfer agents does not include the code in form 5.X.X., enter a string that should be matched so this Bounce Code can be assigned.\n(example: ' returned: Unknown User')" = "Si votre agent de transfert de courriels(MTA) n'inclut pas le code sous la forme 5.X.X., entrez la chaîne de caractère qui devrait correspondre à ce code de Rebond.\n (exemple :  returned: Unknown User')"
"License agreement does not allow you to set your own copyright, but you may opt to have the copyright invisibe to public/non-code view. Copyrights may never be removed from the actual code." = "L'accord de licence ne vous permet pas de placer vos propre copyright, mais vous pouvez choisir de masquer les copyright dans la vue public/non-code. Les Copyrights ne peuvent jamais être enlevé du code actuel."
"Logs will be kept for 30 days." = "L'historique sera conservé pendant 30 jours."
"Most mail transfer agents include the code in form 5.X.X. If your server is not returning those, you can enter here some identifier for this Bounce Code." = "La plupart des agents des transferts de courriels(MTA) incluent le code sous la forme 5.X.X. Si votre serveur ne renvoie pas de tels codes, vous pouvez inscrire ici des identificateurs pour ce code de rebond."
"Note: this overrides the 'day of the month' option" = "Note: ceci outrepasse l'option 'jour du mois'"
"Notice: Hold CTRL to select multiple destinations." = "Remarque: Tenir la touche CTRL pour choisir plusieurs destinations."
"Notice: Hold CTRL to select multiple items." = "Remarque: Tenir la touche CTRL pour sélectionner plusieurs éléments."
"Notice: Hold CTRL to select multiple lists." = "Remarque: Tenir la touche CTRL pour choisir les plusieurs listes."
"Notice: Hold CTRL to select multiple statuses." = "Remarque: Tenir la touche CTRL pour choisir plusieurs statuts."
"Notice: This action will be performed on each selected list! Hold CTRL to select multiple lists." = "Remarque: Cette action sera effectuée sur chacune des listes sélectionnées! Tenir la touche CTRL pour choisir plusieurs listes."
"Notice: This custom header will be a member of each selected list! Hold CTRL to select multiple lists." = "Remarque: Cette entête personnalisée sera membre de chacune des listes sélectionnées! Tenir la touche CTRL pour choisir plusieurs listes. "
"Notice: This email account will be a member of each selected list! Hold CTRL to select multiple lists." = "Remarque: Ce compte courriel sera membre de chacune des listes sélectionnées! Tenir la touche CTRL pour choisir plusieurs listes."
"Notice: This message will be a member of each selected group! Hold CTRL to select multiple group." = "Remarque: Ce message sera membre de chacun des groupes sélectionnés! Tenir la touche CTRL pour choisir plusieurs groupes."
"Notice: This personalization will be a member of each selected list! Hold CTRL to select multiple lists." = "Remarque: Cette personnalisation sera membre de chacune des listes sélectionnées! Tenir la touche CTRL pour choisir plusieurs listes."
"Please check if any vital function is listed here." = "Veuillez vérifier si une fonction essentielle est répertoriée ici. "
"Sessions need to be enabled in order for this application to work." = "Les sessions doivent être activés pour que cette application fonctionne."
"Set * for 'every hour'." = "Mettre * pour 'chaque heure'."
"Set * for 'every minute'." = "Mettre * pour 'chaque minute'."
"Soft bounces usually are emails that were not delivered (yet), but the problem appears to be temporal. Hard bounces are the ones that most likely permanent (such as 'invalid email address', etc.)" = "Les rebonds doux sont habituellement des courriels qui n'ont pas été livrés (encore), mais le problème semble temporaire. Les rebonds durs sont ceux qui sont probablement permanent (comme un courriel invalide, etc.)"
"Some scripts might not finish the execution within the allowed timeframe, which is set to a value lower than a default PHP value (30 seconds)." = "Quelques scripts ne pourraient pas finir l'exécution dans le temps donné, qui à une valeur inférieure à la valeur de PHP de défaut (30 secondes)."
"Subscriber will receive an opt-in email with a link to confirm list subscription(s)." = "L'abonné recevra un courriel d'adhésion avec un lien pour confirmer l'abonnement aux liste(s)"
"The Analytics account number should appear in the format UA-xxxxxxx-y. The '-y' towards the end refers to the profile number." = "Le numéro de compte de google Analytics devrait être dans le format UA-xxxxxxx-y. Le '-y' de la fin se réfère au numéro du profil. "
"The contents of this field will show up as a tooltip when visitor hovers over the field." = "Le contenu de ce champ apparaîtra en tooltip lorsque le visiteur placera la souris au-dessus du champ."
"This is just a label for your own internal use" = "C'est juste une étiquette pour votre propre usage"
"This is the character that separates the fields within the file. Usually this is a comma." = "C'est le caractère qui sépare les champs dans le fichier. Habituellement c'est une virgule"
"This is the internal name for you to use. Every cron job needs to have a unique identifier." = "C'est un nom interne pour vous. Chaque tâche cron doit avoir un identifiant unique."
"This value is too low, and this server imposed limit cannot be changed." = "Cette valeur est trop basse, et cette limite imposée par serveur ne peut pas être changée."
"This will turn this Cron Job on or off" = "Ceci activera ou désactivera cette tâche Cron"
"Upon saving, the system will try to add this content to your .htaccess file if it doesn't have it already. If the file cannot be written to, you will have to put this into file %s" = "Lors de l'enregistrement, le système essayera d'ajouter ce contenu à votre fichier .htaccess s'il n'y est pas déjà. Si le fichier n'a pas les permissions d'écriture, vous devrez mettre ceci dans le fichier %s"
"With safe mode on, the script will not be able to adjust the server configuration for optimal performance." = "Avec le mode sécuritaire activé, le script ne pourra pas ajuster la configuration du serveur pour des performances optimales"
"You are responsible for escaping! Also, if constructing a LIST, then strings should be encapsulated with quotes." = "Vous êtes responsable de l'échapement des caractères ! En outre, si construisant une LISTE, alors les chaînes de caractères devraient être entouré de guillemet."
"Your server imposes the following limits: maximum file upload size is %s and maximum post size is %s." = "Votre serveur impose les limites suivantes : la taille maximum de téléversement d'un fichier est %s et la taille maximum d'un envoi de formulaire est %s."
"Your server is set to allow less memory than this script requires." = "Votre serveur est configuré pour permettre moins de mémoire que ce script a besoin."



##### 2009-05-28 #####

"%REPORTLINK% is shorthand for the shared report link that you see above." = "REPORTLINK% est raccourci pour le lien du rapport partagé que vous voyez ci-dessus"



##### 2009-07-10 #####




##### 2009-07-14 #####

"(Optional) This option will automatically pause your mailing after a certain number of messages.  This helps reduce your mail servers load." = "(Facultatif) Cette option suspendra automatiquement votre envoi après un certain nombre de message. Ceci aide à réduire la charge de votre serveur de courriel."
"The label of this field (a title) can be shown both above the field, or on its left side." = "L'étiquette de ce champ (un titre) peut être affiché soit en haut ou à gauche du champ."



##### 2009-08-27 #####

"This feature will use Apache's mod_rewrite plugin to reformat its public URLs to appear more descriptive, mentioning for example the title of a list or form in the URL." = "Cette fonctionnalité utilisera l'extension mod_rewrite d'Apache pour reformater ses URLs public pour sembler plus descriptif, en mentionnant par exemple le titre de la liste ou du formulaire dans l'URL."



##### 2009-11-25 #####




##### 2009-12-21 #####

"Begin typing their name and live results will be displayed." = "Commencer à inscrire leur nom et le résultat sera affiché directement."



##### 2010-03-28 #####

"If you check this option all new subscribers will be sent a copy of the last campaign you sent immediately upon subscribing." = "Si vous cochez cette option, tous les nouveaux abonnés recevront une copie de la dernière campagne lors de leur inscription."



##### 2010-04-07 #####

"During importing the subscribers, they will be sent any instant AutoResponders you might have setup." = "Lors de l'importation des abonnés, ils vont recevoir toutes les réponses automatiques que vous aurez configurés"
"Subscribers will be treated the same as any person that subscribed from a public side or a subscription form; all instant and/or delayed AutoResponders will be sent to them in due time." = "Les abonnés seront traités de la même manière qu'ils ait été ajouté à partir de la partie publique ou d'un formulaire d'abonnement; Toutes les réponses automatiques instantannée ou délayée leur seront envoyés au moment déterminé."
"These subscribers will never receive any further AutoResponders setup in the system." = "Ces abonnés ne recevront jamais de réponses automatiques du système."
"You can flag certain AutoResponders as already been sent to these subscribers, in case imported subscribers shouldn't receive some of them." = "Vous pouvez marquer certaines réponses automatiques comme ayant déjà été envoyé à ces abonnés, au cas où les abonnés importés ne devrait pas recevoir certains de ces messages."



##### 2010-07-02 #####

"In some cases, particularly Active Directory, the user name you normally log in with is not actually what the server expects through its normal LDAP interface.  This field is the name of the attribute that you want to use to log in with.  In most of those cases, the value which must go here would be 'samAccountName'." = "Dans certains cas, en particulier avec Active Directory, le nom d'usager avec lequel vous vous connectez normallement n'est pas ce que le serveur s'attends via l'interface LDAP normal. Ce champ est le nom de l'attribut que vous voulez utiliser pour vous connecter. Dans la plupart des cas, la valeur qui viens ici est 'samAccountName'"
"In some cases, particularly with Active Directory, we must log in with an administrative account before we can truly log in with a user.  In that case, you would place here the full distinguished name of the LDAP server's administrative user (e.g. cn=Administrator,ou=Users,dc=domain,dc=com)." = "Dans certains cas, en particulier avec Active Directory, nous devons archiver à l'aide d'un compte d'administrateur avant de vraiment pouvoir archiver avec un utilisateur. Dans ce cas, Vous devrez placer ici le nom complet de l'administrateur du serveur LDAP (ex:cn=Administrator,ou=Users,dc=domain,dc=com)."
"The attribute that the LDAP server expects us to log in with.  In the vast majority of cases, this field should be 'cn' (short for 'common name')." = "L'attribut avec lequel le serveur LDAP s'attends que nous nous connections. Dans la plupart des cas, ce champ devrait être le 'cn' (version courte de 'nom commun')"
"The base of the distinguished name that users would log in with; e.g., ou=Users,dc=domain,dc=com" = "La base du nom distinctif avec lequel les utilisateurs vont se connecter; ex: ou=Users,dc=domain,dc=com"
"The full distinguished name of an administrator account, necessary for Active Directory's login process.  Example: cn=Administrator,ou=Users,dc=domain,dc=com" = "Le nom distinctif complet d'un compte administrateur, nécéssaire au processus de connexion d'Active directory."
"The password for the administrator account, mentioned above." = "Le mot de passe pour le compte administrateur, mentionné ci-haut."
"The password of the administrative user mentioned above." = "Le mot de passe du compte administrateur, mentionné ci-haut."
"With this checked, we will log in with a fully-formed distinguished name (cn=user,ou=Users,dc=domain,dc=com); without it, we'll log in with 'user' and let the server sort things out." = "Avec cette option activée, nous archiverons les noms distinctifs entièrement formés (cn=user,ou=Users,dc=domain,dc=com); sans cette option l'archivage sera fait avec 'utilisateur' et le serveur démêlera le tout."



##### 2010-09-07 #####

"The location of this software on the web. To change this, run Updater from a new location." = "L'emplacement de ce logiciel sur le web. Pour changer cela, exécutez la mise à jour à partir d'un nouvel emplacement."



##### 2010-09-30 #####

"To update a different Twitter account, uncheck Twitter above, save, then come back and re-enter your information." = "Pour mettre à jour un compte Twitter différent, décochez la case Twitter ci-dessus, enregistrez, et revenez inscrire vos informations."
