<?php
/**
Title: Modify existing subscriber custom field.

Description: Modify existing subscriber custom field, just like you would on the Custom Fields page of the admin section.

Supported formats: xml, json, serialize

Supported request methods: POST

Requires authentication: true

Parameters (* denotes requirement):
{
	[*api_key] => Your API key
	[*api_action] => list_field_edit
	[*api_output] => xml, json, or serialize
}

POST variables (* denotes requirement):
{
	[*id] => ID of the custom field you are editing
	[*title] => Internal field name. Example: 'Field 1'
	[*type] => Possible values: 1 = Text Field, 2 = Text Box (textarea), 3 = Checkbox, 4 = Radio, 5 = Dropdown, 6 = Hidden field, 7 = List Box, 9 = Date
	[*req] => Required? (1 = yes, 0 = no)
	[*show_in_list] => Show on subscriber list page (as another column)? (1 = yes, 0 = no)
	[*perstag] => Unique tag used as a placeholder for dynamic content
	[ *p[1] ] => Lists that the custom field is associated with. Example: array(1 => 1, 2 => 2)
	[ options[] ] => Options for fields that have them (IE: checkbox, radio, dropdown, listbox). Example: array("label" => "value1", "label" => "value2")
}

Example response:
{
	[result_code] => Whether or not the response was successful. Examples: 1 = yes, 0 = no
	[result_message] => A custom message that appears explaining what happened. Example: Custom Field updated
	[result_output] => The result output used. Example: serialize
}
**/


// By default, this sample code is designed to get the result from your
// server (where ActiveCampaign Email Marketing is installed) and to print out the result
$url    = 'http://account.activehosted.com';


$params = array(

	// the API Key can be found on the "Your Settings" page under the "API" tab.
	// replace this with your API Key
	'api_key'      => 'YOUR_API_KEY',

	// this is the action that adds a list
	'api_action'   => 'list_field_edit',

	// define the type of output you wish to get back
	// possible values:
	// - 'xml'  :      you have to write your own XML parser
	// - 'json' :      data is returned in JSON format and can be decoded with
	//                 json_decode() function (included in PHP since 5.2.0)
	// - 'serialize' : data is returned in a serialized format and can be decoded with
	//                 a native unserialize() function
	'api_output'   => 'serialize',
);

// here we define the data we are posting in order to perform an update
$post = array(
	'id'                        => 5, // field ID to edit
	'title'                     => 'Field 2', // internal field name
	'type'                      => 1, // 1 = Text Field, 2 = Text Box, 3 = Checkbox, 4 = Radio, 5 = Dropdown, 6 = Hidden field, 7 = List Box, 9 = Date
	'req'                       => 0, // required? 1 or 0
	'show_in_list'              => 0, // show on subscriber list page (as another column)?
	'perstag'                   => '', // unique tag used as a placeholder for dynamic content
	'p[5]'                      => 5, // for use in lists. use 0 for All lists
	//'p[2]'                    => 2, // more lists?
	'options[label]'            => 'value',
	//'options[label]'          => 'value2', // more options?
);

// This section takes the input fields and converts them to the proper format
$query = "";
foreach( $params as $key => $value ) $query .= $key . '=' . urlencode($value) . '&';
$query = rtrim($query, '& ');

// This section takes the input data and converts it to the proper format
$data = "";
foreach( $post as $key => $value ) $data .= $key . '=' . urlencode($value) . '&';
$data = rtrim($data, '& ');

// clean up the url
$url = rtrim($url, '/ ');

// This sample code uses the CURL library for php to establish a connection,
// submit your request, and show (print out) the response.
if ( !function_exists('curl_init') ) die('CURL not supported. (introduced in PHP 4.0.2)');

// If JSON is used, check if json_decode is present (PHP 5.2.0+)
if ( $params['api_output'] == 'json' && !function_exists('json_decode') ) {
	die('JSON not supported. (introduced in PHP 5.2.0)');
}

// define a final API request - GET
$api = $url . '/admin/api.php?' . $query;

$request = curl_init($api); // initiate curl object
curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
curl_setopt($request, CURLOPT_POSTFIELDS, $data); // use HTTP POST to send form data
//curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment if you get no gateway response and are using HTTPS

$response = (string)curl_exec($request); // execute curl post and store results in $response

// additional options may be required depending upon your server configuration
// you can find documentation on curl options at http://www.php.net/curl_setopt
curl_close($request); // close curl object

if ( !$response ) {
	die('Nothing was returned. Do you have a connection to Email Marketing server?');
}

// This line takes the response and breaks it into an array using:
// JSON decoder
//$result = json_decode($response);
// unserializer
$result = unserialize($response);
// XML parser...
// ...

// Result info that is always returned
echo 'Result: ' . ( $result['result_code'] ? 'SUCCESS' : 'FAILED' ) . '<br />';
echo 'Message: ' . $result['result_message'] . '<br />';

// The entire result printed out
echo 'The entire result printed out:<br />';
echo '<pre>';
print_r($result);
echo '</pre>';

// Raw response printed out
echo 'Raw response printed out:<br />';
echo '<pre>';
print_r($response);
echo '</pre>';

// API URL that returned the result
echo 'API URL that returned the result:<br />';
echo $api;

echo '<br /><br />POST params:<br />';
echo '<pre>';
print_r($post);
echo '</pre>';

?>