var subscriber_delete_str = '{"Are you sure you want to delete subscriber %s?"|alang|js}';
var subscriber_delete_str2 = '{"Delete Subscriber"|alang|js}';
var subscriber_delete_str3 = '{"Delete Subscribers"|alang|js}';
var subscriber_delete_str_multi = '{"Are you sure you want to delete the following subscribers?"|alang|js}';
var subscriber_delete_str_cant_delete = '{"You do not have permission to delete subscribers"|alang|js}';
{literal}
var subscriber_delete_id = 0;
var subscriber_delete_id_multi = "";

function subscriber_delete_check(id) {
	if (ac_js_admin.pg_subscriber_delete != 1) {
		ac_ui_anchor_set(subscriber_list_anchor());
		alert(subscriber_delete_str_cant_delete);
		return;
	}

	if (id < 1) {
		subscriber_delete_check_multi();
		return;
	}

	if ($J("#JSListManager").val() == 0) {
		alert({/literal}'{"You must choose a list to filter with in order to delete a subscriber."|alang|js}'{literal});
		return;
	}

	$("delete_header").innerHTML = subscriber_delete_str2;
	$("delete_list").hide();
	ac_dom_remove_children($("delete_list"));

	ac_ajax_call_cb("api.php", "subscriber.subscriber_select_row", subscriber_delete_check_cb, id);
}

function subscriber_delete_check_cb(xml) {
	var ary = ac_dom_read_node(xml);

	subscriber_delete_id = ary.id;
	$("delete_message").innerHTML = sprintf(subscriber_delete_str, ary.email);
	ac_dom_display_block("delete");
}

function subscriber_delete_check_multi() {
	if (ac_js_admin.pg_subscriber_delete != 1) {
		ac_ui_anchor_set(subscriber_list_anchor());
		alert(subscriber_delete_str_cant_delete);
		return;
	}

	if (!ac_form_check_selection_check($("list_table"), "multi[]", jsNothingSelected, jsNothingFound)) {
		ac_ui_anchor_set(subscriber_list_anchor());
		return;
	}

	if ($J("#JSListManager").val() == 0 && subscriber_list_segmentid_new == 0) {
		alert({/literal}'{"You must choose a list to filter with in order to delete a subscriber."|alang|js}'{literal});
		return;
	}

	var sel = ac_form_check_selection_get($("list_table"), "multi[]");
	subscriber_delete_id = 0; // in case it was set already. we are deleting multi now so it should be 0
	$("delete_header").innerHTML = subscriber_delete_str3;
	$("delete_list").show();
	ac_dom_remove_children($("delete_list"));
	ac_ajax_call_cb("api.php", "subscriber.subscriber_select_array_alt", subscriber_delete_check_multi_cb, 0, sel.join(","));
	subscriber_delete_id_multi = sel.join(",");
}

function subscriber_delete_check_multi_cb(xml) {
	var ary = ac_dom_read_node(xml);

	$("delete_message").innerHTML = subscriber_delete_str_multi;

	ac_dom_remove_children($("delete_list"));

	if (!selectAllSwitch) {
		for (var i = 0; i < ary.row.length; i++)
			$("delete_list").appendChild(Builder.node("li", [ ary.row[i].email ]));
	} else {
		$("delete_list").appendChild(Builder.node("li", [ jsAllItemsWillBeDeleted ]));
	}

	ac_dom_display_block("delete");
}

function subscriber_delete(id) {
	// id has to be 0 for a multi-delete to be happening
	if (subscriber_delete_id_multi != "" && id == 0) {
		subscriber_delete_multi();
		return;
	}

	var post = ac_form_post("delete");
	post.id = id;
	post.listid = $J("#JSListManager").val();

	ac_ajax_post_cb("api.php", "subscriber.subscriber_delete_post", subscriber_delete_cb, post);
}

function subscriber_delete_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();

	if (ary.succeeded != "0") {
		ac_result_show(ary.message);
		ac_ui_anchor_set(subscriber_list_anchor());
	} else {
		ac_error_show(ary.message);

		if (typeof ary.pastlimit != "undefined")
			$("sublimit").show();
	}

	ac_dom_toggle_display("delete", "block");
}

function subscriber_delete_multi() {
	var post = ac_form_post("delete");

	post.ids = subscriber_delete_id_multi;
	
	post.listid = $J("#JSListManager").val();

	if (selectAllSwitch) {
		post.ids = "_all";
		post.filter = subscriber_list_filter;
		ac_ajax_post_cb("api.php", "subscriber.subscriber_delete_multi_post", subscriber_delete_multi_cb, post);
		return;
	}

	ac_ajax_post_cb("api.php", "subscriber.subscriber_delete_multi_post", subscriber_delete_multi_cb, post);
	subscriber_delete_id_multi = "";
}

function subscriber_delete_multi_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();

	if (ary.succeeded != "0") {
		ac_result_show(ary.message);
		ac_ui_anchor_set(subscriber_list_anchor());
	} else {
		ac_error_show(ary.message);

		if (typeof ary.pastlimit != "undefined")
			$("sublimit").show();
	}

	$('selectXPageAllBox').className = 'ac_hidden';
	$('acSelectAllCheckbox').checked = false;
	
	ac_dom_toggle_display("delete", "block");
}
{/literal}
