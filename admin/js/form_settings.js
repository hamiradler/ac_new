var form_obj = {jsvar var=$form};

{literal}

function form_save_button() {
	$J(".saving").show('drop', { direction: "right" }, 500);
	ac.wait(1.5, function() {
		$J(".saving").hide('drop', { direction: "right" }, 500);
	});
}

function form_save_name() {
	ac.post("form.form_save_name", { id: form_obj.id, name: $J("#form_name").val() });
}

function form_settings_submit(nextstep) {
	var listschecked = false;

	$J(".form_settings_list").each(function() {
		if (this.checked)
			listschecked = true;
	});

	if (!listschecked) {
		alert({/literal}'{"You must check at least one list for this form"|alang|js}'{literal});
		return false;
	}

	$J("#form_settings_next").val(nextstep);
	$("settingsform").submit();
}

function form_settings_changelist() {
	var len = $J(".form_settings_list:checked").length;

	if (len > 1) {
		$J("#havemultiple").show();
	} else {
		$J("#havemultiple").hide();

		// Set the dropdown to be just the one list we have.
		if (len == 1)
			$J("#select_useconf").val($J(".form_settings_list:checked").val());

		if (len == 0)
			$J(".form_settings_list:first").attr("checked", 1);
	}
}

function form_settings_save() {
	var post = ac_form_post($("settingsform"));
	ac_ajax_post_cb("api.php", "form.form_settings_save", form_settings_save_cb, post);
}

function form_settings_save_cb() {
	form_changed = false;
}

{/literal}
