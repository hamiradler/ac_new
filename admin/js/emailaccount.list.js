var emailaccount_list_pipe = '{"- PIPE -"|alang|js}';
var emailaccount_list_pop3 = '{"- POP3 -"|alang|js}';
var emailaccount_list_sub = '{"Subscribe"|alang|js}';
var emailaccount_list_unsub = '{"Unsubscribe"|alang|js}';

var emailaccount_confirm_run = '{"Connection has been successfully established."|alang|js}\n'
	+ '{"Email Check will be opened in a separate window."|alang|js}\n\n'
	+ '{"Do you wish to continue?"|alang|js}';

var emailaccount_table = new ACTable();
var emailaccount_list_sort = "02";
var emailaccount_list_offset = "0";
var emailaccount_list_filter = {jsvar var=$filterid};
var emailaccount_list_sort_discerned = false;

{literal}
emailaccount_table.addcol(function(row) {
	return Builder.node("input", { type: "checkbox", name: "multi[]", value: row.id, onclick: "ac_form_check_selection_none(this, $('acSelectAllCheckbox'), $('selectXPageAllBox'))" });
});

emailaccount_table.addcol(function(row) {
	var edit = Builder.node("a", { href: sprintf("#form-%d", row.id) }, jsOptionEdit);
	var test = Builder.node("a", { href: sprintf("#test-%d", row.id), onclick: sprintf('return emailaccount_run(%d, 1);', row.id) }, jsOptionTest);
	var run  = Builder.node("a", { href: sprintf("#run-%d", row.id), onclick: sprintf('return emailaccount_run(%d, 0);', row.id) }, jsOptionRun);
	var log  = Builder.node("a", { href: sprintf("#log-%d", row.id) }, jsOptionLog);
	var dele = Builder.node("a", { href: sprintf("#delete-%d", row.id) }, jsOptionDelete);

	var ary = [];

	if (ac_js_admin.pg_list_edit) {
		ary.push(edit);
		ary.push(" ");
		if ( row.type == 'pop3' ) {
			ary.push(test);
			ary.push(" ");
			ary.push(run);
			ary.push(" ");
		}
		ary.push(log);
		ary.push(" ");
	}

	if (ac_js_admin.pg_list_edit) {
		ary.push(dele);
	}

	return Builder.node("div", { className: "ac_table_row_options" }, ary);
});

emailaccount_table.addcol(function(row) {
	return Builder._text( row.action == 'unsub' ? emailaccount_list_unsub : emailaccount_list_sub );
});

emailaccount_table.addcol(function(row) {
	return txt = Builder._text(row.email);
});
{/literal}

{if !$__ishosted}

{literal}
emailaccount_table.addcol(function(row) {
	if ( row.type == 'pipe' ) {
		var txt = Builder.node('em', [ Builder._text(emailaccount_list_pipe) ]);
	} else {
		var txt = Builder._text(row.host);
	}
	return txt;
});

emailaccount_table.addcol(function(row) {
	if ( row.type == 'pipe' ) {
		var txt = Builder.node('em', [ Builder._text(emailaccount_list_pipe) ]);
	} else {
		var txt = Builder._text(row.user);
	}
	return txt;
});
{/literal}

{/if}

{literal}
emailaccount_table.addcol(function(row) {
	return Builder._text(parseInt(row.lists));
});

function emailaccount_list_anchor() {
	return sprintf("list-%s-%s-%s", emailaccount_list_sort, emailaccount_list_offset, emailaccount_list_filter);
}

function emailaccount_list_tabelize(rows, offset) {
	if (rows.length < 1) {
		// We may have some trs left if we just deleted the last row.
		ac_dom_remove_children($("list_table"));

		$("list_noresults").className = "ac_block";
		if($("list_delete_button")) $("list_delete_button").className = "ac_hidden";
		$("loadingBar").className = "ac_hidden";
		ac_ui_api_callback();
		return;
	}
	$("list_noresults").className = "ac_hidden";
	if($("list_delete_button")) $("list_delete_button").className = "ac_inline";
	ac_paginator_tabelize(emailaccount_table, "list_table", rows, offset);
	$("loadingBar").className = "ac_hidden";
}

// This function should only be run through a paginator (e.g., paginators[n].paginate(offset))
function emailaccount_list_paginate(offset) {
	if (!ac_loader_visible() && !ac_result_visible() && !ac_error_visible())
		ac_ui_api_call(jsLoading);

	if (emailaccount_list_filter > 0)
		$("list_clear").style.display = "inline";
	else
		$("list_clear").style.display = "none";

	emailaccount_list_offset = parseInt(offset, 10);

	ac_ui_anchor_set(emailaccount_list_anchor());
	$("loadingBar").className = "ac_block";
	ac_ajax_call_cb(this.ajaxURL, this.ajaxAction, paginateCB, this.id, emailaccount_list_sort, emailaccount_list_offset, this.limit, emailaccount_list_filter);

	$("list").className = "ac_block";
}

function emailaccount_list_clear() {
	emailaccount_list_sort = "02";
	emailaccount_list_offset = "0";
	emailaccount_list_filter = "0";
	emailaccount_listfilter = null;
	$("JSListManager").value = 0;
	$("list_search").value = "";
	list_filters_update(0, 0, true);
	emailaccount_search_defaults();
	ac_ui_anchor_set(emailaccount_list_anchor());
}

function emailaccount_list_search() {
	var post = ac_form_post($("list"));
	emailaccount_listfilter = post.listid;
	list_filters_update(0, post.listid, false);
	ac_ajax_post_cb("api.php", "emailaccount.emailaccount_filter_post", emailaccount_list_search_cb, post);
}

function emailaccount_list_search_cb(xml) {
	var ary = ac_dom_read_node(xml);

	emailaccount_list_filter = ary.filterid;
	ac_ui_anchor_set(emailaccount_list_anchor());
}

function emailaccount_list_chsort(newSortId) {
	var oldSortId = ( emailaccount_list_sort.match(/D$/) ? emailaccount_list_sort.substr(0, 2) : emailaccount_list_sort );
	var oldSortObj = $('list_sorter' + oldSortId);
	var sortObj = $('list_sorter' + newSortId);
	// if sort column didn't change (only direction [asc|desc] did)
	if ( oldSortId == newSortId ) {
		// switching asc/desc
		if ( emailaccount_list_sort.match(/D$/) ) {
			// was DESC
			newSortId = emailaccount_list_sort.substr(0, 2);
			sortObj.className = 'ac_sort_asc';
		} else {
			// was ASC
			newSortId = emailaccount_list_sort + 'D';
			sortObj.className = 'ac_sort_desc';
		}
	} else {
		// remove old emailaccount_list_sort
		if ( oldSortObj ) oldSortObj.className = 'ac_sort_other';
		// set sort field
		sortObj.className = 'ac_sort_asc';
	}
	emailaccount_list_sort = newSortId;
	ac_ui_api_call(jsSorting);
	ac_ui_anchor_set(emailaccount_list_anchor());
	return false;
}

function emailaccount_list_discern_sortclass() {
	if (emailaccount_list_sort_discerned)
		return;

	var elems = $("list_head").getElementsByTagName("a");

	for (var i = 0; i < elems.length; i++) {
		var str = sprintf("list_sorter%s", emailaccount_list_sort.substring(0, 2));

		if (elems[i].id == str) {
			if (emailaccount_list_sort.match(/D$/))
				elems[i].className = "ac_sort_desc";
			else
				elems[i].className = "ac_sort_asc";
		} else {
			elems[i].className = "ac_sort_other";
		}
	}

	emailaccount_list_sort_discerned = true;
}


function emailaccount_run(id, isTest) {
	// Increase delay to 60 seconds, to account for timeouts when processing POP accounts.
	ac_ui_api_call(jsWorking, 60);
	ac_ajax_call_cb("api.php", "emailaccount.emailaccount_run", emailaccount_run_cb, id, isTest);
	return false;
}

function emailaccount_run_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();

	if (ary.succeeded != "0") {
		ac_result_show(ary.message);
		if ( ary.istest == "0" ) {
			if ( confirm(emailaccount_confirm_run) ) {
				ac_ui_openwindow('functions/crons/emailparser.php?debug=1&id=' + ary.id);
			}
		}
	} else {
		ac_error_show(ary.message);
	}
}

{/literal}
