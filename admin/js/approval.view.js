var approval_form_str_cant_insert = '{"You do not have permission to add Campaign Approval"|alang|js}';
var approval_form_str_cant_update = '{"You do not have permission to edit Campaign Approval"|alang|js}';
var approval_form_str_cant_find   = '{"Campaign Approval not found."|alang|js}';
{literal}
var approval_form_id = 0;

function approval_form_defaults() {
	$("form_id").value = 0;
}

function approval_form_load(id) {
	approval_form_defaults();
	approval_form_id = id;

	if (id > 0) {
		/*
		if (ac_js_admin.__CAN_EDIT__ != 1) {
			ac_ui_anchor_set(approval_list_anchor());
			alert(approval_form_str_cant_update);
			return;
		}
		*/

		ac_ui_api_call(jsLoading);
		$("form_submit").className = "ac_button_update";
		$("form_submit").value = jsUpdate;
		ac_ajax_call_cb("api.php", "approval.approval_select_row", approval_form_load_cb, id);
	} else {
		/*
		if (ac_js_admin.__CAN_ADD__ != 1) {
			ac_ui_anchor_set(approval_list_anchor());
			alert(approval_form_str_cant_insert);
			return;
		}
		*/

		$("form_submit").className = "ac_button_add";
		$("form_submit").value = jsAdd;
		$("form").className = "ac_block";
	}
}

function approval_form_load_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();
	if ( !ary.id ) {
		ac_error_show(approval_form_str_cant_find);
		ac_ui_anchor_set(approval_list_anchor());
		return;
	}
	approval_form_id = ary.id;

	$("form_id").value = ary.id;

	$("form").className = "ac_block";
}

function approval_form_save(id) {
	var post = ac_form_post($("form"));
	ac_ui_api_call(jsSaving);

	if (id > 0)
		ac_ajax_post_cb("api.php", "approval.approval_update_post", approval_form_save_cb, post);
	else
		ac_ajax_post_cb("api.php", "approval.approval_insert_post", approval_form_save_cb, post);
}

function approval_form_save_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();

	if (ary.succeeded && ary.succeeded == "1") {
		ac_result_show(ary.message);
		ac_ui_anchor_set(approval_list_anchor());
	} else {
		ac_error_show(ary.message);
	}
}
{/literal}
