{include file="service.list.js"}
{include file="service.form.fields.js"}
{include file="service.form.wufoo.js"}
{include file="service.form.surveymonkey.js"}
{include file="service.form.shopify.js"}
{include file="service.form.unbounce.js"}
{include file="service.form.js"}
var service_form_str_external_services = '{"External Services"|alang|js}';

{jsvar var=$__ishosted name=service_ishosted}
{jsvar var=$serial_hash name=serial_hash}
{jsvar var=$ac_admin_ismaingroup name=ac_admin_ismaingroup}

var lists = {jsvar var=$lists};

var service_edit_no = '{"You do not have permission to edit this External Service."|alang|js}';
var service_delete_no = '{"You do not have permission to delete External Services."|alang|js}';

{literal}

var total_lists_user = 0;
for (var i in lists) {
	total_lists_user++;
}

function service_process(loc, hist) {
	if ( loc == '' ) {
		loc = 'list-' + service_list_sort + '-' + service_list_offset + '-' + service_list_filter;
		ac_ui_rsh_save(loc);
	}
	var args = loc.split("-");

	$("list").hide();
	$("form").className = "ac_hidden";
	$("service_header").innerHTML = service_form_str_external_services;
	var func = null;
	try {
		var func = eval("service_process_" + args[0]);

	} catch (e) {
		if (typeof service_process_list == "function")
			service_process_list(args);
	}
	if (typeof func == "function")
		func(args);
}

function service_process_list(args) {
	service_cleartimeouts();
	if (args.length < 2)
		args = ["list", service_list_sort, service_list_offset, service_list_filter];

	service_list_sort = args[1];
	service_list_offset = args[2];
	service_list_filter = args[3];

	if (!total_lists_user) {
		$("list").hide();
		$("service_header").hide();
		$("noforms_nolists").show();
		//var h1s = $("noforms_nolists").getElementsByTagName("h1");
		//h1s[0].style.margin = "0";
	}
	else {
		// lists present
		$("list").show();
	}
	//service_list_discern_sortclass();
	//paginators[1].paginate(service_list_offset);
}

function service_process_form(args) {
	if (args.length < 2)
		args = ["form", "0"];

	var id = parseInt(args[1], 10);

	service_form_load(id);
}

function service_process_delete(args) {
	alert(service_delete_no);
	service_process_list(["list", "0", "0", "0"]);
	return;

	if (args.length < 2) {
		service_process_list(["list", "0"]);
		return;
	}

	$("list").className = "ac_block";
	var id = parseInt(args[1], 10);

	service_delete_check(id);
}

function service_process_delete_multi(args) {
	$("list").className = "ac_block";
	service_delete_check_multi();
}

function service_process_search(args) {
	$("list").className = "ac_block";
	service_search_check();
}

function service_cleartimeouts() {
	// clear timeouts
	for (var i = 0; i < timeouts.length; i++) clearTimeout(timeouts[i]);
	timeouts = [];
}

{/literal}
