{literal}

var field_modal_lists = 0;

function field_modal_type() {
	var types = $$(".field_modal_type");

	for (var i = 0, val = "text"; i < types.length; i++) {
		if (types[i].checked) {
			val = types[i].value;
			break;
		}
	}

	return val;
}
function field_modal_next() {
	var val = field_modal_type();
	$("field_modal_addtype").hide();
	$("field_modal_add" + val).show();
	field_modal_set_enter(val);
}

function field_modal_save_cb(ary) {
	ac_ui_api_callback();

	if (ary.succeeded)
		$("field_modal_add" + ary.source).hide();
	else {
		ac_error_show(ary.message);
		return;
	}

	if (typeof field_modal_save_after == "function")
		field_modal_save_after(ary);
}

function field_modal_save_text() {
	var post = {};
	ac_ui_api_call(jsSaving);

	post.lists = field_modal_lists;
	post.type = "text";
	post.title = $("field_modal_text_title").value;
	post.perstag = $("field_modal_text_perstag").value;

	ac_ajax_post_cb("api.php", "field.field_insert_post", ac_ajax_cb(field_modal_save_cb), post);
}

function field_modal_save_date() {
	var post = {};
	ac_ui_api_call(jsSaving);

	post.lists = field_modal_lists;
	post.type = "date";
	post.title = $("field_modal_date_title").value;
	post.perstag = $("field_modal_date_perstag").value;

	ac_ajax_post_cb("api.php", "field.field_insert_post", ac_ajax_cb(field_modal_save_cb), post);
}

function field_modal_save_hidden() {
	var post = {};
	ac_ui_api_call(jsSaving);

	post.lists = field_modal_lists;
	post.type = "hidden";
	post.title = $("field_modal_hidden_title").value;
	post.perstag = $("field_modal_hidden_perstag").value;

	ac_ajax_post_cb("api.php", "field.field_insert_post", ac_ajax_cb(field_modal_save_cb), post);
}

function field_modal_save_textarea() {
	var post = {};
	ac_ui_api_call(jsSaving);

	post.lists = field_modal_lists;
	post.type = "textarea";
	post.title = $("field_modal_textarea_title").value;
	post.rows = $("field_modal_textarea_rows").value;
	post.cols = $("field_modal_textarea_cols").value;
	post.perstag = $("field_modal_textarea_perstag").value;

	ac_ajax_post_cb("api.php", "field.field_insert_post", ac_ajax_cb(field_modal_save_cb), post);
}

function field_modal_save_radio() {
	var post = {};
	ac_ui_api_call(jsSaving);

	post.lists = field_modal_lists;
	post.type = "radio";
	post.title = $("field_modal_radio_title").value;
	post.perstag = $("field_modal_radio_perstag").value;

	var opts = $$(".field_modal_radio_option");
	var ary = [];
	for (var i = 0; i < opts.length; i++) {
		ary.push(opts[i].value);
	}

	post.options = ary;

	ac_ajax_post_cb("api.php", "field.field_insert_post", ac_ajax_cb(field_modal_save_cb), post);
}

function field_modal_save_dropdown() {
	var post = {};
	ac_ui_api_call(jsSaving);

	post.lists = field_modal_lists;
	post.type = "dropdown";
	post.title = $("field_modal_dropdown_title").value;
	post.perstag = $("field_modal_dropdown_perstag").value;

	var opts = $$(".field_modal_dropdown_option");
	var ary = [];
	for (var i = 0; i < opts.length; i++) {
		ary.push(opts[i].value);
	}

	post.options = ary;

	ac_ajax_post_cb("api.php", "field.field_insert_post", ac_ajax_cb(field_modal_save_cb), post);
}

function field_modal_save_listbox() {
	var post = {};
	ac_ui_api_call(jsSaving);

	post.lists = field_modal_lists;
	post.type = "listbox";
	post.title = $("field_modal_listbox_title").value;
	post.perstag = $("field_modal_listbox_perstag").value;

	var opts = $$(".field_modal_listbox_option");
	var ary = [];
	for (var i = 0; i < opts.length; i++) {
		ary.push(opts[i].value);
	}

	post.options = ary;

	ac_ajax_post_cb("api.php", "field.field_insert_post", ac_ajax_cb(field_modal_save_cb), post);
}

function field_modal_save_checkbox() {
	var post = {};
	ac_ui_api_call(jsSaving);

	post.lists = field_modal_lists;
	post.type = "checkbox";
	post.title = $("field_modal_checkbox_title").value;
	post.perstag = $("field_modal_checkbox_perstag").value;

	var opts = $$(".field_modal_checkbox_option");
	var ary = [];
	for (var i = 0; i < opts.length; i++) {
		ary.push(opts[i].value);
	}

	post.options = ary;

	ac_ajax_post_cb("api.php", "field.field_insert_post", ac_ajax_cb(field_modal_save_cb), post);
}

{/literal}
