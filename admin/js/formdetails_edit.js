var form_obj = {jsvar var=$form};
var form_lists = [];
var form_compile_flashid = 0;
{foreach from=$lists item=e}
form_lists.push({$e});
{/foreach}
{literal}
var form_insert_parts = {};

// This should be set to either "build" or "theme", based on what you are
// looking at.  If you edit a form part, this variable should guide you back to
// the right place.
var form_mode = 'edit';

// The original width of the _form class, which we may need if you set the width to 0 pixels...
var form_original_width;

var form_parts = [];

if (form_obj.widthpx > 0) {
	$J(document).ready(function() {
		$J("._form").css("width", form_obj.widthpx);
		$J("#form_width_custom").show();
	});
} else {
	$J(document).ready(function() {
		$J("#form_width_specify").show();
	});
}

function form_save_widthpx() {
	ac.post("form.form_save_widthpx", { id: form_obj.id, widthpx: $J("#form_widthpx").val() }, function(data) {
		var widthpx = $J("#form_widthpx").val();

		if (widthpx == "" || widthpx == 0 || widthpx == "0px") {
			$J("._form").css("width", form_original_width);
			$J("#form_width_specify").show();
			$J("#form_width_custom").hide();
		} else {
			$J("._form").css("width", widthpx);
		}
	});
}


function field_modal_save_after(ary) {
	$J("#fieldcontainer").append(sprintf("<div class='form_content_fields_item part_%s' id='part_%s' onclick='form_part_insert_field(%s)'><span>%s</span></div>", ary.source, ary.id, ary.id, ary.title));
	form_part_insert_field(ary.id);
}

function field_modal_open() {
	field_modal_lists = form_lists;
	$J("#field_modal_addtype").show();
}

function form_background_color(elem) {
	var bgcolor = "";

	if (elem.length == 0)
		return "";

	// This is coming in from jquery -- we just want the first one, since elem
	// was likely gotten from an id selector
	elem = elem[0];

	while (elem) {
		var style = ac_dom_compstyle(elem);
		
		if (style.backgroundColor == "rgba(0, 0, 0, 0)") {
			elem = elem.parentNode;
			continue;
		}

		return style.backgroundColor;
	}
}

function form_compile() {
	ac_ajax_call_cb("api.php", "form.form_compile_admin", ac_ajax_cb(form_compile_cb), form_obj.id);
}

function form_compile_cb(ary) {
	ac_dom_remove_children($I("preview"));
	
	if (typeof ary.parts == "undefined")
		return;

	var options =
		"<div style='float:right;'><div class='_form_options'><a href='#' onclick='form_part_edit_show(%s, \"%s\"); return false'>" +
		{/literal}'{"edit"|alang|js}'{literal} +
		"</a>";

	$J(".form_content_fields_item").removeClass("preview_part_disabled");
	$J(".form_content_fields_item_gray").removeClass("preview_part_disabled");
		
	$J("#preview").append(ary.header);
		
	for (var i = 0; i < ary.parts.length; i++) {
		var myopts = options;

		// Hide the edit link if this is a hidden field
		if (ary.parts[i].html.match(/class='_label _hiddenlabel'/))
			myopts = myopts.replace(/onclick='form_part_edit/, "style='display:none' onclick='form_part_edit");

		if (ary.parts[i].builtin != "email" && ary.parts[i].builtin != "subscribe") {
			myopts = 
				myopts + " <a href='#' onclick='form_part_delete(%s); return false'>" +
				{/literal}'{"delete"|alang|js}'{literal} +
				"</a></div></div>";
		} else {
			myopts = myopts + "</a></div></div>";
		}

		$J("#preview").append($J("<div class='preview_part' onmouseover='this.className=\"preview_part_hover\"' onmouseout='this.className=\"preview_part\"' >" + sprintf(myopts, ary.parts[i].id, (ary.parts[i].builtin != "") ? ary.parts[i].builtin : {/literal}'{"field"|alang|js}'{literal}, ary.parts[i].id) + ary.parts[i].html + "</div>"));

		if (ary.parts[i].fieldid > 0) {
			$J(sprintf("#part_%s", ary.parts[i].fieldid)).addClass("preview_part_disabled");
			if ($J(sprintf("#form_calendar%s", ary.parts[i].fieldid)).length > 0)
				Calendar.setup({inputField: sprintf("form_field%s", ary.parts[i].fieldid), ifFormat: '%Y-%m-%d', button: sprintf("form_calendar%s", ary.parts[i].fieldid), showsTime: false, timeFormat: "24"});
		} else {
			if (ary.parts[i].builtin != "header" && ary.parts[i].builtin != "freeform" && ary.parts[i].builtin != "image") {
				$J(sprintf("#part_%s", ary.parts[i].builtin)).addClass("preview_part_disabled");
				if (ary.parts[i].builtin == "firstlastname")
					$J("#part_fullname").addClass("preview_part_disabled");
				if (ary.parts[i].builtin == "fullname")
					$J("#part_firstlastname").addClass("preview_part_disabled");
			}
		}

		form_parts[ary.parts[i].id] = ary.parts[i];

		if (ary.parts[i].id == form_compile_flashid) {
			var bgcolor = form_background_color($J(sprintf("#compile%s", ary.parts[i].id)));
			$J(sprintf("#compile%s", ary.parts[i].id)).addClass("form_content_newestpart");
			$J(sprintf("#compile%s", ary.parts[i].id)).animate({ backgroundColor: bgcolor }, 1000);

			if (ary.parts[i].builtin == "image")
				form_part_edit_show(ary.parts[i].id, "image");
		}
	}

	$J("#preview").append(ary.notice);
	$J("#preview").append(ary.footer);
	$J(".preview_part").wrapAll("<div class='formwrapper'>");

	$J(".formwrapper").sortable({
		start: function(event, ui) {
			$J(".preview_part").addClass("preview_part_sorting");
		},
		stop: function(event, ui) {
			$J(".preview_part").removeClass("preview_part_sorting");
			form_part_saveorder();
		}
	});
}

function form_part_insert_field(id) {
	var post = {};

	if (form_insert_parts[id])
		return;

	form_insert_parts[id] = 1;

	if ($J(sprintf("#part_%s", id)).hasClass("preview_part_disabled")) {
		alert({/literal}'{"This field is already a part of your form."|alang|js}'{literal});
		return;
	}

	post.fieldid = id;
	post.formid = form_obj.id;
	ac_ajax_post_cb("api.php", "form.form_part_insert", ac_ajax_cb(form_part_insert_cb), post);
}

function form_part_insert_builtin(str) {
	var post = {};

	if (form_insert_parts[str])
		return;

	if (str == "firstlastname" || str == "fullname")
		form_insert_parts[str] = 1;

	if ($J(sprintf("#part_%s", str)).hasClass("preview_part_disabled"))
		return;

	post.builtin = str;
	post.formid = form_obj.id;
	ac_ajax_post_cb("api.php", "form.form_part_insert", ac_ajax_cb(form_part_insert_cb), post);
}

function form_part_insert_cb(ary) {
	form_compile();

	if (ary.fieldid > 0)
		$J(sprintf("#part_%s", ary.fieldid)).addClass("preview_part_disabled");
	else {
		if (ary.builtin != "header" && ary.builtin != "freeform" && ary.builtin != "image") {
			$J(sprintf("#part_%s", ary.builtin)).addClass("preview_part_disabled");
		}
	}

	form_compile_flashid = ary.id;
}

function form_part_saveorder() {
	var post = {};

	post.formid = form_obj.id;
	post.partids = [];
	$J("._partid").each(function() {
		post.partids.push(this.value);
	});

	ac_ajax_post_cb("api.php", "form.form_part_saveorder", ac_ajax_cb(form_part_saveorder_cb), post);
}

function form_part_saveorder_cb(ary) {
}

function form_part_edit_hide() {
	$J("#editpart").hide();
	$J("#fieldsdiv").show();

	if (form_mode == 'theme') {
		form_show_theme();
	}
}

function form_part_edit_show(id, builtin) {
	if (typeof form_parts[id] == "undefined")
		return;

	form_show_preview();
	$J("#editpart").show();
	$J("#fieldsdiv").hide();
	$J("#edit_partid").val(id);

	if (builtin != "freeform")
		$J("#edit_freeform_div").hide();

	if (builtin == "firstlastname") {
		$J("#edit_firstlastname_div").show();
		$J("#edit_header_div").hide();
		$J("#edit_button_div").hide();
		$J("#edit_image_upload_div").hide();

		$J("#edit_header").val("");
		$J("#edit_headerfirst").val(form_parts[id].header);
		$J("#edit_headerlast").val(form_parts[id].headerlast);
	} else if (builtin == "image") {
		$J("#edit_header_div").hide();
		$J("#edit_firstlastname_div").hide();
		$J("#edit_image_upload_div").show();
		$J("#edit_button_div").hide();
		$J("#image_partid").val(id);

		$J("#image_alt").val(form_parts[id].imagealt);
		$J("#image_align").val(form_parts[id].imagealign);
	} else if (builtin == "freeform") {
		$J("#edit_freeform_div").show();
		$J("#edit_firstlastname_div").hide();
		$J("#edit_image_upload_div").hide();
		$J("#edit_header_div").hide();
		$J("#edit_button_div").hide();

		ac_form_value_set($("edit_freeformEditor"), form_parts[id].content);
	} else if (builtin == "subscribe" || builtin == "unsubscribe") {
		$J("#edit_firstlastname_div").hide();
		$J("#edit_header_div").hide();
		$J("#edit_button_div").show();
		$J("#edit_image_upload_div").hide();

		$J("#edit_header").val("");
		$J("#edit_headerfirst").val("");
		$J("#edit_headerlast").val("");

		$J("#edit_button").val(form_parts[id].content);
	} else {
		$J("#edit_image_upload_div").hide();
		$J("#edit_firstlastname_div").hide();
		$J("#edit_header_div").show();
		$J("#edit_button_div").hide();

		$J("#edit_header").val(form_parts[id].header);
		$J("#edit_headerfirst").val("");
		$J("#edit_headerlast").val("");
	}

	window.location.hash = "#form_nil";
	window.location.hash = "#form_top";
}

function form_part_edit() {
	var post = {};

	post.id = $J("#edit_partid").val();

	if (!form_parts[post.id])
		return;

	if (form_parts[post.id].builtin == "image") {
		$J("#imageform").submit();
	}

	if (form_parts[post.id].builtin == "firstlastname") {
		post.header = $J("#edit_headerfirst").val();
		post.headerlast = $J("#edit_headerlast").val();

		if (post.header == "" || post.headerlast == "") {
			alert({/literal}'{"Please enter something for the first/last name headers"|alang|js}'{literal});
			return;
		}
	} else {
		post.header = $J("#edit_header").val();
		if (post.header == "" && form_parts[post.id].builtin != "freeform" && form_parts[post.id].builtin != "image" && form_parts[post.id].builtin != "subscribe" && form_parts[post.id].builtin != "unsubscribe") {
			alert({/literal}'{"Please enter something for the field header"|alang|js}'{literal});
			return;
		}
	}

	if (form_parts[post.id].builtin == "freeform") {
		post.content = ac_form_value_get($("edit_freeformEditor"));

		if (post.content == "") {
			alert({/literal}'{"Please enter something for your free-form HTML content"|alang|js}'{literal});
			return;
		}
	}

	if (form_parts[post.id].builtin == "subscribe" || form_parts[post.id].builtin == "unsubscribe") {
		post.content = $J("#edit_button").val();

		if (post.content == "") {
			alert(sprintf({/literal}'{"Please enter something for your %s button label"|alang|js}'{literal}, form_parts[post.id].builtin));
			return;
		}
	}

	ac_ajax_post_cb("api.php", "form.form_part_update", ac_ajax_cb(form_part_edit_cb), post);
	form_part_edit_hide();
}

function form_part_edit_cb(ary) {
	form_compile_flashid = ary.id;
	form_compile();
}

function form_part_delete(id) {
	if (confirm({/literal}'{"Are you sure you wish to remove this option from your form?"|alang|js}'{literal})) {
		var post = {};
		post.id = id;
		ac_ajax_post_cb("api.php", "form.form_part_delete", ac_ajax_cb(form_part_delete_cb), post);
	}
}

function form_part_delete_cb(ary) {
	form_compile_flashid = 0;
	form_compile();

	if (ary.fieldid > 0)
		form_insert_parts[ary.fieldid] = 0;

	if (ary.builtin != "")
		form_insert_parts[ary.builtin] = 0;
}

function form_toggle_editor(id, action, settings) {
	if ( action == ac_editor_is(id + 'Editor') ) return false;
	ac_editor_toggle(id + 'Editor', settings);
	return false;
}

function form_show_preview() {
	if ($J("#form_content_builddiv").hasClass("form_content_tab_selected"))
		return;

	$J("#form_content_builddiv").removeClass();
	$J("#form_content_builddiv").addClass("form_content_tab_selected");
	$J("#form_content_build_headerdiv").removeClass();
	$J("#form_content_build_headerdiv").addClass("header");

	$J("#form_content_themediv").removeClass();
	$J("#form_content_themediv").addClass("form_content_tab");
	$J("#form_content_theme_headerdiv").removeClass();
	$J("#form_content_theme_headerdiv").addClass("form_content_fields_header_gray");

	$J("#fieldsdiv").show();
	$J("#themediv").hide();
}

function form_show_theme() {
	if ($J("#form_content_themediv").hasClass("form_content_tab_selected"))
		return;

	$J("#form_content_themediv").removeClass();
	$J("#form_content_themediv").addClass("form_content_tab_selected");
	$J("#form_content_theme_headerdiv").removeClass();
	$J("#form_content_theme_headerdiv").addClass("header");

	$J("#form_content_builddiv").removeClass();
	$J("#form_content_builddiv").addClass("form_content_tab");
	$J("#form_content_build_headerdiv").removeClass();
	$J("#form_content_build_headerdiv").addClass("form_content_fields_header_gray");

	$J("#themediv").show();
	$J("#fieldsdiv").hide();
	$J("#editpart").hide();
}

function form_theme_apply(name) {
	$J("link#appliedtheme").remove();

	// Wipe out any background colors set via .animate()
	$J("._field").css("background-color", "");

	$J("head").append($J(sprintf("<link id='appliedtheme' rel='stylesheet' type='text/css' href='templates/form-themes/%s/style.css?v='" + Math.floor(Math.random() * 10.0) + ">", name)));
	$J(".form_content_theme_item").removeClass("form_content_theme_item_selected");
	$J(sprintf("#theme_%s", name)).addClass("form_content_theme_item_selected");

	ac.wait(1, function() {
		form_original_width = $J("._form").css("width");
		
		var match = form_original_width.match(/[0-9]+/);
		if (match)
			$J("#form_widthpx").val(match[0]);
	});

	var post = {
		id: form_obj.id,
		formname: name
	};

	ac_ajax_post_cb("api.php", "form.form_apply_theme", ac_ajax_cb(form_theme_apply_cb), post);
}

function form_theme_apply_cb(ary) {
}

function form_switch_list(val) {
	window.location.href = "main.php?action=formdetails_edit&listid=" + val;
}

{/literal}
