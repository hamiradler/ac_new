var startup_nothingfound = '{"Nothing found."|alang|js}';
var startup_viewreports  = '{"View Reports"|alang|js}';
var startup_nocampaigns  = '{"You currently do not have any drafts"|alang|js}';
var startup_title_chart_recenttrends = '{"Recent Trends"|alang|js}';
var startup_title_chart_subscriptions = '{"Subscription Trends"|alang|js}';

{literal}



function startup_viable() {
  if (typeof ac_js_admin.groups[3] != "undefined")
	ac_ajax_call_cb("api.php", "startup.startup_viable", ac_ajax_cb(startup_viable_cb));
}

function startup_viable_cb(ary) {
  // Everything worked fine!
  if (ary.result == 1)
	return;
  else {
	$("badhttp").className = "ac_block";
  }
}

function startup_rewrite() {
  if (typeof ac_js_admin.groups[3] != "undefined")
	ac_ajax_call_cb("api.php", "startup.startup_rewrite", ac_ajax_cb(startup_rewrite_cb));
}

function startup_rewrite_cb(ary) {
  if (ary.result == 1)
	return;
  else {
	$("badfriendlyurls").className = "ac_block";
  }
}

function startup_recent_campaigns(limit) {
  ac_ajax_call_cb("api.php", "startup.startup_recent_campaigns", ac_ajax_cb(startup_recent_campaigns_cb), limit);
}

function startup_recent_campaigns_cb(ary) {
  if (typeof ary.row != "undefined" && typeof ary.row.length != 'undefined' && ary.row.length > 0) {
	var sdate = "";
	var viewr = null;
	for (var i = 0; i < ary.row.length; i++) {
	  if (ary.row[i].sdate != "")
		sdate = sql2date(ary.row[i].sdate).format(datetimeformat);
	  else
		sdate = "N/A";

	  if (ary.row[i].status != 0 && ac_js_admin.pg_reports_campaign)
		viewr = Builder.node("a", { href: sprintf("main.php?action=report_campaign&id=%s", ary.row[i].id) }, startup_viewreports);
	  else
		viewr = "N/A";

	  var new_li = Builder.node(
	    "li",
	    {},
	    [
	      Builder.node("a", { href: "main.php?action=campaign_new_summary&id=" + ary.row[i].id }, ary.row[i].name)
	    ]
	  );
	  $("campaign_list_draft").appendChild(new_li);
	}
	$J('div#cpview> ul.drafts').show();
	$J('#cpview_div_drafts').addClass('expanded');
	$J('div#cpview> ul.templates').hide();
	$J('#cpview_div_templates').removeClass('expanded');
  }
  else {
  	//$("campLoadingBar").innerHTML        = startup_nothingfound;
  	//$("campLoadingBar").style.background = "";
		// no campaigns to show
  	if (ac_js_admin.pg_message_add) {
  		var no_campaigns = Builder.node("a", {href: "main.php?action=campaign_new"}, startup_nocampaigns);
  	}
  	else {
  		var no_campaigns = startup_nocampaigns;
  	}
	  var new_li = Builder.node(
		  "li",
		  {},
		  [
		    no_campaigns,
		    Builder._text(".")
		  ]
		);
		$("campaign_list_draft").appendChild(new_li);
		$J('div#cpview> ul.drafts').hide();
		$J('#cpview_div_drafts').removeClass('expanded');
		if ( $J('div#cpview> ul.templates li').length > 0 ) {
			$J('div#cpview> ul.templates').show();
			$J('#cpview_div_templates').addClass('expanded');
		} else {
			$J('div#cpview> ul.templates').hide();
			$J('#cpview_div_templates').removeClass('expanded');
		}
  }
}

function startup_gettingstarted_hide(groupids) {
	$J('.getstart').hide();
	ac_ajax_call_cb("api.php", "settings.settings_gettingstarted_hide", function() {}, groupids);
}

function campaign_sendlog_switch() {
	ac_ajax_call_cb(
		"api.php",
		"settings.settings_sendlog_switch",
		ac_ajax_cb(function(ary) {
			if ( ary.succeeded == 1 ) {
				ac_result_show(ary.message);
				var rel = $('campaign_sendlog_warn');
				if ( rel ) rel.className = ary.newval > 0 ? 'resultMessage' : 'ac_hidden';
			} else {
				ac_error_show(ary.message);
			}
		})
	);
	return false;
}




function startup_charts_draw() {

	// small chart properties
	var smallchart = chart_small();

	var largechart = chart_large();

	// subscriber rate
	ac.graph('subscriber_rate', {}, function(data) {
		var rows = [];
		var max = 0;
		$J(data.series).each(function(i) {
			rows.push([this, data.graph[i]]);
			var val = parseInt(data.graph[i], 10);
			if ( val > max ) max = val;
			//alert(this + ' => ' + data.graph[i]);
		});
		$J('#chart_subscriptions_blank').toggle(!!data.extras.empty);
		largechart.colors[0] = data.extras.empty ? '#d9eff8' : '#52aed8';
		largechart.vAxis.baselineColor = data.extras.empty ? '#cbe7f3' : '#cbe7f3';
		largechart.max = max * 1.3;
		chart_draw('chart_subscriptions', rows, largechart);
		// now set average in H4
		$J('#current_subscriber_rate').html(ac.str.number_format(data.extras.last));
	});

	// interaction trend
	ac.graph('interaction_rate', {}, function(data) {
		// build graph
		var rows = [];
		var max = 0;
		$J(data.series).each(function(i) {
			//if ( typeof data.graph[i] == 'undefined' )
			rows.push([data.series[i], data.graph[i]]);
			var val = parseInt(data.graph[i], 10);
			if ( val > max ) max = val;
			//alert(this + ' => ' + data.graph[i]);
		});
		$J('#chart_interaction_blank').toggle(!!data.extras.empty);
		largechart.colors[0] = data.extras.empty ? '#d9eff8' : '#52aed8';
		largechart.vAxis.baselineColor = data.extras.empty ? '#cbe7f3' : '#cbe7f3';
		largechart.max = max * 1.3;
		chart_draw('chart_interaction', rows, largechart);
		// now set average in H4
		$J('#avg_interaction').html(data.extras.avg + '%');
	});

	// subscribe trend
	ac.graph('subscribed_bydate', {}, function(data) {
		var rows = [];
		$J(data.series).each(function(i) {
			rows.push([data.series[i], data.graph[i]]);
		});
		$J('#chart_recent_trend_blank').toggle(!!data.extras.empty);
		smallchart.colors[0] = data.extras.empty ? '#d9eff8' : '#52aed8';
		smallchart.vAxis.baselineColor = data.extras.empty ? '#cbe7f3' : '#cbe7f3';
		chart_draw('chart_recent_trend', rows, smallchart);
	});

	// unsubscribe trend
	ac.graph('unsubscribed_bydate', {}, function(data) {
		var rows = [];
		$J(data.series).each(function(i) {
			//if ( typeof data.graph[i] == 'undefined' )
			rows.push([data.series[i], data.graph[i]]);
		});
		$J('#chart_unsub_trend_blank').toggle(!!data.extras.empty);
		smallchart.colors[0] = data.extras.empty ? '#d9eff8' : '#b1bfc6';
		smallchart.vAxis.baselineColor = data.extras.empty ? '#cbe7f3' : '#cbe7f3';
		chart_draw('chart_unsub_trend', rows, smallchart);
	});

	// open trend
	ac.graph('read_rate', {}, function(data) {
		var rows = [];
		$J(data.series).each(function(i) {
			//if ( typeof data.graph[i] == 'undefined' )
			rows.push([data.series[i], data.graph[i]]);
		});
		$J('#chart_open_trend_blank').toggle(!!data.extras.empty);
		smallchart.colors[0] = data.extras.empty ? '#d9eff8' : '#52aed8';
		smallchart.vAxis.baselineColor = data.extras.empty ? '#cbe7f3' : '#cbe7f3';
		chart_draw('chart_open_trend', rows, smallchart);
	});

	// clicks trend
	ac.graph('link_rate', {}, function(data) {
		var rows = [];
		$J(data.series).each(function(i) {
			//if ( typeof data.graph[i] == 'undefined' )
			rows.push([data.series[i], data.graph[i]]);
		});
		$J('#chart_click_trend_blank').toggle(!!data.extras.empty);
		smallchart.colors[0] = data.extras.empty ? '#d9eff8' : '#52aed8';
		smallchart.vAxis.baselineColor = data.extras.empty ? '#cbe7f3' : '#cbe7f3';
		chart_draw('chart_click_trend', rows, smallchart);
	});
}

function activity_stream(lim, append) {
	var off = ( append ? $J('td.streaminfo').length : 0 );
	var p = { limit: lim, offset: off, filter: [] };
	$J('tr.filteroptions input').filter(':checked').each(function(k,v) { p.filter.push(v.value); });
	ac.post("stats.stats_activity_stream", p, function(data) {
		// remove the loader
		//$J('#streamloader').hide();
		// add the html
		if ( append ) {
			$J('#streamview').append(data.html);
		} else {
			$J('#streamview').fadeTo(250, 0.65);
			$J('#streamview').html(data.html);
			$J('#streamview').fadeTo(1000, 1.00);
		}
		// fix the graphs for newly added html
		$J('#streamview .sparkme').sparkline('html',{type:'bar', barColor:'#0c87c9', barWidth:8, barSpacing:1, height:'18px'});
		$J('#streamview .sparkme').removeClass('sparkme');

		// set the scene based on other vars
		if ( !data.hasmore ) {
			$J('.actstreamreload').hide();
		} else {
			$J('.actstreamreload').show();
		}
		if ( !append && data.empty ) {
			$J('#nodataoverlay').show();
		} else {
			$J('#nodataoverlay').hide();
		}
	});
}

ac.interval(60, function() { activity_stream($J('td.streaminfo').length, 0); });



function walkthrough_close(alertid) {
	ac.get("startup.startup_walkthrough_close", [alertid], function(data) {
		/*
		if ( $J('#walkthrough') ) {
			$J('#walkthrough').modal('hide');
		}
		*/
	});
	return false;
}


{/literal}
