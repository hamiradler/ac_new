var header_delete_str = '{"Are you sure you want to delete email header %s?"|alang|js}';
var header_delete_str_multi = '{"Are you sure you want to delete the following email headers?"|alang|js}';
var header_delete_str_cant_delete = '{"You do not have permission to delete email headers"|alang|js}';
{literal}
var header_delete_id = 0;
var header_delete_id_multi = "";

function header_delete_check(id) {
	if (!ac_js_admin.pg_list_edit) {
		ac_ui_anchor_set(header_list_anchor());
		alert(header_delete_str_cant_delete);
		return;
	}

	if (id < 1) {
		header_delete_check_multi();
		return;
	}
	ac_ajax_call_cb("api.php", "header.header_select_row", header_delete_check_cb, id);
}

function header_delete_check_cb(xml) {
	var ary = ac_dom_read_node(xml);

	ac_dom_remove_children($("delete_list"));

	header_delete_id = ary.id;
	$("delete_message").innerHTML = sprintf(header_delete_str, ary.title);
	ac_dom_display_block("delete");
}

function header_delete_check_multi() {
	if (!ac_js_admin.pg_list_edit) {
		ac_ui_anchor_set(header_list_anchor());
		alert(header_delete_str_cant_delete);
		return;
	}

	if (!ac_form_check_selection_check($("list_table"), "multi[]", jsNothingSelected, jsNothingFound)) {
		ac_ui_anchor_set(header_list_anchor());
		return;
	}

	var sel = ac_form_check_selection_get($("list_table"), "multi[]");
	ac_ajax_call_cb("api.php", "header.header_select_array", header_delete_check_multi_cb, 0, sel.join(","));
	header_delete_id_multi = sel.join(",");
}

function header_delete_check_multi_cb(xml) {
	var ary = ac_dom_read_node(xml);

	$("delete_message").innerHTML = header_delete_str_multi;

	ac_dom_remove_children($("delete_list"));
	if (!selectAllSwitch) {
		for (var i = 0; i < ary.row.length; i++)
			$("delete_list").appendChild(Builder.node("li", [ ary.row[i].title]));
	} else {
		t$("delete_list").appendChild(Builder.node("li", [ jsAllItemsWillBeDeleted ]));
	}

	ac_dom_display_block("delete");
}

function header_delete(id) {
	if (header_delete_id_multi != "") {
		header_delete_multi();
		return;
	}
	ac_ajax_call_cb("api.php", "header.header_delete", header_delete_cb, id);
}

function header_delete_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();

	if (ary.succeeded != "0") {
		ac_result_show(ary.message);
		ac_ui_anchor_set(header_list_anchor());
	} else {
		ac_error_show(ary.message);
	}

	ac_dom_toggle_display("delete", "block");
}

function header_delete_multi() {
	if (selectAllSwitch) {
		ac_ajax_call_cb("api.php", "header.header_delete_multi", header_delete_multi_cb, "_all", header_list_filter);
		return;
	}
	header_delete_id_multi = "";
}

function header_delete_multi_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();

	if (ary.succeeded != "0") {
		ac_result_show(ary.message);
		ac_ui_anchor_set(header_list_anchor());
	} else {
		ac_error_show(ary.message);
	}

	ac_dom_toggle_display("delete", "block");
	$('selectXPageAllBox').className = 'ac_hidden'; 
	$('acSelectAllCheckbox').checked = false;
}
{/literal}
