var template_delete_str = '{"Are you sure you want to delete template %s?"|alang|js}';
var template_delete_str2 = '{"Are you sure you want to delete this template?"|alang|js}';
var template_delete_str_multi = '{"Are you sure you want to delete these template(s)?"|alang|js}';
var template_delete_str_cant_delete = '{"You do not have permission to delete templates"|alang|js}';
var template_delete_str3 = '{"Please select the templates you wish to delete."|alang|js}';
{literal}
var template_delete_id = 0;
var template_delete_id_multi = "";
var template_delete_imgurl = "";

function template_delete_check(id) {
	if (ac_js_admin.pg_template_delete != 1) {
		ac_ui_anchor_set(template_preview_anchor());
		alert(template_delete_str_cant_delete);
		return;
	}

	if (id < 1) {
		template_delete_check_multi();
		return;
	}
	
	template_delete_id = id;
	ac_dom_remove_children($("delete_list"));
	$("delete_message").innerHTML = template_delete_str2 + "<br /><br /><img src='" + template_delete_imgurl + "' /><br /><br />";
	ac_dom_display_block("delete");
}

function template_delete_check_multi() {
	if (ac_js_admin.pg_template_delete != 1) {
		ac_ui_anchor_set(template_preview_anchor());
		alert(template_delete_str_cant_delete);
		return;
	}

	var sel = template_choice_selected();
	
	if (sel.length == 0) {
		alert(template_delete_str3);
		ac_ui_anchor_set(template_preview_anchor());
		return;
	}
	
	/*
	if (!ac_form_check_selection_check($("list_table"), "multi[]", jsNothingSelected, jsNothingFound)) {
		ac_ui_anchor_set(template_preview_anchor());
		return;
	}
	*/

	//var sel = ac_form_check_selection_get($("list_table"), "multi[]");
	ac_ajax_call_cb("api.php", "template.template_select_array", template_delete_check_multi_cb, 0, sel.join(","));
	template_delete_id_multi = sel.join(",");
}

function template_delete_check_multi_cb(xml) {
	var ary = ac_dom_read_node(xml);

	$("delete_message").innerHTML = template_delete_str_multi;

	/*
	ac_dom_remove_children($("delete_list"));
	if (!selectAllSwitch) {
		for (var i = 0; i < ary.row.length; i++)
			$("delete_list").appendChild(Builder.node("li", [ ary.row[i].name ]));
	} else {
		$("delete_list").appendChild(Builder.node("li", [ jsAllItemsWillBeDeleted ]));
	}
	*/

	ac_dom_display_block("delete");
}

function template_delete(id) {
	if (template_delete_id_multi != "") {
		template_delete_multi();
		return;
	}
	ac_ajax_call_cb("api.php", "template.template_delete", template_delete_cb, id);
}

function template_delete_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();

	if (ary.succeeded != "0") {
		ac_result_show(ary.message);
		ac_ui_anchor_set(template_preview_anchor());
		template_clear();
		template_display();
	} else {
		ac_error_show(ary.message);
	}

	ac_dom_toggle_display("delete", "block");
}

function template_delete_multi() {
	/*
	if (selectAllSwitch) {
		ac_ajax_call_cb("api.php", "template.template_delete_multi", template_delete_multi_cb, "_all", template_list_filter);
		return;
	}
	*/
	ac_ajax_call_cb("api.php", "template.template_delete_multi", template_delete_multi_cb, template_delete_id_multi);
	template_delete_id_multi = "";
}

function template_delete_multi_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();

	if (ary.succeeded != "0") {
		ac_result_show(ary.message);
	} else {
		ac_error_show(ary.message);
	}

	ac_ui_anchor_set(template_preview_anchor());
	template_clear();
	template_display();
	
	ac_dom_toggle_display("delete", "block");
	//$("acSelectAllCheckbox").checked = false;
	//$('selectXPageAllBox').className = 'ac_hidden';
}
{/literal}
