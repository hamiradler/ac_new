{literal}

var field = {};

var choice_counter = 1;
var field_changed = false;

var countries = "Afghanistan\nAlbania\nAlgeria\nAmerican Samoa\nAndorra\nAngola\nAnguilla\nAntarctica\nAntigua and Barbuda\nArgentina\nArmenia\nAruba\nAustralia\nAustria\nAzerbaijan\nBahamas\nBahrain\nBangladesh\nBarbados\nBelarus\nBelgium\nBelize\nBenin\nBermuda\nBhutan\nBolivia\nBosnia and Herzegovina\nBotswana\nBouvet Island\nBrazil\nBritish Indian Ocean Territory\nBrunei Darussalam\nBulgaria\nBurkina Faso\nBurundi\nCambodia\nCameroon\nCanada\nCape Verde\nCayman Islands\nCentral African Republic\nChad\nChile\nChina\nChristmas Island\nCocos (Keeling) Islands\nColombia\nComoros\nCongo\nCongo, the Democratic Republic of the\nCook Islands\nCosta Rica\nCote D\'Ivoire\nCroatia\nCuba\nCyprus\nCzech Republic\nDenmark\nDjibouti\nDominica\nDominican Republic\nEcuador\nEgypt\nEl Salvador\nEquatorial Guinea\nEritrea\nEstonia\nEthiopia\nFalkland Islands (Malvinas)\nFaroe Islands\nFiji\nFinland\nFrance\nFrench Guiana\nFrench Polynesia\nFrench Southern Territories\nGabon\nGambia\nGeorgia\nGermany\nGhana\nGibraltar\nGreece\nGreenland\nGrenada\nGuadeloupe\nGuam\nGuatemala\nGuinea\nGuinea-Bissau\nGuyana\nHaiti\nHeard Island and Mcdonald Islands\nHoly See (Vatican City State)\nHonduras\nHong Kong\nHungary\nIceland\nIndia\nIndonesia\nIran, Islamic Republic of\nIraq\nIreland\nIsrael\nItaly\nJamaica\nJapan\nJordan\nKazakhstan\nKenya\nKiribati\nKorea, Democratic People\'s Republic of\nKorea, Republic of\nKuwait\nKyrgyzstan\nLao People\'s Democratic Republic\nLatvia\nLebanon\nLesotho\nLiberia\nLibyan Arab Jamahiriya\nLiechtenstein\nLithuania\nLuxembourg\nMacao\nMacedonia, the Former Yugoslav Republic of\nMadagascar\nMalawi\nMalaysia\nMaldives\nMali\nMalta\nMarshall Islands\nMartinique\nMauritania\nMauritius\nMayotte\nMexico\nMicronesia, Federated States of\nMoldova, Republic of\nMonaco\nMongolia\nMontserrat\nMorocco\nMozambique\nMyanmar\nNamibia\nNauru\nNepal\nNetherlands\nNetherlands Antilles\nNew Caledonia\nNew Zealand\nNicaragua\nNiger\nNigeria\nNiue\nNorfolk Island\nNorthern Mariana Islands\nNorway\nOman\nPakistan\nPalau\nPalestinian Territory, Occupied\nPanama\nPapua New Guinea\nParaguay\nPeru\nPhilippines\nPitcairn\nPoland\nPortugal\nPuerto Rico\nQatar\nReunion\nRomania\nRussian Federation\nRwanda\nSaint Helena\nSaint Kitts and Nevis\nSaint Lucia\nSaint Pierre and Miquelon\nSaint Vincent and the Grenadines\nSamoa\nSan Marino\nSao Tome and Principe\nSaudi Arabia\nSenegal\nSerbia and Montenegro\nSeychelles\nSierra Leone\nSingapore\nSlovakia\nSlovenia\nSolomon Islands\nSomalia\nSouth Africa\nSouth Georgia and the South Sandwich Islands\nSpain\nSri Lanka\nSudan\nSuriname\nSvalbard and Jan Mayen\nSwaziland\nSweden\nSwitzerland\nSyrian Arab Republic\nTaiwan, Province of China\nTajikistan\nTanzania, United Republic of\nThailand\nTimor-Leste\nTogo\nTokelau\nTonga\nTrinidad and Tobago\nTunisia\nTurkey\nTurkmenistan\nTurks and Caicos Islands\nTuvalu\nUganda\nUkraine\nUnited Arab Emirates\nUnited Kingdom\nUnited States\nUnited States Minor Outlying Islands\nUruguay\nUzbekistan\nVanuatu\nVenezuela\nViet Nam\nVirgin Islands, British\nVirgin Islands, U.s.\nWallis and Futuna\nWestern Sahara\nYemen\nZambia\nZimbabwe";
var usstates  = "Alabama\nAlaska\nAmerican Samoa\nArizona\nArkansas\nArmed Forces Africa\nArmed Forces Americas (except Canada)\nArmed Forces Canada\nArmed Forces Europe\nArmed Forces Middle East\nArmed Forces Pacific\nCalifornia\nColorado\nConnecticut\nDelaware\nDistrict of Columbia\nFederated States of Micronesia\nFlorida\nGeorgia\nGuam\nHawaii\nIdaho\nIllinois\nIndiana\nIowa\nKansas\nKentucky\nLouisiana\nMaine\nMarshall Islands\nMaryland\nMassachusetts\nMichigan\nMinnesota\nMississippi\nMissouri\nMontana\nNebraska\nNevada\nNew Hampshire\nNew Jersey\nNew Mexico\nNew York\nNorth Carolina\nNorth Dakota\nNorthern Mariana Islands\nOhio\nOklahoma\nOregon\nPalau\nPennsylvania\nPuerto Rico\nRhode Island\nSouth Carolina\nSouth Dakota\nTennessee\nTexas\nUtah\nVermont\nVirgin Islands\nVirginia\nWashington\nWest Virginia\nWisconsin\nWyoming";

field.uncheck_other_lists = function() {
	$J("input[name=inlist][value!='0,1']").attr("checked", false);
};

field.uncheck_all_option = function() {
	$J("input[name=inlist][value='0,1']").attr("checked", false);
};

function field_different() {
	field_changed = true;
}

function field_safe() {
	field_changed = false;
}

function field_unload() {
	if (field_changed)
		return messageLP;
}

// set unload
ac_dom_unload_hook(field_unload);

function field_move_up(id) {
	field_safe();
	$J("#field_movefieldid").val(id);
	$J("#field_movefielddir").val("up");
	$("fieldform").submit();
}

function field_move_down(id) {
	field_safe();
	$J("#field_movefieldid").val(id);
	$J("#field_movefielddir").val("down");
	$("fieldform").submit();
}

function field_errorcheck() {
	var post = {};

	post.names = [];
	post.perstags = [];
	post.origtags = [];
	post.fieldids = [];
	post.orignames = [];

	$J(".field_input_name").each(function() {
		post.names.push(this.value);
	});

	$J(".field_input_perstag").each(function() {
		post.perstags.push(this.value);
	});

	$J(".field_hidden_origtag").each(function() {
		post.origtags.push(this.value);
	});

	$J(".field_hidden_fieldid").each(function() {
		post.fieldids.push(this.value);
	});

	$J(".field_hidden_origname").each(function() {
		post.orignames.push(this.value);
	});

	ac_ajax_post_cb("api.php", "field.field_errorcheck", ac_ajax_cb(field_errorcheck_cb), post);
}

function field_errorcheck_cb(ary) {
	if (ary.succeeded == 0) {
		ac_error_show(ary.message);
	} else {
		$("fieldform").submit();
	}
}

function field_listchange(val) {
	window.location.href = "main.php?action=field&listid=" + val;
}

function field_add() {
	field_modal_lists = $("field_list").value;
	$("field_modal_addtype").show();
	field_modal_set_enter("");
}

function field_modal_save_after(ary) {
	field_listchange($("field_list").value);
	window.location.reload(true);	// Force reload
}

function field_modal_set_enter(field_type) {
	// establishes the "Enter" key as another way to proceed to the next step
	if (!field_type) {
		var div_id = "field_modal_addtype";
		var next_button_id = "field_modal_next_button";
	}
	else {
		var div_id = "field_modal_add" + field_type;
		var next_button_id = "field_modal_" + field_type + "_save_button";		
	}

	// remove all click events from each Next/Save buttons
	$J(".ac_button_ok").unbind("click");
	// first focus on Next/Save button so hitting Enter causes the button to be clicked
	$J("#" + next_button_id).focus();
	// then target the div we are currently displaying, and capture Enter clicks
	$J(document).keypress(function(e) {
		if (e.keyCode == 13) {
			// emulate the button click when hitting Enter
			field_modal_set_enter_click(next_button_id);
		}
	});
}

function field_modal_set_enter_click(next_button_id) {
//alert(next_button_id);
	$J("#" + next_button_id).click();
}

function field_namereq(val) {
	if (val) {
		$("firstreq").checked = true;
		$("lastreq").checked = true;
	} else {
		$("firstreq").checked = false;
		$("lastreq").checked = false;
	}
}

function field_defname_load() {
	ac_ui_api_call(jsLoading);
	ac_ajax_call_cb("api.php", "field.field_loadname", ac_ajax_cb(field_defname_load_cb), $J("#defname_listid").val());
}

function field_defname_load_cb(ary) {
	ac_ui_api_callback();
	$J("#defname_value").val(ary.defname);
	$J("#field_defname").show();
}

function field_defname_save() {
	var post = $("defnameform").serialize(true);

	post.listid = $J("#defname_listid").val();

	ac_ui_api_call(jsSaving);
	ac_ajax_post_cb("api.php", "field.field_savename", ac_ajax_cb(field_defname_save_cb), post);
}

function field_defname_save_cb(ary) {
	ac_ui_api_callback();

	if (ary.succeeded == 1) {
		ac_result_show(ary.message);
	} else {
		ac_error_show(ary.message);
	}

	$J("#field_defname").hide();
}

function field_options_load(id) {
	ac_ui_api_call(jsLoading);
	ac_ajax_call_cb("api.php", "field.field_loadopts", ac_ajax_cb(field_options_load_cb), id);
}

function field_options_load_cb(ary) {
	ac_ui_api_callback();

	$("option_fieldid").value = ary.id;
	$("option_showinlist").checked = (ary.show_in_list == 1);
	$("option_rows").value = ary.rows;
	$("option_cols").value = ary.cols;
	$("option_visible").checked = (ary.visible == 1);

	$("option_defval_text").value = "";
	$("option_defval_dropdown").value = "";

	if (ary.type == "textarea")
		$("option_rowcol_div").show();
	else
		$("option_rowcol_div").hide();

	if (ary.type == "text" || ary.type == "textarea" || ary.type == "hidden") {
		$("option_defval_text_div").show();
		$("option_defval_dropdown_div").hide();
		$("option_defval_date_div").hide();
		$("option_defval_text").value = ary.defval;
	} else if (ary.type == "date") {
		$("option_defval_date_div").show();
		$("option_defval_dropdown_div").hide();
		$("option_defval_text_div").hide();
	} else {
		$("option_defval_text_div").hide();
		$("option_defval_date_div").hide();
		$("option_defval_dropdown_div").show();

		if (ary.type == "checkbox" || ary.type == "listbox")
			$J("#option_defval_dropdown").attr("multiple", 1);
		else
			$J("#option_defval_dropdown").removeAttr("multiple");

		ac_dom_remove_children($("option_defval_dropdown"));

		for (var i = 0; i < ary.options.length; i++) {
			$J("#option_defval_dropdown").append(sprintf("<option %s value='%s'>%s</option>", ary.options[i].isdefault ? "selected" : "", ary.options[i].id, ary.options[i].value));
		}
	}

	if (ary.lists.toString()) {
		var lists = ary.lists.toString().split(",");

		for (var i = 0; i < lists.length; i++) {
			var elemid = sprintf("option_list%s", lists[i]);

			if ($(elemid))
				$(elemid).checked = true;
		}
	}

	$("field_options").show();
}

function field_options_save() {
	var post = $("optionform").serialize(true);
	var hasalist = false;

	for (var i = 0; i < post.inlist.length; i++) {
		if (post.inlist[i].match(/,1$/))
			hasalist = true;
	}

	if (!hasalist) {
		alert({/literal}'{"You do not have any lists selected -- please choose one"|alang|js}'{literal});
		return false;
	}

	ac_ui_api_call(jsSaving);
	ac_ajax_post_cb("api.php", "field.field_saveopts", ac_ajax_cb(field_options_save_cb), post);
}

function field_options_save_cb(ary) {
	ac_ui_api_callback();

	if (ary.succeeded == 1) {
		ac_result_show(ary.message);
	} else {
		ac_error_show(ary.message);
	}

	//if (ary.refresh)
	//	window.location.reload(true);

	$("field_options").hide();
}

function field_newchoice(inc, fieldid, defval, ftype) {
	var rval;

	rval = 
		sprintf("<div id='choice_opt%s'>", inc) +
		sprintf("<a href='#' class='choice_opt_delete' onclick='field_different(); choice_delete(\"%s\", \"%s\"); return false'>x</a>", fieldid, inc) + " " +
		sprintf("<a href='#' class='choice_opt_up' onclick='field_different(); choice_moveup(\"%s\", \"%s\"); return false'>^</a>", fieldid, inc) + " " +
		sprintf("<a href='#' class='choice_opt_down' onclick='field_different(); choice_movedn(\"%s\", \"%s\"); return false'>v</a>", fieldid, inc) + " " +
		sprintf("<input type='text' name='choice[%s][]' value='%s' onkeyup='field_different()'>", fieldid, defval) +
		"</div>";

	$J("#choices" + fieldid).append(rval);
}

var addindex = 0;
function choice_add(fieldid, ftype) {
	addindex++;
	var inc = '0_' + fieldid + '_' + addindex;
	var defval = '';
	field_newchoice(inc, fieldid, defval, ftype);
}

function choice_add_bulk(fieldid, vals, ftype) {
	for ( var i = 0; i < vals.length; i++ ) {
		addindex++;
		var inc = '0_' + fieldid + '_' + addindex;
		var defval = vals[i];
		field_newchoice(inc, fieldid, defval, ftype);
	}
}

function choice_set_bulk(fieldid, vals, ftype) {
	if ( !$("choices" + fieldid) ) { alert("Field not found"); return; }
	if ( !confirm("Are you sure?") ) return;
	ac_dom_remove_children($("choices" + fieldid));
	choice_add_bulk(fieldid, vals.split(/\n/), ftype);
	field_safe();
	field_errorcheck();
}

function choice_delete(fieldid, inc) {
	if ($("choice_opt" + inc)) {
		$("choices" + fieldid).removeChild($("choice_opt" + inc));
	}
}

function choice_moveup(fieldid, inc) {
	var thisone = $("choice_opt" + inc);

	if (!thisone)
		return;

	var thatone = $("choice_opt" + inc).previousSibling;

	if (!thatone)
		return;

	$("choices" + fieldid).insertBefore(thisone, thatone);
}

function choice_movedn(fieldid, inc) {
	var thisone = $("choice_opt" + inc);

	if (!thisone)
		return;

	var thatone = $("choice_opt" + inc).nextSibling;

	if (!thatone)
		return;

	$("choices" + fieldid).insertBefore(thatone, thisone);
}

{/literal}
