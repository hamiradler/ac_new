<?php

$campaignid = intval(ac_http_param("id"));
$messageid = intval(ac_http_param("messageid"));
$period  = intval(ac_http_param("period"));
$from    = strval(ac_http_param("from"));
$to      = strval(ac_http_param("to"));

$series = array();
$graph  = array();

ac_graph_prepare_dateline($series, $graph, $period, $from, $to);

$cond = "";
if ($messageid > 0)
	$cond .= "AND ld.messageid = '$messageid' ";
//else
//	$cond .= "AND ld.messageid = '0' ";

if ($campaignid > 0)
	$cond .= "AND ld.campaignid = '$campaignid' ";

if ( !ac_admin_ismain() ) {
	$liststr = implode("', '", $admin['lists']);
	$cond .= "AND ( SELECT COUNT(*) FROM #subscriber_list sl WHERE ld.subscriberid = sl.subscriberid AND sl.listid IN ('$liststr') ) > 0 ";
}

$minid = (int)ac_sql_select_one("
	SELECT
		ld.id
	FROM
		#link_data ld
	WHERE
		ld.isread = 0
	AND ld.tstamp > '$from 23:59:59'
	AND ld.tstamp < ('$to' + INTERVAL 1 DAY)
	$cond
	ORDER BY ld.id ASC
	LIMIT 1
");
$maxid = (int)ac_sql_select_one("
	SELECT
		ld.id
	FROM
		#link_data ld
	WHERE
		ld.isread = 0
	AND ld.tstamp > '$from 23:59:59'
	AND ld.tstamp < ('$to' + INTERVAL 1 DAY)
	$cond
	ORDER BY ld.id DESC
	LIMIT 1
");

$rs = ac_sql_query("
	SELECT
		DATE_FORMAT(ld.tstamp, '%m/%d') AS tstamp,
		DATEDIFF('$to', tstamp) AS diff,
		COUNT(*) AS count
	FROM
		#link_data ld
	WHERE
		ld.isread = 0
	AND
		ld.id >= $minid
	AND
		ld.id <= $maxid
	$cond
	GROUP BY
		DATE(ld.tstamp)
");

while ($row = ac_sql_fetch_assoc($rs)) {
	$series[$row["diff"]] = $row["tstamp"];
	$graph[$row["diff"]] += $row["count"]; // "+" is here cuz we don't group by DATE(ld.tstamp)
}

if ( isset($_GET['json']) ) {
	krsort($series);
	krsort($graph);
	$series = array_values($series);
	$graph = array_values($graph);
}

$max = $cnt = $sum = $last = 0;
foreach ( $graph as $v ) {
	if ( $v > $max ) $max = $v;
	$sum += $v;
	$cnt++;
	$last = $v;
}
$avg = $cnt ? $sum / $cnt : 0;

$extras = array(
	'avg' => round($avg, 2),
	'max' => $max,
	'cnt' => $cnt,
	'sum' => $sum,
	'last' => $last,
	'empty' => !(bool)$sum,
	'minid' => $minid,
	'maxid' => $maxid,
);


if ( !$sum && isset($_GET['json']) ) {
	$graph[0] = 2;
	$graph[1] = 2.7;
	$graph[2] = 2.5;
	$graph[3] = 2.6;
	$graph[4] = 2.4;
	$graph[5] = 2.2;
	$graph[6] = 2.5;
	$graph[7] = 2.9;
	$graph[8] = 3.5;
	$graph[9] = 4.7;
	$graph[10] = 5.6;
	$graph[11] = 4.9;
	$graph[12] = 4.8;
	$graph[13] = 4.2;
	$graph[14] = 3.8;
	$graph[15] = 3.3;
	$graph[16] = 3.9;
	$graph[17] = 4.2;
	$graph[18] = 4.9;
	$graph[19] = 4.4;
	$graph[20] = 4.5;
	$graph[21] = 4.5;
	$graph[22] = 4.6;
	$graph[23] = 4.6;
	$graph[24] = 4.7;
	$graph[25] = 4.7;
	$graph[26] = 2.7;
	$graph[27] = 2.8;
	$graph[28] = 3.7;
	$graph[29] = 4.9;

}


//dbg($series,1);dbg($graph);


$smarty->assign("series", $series);
$smarty->assign("graph", $graph);

?>
