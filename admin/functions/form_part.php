<?php

require_once ac_admin("functions/rewrite.php");

function form_part_ordernum($formid) {
	$formid = (int)$formid;

	# Figure out the last form element.  Is it the subscribe button?
	$lastpart = ac_sql_select_row("SELECT * FROM #form_part WHERE formid = '$formid' ORDER BY ordernum DESC LIMIT 1");

	if ($lastpart["builtin"] == "subscribe") {
		# We want the Subscribe button to stay on the bottom if it's already
		# there.  Push up the sub button's ordernum by one, and return the old
		# ordernum.
		$up = array(
			"ordernum" => $lastpart["ordernum"] + 1,
		);

		ac_sql_update("#form_part", $up, "id = '$lastpart[id]'");
		return $lastpart["ordernum"];
	} else {
		# Push this form part to the bottom.
		return $lastpart["ordernum"] + 1;
	}
}

function form_part_insert() {
	$formid = (int)ac_http_param("formid");
	$fieldid = (int)ac_http_param("fieldid");
	$builtin = ac_http_param("builtin");

	if ($fieldid > 0) {
		$ins = array(
			"formid" => $formid,
			"fieldid" => $fieldid,
			"header" => (string)ac_sql_select_one("SELECT title FROM #field WHERE id = '$fieldid'"),
			"ordernum" => form_part_ordernum($formid),
		);

		ac_sql_insert("#form_part", $ins);
	} elseif ($builtin != "") {
		$ins = array(
			"formid" => $formid,
			"builtin" => $builtin,
			"content" => form_part_default_content($builtin),
			"header" => form_part_default_header($builtin),
			"ordernum" => form_part_ordernum($formid),
		);

		switch ($builtin) {
			case "firstlastname":
				$ins["headerlast"] = form_part_default_header($builtin, true);
				break;

			case "image":
				$ins["imagedata"] = @file_get_contents($GLOBALS["form_default_image"]);
				$ins["imagetype"] = "image/png";
				$ins["imagesize"] = strlen($ins["imagedata"]);
				$ins["imagealign"] = $GLOBALS["form_default_image_align"];

				if (function_exists('getimagesize')) {
					$size = @getimagesize($GLOBALS["form_default_image"]);
					$ins["imagewidth"] = $size[0];
					$ins["imageheight"] = $size[1];
				}

				break;

			default:
				break;
		}

		$sql = ac_sql_insert("#form_part", $ins);
		if (!$sql) die('There was a database error while adding this form part');
	}

	ac_sql_query("UPDATE #form SET waitpreview = '1' WHERE id = '$formid'");
	return ac_ajax_api_result(true, _a("Added field"), array("id" => (int)ac_sql_insert_id(), "fieldid" => $fieldid, "builtin" => $builtin));
}

function form_part_default_content($builtin) {
	switch ($builtin) {
		default:
			return "";

		case "freeform":
			return _a("This is an example of some free-form HTML.");

		case "subscribe":
			return _a("Subscribe");

		case "unsubscribe":
			return _a("Choose whether to subscribe or unsubscribe");
	}
}

function form_part_default_header($builtin, $islast = false) {
	switch ($builtin) {
		default:
			return "";

		case "header":
			return _a("Header");

		case "email":
			return _a("Email");

		case "firstlastname":
			return $islast ? _a("Last Name") : _a("First Name");

		case "fullname":
			return _a("Full Name");

		case "listselector":
			return _a("Select the lists you wish to subscribe to");

		case "captcha":
			return _a("Enter the following to confirm your subscription");

		case "unsubscribe":
			return _a("Choose whether to subscribe or unsubscribe");
	}
}

function form_md5($id) {
	return md5("Oy=Mv qFkk577S:zrzB~" . $id . "AhdZDzt_urt[|eVK4:9&");
}

function form_compile_admin($formid) {
	$formid = (int)$formid;
	$theme = (string)ac_sql_select_one("SELECT theme FROM #form WHERE id = '$formid'");
	$admin = ac_admin_get();
	$rval = array(
		"header" => "",
		"footer" => "",
		"notice" => "",
		"parts" => array(),
	);

	if (form_theme_safe($theme)) {
		$path = dirname(dirname(__FILE__)) . "/templates/form-themes/$theme";

		if (file_exists($path . "/header.htm"))
			$rval["header"] = @file_get_contents($path . "/header.htm");

		if (file_exists($path . "/header.htm"))
			$rval["footer"] = @file_get_contents($path . "/footer.htm");

		if (file_exists($path . "/notice.htm") && $admin["brand_links"])
			$rval["notice"] = @file_get_contents($path . "/notice.htm");
	}

	$rval["parts"] = form_compile($formid);
	return $rval;
}

function form_compile($formid, $sub = null) {
	$formid = (int)$formid;
	$listid = (int)ac_sql_select_one("SELECT listid FROM #form_list WHERE formid = '$formid' LIMIT 1");
	$list = list_select_row($listid);

	// Select everything but imagedata, which isn't needed.
	$parts = ac_sql_select_array("
		SELECT
			id,
			formid,
			fieldid,
			builtin,
			content,
			ordernum,
			header,
			headerlast,
			defval,
			imagetype,
			imagesize,
			imagealt,
			imagewidth,
			imageheight,
			imagealign
		FROM
			#form_part
		WHERE
			formid = '$formid'
		ORDER BY
			ordernum
	");

	$site = ac_site_get();

	foreach ($parts as $k => $part) {
		$parts[$k]["html"] =
			"<div id='_field$part[id]'>\n" . form_part_compile($part, $list, $sub) . "</div>\n\n";
	}

	return $parts;
}

function form_theme_safe($theme) {
	return !ac_str_instr("..", $theme) && !ac_str_instr("/", $theme);
}

function form_expand_css($content, $fromdir) {
	$m = array();
	$urls = array();

	preg_match_all('/url\(([^)]+)\)/', $content, $m);

	if (isset($m[1])) {
		foreach ($m[1] as $url) {
			if (!ac_str_instr("http:", $url) && !ac_str_instr("https:", $url)) {
				// Some URLs may be listed more than once; don't run str_replace() again if so.
				if (isset($urls[$url]))
					continue;

				$urls[$url] = 1;
				$content = str_replace($url, $fromdir . $url, $content);
			}
		}
	}

	return $content;
}

function form_header($form, $extra = null) {
	$site = ac_site_get();
	$rval = "";
	$path = "";

	if ($extra === null) {
		$extra = array(
			"s" => "",
			"c" => "0",
			"m" => "0",
			"act" => "sub",
			"lists" => array(),
			"cal" => false,
		);
	} else {
		if (!isset($extra["s"]))
			$extra["s"] = "";

		if (!isset($extra["c"]))
			$extra["c"] = "0";

		if (!isset($extra["m"]))
			$extra["m"] = "0";

		if (!isset($extra["act"]))
			$extra["act"] = "sub";
	}

	if (form_theme_safe($form["theme"]))
		$path = dirname(dirname(__FILE__)) . "/templates/form-themes/$form[theme]";

	$rval .= "<style>\n";
	$rval .= form_expand_css(@file_get_contents(ac_admin("css/form_defaults.css")), ac_site_plink("admin/css/")) . "\n";
	if (file_exists($path . "/style.css"))
		$rval .= form_expand_css(@file_get_contents($path . "/style.css"), ac_site_plink("admin/templates/form-themes/$form[theme]/"));
	$rval .= "</style>\n";

	if (isset($extra["cal"]) && $extra["cal"]) {
		$path = ac_site_plink() . "/ac_global";
		$rval .= "<style type='text/css'>@import url($path/jscalendar/calendar-win2k-1.css);</style>";
		$rval .= "<script type='text/javascript' src='$path/jscalendar/calendar.js'></script>";
		$rval .= "<script type='text/javascript' src='$path/jscalendar/lang/calendar-en.js'></script>";
		$rval .= "<script type='text/javascript' src='$path/jscalendar/calendar-setup.js'></script>";
	}

	$rval .= "<form action='$site[p_link]/proc.php' method='post' id='_form_$form[id]' accept-charset='utf-8' enctype='multipart/form-data'";

	if (isset($form["incga"]) && $form["incga"])
		$rval .= "onsubmit='_acUTM(this); return true;'";

	$rval .= ">\n";
	$rval .= "<input type='hidden' name='f' value='$form[id]'>\n";
	$rval .= "<input type='hidden' name='s' value='$extra[s]'>\n";
	$rval .= "<input type='hidden' name='c' value='$extra[c]'>\n";
	$rval .= "<input type='hidden' name='m' value='$extra[m]'>\n";
	$rval .= "<input type='hidden' name='act' value='$extra[act]'>\n";

	if (isset($extra["lists"])) {
		foreach ($extra["lists"] as $listid)
			$rval .= "<input type='hidden' name='nlbox[]' value='$listid'>\n";
	}

	$rval .= "<div class='_form'>\n";

	if (file_exists($path . "/header.htm"))
		$rval .= @file_get_contents($path . "/header.htm");

	$rval .= "<div class='formwrapper'>";

	return $rval;
}

function form_footer($form, $extra = array()) {
	$rval = "";
	$path = "";

	$shownotice = !isset($extra["shownotice"]) || $extra["shownotice"];

	$admin = ac_admin_get();

	if (form_theme_safe($form["theme"])) {
		$path = dirname(dirname(__FILE__)) . "/templates/form-themes/$form[theme]";

		$rval .= "</div>";

		if (file_exists($path . "/notice.htm") && $admin["brand_links"] && $shownotice)
			$rval .= @file_get_contents($path . "/notice.htm");

		if (file_exists($path . "/footer.htm"))
			$rval .= @file_get_contents($path . "/footer.htm");
	} else {
		$rval .= "</div>";
	}

	$rval .= "</div>\n";
	$rval .= "</form>\n";

	return $rval;
}

function form_part_compile($part, $list, $sub) {
	$site = ac_site_get();
	$rval = "";
	$value = "";

	if ( $GLOBALS["form_compile_view"] == "preview" ) {
		$rval .= "    <input type='hidden' class='_partid' value='$part[id]'>\n";
	}

	if ($part["fieldid"] > 0) {
		# It's a custom field.
		$field = ac_sql_select_row("SELECT * FROM #field WHERE id = '$part[fieldid]' AND visible = '1'");

		if (!$field)
			return "";

		if ($GLOBALS["form_compile_view"] == "preview" || $field["type"] != "hidden") {
			if ($field["isrequired"])
				$rval .= form_part_compile_header($part["header"] . " *", $field["type"] == "hidden" ? "_hiddenlabel" : "");
			else
				$rval .= form_part_compile_header($part["header"], $field["type"] == "hidden" ? "_hiddenlabel" : "");
		}

		$rval .= form_part_compile_data_field($field, isset($sub["fields"]) ? $sub["fields"] : array());
		$rval = form_part_compile_wrap_field($field, $rval, $part["id"]);
	} else {
		switch ($part["builtin"]) {
			default:
				break;

			case "email":
				if ($sub)
					$value = "value='$sub[email]'";

				$rval .= form_part_compile_header($part["header"] . " *");
				$rval .= form_part_compile_data("<input type='email' name='email' $value>\n");
				$rval = form_part_compile_wrap("_type_input", $rval, $part["id"]);
				break;

			case "firstlastname":
				if ($sub)
					$value = "value='$sub[first_name]'";

				$rval .= "<table cellpadding='0' cellspacing='0'>\n";
				$rval .= "<tr>\n";
				$rval .= "<td>\n";
				$rval .= form_part_push(6, form_part_compile_header($part["header"] . ($list["require_name"] ? " *" : "")));
				$rval .= "</td>\n";
				$rval .= "<td>\n";
				$rval .= form_part_push(6, form_part_compile_header($part["headerlast"] . ($list["require_name"] ? " *" : "")));
				$rval .= "</td>\n";
				$rval .= "</tr>\n";
				$rval .= "<tr>\n";
				$rval .= "<td>\n";
				$rval .= form_part_push(6, form_part_compile_data("<input type='text' name='firstname' $value>\n"));
				$rval .= "</td>\n";
				$rval .= "<td>\n";

				if ($sub)
					$value = "value='$sub[last_name]'";

				$rval .= form_part_push(6, form_part_compile_data("<input type='text' name='lastname' $value>\n"));
				$rval .= "</td>\n";
				$rval .= "</tr>\n";
				$rval .= "</table>\n";
				$rval = form_part_compile_wrap("_type_input", $rval, $part["id"]);
				break;

			case "fullname":
				if ($sub)
					$value = "value='$sub[first_name] $sub[last_name]'";
				$rval .= form_part_compile_header($part["header"] . ($list["require_name"] ? " *" : ""));
				$rval .= form_part_compile_data("<input type='text' name='fullname' $value>\n");
				$rval = form_part_compile_wrap("_type_input", $rval, $part["id"]);
				break;

			case "listselector":
				$all_lists = ac_sql_select_array("SELECT l.id, l.name FROM #list l WHERE l.id IN (SELECT f.listid FROM #form_list f WHERE f.formid = '$part[formid]')");
				$rval .= form_part_compile_header($part["header"]);

				$html = "";
				foreach ($all_lists as $each)
					$html .= "<div><input type='checkbox' value='$each[id]' name='nlbox[]' checked> $each[name]</div>\n";

				$rval .= form_part_compile_data($html);
				$rval = form_part_compile_wrap("_type_checkbox", $rval, $part["id"]);

				break;

			case "captcha":
				$plink = ac_site_plink();
				$rand  = base64_encode(rand());
				$rval .= form_part_compile_header($part["header"] . " *");
				$rval .= "<div><img src='$plink/ac_global/scripts/randomimage.php?rand=$rand' width='60' height='30'/></div>\n";
				$rval .= "<div><input type='text' name='captcha' autocomplete='off'></div>\n";
				$rval = form_part_compile_wrap("_type_captcha", $rval, $part["id"]);
				break;

			case "header":
				$rval .= form_part_compile_header($part["header"]);
				$rval = form_part_compile_wrap("_type_header", $rval, $part["id"]);
				break;

			case "image":
				$alt = addcslashes($part["imagealt"], "'");
				$sizes = "";

				if ($alt != "")
					$alt = "alt='$alt'";

				if ($part["imagewidth"] > 0)
					$sizes = sprintf('width="%s" height="%s"', $part["imagewidth"], $part["imageheight"]);

				$align = "style='text-align: $part[imagealign]'";

				$rval .= form_part_compile_data("<div $align><img src='$site[p_link]/formimage.php?partid=$part[id]' $alt $sizes></div>\n");
				$rval = form_part_compile_wrap("_type_image", $rval, $part["id"]);
				break;

			case "_forward_name":
				$value = "";
				if ($sub)
					$value = sprintf("value='%s'", str_replace("'", "\\'", $sub["first_name"] . " " . $sub["last_name"]));

				$rval .= form_part_compile_header($part["header"] . " *");
				$rval .= form_part_compile_data("<div><input type='text' name='yrname' $value></div>\n");
				$rval = form_part_compile_wrap("_type_input", $rval, $part["id"]);
				break;

			case "_forward_email":
				$value = "";
				if ($sub)
					$value = sprintf("value='%s'", str_replace("'", "\\'", $sub["email"]));

				$rval .= form_part_compile_header($part["header"] . " *");
				$rval .= form_part_compile_data("<div><input type='text' name='yremail' readonly $value></div>\n");
				$rval = form_part_compile_wrap("_type_input", $rval, $part["id"]);
				break;

			case "_forward_rcpt":
				$path = ac_site_plink() . "/admin/images";

				$rval .= form_part_compile_header($part["header"]);
				$rval .= form_part_compile_data("<div>\n");
				$rval .= form_part_compile_data("<table border='0'>\n");
				$rval .= form_part_compile_data("<tr>\n");
				$rval .= form_part_compile_data("<td>" . _a("Email") . "</td>\n");
				$rval .= form_part_compile_data("<td>" . _a("Name") . "</td>\n");
				$rval .= form_part_compile_data("<td>&nbsp;</td>\n");
				$rval .= form_part_compile_data("</tr>\n");
				$rval .= form_part_compile_data("<tbody id='_forward_rcpt'>\n");
				$rval .= form_part_compile_data("<tr>\n");
				$rval .= form_part_compile_data("<td><input type='text' name='rcpt_email[]'></td>\n");
				$rval .= form_part_compile_data("<td><input type='text' name='rcpt_name[]'></td>\n");
				$rval .= form_part_compile_data("<td>&nbsp;</td>\n");
				$rval .= form_part_compile_data("</tr>\n");
				$rval .= form_part_compile_data("<tr>\n");
				$rval .= form_part_compile_data("<td><input type='text' name='rcpt_email[]'></td>\n");
				$rval .= form_part_compile_data("<td><input type='text' name='rcpt_name[]'></td>\n");
				$rval .= form_part_compile_data("<td>&nbsp;</td>\n");
				$rval .= form_part_compile_data("</tr>\n");
				$rval .= form_part_compile_data("<tr>\n");
				$rval .= form_part_compile_data("<td><input type='text' name='rcpt_email[]'></td>\n");
				$rval .= form_part_compile_data("<td><input type='text' name='rcpt_name[]'></td>\n");
				$rval .= form_part_compile_data("<td><div onclick='form_add_recipient()'><img class='image_addrcpt' src='$path/user_add-32.png' border='0'></div></td>\n");
				$rval .= form_part_compile_data("</tr>\n");
				$rval .= form_part_compile_data("</tbody>\n");
				$rval .= form_part_compile_data("</table>\n");
				$rval .= form_part_compile_data("</div>\n");
				$rval = form_part_compile_wrap("_type_input", $rval, $part["id"]);
				break;

			case "_forward_preview":
				if ($sub) {
					$preview =
						_a("This message is being sent by") . " " . $sub["email"] . "\n\n" .
						sprintf(_a("The sender thought the mailing entitled '%s' would be of interest to you."), $sub["c_name"]) . "\n\n" .
						_a("To view the mailing, please visit") . "\n" .
						$sub["c_link"];
				} else {
					// Print out some example text.
					$preview =
						_a("This message is being sent by") . " " . "(" . _a("your email address") . ")" . "\n\n" .
						sprintf(_a("The sender thought the mailing entitled '%s' would be of interest to you."), "(" . _a("your mailing subject") . ")") . "\n\n" .
						_a("To view the mailing, please visit") . "\n" .
						"(" . _a("your web copy link") . ")";
				}

				$rval .= form_part_compile_header($part["header"]);
				$rval .= form_part_compile_data("<div><textarea name='preview' readonly style='height:200px; width:400px'>" . $preview . "</textarea>\n");

				if ($sub) {
					$rval .= form_part_compile_data("<input type='hidden' name='yrcampaign' value='$sub[c_cid]'>\n");
					$rval .= form_part_compile_data("<input type='hidden' name='yrmessage' value='$sub[c_mid]'>\n");
				}

				$rval = form_part_compile_wrap("_type_input", $rval, $part["id"]);
				break;

			case "archive":
				$lists = ac_sql_select_list("SELECT f.listid FROM #form_list f WHERE f.formid = '$part[formid]'");
				$liststr = implode("','", $lists);

				$html = "";
				$rs = ac_sql_query("
					SELECT
						c.id,
						c.name,
						(SELECT m.id FROM #campaign_message m WHERE m.campaignid = c.id LIMIT 1) AS messageid
					FROM
						#campaign c,
						#campaign_list L
					WHERE
						L.listid IN ('$liststr')
					AND
						L.campaignid = c.id
					AND
						c.status IN (4, 5)
					AND
						public = 1
					GROUP BY
						c.id
					ORDER BY
						c.id DESC
					LIMIT
						20
				");

				while ($row = ac_sql_fetch_assoc($rs)) {
					$link = rewrite_plink("social", "c=" . md5($row["id"]) . "." . $row["messageid"]);
					$html .= "<div><a href='$link'>$row[name]</a></div>\n";
				}

				if ($html == "") {
					// No campaigns...
					$html = sprintf("<div>%s</div>\n", _a("No campaigns are available"));
				}

				$rval .= form_part_compile_data($html);
				$rval = form_part_compile_wrap("_type_input", $rval, $part["id"]);

				break;

			case "freeform":
				$content = explode("\n", $part["content"]);
				$fmt = array();

				foreach ($content as $line) {
					$fmt[] = trim($line) . "\n";
				}

				$content = implode("\n", $fmt);
				$rval .= form_part_compile_data($content);
				$rval = form_part_compile_wrap("_type_input", $rval, $part["id"]);
				break;

			case "error":
				if (isset($sub["errors"])) {
					$rval .= form_part_compile_data($sub["errors"]);
					$rval .= form_part_compile_data(sprintf("<input type='button' onclick='window.history.go(-1)' value='%s'>", str_replace("'", "\\'", _a("Back"))));
				} else {
					$rval .= form_part_compile_data(sprintf("<div>(%s)</div>", _a("This block will contain the specific error that occurred during the process.")));
				}

				$rval = form_part_compile_wrap("_type_input", $rval, $part["id"]);

				break;

			case "unsubscribe":
				$rval .= form_part_compile_header($part["header"] . " *");
				$rval .= form_part_compile_data(sprintf("<input type='radio' value='sub' name='act' checked> ") . _a("Subscribe") . "\n");
				$rval .= form_part_compile_data(sprintf("<input type='radio' value='unsub' name='act'> ") . _a("Unsubscribe") . "\n");
				$rval = form_part_compile_wrap("_type_radio", $rval, $part["id"]);
				break;

			case "subscribe":
				$btntype = ( $GLOBALS["form_compile_view"] == "preview" ? 'button' : 'submit' );

				$rval .= form_part_compile_data(sprintf("<input type='%s' value='%s'>\n", $btntype, $part["content"])) . "\n";
				$rval = form_part_compile_wrap("_type_input", $rval, $part["id"]);
				break;

			case "unsubreasonlist":
				$rval .= "      <div id='unsubreasons'>\n";
				$rval .= form_part_compile_data(sprintf("<label><input type='radio' onclick='if (this.value == \"other\") document.getElementById(\"explanation_id\").style.display = \"block\"; else document.getElementById(\"explanation_id\").style.display = \"none\"' value='donotwant' name='reason' checked> ") . _a("I no longer wish to receive these emails") . "</label>\n");
				$rval .= form_part_compile_data(sprintf("<label><input type='radio' onclick='if (this.value == \"other\") document.getElementById(\"explanation_id\").style.display = \"block\"; else document.getElementById(\"explanation_id\").style.display = \"none\"' value='diffcontent' name='reason'> ") . _a("The emails are no longer for content that I originally signed up for") . "</label>\n");
				$rval .= form_part_compile_data(sprintf("<label><input type='radio' onclick='if (this.value == \"other\") document.getElementById(\"explanation_id\").style.display = \"block\"; else document.getElementById(\"explanation_id\").style.display = \"none\"' value='noperm' name='reason'> ") . _a("I never gave my permission to receive these emails, please report this as abuse") . "</label>\n");
				$rval .= form_part_compile_data(sprintf("<label><input type='radio' onclick='if (this.value == \"other\") document.getElementById(\"explanation_id\").style.display = \"block\"; else document.getElementById(\"explanation_id\").style.display = \"none\"' value='other' name='reason'> ") . _a("Other") . "</label>\n");
				$rval .= form_part_compile_data(sprintf("<textarea name='explanation' id='explanation_id' rows='5' cols='40' style='display:none'></textarea>") . "\n");
				$rval .= "      </div>\n";
				$rval = form_part_compile_wrap("_type_input", $rval, $part["id"]);
				break;
		}
	}

	return $rval;
}

function form_part_compile_header($str, $addclasses = '') {
	return
		"<div class='_label $addclasses'>\n" .
		ac_str_strip_tags($str) . "\n" .
		"</div>\n";
}

function form_part_compile_data($str) {
	return
		"<div class='_option'>\n" .
		ac_str_strip_malicious($str) .
		"</div>\n";
}

function form_part_compile_wrap($class, $str, $partid) {
	return "<div id='compile$partid' class='_field $class'>\n" . $str . "</div>\n";
}

function form_part_compile_wrap_field($field, $str, $partid) {
	$class = "";

	switch ($field["type"]) {
		default:
			break;

		case "text":
			$class = "_type_input";
			break;

		case "textarea":
			$class = "_type_textarea";
			break;

		case "checkbox":
			$class = "_type_checkbox";
			break;

		case "radio":
			$class = "_type_radio";
			break;

		case "dropdown":
			$class = "_type_dropdown";
			break;

		case "hidden":
			$class = "_type_hidden";
			break;

		case "listbox":
			$class = "_type_listbox";
			break;

		case "date":
			$class = "_type_date";
			break;
	}

	return
		"<div id='compile$partid' class='_field $class'>\n" .
		$str .
		"</div>\n";
}

function form_part_compile_data_field($field, $fields) {
	$html = "";

	# Just in case someone tries something sneaky...
	$field["defval"] = htmlspecialchars($field["defval"], ENT_QUOTES, "UTF-8");

	$thisval = "";
	$value = "";

	if (isset($fields[$field["id"]]))
		$thisval = htmlspecialchars($fields[$field["id"]], ENT_QUOTES, "UTF-8");

	switch ($field["type"]) {
		default:
			break;

		case "text":
			if ($thisval)
				$value = ac_custom_fields_check_blank($thisval);
			else
				$value = $field["defval"];

			$html = "<input type='text' name='field[$field[id]]' value='$value'>\n";
			break;

		case "textarea":
			if ($thisval)
				$value = ac_custom_fields_check_blank($thisval);
			else
				$value = $field["defval"];

			$html = "<textarea name='field[$field[id]]' rows='$field[rows]' cols='$field[cols]'>$value</textarea>\n";
			break;

		case "checkbox":
			$options = ac_sql_select_array("SELECT * FROM #field_option WHERE fieldid = '$field[id]' ORDER BY orderid");
			$thisval = explode("||", $thisval);

			$html .= "<div><label>\n<input type='hidden' name='field[$field[id]][]' value='~|'>\n</label>\n</div>\n";

			foreach ($options as $opt) {
				if ($thisval) {
					if (in_array($opt["value"], $thisval))
						$selected = "checked";
					else
						$selected = "";
				} else {
					$selected = ($opt["isdefault"]) ? "checked" : "";
				}

				$opt["value"] = htmlspecialchars($opt["value"], ENT_QUOTES, "UTF-8");
				$opt["label"] = htmlspecialchars($opt["label"], ENT_QUOTES, "UTF-8");
				$html .= "<div><label>\n<input type='checkbox' name='field[$field[id]][]' value='$opt[value]' $selected> $opt[label]\n</label>\n</div>\n";
			}
			break;

		case "radio":
			$options = ac_sql_select_array("SELECT * FROM #field_option WHERE fieldid = '$field[id]' ORDER BY orderid");
			$thisval = explode("||", $thisval);

			foreach ($options as $opt) {
				if ($thisval) {
					if (in_array($opt["value"], $thisval))
						$selected = "checked";
					else
						$selected = "";
				} else {
					$selected = ($opt["isdefault"]) ? "checked" : "";
				}

				$opt["value"] = htmlspecialchars($opt["value"], ENT_QUOTES, "UTF-8");
				$opt["label"] = htmlspecialchars($opt["label"], ENT_QUOTES, "UTF-8");
				$html .= "<div><label>\n<input type='radio' name='field[$field[id]]' value='$opt[value]' $selected> $opt[label]\n</label>\n</div>\n";
			}
			break;

		case "dropdown":
			$options = ac_sql_select_array("SELECT * FROM #field_option WHERE fieldid = '$field[id]' ORDER BY orderid");
			$html .= "<select name='field[$field[id]]'>\n";
			$thisval = explode("||", $thisval);

			foreach ($options as $opt) {
				if ($thisval) {
					if (in_array($opt["value"], $thisval))
						$selected = "selected";
					else
						$selected = "";
				} else {
					$selected = ($opt["isdefault"]) ? "selected" : "";
				}

				$opt["value"] = htmlspecialchars($opt["value"], ENT_QUOTES, "UTF-8");
				$opt["label"] = htmlspecialchars($opt["label"], ENT_QUOTES, "UTF-8");
				$html .= "<option value='$opt[value]' $selected>$opt[label]</option>\n";
			}

			$html .= "      </select>\n";
			break;

		case "hidden":
			if ($thisval)
				$value = ac_custom_fields_check_blank($thisval);
			else
				$value = $field["defval"];

			$html = "<input type='hidden' name='field[$field[id]]' value='$value'>\n";

			if ($GLOBALS['form_compile_view'] == "preview")
				$html .= "<em>" . _a("This is a hidden field.") . "</em>\n";

			break;

		case "listbox":
			$options = ac_sql_select_array("SELECT * FROM #field_option WHERE fieldid = '$field[id]' ORDER BY orderid");
			$html .= "<select name='field[$field[id]][]' multiple>\n";
			$thisval = explode("||", $thisval);

			foreach ($options as $opt) {
				if ($thisval) {
					if (in_array($opt["value"], $thisval))
						$selected = "checked";
					else
						$selected = "";
				} else {
					$selected = ($opt["isdefault"]) ? "checked" : "";
				}

				$opt["value"] = htmlspecialchars($opt["value"], ENT_QUOTES, "UTF-8");
				$opt["label"] = htmlspecialchars($opt["label"], ENT_QUOTES, "UTF-8");
				$html .= "<option value='$opt[value]' $selected>$opt[label]</option>\n";
			}

			$html .= "</select>\n";
			break;

		case "date":
			if ($thisval)
				$value = ac_custom_fields_check_blank($thisval);
			else
				$value = $field["defval"];

			$html = "<input type='text' id='form_field$field[id]' name='field[$field[id]]' value='$value'>\n";
			$html .= sprintf("<input id='form_calendar$field[id]' type='button' value='%s' />\n", _a(" + "));
			$html .= "<script type='text/javascript'>Calendar.setup({ inputField: 'form_field$field[id]', ifFormat: '%Y-%m-%d', button: 'form_calendar$field[id]', showsTime: false, timeFormat: '24' });</script>";
			break;
	}

	return
		"    <div class='_option'>\n" .
		$html .
		"    </div>\n";
}

function form_part_saveorder() {
	$partids = ac_http_param_forcearray("partids");
	$formid = (int)ac_http_param("formid");

	$i = 0;
	foreach ($partids as $partid) {
		$partid = (int)$partid;
		$up = array(
			"ordernum" => $i++,
		);

		ac_sql_update("#form_part", $up, "id = '$partid'");
	}

	ac_sql_query("UPDATE #form SET waitpreview = '1' WHERE id = '$formid'");
}

function form_part_push($num, $str) {
	$ary = explode("\n", $str);
	$rval = array();

	foreach ($ary as $line) {
		if ($line == "")
			continue;
		$rval[] = str_repeat(" ", $num) . $line;
	}

	return implode("\n", $rval);
}

function form_part_update() {
	$id = (int)ac_http_param("id");

	$up = array(
		"content" => ac_str_strip_malicious(ac_http_param("content")),
		"header" => ac_http_param("header"),
		"headerlast" => ac_http_param("headerlast"),
	);

	ac_sql_update("#form_part", $up, "id = '$id'");

	$formid = (int)ac_sql_select_one("SELECT formid FROM #form_part WHERE id = '$id'");
	ac_sql_query("UPDATE #form SET waitpreview = '1' WHERE id = '$formid'");

	return ac_ajax_api_result(true, _a("Form part updated"), array("id" => $id));
}

function form_part_delete() {
	$id = (int)ac_http_param("id");
	$row = ac_sql_select_row("SELECT fieldid, builtin FROM #form_part WHERE id = '$id'");
	ac_sql_delete("#form_part", "id = '$id'");

	$formid = (int)ac_sql_select_one("SELECT formid FROM #form_part WHERE id = '$id'");
	ac_sql_query("UPDATE #form SET waitpreview = '1' WHERE id = '$formid'");

	return ac_ajax_api_result(true, _a("Form part deleted"), array("fieldid" => $row["fieldid"], "builtin" => $row["builtin"]));
}

?>
