<?php

// generic cURL function (could eventually be moved, and re-named, to a more global spot)
function facebook_http_curl($url, $method = "get", $options = array("opt_returntransfer" => true), $just_check = false, $format = "xml") {
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, $options["opt_returntransfer"]);

	// "opt_userpwd" SHOULD BE A USERNAME/PASSWORD STRING THAT cURL UNDERSTANDS: "username:password"
	if (isset($options["opt_userpwd"]) && $options["opt_userpwd"]) curl_setopt($curl, CURLOPT_USERPWD, $options["opt_userpwd"]);

	if ($method == "post") {
		curl_setopt($curl, CURLOPT_POST, 1);

		// $post_fields SHOULD BE AN ARRAY IN THIS FORMAT: array(0 => "Hello=World", 1 => "Foo=Bar", 2 => "Baz=Wombat")
		// WHEN implode IS APPLIED, IT TURNS INTO WHAT cURL UNDERSTANDS: "Hello=World&Foo=Bar&Baz=Wombat"
		curl_setopt( $curl, CURLOPT_POSTFIELDS, implode("&", $options["post_fields"]) );
//campaign_sender_log(print_r($options["post_fields"],1));
	}

	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);

	if (isset($options["opt_httpheader"]) && $options["opt_httpheader"]) curl_setopt($curl, CURLOPT_HTTPHEADER, $options["opt_httpheader"]);

	$output = curl_exec($curl);
//dbg($output);
//campaign_sender_log(print_r($output,1));
//campaign_sender_log($http_code);

//dbg( curl_error($curl) );

	$http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);

	if ($just_check) return $http_code;

	//if ($http_code != 200) return false;

	if ($format == "xml") {
		$xml_object = simplexml_load_string($output);
		return $xml_object;
	}

	if ($format == "text") {
		return $output;
	}

	if ($format == "serialize") {
		return unserialize($output);
	}

	return "Specify a format.";
}

function facebook_oauth_get_loginurl($redirect_url, $scope = "publish_stream,manage_pages") {
	$backend = ac_sql_select_row("SELECT facebook_app_id, facebook_app_secret FROM #backend");
	$url = "https://www.facebook.com/dialog/oauth?client_id=" . $backend["facebook_app_id"] . "&redirect_uri=" . urlencode($redirect_url) . "&scope=" . $scope;
	return $url;
}

function facebook_oauth_get_logouturl($redirect_url, $access_token) {
	$url = "https://www.facebook.com/logout.php?next=" . urlencode($redirect_url) . "&access_token=" . $access_token;
	return $url;
}

function facebook_oauth_get_useraccesstoken($redirect_url, $authorization_code) {
	$backend = ac_sql_select_row("SELECT facebook_app_id, facebook_app_secret FROM #backend");
	$url = "https://graph.facebook.com/oauth/access_token?client_id=" . $backend["facebook_app_id"] . "&redirect_uri=" . urlencode($redirect_url) . "&client_secret=" . $backend["facebook_app_secret"] . "&code=" . $authorization_code;
//dbg($url);
	$access_token = facebook_http_curl($url, "get", array("opt_returntransfer" => true), false, "text");
//dbg($access_token);
	// example string returned: access_token=AAABkc8yMB50BAB3kfo...&expires=5166591
	$response_array = explode("&", $access_token);
	$access_token = substr($response_array[0], 13);
	//$expires = substr($response_array[1], 8);
	return $access_token;
}

function facebook_oauth_get_appaccesstoken($app_id, $app_secret) {
	$url = "https://graph.facebook.com/oauth/access_token?client_id=" . $app_id . "&client_secret=" . $app_secret . "&grant_type=client_credentials";
//dbg($url);
	$access_token = facebook_http_curl($url, "get", array("opt_returntransfer" => true), false, "text");
	$access_token_array = explode("=", $access_token);
	$access_token = $access_token_array[1];
	return $access_token;
}

function facebook_graph($access_token, $path, $method = "get", $post_fields = array()) {
	if ( preg_match("/\?/", $path) ) {
		$uri = $path;
	}
	else {
		$uri = $path . "?access_token=" . $access_token;
	}
	$url = "https://graph.facebook.com/" . $uri;
//campaign_sender_log($url);
//campaign_sender_log(print_r($post_fields,1));
	$result = facebook_http_curl($url, $method, array("opt_returntransfer" => true, "post_fields" => $post_fields), false, "text");
//dbg($result);
//campaign_sender_log(print_r($result,1));
	//$result = json_decode($result);
	return $result;
}

?>