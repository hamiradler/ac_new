<?php

function class_table($class) {
	$rval = array();

	for ($i = 0, $append = ""; $i < strlen($class); $i++) {
		if (strtoupper($class[$i]) == $class[$i]) {
			if ($append) {
				$rval[] = $append;
				$append = "";
			}

			$append .= strtolower($class[$i]);
		} else {
			$append .= $class[$i];
		}
	}

	if ($append)
		$rval[] = $append;

	return '#' . implode("_", $rval);
}

?>
