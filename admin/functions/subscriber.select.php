<?php

require_once ac_admin("functions/filter.php");
require_once ac_global_functions("log.php");

function subscriber_select_query(&$so, $count = false, $unsub = false) {
	if ( !ac_admin_ismaingroup() ) {
		$admin = ac_admin_get();
		if ( $admin['id'] > 1 ) {
			$liststr = implode("','", $admin["lists"]);

			if ($so->counting)
				$cond = "AND (SELECT COUNT(*) FROM #subscriber_list subq WHERE subq.subscriberid = s.id AND subq.listid IN ('$liststr')) > 0";
			else
				$cond = "AND l.listid IN ('$liststr')";
			if ( !in_array($cond, $so->conds) ) $so->push($cond);
			/*
			if (!$so->counting)
				$so->push("AND l.listid IN ('" . implode("', '", $admin['lists']) . "')");
			else
				$so->push("AND s.id IN (SELECT subl.subscriberid FROM #subscriber_list subl WHERE subl.listid IN ('$liststr'))");
			*/
		}
	}

	// If no real conditions, just return the simplest count query...
	if ($so->counting && ac_admin_ismaingroup()) {
		if ($so->conds == array(1))
			return "SELECT COUNT(*) FROM #subscriber";
	}

	$listtab = ", #subscriber s";
	if ( !in_array("AND s.id = l.subscriberid", $so->conds) ) $so->push("AND s.id = l.subscriberid");

	return $so->query("
		SELECT
			*,
			s.id AS id,
			l.id AS lid,
			INET_NTOA(s.ip) AS ip,
			INET_NTOA(l.ip4_sub) AS ip4,
			TRIM(CONCAT(first_name, ' ', last_name)) AS name,
			DATE(l.udate) AS a_unsub_date,
			TIME(l.udate) AS a_unsub_time
		FROM
			#subscriber_list l
			$listtab
			[___]
		WHERE
		[...]
	");
}

function subscriber_select_query_alt(&$so, $count = false, $unsub = false, $alwayshaslist = false) {
	$liststr = "";
	if ( !ac_admin_ismain() ) {
		$admin = ac_admin_get();
		if ( !ac_admin_ismaingroup() ) {
			$liststr = implode("','", $admin["lists"]);
			$cond = "AND (SELECT COUNT(*) FROM #subscriber_list subx WHERE subx.listid IN ('$liststr') AND subx.subscriberid = s.id) > 0";
			//$cond = "AND l.listid IN ('" . implode("', '", $admin['lists']) . "')";
			if ( !in_array($cond, $so->conds) ) $so->push($cond);
			/*
			if (!$so->counting)
				$so->push("AND l.listid IN ('" . implode("', '", $admin['lists']) . "')");
			else
				$so->push("AND s.id IN (SELECT subl.subscriberid FROM #subscriber_list subl WHERE subl.listid IN ('$liststr'))");
			*/
		}
	}

	$subcond = ( $liststr ? array("listid IN ('$liststr')") : array("1") );
	$conds   = implode(", ", $so->conds);
	$matches = array();

	# These should be separate from $haslist, $hasstat, etc., since we only
	# care about about lx being inside a subquery.
	if (preg_match('/lx\.listid = \'(\d+)\'/', $conds, $matches))
		$subcond[] = "listid = '$matches[1]'";

	if (preg_match('/lx\.status = \'(\d+)\'/', $conds, $matches))
		$subcond[] = "status = '$matches[1]'";

	$haslist = strpos($conds, "l.listid") !== false;
	$hasstat = strpos($conds, "l.status") !== false;
	$hasname = strpos($conds, "l.first_name") !== false || strpos($conds, "l.last_name") !== false;

	$subcond = "AND " . implode(" AND ", $subcond);

	if ($haslist || $alwayshaslist) {
		$so->join("#subscriber_list l", array("AND l.subscriberid = s.id $subcond"));
	} elseif ($hasstat) {
		if ($liststr != "")
			$so->conds = explode(", ", preg_replace('/l\.status = \'?(\d+)\'?/', '\1 IN (SELECT subx.status FROM #subscriber_list subx WHERE subx.subscriberid = s.id AND subx.listid IN (\'' . $liststr . '\'))', $conds));
		else
			$so->conds = explode(", ", preg_replace('/l\.status = \'?(\d+)\'?/', '\1 IN (SELECT subx.status FROM #subscriber_list subx WHERE subx.subscriberid = s.id)', $conds));
	} elseif ($hasname) {
		$so->join("#subscriber_list l", array("AND l.subscriberid = s.id $subcond"));
	}

	//$subcond = "AND " . implode(" AND ", $subcond);

	// this one might need a conversion into subscriber_list for first|last_name fields
	return $so->query("
		SELECT
			*,
			s.id AS id,
			INET_NTOA(s.ip) AS ip,
			( SELECT INET_NTOA(ip4_sub) FROM #subscriber_list WHERE subscriberid = s.id $subcond LIMIT 1) AS ip4,
			( SELECT first_name FROM #subscriber_list WHERE subscriberid = s.id $subcond LIMIT 1 ) AS first_name,
			( SELECT last_name FROM #subscriber_list WHERE subscriberid = s.id $subcond LIMIT 1 ) AS last_name,
			( SELECT formid FROM #subscriber_list WHERE subscriberid = s.id $subcond LIMIT 1 ) AS formid,
			( SELECT TRIM(CONCAT(first_name, ' ', last_name)) FROM #subscriber_list WHERE subscriberid = s.id $subcond LIMIT 1 ) AS name
		FROM
			#subscriber s
			[___]
		WHERE
			[...]
	");
}

function subscriber_select_row($id) {
	$so = new AC_Select;
	if ( ac_str_is_email((string)$id) ) {
		$email = ac_sql_escape($id);
		$so->push("AND s.email = '$email'");
	} elseif ( $id = (int)$id ) {
		$so->push("AND s.id = '$id'");
	} else {
		return array();
	}

	$r = ac_sql_select_row(subscriber_select_query($so), array('cdate', 'sdate', 'udate'));

	if ( $r ) {
		$id = $r['id'];
		$r['lists'] = subscriber_get_lists($id, null);
		$r['listslist'] = implode('-', array_keys($r['lists']));
		$r['fields'] = subscriber_get_fields($id, array_keys($r['lists']));
		if (defined('AC_API_REMOTE') && AC_API_REMOTE) $r['actions'] = subscriber_get_actions($id); // recent activity/actions
	}

	return $r;
}

function subscriber_view($id) {
	$r = subscriber_select_row($id);
	if ( !$r ) return $r;
	$id = (int)$r['id'];
	$email = ac_sql_escape($r['email']);
	// collect bounce data
	$r['bounces'] = array(
		'mailing' => array(),
		'mailings' => 0,
		'responder' => array(),
		'responders' => 0
	);
	// for messages
	$query = "
		SELECT
			b.*
		FROM
			#bounce_data b
		WHERE
			b.email = '$email'
	";
	$r['bounces']['mailing'] = ac_sql_select_array($query, array('tstamp'));
	$r['bounces']['mailings'] = count($r['bounces']['mailing']);
	// for responders
	$query = "
		SELECT
			b.*,
			l.name AS listname,
			m.name AS campaignname,
			c.descript AS description
		FROM
			#bounce_data b,
			#bounce_code c,
			#campaign m,
			#campaign_list s,
			#list l
		WHERE
		(
			b.subscriberid = '$id'
		OR
			b.email = '$email'
		)
		AND
			m.type = 'responder'
		AND
			b.campaignid = m.id
		AND
			s.campaignid = m.id
		AND
			s.listid = l.id
		AND
			c.code = b.code
		GROUP BY
			l.id
	";
	$r['bounces']['responder'] = ac_sql_select_array($query, array('tstamp'));
	$r['bounces']['responders'] = count($r['bounces']['responder']);
	$r['bouncescnt'] = $r['bounces']['mailings'] + $r['bounces']['responders'];
	// message/responder history might be bigger than 'a few'
	return $r;
}

function subscriber_select_array($so = null, $ids = null) {
	if ($so === null || !is_object($so))
		$so = new AC_Select;

	if ($ids !== null) {
		if ( !is_array($ids) ) $ids = explode(',', $ids);
		$tmp = array_diff(array_map("intval", $ids), array(0));
		$ids = implode("','", $tmp);
		$so->push("AND s.id IN ('$ids')");
	}
	return ac_sql_select_array(subscriber_select_query($so), array('cdate', 'sdate', 'udate'));
}

function subscriber_select_array_alt($so = null, $ids = null) {
	if ($so === null || !is_object($so))
		$so = new AC_Select;

	if ($ids !== null) {
		if ( !is_array($ids) ) $ids = explode(',', $ids);
		$tmp = array_diff(array_map("intval", $ids), array(0));
		$ids = implode("','", $tmp);
		$so->push("AND s.id IN ('$ids')");
	}
	return ac_sql_select_array(subscriber_select_query_alt($so), array('cdate', 'sdate', 'udate'));
}

function subscriber_select_array_paginator($id = 1, $sort = '', $offset = 0, $limit = 20, $filter = 0, $unsub = false, $fieldsneedtitles = false, $bounced = false) {
	$so_count  = new AC_Select;
	$so_select = new AC_Select;

	if (!$offset) $offset = 0; // if NULL (not passed via API)

	$filter = intval($filter);
	$date = 's.cdate';
	if ($filter > 0) {
		$admin = ac_admin_get();
		$conds = ac_sql_select_one("SELECT conds FROM #section_filter WHERE id = '$filter' AND userid = '$admin[id]' AND sectionid = 'subscriber'");
		$so_count->push($conds);
		$so_select->push($conds);
		if ( ac_str_instr('l.listid', $conds) ) {
			$date = 'l.sdate';
		}

		$so_select->groupby("s.id");
	} elseif ( $unsub ) {
		$so_count->push("AND l.status = '2'");
		$so_select->push("AND l.status = '2'");
	} elseif ( !$unsub and $bounced ) {
		$so_count->push("AND l.status = '3'");
		$so_select->push("AND l.status = '3'");
	} elseif ( !$unsub and !$bounced ) {
		$so_count->push("AND l.status = 1"); // subscribed = DEFAULT
		$so_select->push("AND l.status = 1"); // subscribed = DEFAULT
	}

	$so_count->dcount("s.id");
	//dbg(subscriber_select_query_alt($so_count),1);
	$total = (int)ac_sql_select_one(subscriber_select_query_alt($so_count));

	$orderby = subscriber_select_sorter($sort, $so_select, $date);
	$so_select->orderby($orderby);

	if ( (int)$limit == 0 ) $limit = 999999999;
	$so_select->limit("$offset, $limit");

	//dbg(ac_prefix_replace(subscriber_select_query_alt($so_select)));
	$rows = subscriber_select_array_alt($so_select);

	$nl = null;
	if ( isset($_SESSION['nlp']) and defined('ACPUBLIC') ) {
		$nl = $_SESSION['nlp'];
	} elseif ( isset($_SESSION['nla']) ) {
		$nl = $_SESSION['nla'];
	}
	if ( $nl ) {
		foreach ( $rows as $k => $v ) {
			$sel = "";

			if ($fieldsneedtitles)
				$sel = ", f.title AS a_title";

			$rs  = ac_sql_query("
				SELECT
					v.fieldid,
					v.val
					$sel
				FROM
					#field_value v,
					#field f
				WHERE
					v.relid = '$v[id]'
				AND
					f.id = v.fieldid
				AND
					f.show_in_list = '1'

			");

			while ($fieldrow = ac_sql_fetch_assoc($rs)) {
				if ($fieldsneedtitles)
					$rows[$k]["field" . $fieldrow["fieldid"]] = array($fieldrow["a_title"], ac_cfield_check_blank($fieldrow["val"]));
				else
					$rows[$k]["field" . $fieldrow["fieldid"]] = ac_cfield_check_blank($fieldrow["val"]);
			}
		}
	}

	return array(
		"paginator"   => $id,
		"offset"      => $offset,
		"limit"       => $limit,
		"total"       => $total,
		"cnt"         => count($rows),
		"rows"        => $rows,
	);
}

function subscriber_export($fields, $sort, $offset, $limit, $filter, $segmentid = 0) {
	$so = new AC_Select();
	$so->remove = false;
	$so->slist = array();

	$segmentid = (int)$segmentid;

	$r = array(
		'fields' => array(),
		'customfields' => array(),
		'rs' => false,
	);


	$nl = null;
	if ( isset($_SESSION['nlp']) and defined('ACPUBLIC') ) {
		$nl = $_SESSION['nlp'];
	} elseif ( isset($_SESSION['nla']) ) {
		$nl = $_SESSION['nla'];
	}

	$liststr = "";
	if ($nl) {
		// filter by just list
		if ( is_array($nl) ) {
			if ( count($nl) > 0 ) {
				$liststr = implode("','", array_map('intval', $nl));
			} else {
				if ( defined('ACPUBLIC') ) {
					unset($_SESSION['nlp']);
				} else {
					unset($_SESSION['nla']);
				}
			}
		} else {
			$listid = (int)$nl;
			if ( $listid > 0 ) {
				$liststr = $listid;
			} else {
				if ( defined('ACPUBLIC') ) {
					unset($_SESSION['nlp']);
				} else {
					unset($_SESSION['nla']);
				}
			}
		}
	}

	if ($segmentid > 0) {
		# Here, we just want to see what lists are used by the segment.  If $liststr is not
		# empty, we'll assign it based on those lists.

		$usedinlists = ac_sql_select_list("SELECT listid FROM #filter_list WHERE filterid = '$segmentid'");
		$inlistconds = ac_sql_select_list("SELECT rhs FROM #filter_group_cond WHERE lhs = 'inlist' AND filterid = '$segmentid'");

		$overall = array_unique(array_merge($usedinlists, $inlistconds));
		if (count($overall) > 0)
			$liststr = implode("','", array_map("intval", $overall));
	}

	$subcond1 = ( $liststr ? "AND lx.listid IN ('$liststr')" : "" );
	//$subcond2 = ( $liststr ? "AND ll.id IN ('$liststr')" : "" );
	$subcond2 = ( $liststr ? "WHERE ll.id IN ('$liststr')" : "WHERE ll.id = l.listid" );

	$r['customfields'] = array();
	$so->slist[] = "s.id AS id";
	foreach ( $fields as $k => $v ) {
		if ( !preg_match('/^\d+$/', $v) ) {
			// if a standard field
			$r['fields'][$k] = $v;
			if ($v == "id")
				continue;
			if ( $v == 'ip' ) {
				$so->slist[] = "(SELECT INET_NTOA(lx.ip4_sub) FROM em_subscriber_list lx WHERE lx.subscriberid = s.id $subcond1 LIMIT 1) AS ip4";
			} elseif ($v == 'listname') {
				//$so->slist[] = "(SELECT ll.name FROM #list ll WHERE ll.id = l.listid $subcond2) AS listname";
				$so->slist[] = "(SELECT ll.name FROM #list ll $subcond2 LIMIT 1) AS listname";
			} elseif ($v == 'first_name') {
				//$so->slist[] = "l.first_name";
				$so->slist[] = "( SELECT first_name FROM em_subscriber_list lx WHERE lx.subscriberid = s.id $subcond1 LIMIT 1 ) AS first_name";
			} elseif ($v == 'last_name') {
				$so->slist[] = "( SELECT last_name FROM em_subscriber_list lx WHERE lx.subscriberid = s.id $subcond1 LIMIT 1 ) AS last_name";
				//$so->slist[] = "l.last_name";
			} else {
				$so->slist[] = "$v";
			}
		} else {
			// custom fields
			$r['customfields'][$v] = $k;
			$so->slist[] = "'' AS field$v";
			// subqueries would be too much here
			//$so->slist[] = "( SELECT val FROM #field_value WHERE relid = l.subscriberid AND fieldid = '$v' LIMIT 1 ) AS field$v";
		}
	}
	if ( !count($so->slist) ) {
		$so->slist[] = 's.email';
	}

	// if there is some custom fields
	if ( count($r['customfields']) ) {
		// fetch them
		$ids = implode("','", array_keys($r['customfields']));
		$rs = ac_sql_select_box_array("SELECT id, title FROM #field WHERE id IN ('$ids')");
		// and assign their names
		foreach ( $rs as $k => $v ) {
			$r['fields'][$r['customfields'][$k]] = $v;
		}
	}

	if ( count($r['fields']) == 0 ) return false;

	ksort($r['fields']);

	// now fetch the data

	// add filter conditions
	$filter = intval($filter);
	$date = 's.cdate';
	if ($filter > 0) {
		$admin = ac_admin_get();
		$conds = ac_sql_select_one("SELECT conds FROM #section_filter WHERE id = '$filter' AND userid = '$admin[id]' AND sectionid = 'subscriber'");
		//dbg($conds);
		$so->push($conds);
		if ( ac_str_instr('l.listid', $conds) ) {
			$date = 'l.sdate';
		}
	} else {
		$so->push("AND l.status = 1"); // subscribed = DEFAULT
	}

	if($liststr) {
		if(ac_str_instr(',', $liststr))
			$so->push("AND l.listid IN('$liststr')");
		else
			$so->push("AND l.listid = '$liststr'");
	}

	// sort
	$orderby = subscriber_select_sorter($sort, $so, $date);
	$so->orderby($orderby);

	// limit
	if ( (int)$limit == 0 ) $limit = 999999999;
	$limit  = (int)$limit;
	$offset = (int)$offset;
	$so->limit("$offset, $limit");
	$so->groupby("s.id");

	$query = subscriber_select_query_alt($so, false, false, true);
	$r['rs'] = ac_sql_query($query);

	if ( !$r['rs'] or !ac_sql_num_rows($r['rs']) ) return false;

	ac_ihook_define('ac_export_row', 'subscriber_export_row');

	return $r;
}

function subscriber_export_row($row, $export) {
	# Convert all values to the proper character set.
	$tocharset = _i18n("utf-8");

	if (isset($row["ip4"])) {
		if ($row["ip4"] == "" || $row["ip4"] == "0.0.0.0")
			$row["ip4"] = (string)ac_sql_select_one("SELECT INET_NTOA(ip) AS `ip` FROM #subscriber WHERE id = '$row[id]'");
	}

	foreach ($row as $k => $v)
		$row[$k] = ac_utf_conv("UTF-8", $tocharset, $v);

	if ( !count($export['customfields']) ) {

		if (isset($row["id"]) && !in_array("id", $export["fields"]))
			unset($row["id"]);

		return $row;
	}

	$ids = implode("','", array_keys($export['customfields']));
	$query = "
		SELECT
		fieldid,
		val
		FROM
		#field_value
		WHERE
		relid = '$row[id]'
		AND
		fieldid IN ('$ids')
		";
	$rs  = ac_sql_query($query);

	while ( $field = ac_sql_fetch_assoc($rs) ) {
		if ( isset($row['field' . $field['fieldid']]) ) {
			$row['field' . $field['fieldid']] = ac_utf_conv("UTF-8", $tocharset, ac_cfield_check_blank($field['val']));
		}
	}

	if (isset($row["id"]) && !in_array("id", $export["fields"]))
		unset($row["id"]);

	return $row;
}

function subscriber_select_filter($sort, $datefield = 'cdate') {
	switch ($sort) {
		default:
		case "01":
			return "s.email";
		case "01D":
			return "s.email DESC";
		case "02":
			return "l.first_name, l.last_name";
		case "02D":
			return "l.first_name DESC, l.last_name DESC";
		case "03":
			return $datefield;
		case "03D":
			return "$datefield DESC";
		case "99":
			return "l.udate";
		case "99D":
			return "l.udate DESC";
	}
}

function subscriber_select_sorter($sort, &$so, $datefield = 'cdate') {
	switch ($sort) {
		default:
		case "01":
			$so->modify("#subscriber s", "#subscriber s FORCE INDEX (email)");
			return "s.email";
		case "01D":
			$so->modify("#subscriber s", "#subscriber s FORCE INDEX (email)");
			return "s.email DESC";
		case "02":
			//$so->modify("#subscriber s", "#subscriber s");
			return "first_name, last_name";
		case "02D":
			$so->modify("#subscriber s", "#subscriber s");
			return "first_name DESC, last_name DESC";
		case "03":
			if ($datefield == "cdate") {
				$so->modify("#subscriber s", "#subscriber s FORCE INDEX (cdate)");
			} else {
				$so->modify("#subscriber_list l", "#subscriber_list l FORCE INDEX (sdate)");
			}
			return $datefield;
		case "03D":
			if ($datefield == "cdate") {
				$so->modify("#subscriber s", "#subscriber s FORCE INDEX (cdate)");
			} else {
				$so->modify("#subscriber_list l", "#subscriber_list l FORCE INDEX (sdate)");
			}
			return "$datefield DESC";
		case "99":
			$so->modify("#subscriber_list l", "#subscriber_list l FORCE INDEX (udate)");
			return "l.udate";
		case "99D":
			$so->modify("#subscriber_list l", "#subscriber_list l FORCE INDEX (udate)");
			return "l.udate DESC";
	}
}

function subscriber_filter_segment($segmentid) {
	$ary = array(
		"userid"    => $GLOBALS['admin']['id'],
		"sectionid" => "subscriber",
		"=tstamp"   => "NOW()",
	);

	$ary["conds"] = "AND " . filter_compile($segmentid);

	$filter  = filter_select_row($segmentid);
	$liststr = $filter["lists"];
	$cond = " AND (SELECT COUNT(*) FROM #subscriber_list subx WHERE subx.listid IN ($liststr) AND subx.status = 1 AND subx.subscriberid = s.id) > 0";
	if (!ac_str_instr(".status", $ary["conds"])) {
		$cond = " AND (SELECT COUNT(*) FROM #subscriber_list subx WHERE subx.listid IN ($liststr) AND subx.status = 1 AND subx.subscriberid = s.id) > 0";
		$ary["conds"] .= $cond;
	}

	$conds_esc = ac_sql_escape($ary['conds']);
	$filterid = ac_sql_select_one("
		SELECT
			id
		FROM
			#section_filter
		WHERE
			userid = '$ary[userid]'
		AND
			sectionid = 'subscriber'
		AND
			conds = '$conds_esc'
	");

	if (intval($filterid) > 0)
		return array("filterid" => $filterid);
	ac_sql_insert("#section_filter", $ary);
	return array("filterid" => ac_sql_insert_id());
}

function subscriber_filter_post() {
	$whitelist = array("s.email", "l.first_name", "l.last_name");

	$ary = array(
		"userid" => $GLOBALS['admin']['id'],
		"sectionid" => "subscriber",
		"conds" => "",
		"=tstamp" => "NOW()",
	);

	if (isset($_POST["qsearch"]) && !isset($_POST["content"])) {
		$_POST["content"] = $_POST["qsearch"];
	}

	if (isset($_POST["content"]) and $_POST['content'] != '') {
		$content = ac_sql_escape($_POST["content"], true);

		// if there is at least one space present, and not at beginning or end of string
		$space = preg_match("/[A-Za-z0-9]+\s[A-Za-z0-9]+/", $content);
		// allow search for full name - only do this if a space is present in the search query
		if ($space) $whitelist[] = "CONCAT(l.first_name,' ',l.last_name)";

		$conds = array();

		if (!isset($_POST["section"]) || !is_array($_POST["section"]))
			$_POST["section"] = $whitelist;

		foreach ($_POST["section"] as $sect) {
			if (!in_array($sect, $whitelist)) {
				continue;
			}
			$conds[] = "$sect LIKE '%$content%'";
		}

		if (isset($_POST["custom"])) {
			$conds[] = "(
				SELECT
					COUNT(*)
				FROM
					#field_value fv
				WHERE
					fv.relid = s.id
				AND
					val LIKE '%$content%'
			) > 0";
		}

		$conds = implode(" OR ", $conds);
		$ary["conds"] = "AND ($conds) ";
	}

	if (isset($_POST["listid"])) {
		if (is_array($_POST["listid"]))
			$_SESSION["nla"] = $_POST["listid"];
		elseif ((int)$_POST["listid"] > 0)
			$_SESSION["nla"] = (int)$_POST["listid"];
		else
			unset($_SESSION["nla"]);
	}

	$nl = null;
	if ( isset($_SESSION['nlp']) and defined('ACPUBLIC') ) {
		$nl = $_SESSION['nlp'];
	} elseif ( isset($_SESSION['nla']) ) {
		$nl = $_SESSION['nla'];
	//} elseif ( isset($_GET['listid']) and (int)$_GET['listid'] ) {
		//$nl = array($_GET['listid']);
	}

// New code for filter status
	if (isset($_POST["status"])) {
		$filter_status = $_SESSION["12all_subscriber_status"] = $_POST["status"];
	}
	else {
		if (isset($_SESSION["12all_subscriber_status"])) {
			$filter_status = $_SESSION["12all_subscriber_status"];
		}
	}

	if (isset($filter_status) && $nl && ($filter_status != '')) {

		//filter by both list and status

		$x = '';


		if ( $nl ) {
			if ( is_array($nl) ) {
				if ( count($nl) > 0 ) {
					$ids = implode("', '", array_map('intval', $nl));
					# This needs to use "lx" (or something similar) to avoid the pattern checks for "l.listid".
					$x = "AND lx.listid IN ('$ids')";
				} else {
					if ( defined('ACPUBLIC') ) {
						unset($_SESSION['nlp']);
					} else {
						unset($_SESSION['nla']);
					}
				}
			} else {
				$listid = (int)$nl;
				if ( $listid > 0 ) {
					$x = "AND lx.listid = '$listid'";
					#$ary['conds'] .= "AND l.listid = '$listid' ";
				} else {
					if ( defined('ACPUBLIC') ) {
						unset($_SESSION['nlp']);
					} else {
						unset($_SESSION['nla']);
					}
				}
			}
		}



		$y = '';


		if ( is_array($filter_status) ) {
			if ( count($filter_status) > 0 ) {
				$pos = array_search("", $filter_status);

				if ($pos !== false) {
					array_splice($filter_status, $pos, 1);
				}

				$ids = implode("', '", array_map('intval', $filter_status));

				if ($ids != "")
					$y = "AND lx.status IN ('$ids')";
			}
		} else {
			if ( $filter_status != '' ) {
				$filter_status = (int)$filter_status;
				$y = "AND lx.status = '$filter_status'";
				#$ary['conds'] .= "AND l.status = '$filter_status' ";
			} else {
				$ary['conds'] .= "AND 1 = 1 "; // dummy condition to save no-condition-for-status as filter
			}
		}


		$ary['conds'] .= "AND (SELECT COUNT(*) FROM #subscriber_list lx WHERE lx.subscriberid = s.id $x $y) > 0 ";


	}

	else{

		if ( $nl ) {
			if ( is_array($nl) ) {
				if ( count($nl) > 0 ) {
					$ids = implode("', '", array_map('intval', $nl));
					# This needs to use "lx" (or something similar) to avoid the pattern checks for "l.listid".
					$ary['conds'] .= "AND (SELECT COUNT(*) FROM #subscriber_list lx WHERE lx.subscriberid = s.id AND lx.listid IN ('$ids')) > 0 ";
				} else {
					if ( defined('ACPUBLIC') ) {
						unset($_SESSION['nlp']);
					} else {
						unset($_SESSION['nla']);
					}
				}
			} else {
				$listid = (int)$nl;
				if ( $listid > 0 ) {
					$ary['conds'] .= "AND (SELECT COUNT(*) FROM #subscriber_list lx WHERE lx.subscriberid = s.id AND lx.listid = '$listid') > 0 ";
					#$ary['conds'] .= "AND l.listid = '$listid' ";
				} else {
					if ( defined('ACPUBLIC') ) {
						unset($_SESSION['nlp']);
					} else {
						unset($_SESSION['nla']);
					}
				}
			}
		}


		if (isset($filter_status)) {

			//filter by just status

			if ( is_array($filter_status) ) {
				if ( count($filter_status) > 0 ) {
					$pos = array_search("", $filter_status);

					if ($pos !== false) {
						array_splice($filter_status, $pos, 1);
					}

					$ids = implode("', '", array_map('intval', $filter_status));

					if ($ids != "")
						$ary['conds'] .= "AND (SELECT COUNT(*) FROM #subscriber_list lx WHERE lx.status IN ('$ids') AND lx.subscriberid = s.id) > 0 ";
				}
			} else {
				if ( $filter_status != '' ) {
					$filter_status = (int)$filter_status;
					$ary['conds'] .= "AND (SELECT COUNT(*) FROM #subscriber_list lx WHERE lx.status = '$filter_status' AND lx.subscriberid = s.id) > 0 ";
					#$ary['conds'] .= "AND l.status = '$filter_status' ";
				} else {
					$ary['conds'] .= "AND 1 = 1 "; // dummy condition to save no-condition-for-status as filter
				}
			}
		}
	}

	/* Old code for filter status
	if (isset($_POST["status"]))
		$_SESSION["12all_subscriber_status"] = $_POST["status"];

	if (isset($_POST["status"])) {
		if ( is_array($_POST['status']) ) {
			if ( count($_POST['status']) > 0 ) {
				$ids = implode("', '", array_map('intval', $_POST['status']));
				$ary['conds'] .= "AND l.status IN ('$ids') ";
			}
		} else {
			if ( $_POST['status'] != '' ) {
				$status = (int)$_POST['status'];
				$ary['conds'] .= "AND l.status = '$status' ";
			} else {
				$ary['conds'] .= "AND 1 = 1 "; // dummy condition to save no-condition-for-status as filter
			}
		}
	}
	*/

	if (isset($_POST["segmentid"]) && $_POST["segmentid"] > 0) {
		$ary['conds'] = "AND " . filter_compile($_POST["segmentid"]) . ' ' . $ary['conds'];
	}

	if ( $ary['conds'] == '' ) return array("filterid" => 0);

	$conds_esc = ac_sql_escape($ary['conds']);
	$filterid = ac_sql_select_one("
		SELECT
			id
		FROM
			#section_filter
		WHERE
			userid = '$ary[userid]'
		AND
			sectionid = 'subscriber'
		AND
			conds = '$conds_esc'
	");

	$r = array();

	if ( isset($content) ) $r["content"] = $content;

	if (intval($filterid) > 0) {
		$r["filterid"] = $filterid;
		return $r;
	}

	ac_sql_insert("#section_filter", $ary);
	$r["filterid"] = ac_sql_insert_id();
	return $r;
}

// api
function subscriber_view_hash($hash) {
	if ( !ac_admin_ismain() ) {
		$admin = ac_admin_get();
		if ( $admin['id'] > 1 ) {
			//$liststr = implode("','", $admin["lists"]);
			$listids = $admin["lists"];
		}
	}
	else {
	  $listids = 0;
	}
	$subscriber = subscriber_exists($hash, $listids, "hash");
	if ( isset($subscriber["id"]) && $subscriber["id"] ) $subscriber = subscriber_view($subscriber["id"]);
	return $subscriber;
}

// $match: email address or hash value
// actions: exact, like, hash
function subscriber_exists($match, $listID = 0, $action = 'exact', $status = null) {
	$whitelist = array('exact', 'like', 'hash', 'id');
	if ( !in_array($action, $whitelist) ) $action = 'exact';
	$s = ac_sql_escape($match);
	switch ( $action ) {
		case 'exact':
			$where = "s.email = '$s'";
			break;
		case 'like':
			$s = ac_sql_escape($match, true);
			$where = "s.email LIKE '%$s%'";
			break;
		case 'hash':
			//$where = "MD5(CONCAT(s.id, s.email)) = '$s'";
			$where = "s.hash = '$s'";
			break;

		case 'id':
			$s = (int)$s;
			$where = "s.id = '$s'";
			break;

		default:
			$where = 0;
			break;
	}
	// if list is 0, we're looking if subscriber exists at all
	if ( is_array($listID) ) {
		$listID_original_array = $listID;
		$listID = implode("','", array_map('intval', $listID));
		if ( !$listID ) $listID = 0;
	} else $listID = (int)$listID;
	$q = "
		SELECT
			*,
			INET_NTOA(s.ip) AS ip,
			INET_NTOA(l.ip4_sub) AS ip4,
			TRIM(CONCAT(first_name, ' ', last_name)) AS name,
			s.id AS id,
			l.id AS lid
		FROM
			#subscriber s
		LEFT JOIN
			#subscriber_list l
		ON
			s.id = l.subscriberid
		AND
			l.listid IN ('$listID')
		WHERE
			$where
		GROUP BY
			s.id
	";

	$r = ac_sql_select_row($q, array('cdate', 'sdate'));

	if ( !$r ) return false;
	if ( $listID > 0 ) {

		if(isset($listID_original_array))
			$listID_array = $listID_original_array;
		else
			$listID_array = explode(",", $listID);

		if ( !in_array($r['listid'], $listID_array) ) {
		//if ( !ac_str_instr($r['listid'], $listID) ) {
		//if ( $r['listid'] != $listID ) {
			return false;
		}
	}
	if ( !is_null($status) ) {
		if ( $r['status'] != $status ) {
			return false;
		}
	}
	if ( !$r['subscriberid'] ) $r['subscriberid'] = $r['id'];
	if ( $r['subscriberid'] != $r['id'] ) {
		$r['lid'] = $r['id'];
		$r['id']  = $r['subscriberid'];
	}
	return $r;
}

// $match: email address or hash value
// actions: exact, like, hash
function subscriber_exists_new($match, $listID = 0, $action = 'exact', $status = null) {
	$whitelist = array('exact', 'like', 'hash', 'id');
	if ( !in_array($action, $whitelist) ) $action = 'exact';
	
	if ( !$match ) return false;
	if ( is_array($listID) ) $listID = array_diff(array_map('intval', $listID), array(0));

	$s = ac_sql_escape($match);
	switch ( $action ) {
		case 'exact':
			$where = "s.email = '$s'";
			break;
		case 'like':
			$s = ac_sql_escape($match, true);
			$where = "s.email LIKE '%$s%'";
			break;
		case 'hash':
			//$where = "MD5(CONCAT(s.id, s.email)) = '$s'";
			$where = "s.hash = '$s'";
			break;

		case 'id':
			$s = (int)$s;
			$where = "s.id = '$s'";
			break;

		default:
			return false;
			//$where = 0;
			//break;
	}
	$q = "
		SELECT
			*,
			INET_NTOA(s.ip) AS ip
		FROM
			em_subscriber s
		WHERE
			$where
	";
	$s = ac_sql_select_row($q, array('cdate'));
	if ( !$s ) return false;

	// if list is 0, we're looking if subscriber exists at all
	if ( !$listID ) {
		$l = ac_sql_default_row('em_subscriber_list');
		foreach ( $l as $k => $v ) $l[$k] = null;
		$l['subscriberid'] = $s['id'];
		$l['ip4'] = '';
		$l['name'] = '';
		$l['lid'] = '';
	} else {
		// check in lists
		if ( is_array($listID) ) {
			$listID = implode("','", $listID);
			if ( !$listID ) $listID = 0;
		} else $listID = (int)$listID;
		$cond = "";
		if ( !is_null($status) ) {
			$cond = "AND l.status = '$status'";
		}
		$q = "
			SELECT
				*,
				INET_NTOA(l.ip4_sub) AS ip4,
				TRIM(CONCAT(first_name, ' ', last_name)) AS name,
				l.id AS lid
			FROM
				em_subscriber_list l
			WHERE
				l.listid IN ('$listID')
			AND
				l.subscriberid = '$s[id]'
			$cond
		";
		$l = ac_sql_select_row($q, array('sdate', 'udate'));
		if ( !$l ) return false;
	}

	$r = array_merge($s, $l);
	$r['id'] = $s['id'];

	return $r;
}

function subscriber_list($ids, $filters = array(), $full = 1, $sort = "id", $sort_direction = "DESC", $page = 1) {
	$r = array();

	if ($full == null) $full = 1; // if NULL (not passed)

	$sort_allowed = array("id", "datetime", "first_name", "last_name");
	if (!in_array($sort, $sort_allowed)) $sort = "datetime";
	if ($sort == "datetime") $sort = "sdate"; // the real field name in #subscriber_list
	if ($sort_direction != "ASC" && $sort_direction != "DESC") $sort_direction = "DESC";
	$order = " ORDER BY l." . $sort . " " . $sort_direction;
	if ($page < 1) $page = 1;
	$skipover = ((int)$page - 1) * 20;
	$limit = " LIMIT {$skipover}, 20";

	if ($filters) {
		$whitelist = array("segmentid", "id_greater", "id_less", "email", "first_name", "last_name", "listid", "status", "datetime", "since_datetime", "until_datetime", "fields");
		$conds = array();

		foreach ($filters as $k => $v) {

			if (!in_array($k, $whitelist)) {
				continue;
			}

			// #subscriber DB fields
			if ($k == "email") {
				$conds[] = "AND s." . $k . " LIKE '%" . ac_sql_escape($v, true) . "%'";
			}
			elseif ($k == "fields") {
				// passing custom field filters (filtering on the value)
				// OLD approach used unserialize to pass the fields filter across in the URL
				$fields = ac_str_unserialize($v);
				if (!$fields) {
					if (is_array($v) && $v) {
						// new approach. example:
						/*
						Array
						(
						    [%PERS_6%] => one
						    [%PERS_7%] => two
						)
						*/
						$fields_new = array();
						foreach ($v as $fieldid => $value) {
							// if it starts and ends with a percentage sign, it is a personalization tag
							// (you can pass a field ID, or pers tag, since both are unique)
							$pers_tag = $fieldid;
							if ( preg_match("/^%/", trim($fieldid)) && preg_match("/%$/", trim($fieldid)) ) {
								$pers_tag = trim($fieldid, "%");
								// check if this pers tag actually exists for a field
								$field_row_id = (int)ac_sql_select_one("SELECT id FROM #field WHERE perstag = '$pers_tag'");
							}
							if (isset($field_row_id) && $field_row_id) {
								$fields_new[$field_row_id] = $value;
							}
							else {
								// invalid pers tag
								return ac_ajax_api_result(false, _a("Personalization tag") . " %" . $pers_tag . "% " . _a("cannot be found."));
							}
						}
						$fields = $fields_new;
					}
					else {
						// if it is a bad serialize string (not properly serialized)
						return array("succeeded" => 0, "message" => _a("Error: invalid custom fields filter."));
					}
				}
				$ids = array();
				foreach ($fields as $fieldid => $search) {

					if (!$search) $search = "~|"; // no value for search, so look for all that have the field saved (and empty)

					$relids = ac_sql_select_list("SELECT relid FROM #field_value WHERE fieldid = '$fieldid' AND val LIKE '%" . ac_sql_escape($search, true) . "%' ORDER BY relid{$limit}");
					foreach ($relids as $relid) {
						// add to main ids array, which contains subscriber ID's - should be unique
						if ( !in_array($relid, $ids) ) $ids[] = $relid;
					}

					if ($search == "~|") {
						// also - if searching for blank/empty value, find all subscribers that don't have a row in em_field_value for this field ID
						// (in other words, NOT saved in em_field_value yet)
						$relids = ac_sql_select_list("SELECT s.id FROM #subscriber s WHERE (SELECT COUNT(*) FROM #field_value f WHERE f.relid = s.id AND f.fieldid = '$fieldid') = 0 ORDER BY s.id{$limit}");
						foreach ($relids as $relid) {
							// add to main ids array, which contains subscriber ID's - should be unique
							if ( !in_array($relid, $ids) ) $ids[] = $relid;
						}
					}
				}
				// if subscribers are found matching the custom field value search
				if ($ids) {
					$ids = implode(",", $ids);
				}
				else {
					// otherwise provide blank key so no results are returned
					$ids = array(0);
				}
			}
			else {
				// #subscriber_list DB fields
				if ($k == "listid" || $k == "status" || $k == "first_name" || $k == "last_name") {
					$conds[] = "AND l." . $k . " IN ('{$v}')";
				}

				if ($k == "segmentid") {
					$segment = Cache::lookup("Segment", $v);
					if ($segment) {
						$filterArray = $segment->section_filter();
						$filter = $filterArray["filterid"];
						if ($filter > 0) {
							$segment_conds = ac_sql_select_one("SELECT conds FROM #section_filter WHERE id = '$filter' AND userid = '{$GLOBALS['admin']['id']}' AND sectionid = 'subscriber'");
							if ($segment_conds) $conds[] = $segment_conds;
//dbg($conds);
						}
					}
				}

				if ($k == "id_greater") {
					$conds[] = "AND l.subscriberid > '{$v}'";
				}

				if ($k == "id_less") {
					$conds[] = "AND l.subscriberid < '{$v}'";
				}

				if ($k == "datetime") {
					// Decide which database field to use in the expression, based on what was passed for "status"
					$field = (isset($filters["status"]) && $filters["status"] == 2) ? "udate" : "sdate";
					$conds[] = "AND l." . $field . " LIKE '%" . ac_sql_escape($v, true) . "%'";
				}

				if ($k == "since_datetime") {
					// Decide which database field to use in the expression, based on what was passed for "status"
					if ( isset($filters["status"]) ) {
						$field = ($filters["status"] == 2) ? "udate" : "sdate";
					}
					else {
						$field = "sdate";
					}
					$conds[] = "AND l." . $field . " > '" . $v . "'";
				}

				if ($k == "until_datetime") {
					// Decide which database field to use in the expression, based on what was passed for "status"
					if ( isset($filters["status"]) ) {
						$field = ($filters["status"] == 2) ? "udate" : "sdate";
					}
					else {
						$field = "sdate";
					}
					$conds[] = "AND l." . $field . " < '" . $v . "'";
				}
			}
		}

		// If they do happen to pass $ids as well, OR if $ids is already set, take that into account and filter the results further
		if ($ids) {
			// if it's a string of ID's, IE: "1,2,3"
			if ( !is_array($ids) ) {
				$ids = explode(",", $ids);
			}
			$ids = implode("','", $ids);
			$conds[] = "AND l.subscriberid IN ('" . $ids . "')";
		}

		$query = "SELECT l.subscriberid FROM #subscriber_list l INNER JOIN #subscriber s ON l.subscriberid = s.id WHERE 1 " . implode(" ", $conds) . " GROUP BY l.subscriberid" . $order . $limit;
//dbg($query);
		$ids = ac_sql_select_list($query);

		foreach ($ids as $id) {
			if ( $v = subscriber_view($id) ) {
				if (!(int)$full) {
					$s = array(
						"id" => $v["id"],
						"subscriberid" => $v["subscriberid"],
						"cdate" => $v["cdate"],
						"sdate" => $v["sdate"],
						"first_name" => $v["first_name"],
						"last_name" => $v["last_name"],
						"email" => $v["email"],
					);
					$r[] = $s;
				}
				else {
					$r[] = $v;
				}
			}
		}

		return $r;
	}

	// Default: if only $ids are passed (no filters)
	if (!$r) {
		if ($ids == "all") {
			$query = "SELECT l.subscriberid FROM #subscriber_list l INNER JOIN #subscriber s ON l.subscriberid = s.id WHERE 1 GROUP BY l.subscriberid" . $order . $limit;
			$ids = ac_sql_select_list($query);

			foreach ($ids as $id) {
				if ( $v = subscriber_view($id) ) {
					if (!(int)$full) {
						$s = array(
							"id" => $v["id"],
							"subscriberid" => $v["subscriberid"],
							"cdate" => $v["cdate"],
							"sdate" => $v["sdate"],
							"first_name" => $v["first_name"],
							"last_name" => $v["last_name"],
							"email" => $v["email"],
						);
						$r[] = $s;
					}
					else {
						$r[] = $v;
					}
				}
			}

			return $r;
		}
		else {
			$ids = array_diff(array_map('intval', explode(',', $ids)), array(0));
			if (!$ids) return $ids;

			$r = array();
			foreach ( $ids as $id ) {
				if ( $v = subscriber_view($id) ) {
					if (!(int)$full) {
						$s = array(
							"id" => $v["id"],
							"subscriberid" => $v["subscriberid"],
							"cdate" => $v["cdate"],
							"sdate" => $v["sdate"],
							"first_name" => $v["first_name"],
							"last_name" => $v["last_name"],
							"email" => $v["email"],
						);
						$r[] = $s;
					}
					else {
						$r[] = $v;
					}
				}
			}
			return $r;
		}
	}
}

function subscriber_personalize_get($subscriber, $campaign = null) {
	if ( isset($subscriber['subscriberid']) and $subscriber['subscriberid'] ) {
		$subscriber['id'] = $subscriber['subscriberid'];
	}
	$subscriber['subscriberid'] = $subscriber['id'];
	if ( !isset($subscriber['hash']) or ( $subscriber['id'] and !$subscriber['hash'] ) ) {
		$subscriber['hash'] = md5($subscriber['id'] . $subscriber['email']);
	}
	if ( !ac_str_instr(' ', $subscriber['sdate']) ) $subscriber['sdate'] = AC_CURRENTDATETIME;
	list($sdate, $stime) = explode(' ', $subscriber['sdate']);

	$listname = "";

	// find his list
	$list = null;
	if ( $campaign ) {
		foreach ( $campaign['lists'] as $l ) {
			if ( $l['id'] == $subscriber['listid'] ) $list = $l;
		}
		if ( !$list ) $list = $campaign['lists'][0];
		// check subscriber's name
		if ( $subscriber['first_name'] == '' and $subscriber['last_name'] == '' ) {
			$subscriber['first_name'] = $subscriber['name'] = $list['to_name'];
		}

		$listname = $list["name"];
	}

	$data = subscriber_data_get($subscriber['id']);

	// do general personalization
	$pers = array();
	$pers['%SUBSCRIBERID%'] =
	$pers['%PERS_ID%'] = $subscriber['id'];
	$pers['%PERS_LISTNAME%'] =
	$pers['%LISTNAME%'] = $listname;
	$pers['%PERS_FIRSTNAME%'] =
	$pers['%FIRSTNAME%'] = $subscriber['first_name'];
	$pers['%PERS_LASTNAME%'] =
	$pers['%LASTNAME%'] = $subscriber['last_name'];
	$pers['%FULLNAME%'] =
	$pers['%PERS_NAME%'] = $subscriber['name'];
	$pers['%PERS_EMAIL%'] =
	$pers['%EMAIL%'] = $subscriber['email'];
	$pers['%SUBSCRIBERIP%'] =
	$pers['%PERS_IP%'] = isset($subscriber['ip4']) ? $subscriber['ip4'] : $subscriber['ip'];
	$pers['%SUBDATETIME%'] =
	$pers['%PERS_DATETIME%'] = ac_date_format($subscriber['sdate'], $GLOBALS['site']['datetimeformat']);
	$pers['%SUBDATE%'] =
	$pers['%PERS_DATE%'] = ac_date_format($sdate, $GLOBALS['site']['dateformat']);
	$pers['%SUBTIME%'] =
	$pers['%PERS_TIME%'] = ac_date_format($stime, $GLOBALS['site']['datetimeformat']);
	$pers['%TODAY%'] =
	$pers['%PERS_TODAY%'] =
	$pers['%SENDDATE%'] = ac_date_format(ac_getCurrentDate(), $GLOBALS['site']['dateformat']);
	$pers['%SENDTIME%'] = ac_date_format(ac_getCurrentTime(), $GLOBALS['site']['timeformat']);
	$pers['%SENDDATETIME%'] = ac_date_format(ac_getCurrentDateTime(), $GLOBALS['site']['datetimeformat']);
	//$pers['subscriberemailec'] = base64_encode($subscriber['email']);
	//$pers['subscriberemail'] = urlencode($subscriber['email']);
	//$pers['subscriberid'] = base64_encode($subscriber['id']);
	$pers['subscriberemailec'] =
	$pers['subscriberemail'] =
	$pers['subscriberid'] =
	$pers['%SUBSCRIBER_HASH%'] = $subscriber['hash'];

	// do list id personalization
	if ( !$subscriber['listid'] ) {
		if ( !isset($subscriber['lists']) ) {
			$subscriber2 = subscriber_select_row($subscriber['id']);
			if ( $subscriber2 ) {
				$subscriber2['listid'] = $campaign['lists'][0]['id']; // he might not be confirmed for this one
				$subscriber = $subscriber2;
			} else $subscriber['lists'] = array();
		}
	}
	$pers['%LISTID%'] =
	$pers['currentnl'] = (int)$subscriber['listid'];

	$pers['%SUBSCRIBER_RATING%'] = subscriber_rating($subscriber['id'], $subscriber['email'], $subscriber['listid']);

	// do campaign personalization
	if ( $campaign ) {
		$pers['%SENDDATE%'] = ac_date_format($campaign['sdate'], $GLOBALS['site']['dateformat']);
		$pers['%SENDTIME%'] = ac_date_format($campaign['sdate'], $GLOBALS['site']['timeformat']);
		$pers['%SENDDATETIME%'] = ac_date_format($campaign['sdate'], $GLOBALS['site']['datetimeformat']);
		$pers['rndmnmbr'] = rand('100000', '900000');
		if ( !isset($subscriber['messageid']) or !in_array($subscriber['messageid'], explode('-', $campaign['messageslist'])) ) {
			// set the first one
			$subscriber['messageid'] = $campaign['messages'][0]['id'];
		}
		$pers['%MESSAGEID%'] =
		$pers['currentmesg'] = $subscriber['messageid'];
		$pers['%CAMPAIGNID%'] =
		$pers['cmpgnid'] = ( $campaign['realcid'] ? $campaign['realcid'] : $campaign['id'] );
		$pers['cmpgnhash'] = md5($pers['cmpgnid']);
		$pers['sndid'] = ( isset($campaign['sendid']) ? $campaign['sendid'] : 0 );
		$pers['%TOTAL%'] = $campaign['total_amt'];
		$pers['%%PERS_TBLID%%'] = $subscriber['id']; // id in TEMP table
		$em_xmid = base64_encode($subscriber['email'] . ' , c' . $campaign['id'] . ' , m' . $subscriber['messageid'] . ' , s' . $pers['sndid']);
		$pers['%X-MID%'] = $em_xmid;
		$pers['%ANALYTICSUA%'] = '';
		// find list
		foreach ( $campaign['lists'] as $l ) {
			if ( $l['id'] == $pers['currentnl'] ) {
				$pers['%ANALYTICSUA%'] = $l['analytics_ua'];
			}
		}

//dbg($campaign['fields']);
		foreach ( $campaign['fields'] as $f ) {
			$val = '';
			if ( isset($subscriber['f' . $f['id']]) ) {
				$val = $subscriber['f' . $f['id']];
			} elseif ( isset($subscriber['fields']) ) {
				foreach ( $subscriber['fields'] as $v ) {
					if ( $v['id'] == $f['id'] ) {
						$val = $v['val'];
						break(1);
					}
				}
			}
			// check if exists first (so it doesn't replace any of our internal personalization tags
			if ( !isset($pers[$f['tag']]) ) {
				$pers[$f['tag']] = $val;
			}
		}

		// list sender info
		$pers['%SENDER-INFO%'] = personalization_senderinfo($list, 1);
		$pers['%SENDER-INFO-SINGLELINE%'] = personalization_senderinfo($list, 0);
	} else {
		// if no campaign is provided, do opt-in/opt-out personalization
		//
	}
	return $pers;
}

function subscriber_personalize($subscriber, $listids, $subscription_form_id, $body, $type = 'sub', $format = 'html') {
	global $site;


	//$hash = md5($subscriber["id"] . $subscriber["email"]);
	$hash = $subscriber['hash'];

	if ( !isset($subscriber['sdate']) or !$subscriber['sdate'] ) {
		$subscriber['sdate'] = $subscriber['cdate'];
	}

	$data = subscriber_data_get($subscriber['id']);

	//these things don't work in opt-in/out messages
	$body = str_replace("%SOCIALSHARE%", '', $body);
	$socnets = personalization_social_networks();
	foreach ($socnets as $source => $info) {
		$source = strtoupper($source);
		$body = str_replace("%SOCIALSHARE-$source%", '', $body);
	}
	$body = str_replace("%SOCIAL-FACEBOOK-LIKE%", '', $body);
	$body = str_replace("%FORWARD2FRIEND%", '', $body);
	$body = str_replace("%WEBCOPY%", '', $body);

	$list = null;
	$listname = "";

	if ($listids != "") {
		$tmp = explode(",", $listids);
		if (count($tmp) > 0) {
			$listid   = (int)$tmp[0];
			$list     = ac_sql_select_row("SELECT * FROM #list WHERE id = '$listid'");
			$listname = $list['name'];
		}
	} else {
		$listid = 0;
		$listname = "";
		$list = array();
	}

	$perstags = array(
		"%PERS_EMAIL%" => $subscriber["email"],
		"%EMAIL%" => $subscriber["email"],
		"%PERS_LISTNAME%" => $listname,
		"%LISTNAME%" => $listname,
		"%PERS_FIRSTNAME%" => $subscriber["first_name"],
		"%FIRSTNAME%" => $subscriber["first_name"],
		"%PERS_LASTNAME%" => $subscriber["last_name"],
		"%LASTNAME%" => $subscriber["last_name"],
		"%FULLNAME%" => $subscriber["first_name"].' '.$subscriber["last_name"],
		"%PERS_IP%" => $subscriber["ip4"],
		"%SUBSCRIBERIP%" => $subscriber["ip4"],
		"%PERS_DATE%" => ac_date_format($subscriber["sdate"], $site['dateformat']),
		"%SUBDATETIME%" => ac_date_format($subscriber["sdate"], $site['datetimeformat']),
		"%SUBDATE%" => ac_date_format($subscriber["sdate"], $site['dateformat']),
		"%SENDDATE%" => ac_date_format(AC_CURRENTDATETIME, $site['dateformat']),
		"%PERS_TIME%" => ac_date_format($subscriber["sdate"], $site['timeformat']),
		"%SUBTIME%" => ac_date_format($subscriber["sdate"], $site['timeformat']),
		"%SENDTIME%" => ac_date_format(AC_CURRENTDATETIME, $site['timeformat']),
		"%SENDDATETIME%" => ac_date_format(AC_CURRENTDATETIME, $site['datetimeformat']),
		"%PERS_ID%" => $subscriber["id"],
		"%SUBSCRIBERID%" => $subscriber["id"],
		"%SUBSCRIBER_RATING%" => subscriber_rating($subscriber['id'], $subscriber['email'], $listid ? $listid : 0),

		"%CONFIRMLINK%" => $site["p_link"] . "/proc.php?nl=" . $listids . "&f=" . $subscription_form_id . "&s=" . $hash . "&act=csub",
		"%SUBSCRIBELINK%" => $site["p_link"] . "/index.php?action=subscribe&id=" . $subscription_form_id . "&s=" . $hash,
		"%UNSUBSCRIBELINK%" => $site["p_link"] . "/proc.php?nl=" . $listids . "&f=" . $subscription_form_id . "&s=" . $hash . "&act=unsub",
		"currentnl" => $listids,
		"cmpgnid" => 0,
		"cmpgnhash" => md5(0),
		"sndid" => 0,
		"currentmesg" => 0,
		"subscriberid" => $hash,
		"%SENDER-INFO%" => ( $list ? personalization_senderinfo($list, 1) : '' ),
		"%SENDER-INFO-SINGLELINE%" => ( $list ? personalization_senderinfo($list, 0) : '' ),
		// Internal
		"%PLINK%" => $site['p_link'],
	);

	// Global Custom Fields
	$fields = subscriber_get_fields($subscriber["id"], $listids, false);
	foreach ( $fields as $field ) {
		$perstags[$field["tag"]] = $field["val"];
		$tag = '%PERS_' . $field['id'] . '%';
		if ( $field['tag'] != $tag ) {
			$perstags[$tag] = $field['val'];
		}
		//$body = str_replace($field["tag"], $field["val"], $body);
	}

	if ( $format == 'text' ) {
		// strip html from all replacement tags
		$perstags = str_replace(array('<br />', '<br>'), "\n", $perstags);
		$perstags = array_map('ac_str_strip_tags', $perstags);
	}

	require_once ac_admin("functions/personalization.php");
	// Get the basic tags
	$body = personalization_basic($body, '');



	if ( !ac_str_instr('%/IF%', strtoupper($body)) ) {
		// personalize
		return str_replace(array_keys($perstags), array_values($perstags), $body);
	}
	// run conditional personalization
	return personalization_conditional($perstags, $body, false);

	//return $body;
}

function subscriber_get_fields($id, $list = 0, $userel = true) {
	$id = (int)$id;
	if ( $list === false || $list === null || $list === '' ) $list = "'0'";
	if ( is_array($list) ) {
		if ( !in_array(0, $list) ) $list[] = 0;
		$list = implode(', ', $list);
	} elseif ( $list !== 0 ) {
		$list .= ', 0';
	}
	$fields = ac_sql_select_list("SELECT fieldid FROM #field_rel WHERE relid IN ($list)");
	$fieldstr = implode("','", $fields);
	// using in-loop fetch
	//return ac_cfield_select_nodata_rel('#field', '#field_rel', "r.relid IN ($list)", "SELECT val FROM #field_value WHERE relid = '$id' AND fieldid = '%s' LIMIT 1");
	// using subquery
	//return ac_cfield_select_data_rel_subquery('#field', '#field_rel', "SELECT val FROM #field_value WHERE relid = '$id' AND fieldid = f.id LIMIT 1", "f.id = r.fieldid AND r.relid IN ($list) GROUP BY f.id");
	// using left join
	if ($userel) {
		$rows = ac_cfield_select_data_rel('#field', '#field_rel', '#field_value', "d.relid = '$id' AND f.id = d.fieldid", "f.id = r.fieldid AND r.relid IN ($list) GROUP BY f.id");
		$rval = array();

		foreach ($rows as $row)
			$rval[$row["id"]] = $row;

		return $rval;
	} else {
		$rows = ac_cfield_select_data_norel(
			"#field",
			"#field_value",
			"d.relid = '$id' AND f.id IN ('$fieldstr')"
		);
		$rval = array();

		foreach ($rows as $row)
			$rval[$row["id"]] = $row;

		return $rval;
	}
}

function subscriber_get_lists($subscriberid, $status = null, $ids = null) {
	$so = new AC_Select();
	if ( !ac_admin_ismain() ) {
		$admin = ac_admin_get();
		if ( $admin['id'] > 1 ) {
			$liststr = implode("','", $admin["lists"]);
			$cond = "AND l.id IN ('$liststr')";
			if ( !in_array($cond, $so->conds) ) $so->push($cond);
		} elseif ( $admin['id'] == 0 ) {
			// or maybe we should process list ids only here?
			//$so->push("AND l.private = 0");
		}
	}
	if ( !is_null($status) ) {
		$statuses = ( is_array($status) ? implode("','", array_map('intval', $status)) : (int)$status );
		$so->push("AND s.status IN ('$statuses')");
	}
	if ( !is_null($ids) ) {
		if ( !is_array($ids) ) $ids = array_diff(array_map('intval', explode(',', $ids)), array(0));
		if ( count($ids) ) {
			$ids = implode("', '", $ids);
			$so->push("AND l.id IN ('$ids')");
		}
	}
	$query = $so->query("
		SELECT
			s.*,
			l.name AS listname
		FROM
			#list l,
			#subscriber_list s
		WHERE
		[...]
		AND
			s.subscriberid = '$subscriberid'
		AND
			s.listid = l.id
	");
	$sql = ac_sql_query($query);
	$r = array();
	while ( $row = ac_sql_fetch_assoc($sql, array('sdate', 'udate')) ) {
		$r[$row['listid']] = $row;
	}
	return $r;
}

function subscriber_bounce_lowercounts($id, $email, $type) {
	$rows = ac_sql_query("
		SELECT
			campaignid,
			count(id) AS count
		FROM
			#bounce_data
		WHERE
			(email = '$email' OR id = '$id')
		AND `type` = '$type'
		GROUP BY
			campaignid
	");

	$bouncefield = "hardbounces";
	if ($type == "soft")
		$bouncefield = "softbounces";

	while ($row = ac_sql_fetch_assoc($rows)) {
		$campaignid = intval($row["campaignid"]);
		$count      = intval($row["count"]);
		ac_sql_query("
			UPDATE
				#campaign
			SET
				`$bouncefield` = `$bouncefield` - $count
			WHERE
				id = '$campaignid'
		");
		// try this as well, doesn't hurt
		ac_sql_query("
			UPDATE
				#campaign_deleted
			SET
				`$bouncefield` = `$bouncefield` - $count
			WHERE
				id = '$campaignid'
		");

		ac_sql_query("
			UPDATE
				#campaign_message
			SET
				`$bouncefield` = `$bouncefield` - $count
			WHERE
				campaignid = '$campaignid'
		");
	}
}

function subscriber_stats_query(&$so, $panel = 'mailing') {
	if ( !ac_admin_ismain() ) {
		$admin = ac_admin_get();
		if ( $admin['id'] != 1 ) {
			$cond = "AND l.id IN ('" . implode("', '", $admin['lists']) . "')";
			if ( !in_array($cond, $so->conds) ) $so->push($cond);
		}
	}
	if ( $panel == 'mailing' ) {
		$cond = "AND c.type IN ('single', 'recurring', 'split', 'activerss', 'text')";
		if ( !in_array($cond, $so->conds) ) $so->push($cond);
		return $so->query("
			SELECT
				c.id,
				l.id AS listid,
				l.name AS listname,
				c.name AS campaignname,
				c.sdate,
				d.email,
				d.subscriberid
			FROM
				#list l,
				#link t,
				#link_data d,
				#campaign_list e,
				#campaign c
			WHERE
			[...]
			AND
				t.id = d.linkid
			AND
				c.id = t.campaignid
			AND
				c.id = e.campaignid
			AND
				l.id = e.listid
			AND
				t.tracked = 1
			GROUP BY c.id
		");
	} elseif ( $panel == 'responder' ) {
		$cond = "AND c.type IN ('responder', 'reminder')";
		if ( !in_array($cond, $so->conds) ) $so->push($cond);
		return $so->query("
			SELECT
				c.id,
				l.id AS listid,
				l.name AS listname,
				c.name AS campaignname,
				r.sdate,
				s.email,
				r.subscriberid
			FROM
				#list l,
				#subscriber s,
				#subscriber_list e,
				#subscriber_responder r,
				#campaign c
			WHERE
			[...]
			AND
				s.id = e.subscriberid
			AND
				l.id = e.listid
			AND
				s.id = r.subscriberid
			AND
				l.id = r.listid
			AND
				c.id = r.campaignid
			GROUP BY c.id
		");
	} elseif ( $panel == 'log' ) {
		return $so->query("
			SELECT
				c.id,
				l.id AS listid,
				l.name AS listname,
				c.name AS campaignname,
				c.sdate,
				s.email,
				g.subscriberid,
				g.comment,
				g.successful,
				g.tstamp
			FROM
				#list l,
				#subscriber s,
				#subscriber_list e,
				#log g,
				#campaign c,
				#campaign_list cl
			WHERE
			[...]
			AND
				s.id = e.subscriberid
			AND
				l.id = e.listid
			AND
				s.id = g.subscriberid
			AND
				c.id = g.campaignid
			AND
				cl.campaignid = c.id
			AND
				cl.listid = l.id
			GROUP BY c.id
		");
	}
}

function subscriber_stats_array($so = null, $ids = null, $panel = 'mailing') {
	if ($so === null || !is_object($so))
		$so = new AC_Select;

	if ($ids !== null) {
		$tmp = array_map("intval", explode(",", $ids));
		$ids = implode("','", $tmp);
		$so->push("AND m.id IN ('$ids')");
	}
	//dbg(ac_prefix_replace(subscriber_stats_query($so, $panel), array('sdate')));
	if($panel=='log')
		$r = ac_sql_select_array(subscriber_stats_query($so, $panel), array('sdate, tstamp'));
	else
		$r = ac_sql_select_array(subscriber_stats_query($so, $panel), array('sdate'));

	foreach ( $r as $k => $v ) {
		// fetch reads,links,forwards for this message
		$email = ac_sql_escape($v['email']);
/**/
			$reads_query = "
				SELECT
					tstamp,
					email,
					times
				FROM
					#link l,
					#link_data d
				WHERE
					l.campaignid = '$v[id]'
				AND
				(
					d.email = '$email'
				OR
					d.subscriberid = '$v[subscriberid]'
				)
				AND
					l.link IN ('open', '')
				AND
					l.tracked = 1
				AND
					d.linkid = l.id
				ORDER BY
					d.tstamp DESC
			";
			$r[$k]['reads'] = ac_sql_select_array($reads_query, array('tstamp'));
			$links_query = "
				SELECT
					l.link,
					l.name,
					d.times,
					d.tstamp
				FROM
					#link l,
					#link_data d
				WHERE
					l.campaignid = '$v[id]'
				AND
					(
						d.email = '$email'
					OR
						d.subscriberid = '$v[subscriberid]'
					)
				AND
					l.link NOT IN ('open', '')
				AND
					l.tracked = 1
				AND
					d.linkid = l.id
				ORDER BY
					d.tstamp DESC
			";
			$r[$k]['links'] = ac_sql_select_array($links_query, array('tstamp'));
			// forwards
			$forwards_query = "
				SELECT
					*
				FROM
					#forward f
				WHERE
					f.campaignid = '$v[id]'
				AND
					(
						f.email_from = '$email'
					OR
						f.subscriberid = '$v[subscriberid]'
					)
				ORDER BY
					f.tstamp DESC
			";
			$r[$k]['forwards'] = ac_sql_select_array($forwards_query, array('tstamp'));
/**/
	}
	return $r;
}

function subscriber_stats_array_paginator($paginator, $panel, $id, $sort, $offset, $limit, $list) {
	$so = new AC_Select;

	$list = (int)$list;
	if ( $list > 0 ) {
		$so->push("AND l.id = '$list'");
	}

	$subscriber = subscriber_select_row($id);
	if ( $subscriber ) {
		$email = $subscriber['email'];
		$emailEsc = ac_sql_escape($email);
		//$nl = $subscriber['nl'];
		if ( $panel == 'mailing' ) {
			$so->push("AND t.link IN ('open', '') AND ( d.subscriberid = '$id' OR d.email = '$emailEsc' )");
		} else if ( $panel == 'responder' ) {
			$so->push("AND ( s.id = '$id' OR s.email = '$emailEsc' )");
		} elseif ( $panel == 'log' ) {
			//dbg('2do: logs');
			$so->push("AND s.id = '$id'");
		}
	}


	$so->count("DISTINCT(c.id)");
	$total = (int)ac_sql_select_one(subscriber_stats_query($so, $panel));

	switch ($sort) {
		case "01":
			$so->orderby("l.name"); break;
		case "01D":
			$so->orderby("l.name DESC"); break;
		case "02":
			$so->orderby("c.name"); break;
		case "02D":
			$so->orderby("c.name DESC"); break;
		case "03":
			$so->orderby("g.tstamp"); break;
		case "03D":
			$so->orderby("g.tstamp DESC"); break;
		case "04":
			$so->orderby("g.sent"); break;
		case "04D":
			$so->orderby("g.sent DESC"); break;
	}

	if ( (int)$limit == 0 ) $limit = 999999999;
	$limit  = (int)$limit;
	$offset = (int)$offset;
	$so->limit("$offset, $limit");
	$rows = subscriber_stats_array($so, null, $panel);

	return array(
		"paginator"   => $paginator,
		"offset"      => $offset,
		"limit"       => $limit,
		"total"       => $total,
		"cnt"         => count($rows),
		"rows"        => $rows,
	);
}

function subscriber_search($string, $format = '%%%s%%') {
	$escaped = sprintf($format, ac_sql_escape($string, true));
	$so = new AC_Select();
	$so->push("AND ( s.email LIKE '$escaped' OR l.first_name LIKE '$escaped' OR l.last_name LIKE '$escaped' )");
	return subscriber_select_array_alt($so);
}

function subscriber_exportlist_export() {
	$admin = $GLOBALS["admin"];

	if (!$admin["pg_list_add"])
		return ac_ajax_api_result(false, _a("You do not have permission to export subscribers into a new list."));

	$name = ac_sql_escape($_POST["name"]);
	$c    = ac_sql_select_one("SELECT COUNT(*) FROM #list WHERE name = '$name'");

	if ($c > 0)
		return ac_ajax_api_result(false, _a("There is already a list with that name.  Please choose another."));

	$filterid = intval($_POST["filterid"]);
	if ($filterid < 1)
		return ac_ajax_api_result(false, _a("You did not provide a filter for us to use."));

	# Now create the list.
	# First unset filterid, in case list_insert_post() should one day want that.  That means that
	# we're leaving name and from_email in $_POST for list_insert_post() to use.
	unset($_POST["filterid"]);

	require_once ac_admin("functions/list.php");
	$rval = list_insert_post();

	if (!$rval["succeeded"])
		return ac_ajax_api_result(false, _a("Couldn't add the list."));

	$listid = intval($rval["id"]);
	$conds  = ac_sql_select_one("SELECT conds FROM #section_filter WHERE id = '$filterid' AND userid = '$admin[id]' AND sectionid = 'subscriber'");
	$so     = new AC_Select;

	$so->push($conds);

	$limit = intval($_POST["limit"]);
	if ($limit > 0)
		$so->limit($limit);

	# Create the subscriber_list records, indicating that these subscribers now belong to the
	# new list.
	$newids = ac_sql_select_list($so->query("
		SELECT DISTINCT
			s.id
		FROM
			#subscriber s,
			#subscriber_list l
		WHERE
			[...]
		AND
			l.subscriberid = s.id
	"));

	$ins = array();
	foreach ($newids as $newid) {
		$first_name = ac_sql_escape(ac_sql_select_one('first_name', '#subscriber_list', "subscriberid = '$newid'"));
		$last_name = ac_sql_escape(ac_sql_select_one('last_name', '#subscriber_list', "subscriberid = '$newid'"));
		$ins[] = "('$newid', '$listid', NOW(), '1', '$first_name', '$last_name', 3)";
	}

	$insstr = implode(",", $ins);
	ac_sql_query("
		INSERT INTO #subscriber_list
			(subscriberid, listid, sdate, status, first_name, last_name, sourceid)
		VALUES
			$insstr
	");
}

function subscriber_select_field_dataids($id) {
	$id   = intval($id);
	$rval = array();
	$rs   = ac_sql_query("
		SELECT
			fieldid, id
		FROM
			#field_value
		WHERE
			relid = '$id'
	");

	while ($row = ac_sql_fetch_assoc($rs))
		$rval[$row["fieldid"]] = $row["id"];

	return $rval;
}

function subscriber_select_fields($id, $listid, $editable) {
	$id     = (int)$id;
	$listid = (int)$listid;

	$admin   = ac_admin_get();

	if (!isset($admin["lists"][$listid]))
		return;

	$sub    = ac_sql_select_row("SELECT *, INET_NTOA(ip4_sub) AS ip4str FROM #subscriber_list WHERE subscriberid = '$id' AND listid = '$listid'", array("sdate"));
	$ip     = ac_sql_select_one("SELECT INET_NTOA(ip) FROM #subscriber WHERE id = '$id'");

	// check if excluded from this (or all) list(s) - so we can display a message on subscriber_view page
	$subscriber = subscriber_select_row($id);
	$exclusion_match = ( exclusion_match($subscriber["email"], $listid, false) ) ? 1 : 0;

	return array(
		"editable"   => $editable,
		"first_name" => $sub["first_name"],
		"last_name"  => $sub["last_name"],
		"sdate"      => strftime($GLOBALS["site"]["datetimeformat"], strtotime($sub["sdate"])),
		"udate"      => strftime($GLOBALS["site"]["datetimeformat"], strtotime($sub["udate"])),
		"ip4"        => $sub["ip4str"],
		"ip"         => $ip,
		"status"     => $sub["status"],
		"listname"   => ac_sql_select_one("SELECT name FROM #list WHERE id = '$listid'"),
		"exclusion"  => $exclusion_match,
		"row"        => ac_cfield_select_data_rel("#field", "#field_rel", "#field_value", "d.relid = '$id' AND d.fieldid = f.id", "r.relid IN ('$listid', '0') AND r.fieldid = f.id"),
	);
}

function subscriber_update_fields() {
	if (!permission("pg_subscriber_edit"))
		return;

	$admin   = ac_admin_get();
	$listid  = (int)ac_http_param("listid");

	if (!isset($admin["lists"][$listid]))
		return;

	$id         = (int)ac_http_param("id");
	$first_name = (string)ac_http_param("first_name");
	$last_name  = (string)ac_http_param("last_name");
	$status     = (int)ac_http_param("status");
	$fields     = ac_http_param_forcearray("field");
	//$fields     = array_map("ac_str_strip_tags", $fields);

	ac_cfield_update_data($fields, "#field_value", "fieldid", array("relid" => $id));

	$up = array(
		"first_name" => $first_name,
		"last_name"  => $last_name,
		"status"     => $status,
		'=ip4_last'  => "INET_ATON('127.0.0.1')",
	);

	$oldstatus = (int)ac_sql_select_one('status', "#subscriber_list", "subscriberid = '$id' AND listid = '$listid'");

	// only update the sdate if they are MOVING TO "subscribed" status (from another status)
	if ($oldstatus != 1 && $status == 1)
		$up["=sdate"] = "NOW()";

	// if switching from bounced to any other
	if ( $oldstatus == 3 and $status != 3 ) {
		$email = ac_sql_escape(ac_sql_select_one('email', "#subscriber", "id = '$id'"));
		// update subscriber's bounce totals
		$update = array(
			'bounce_hard' => 0,
			'bounce_soft' => 0,
		);
		ac_sql_update("#subscriber", $update, "id = '$id'");
		// remove bounce log
		ac_sql_delete('#bounce_data', "( `id` = '$id' OR `email` = '$email' )");
		// update campaigns that marked him as bounced
		subscriber_bounce_lowercounts($id, $email, "soft");
		subscriber_bounce_lowercounts($id, $email, "hard");
	}

	ac_sql_update("#subscriber_list", $up, "subscriberid = '$id' AND listid = '$listid'");
}

function subscriber_view_lists($id, $listid) {
	$id      = (int)$id;
	$listid  = (int)$listid;
	$admin   = ac_admin_get();
	$liststr = implode("','", $admin["lists"]);

	$available = ac_sql_select_one("
		SELECT
			COUNT(*)
		FROM
			#list l
		WHERE
			l.id IN ('$liststr')
		AND
			(SELECT COUNT(*) FROM #subscriber_list s WHERE s.subscriberid = '$id' AND s.listid = l.id AND s.status = 1) = 0
	");

	$rval = array(
		"listid"    => $listid,
		"available" => $available,
		"row"       => ac_sql_select_array("SELECT s.listid, s.status, (SELECT l.name FROM #list l WHERE l.id = s.listid) AS name FROM #subscriber_list s WHERE s.subscriberid = '$id' AND s.listid IN ('$liststr') ORDER BY name"),
	);

	return $rval;
}

function subscriber_view_unlists($id) {
	$id      = (int)$id;
	$admin   = ac_admin_get();
	$liststr = implode("','", $admin["lists"]);

	return ac_sql_select_array("
		SELECT
			l.id,
			l.name
		FROM
			#list l
		WHERE
			l.id IN ('$liststr')
		AND
			(SELECT COUNT(*) FROM #subscriber_list s WHERE s.subscriberid = '$id' AND s.listid = l.id AND s.status = 1) = 0
		ORDER BY
			l.name
	");
}

function subscriber_view_subscribe($id, $listid, $copyfrom) {
	if (!permission("pg_subscriber_edit"))
		return ac_ajax_api_result(false, _a("No permission to subscribe"));

	$id     = (int)$id;
	$listid = (int)$listid;
	$admin   = ac_admin_get();

	if (!isset($admin["lists"][$listid]))
		return;

	# Check if exists first.
	$row = ac_sql_select_row("SELECT id FROM #subscriber_list WHERE subscriberid = '$id' AND listid = '$listid'");

	if ($row) {
		ac_sql_query("UPDATE #subscriber_list SET status = 1 WHERE id = '$row[id]'");

		$sub = subscriber_exists($id, $listid, "id");
		mail_responder_send($sub, $listid, 'subscribe');

		return ac_ajax_api_result(true, _a("Subscribed"));
	}

	$ins = array(
		"subscriberid" => $id,
		"listid"       => $listid,
		"status"       => 1,
		"=sdate"       => "NOW()",
		"sourceid"     => 3,
	);

	$copyfrom = (int)$copyfrom;

	if ($copyfrom > 0) {
		$copy = ac_sql_select_row("SELECT * FROM #subscriber_list WHERE subscriberid = '$id' AND listid = '$copyfrom'");
		$ins["first_name"] = $copy["first_name"];
		$ins["last_name"]  = $copy["last_name"];
	}

	ac_sql_insert("#subscriber_list", $ins);

	$sub = subscriber_exists($id, $listid, "id");
	mail_responder_send($sub, $listid, 'subscribe');

	return ac_ajax_api_result(true, _a("Subscribed"));
}

function subscriber_view_unsubscribe($id, $listid) {
	$id     = (int)$id;
	$listid = (int)$listid;

	if (!permission("pg_subscriber_edit"))
		return ac_ajax_api_result(false, _a("No permission to unsubscribe"));

	$admin   = ac_admin_get();

	if (!isset($admin["lists"][$listid]))
		return;

	$rval  = array("result" => subscriber_unsubscribe($id, null, array($listid)), "deleted" => 0);
	$count = (int)ac_sql_select_one("SELECT * FROM #subscriber_list WHERE subscriberid = '$id' AND listid = '$listid' AND status IN (1, 0)");

	return $rval;
}

// creates a dummy subscriber
function subscriber_dummy($email, $listid = 0) {
	return array(
		'id'           => 0,
		'subscriberid' => 0,
		'cdate'        => AC_CURRENTDATETIME,
		'sdate'        => AC_CURRENTDATETIME,
		'hash'         => md5(0 . $email),
		'email'        => $email,
		'first_name'   => '',
		'last_name'    => '',
		'name'         => '',
		'ip'           => ( isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1' ),
		'ip4'          => ( isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1' ),
		'ua'           => ( isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '' ),
		'listid' => $listid,
		'lid' => $listid,
	);
}

function subscriber_rating($subscriberid, $email, $listid = 0) {
	// define result
	$r = 0;

	//if ( isset($GLOBALS['_hosted_account']) ) {
		//require(dirname(dirname(__FILE__)) . '/manage/subscriber.rate.inc.php');
	//}

	// return result
	return $r;
}

function subscriber_data_set($subscriberid) {
	$subscriberid = (int)$subscriberid;
	$found = (int)ac_sql_select_one("id", "#subscriber_data", "subscriberid = '$subscriberid'");
	if ( $found ) return $found;

	$default = ac_sql_default_row("#subscriber_data");
	$insert = ac_sql_field2func(
		$default, // row to convert
		$fields = array(
			// nulls to assign
			'rating_tstamp' => 'NULL',
			'ga_first_visit' => 'NULL',
			// NOW()s to assign
			'tstamp' => 'NOW()',
			'subscriberid' => $subscriberid, // ok cuz it's integer
		)
	);
	$done = ac_sql_insert("#subscriber_data", $insert);
	if ( !$done ) return 0;

	$r = (int)ac_sql_insert_id();
	return $r;
}

function subscriber_data_get($subscriberid) {
	$dataid = subscriber_data_set($subscriberid);
	$data = ac_sql_select_row("SELECT * FROM #subscriber_data WHERE id = '$dataid'");
	return $data;
}

function subscriber_get_actions($id) {
	// gets subscriber activity/actions (stuff that appears on the View Subscriber page)
	// see also: admin/context/subscriber_view.php (function actions)
	$rval = array();
	$id   = (int)$id;

	$admin = ac_admin_get();
	$liststr = implode("','", $admin['lists']);

	$rs = ac_sql_query("
		SELECT
			ld.campaignid,
			ld.tstamp
		FROM
			#link_data ld,
			#campaign_list cl
		WHERE
			ld.subscriberid = '$id'
		AND ld.campaignid = cl.campaignid
		AND cl.listid IN ('$liststr')
		AND ld.isread = 1
		AND ld.messageid != '0'
		LIMIT 20

	");

	while ($row = ac_sql_fetch_assoc($rs)) {
		$rval[] = array(
			'tstamp' => $row["tstamp"],
			'text' => sprintf(_a("Opened \"%s\""), subscriber_get_actions_campaign($row["campaignid"])),
			'type' => 'open',
		);
	}

	$rs = ac_sql_query("
		SELECT
			ld.campaignid,
			ld.tstamp
		FROM
			#link_data ld,
			#campaign_list cl
		WHERE
			ld.subscriberid = '$id'
		AND ld.campaignid = cl.campaignid
		AND cl.listid IN ('$liststr')
		AND ld.isread = 0
		AND ld.tstamp > (NOW() - INTERVAL 1 MONTH)
	");

	while ($row = ac_sql_fetch_assoc($rs)) {
		$rval[] = array(
			'tstamp' => $row["tstamp"],
			'text' => sprintf(_a("Clicked on a link in \"%s\""), subscriber_get_actions_campaign($row["campaignid"])),
			'type' => 'click',
		);
	}

	$rs = ac_sql_query("
		SELECT
			f.campaignid,
			f.tstamp
		FROM
			#forward f,
			#campaign_list cl
		WHERE
			f.subscriberid = '$id'
		AND f.campaignid = cl.campaignid
		AND cl.listid IN ('$liststr')
		AND tstamp > (NOW() - INTERVAL 1 MONTH)
	");

	while ($row = ac_sql_fetch_assoc($rs)) {
		$rval[] = array(
			'tstamp' => $row["tstamp"],
			'text' => sprintf(_a("Forwarded \"%s\""), subscriber_get_actions_campaign($row["campaignid"])),
			'type' => 'forward',
		);
	}

	krsort($rval);
	$rval = array_slice($rval, 0, 10);
	return $rval;
}

function subscriber_get_actions_campaign($id) {
	// called from subscriber_get_actions()
	// see also: admin/context/subscriber_view.php (function campaign)

	$id = (int)$id;

	$name = (string)ac_sql_select_one("SELECT name FROM #campaign WHERE id = '$id'");
	$link = ac_site_plink("admin/main.php?action=report_campaign&id=" . $id . "#general-01-0-0");
	if ( !$name ) {
		$name_del = (string)ac_sql_select_one("SELECT name FROM #campaign WHERE id = '$id'");
		if ( $name_del ) $name = $name_del;
		else $name = '-';
	}

	return "<a href='$link'>$name</a>";
}

?>
