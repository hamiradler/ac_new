<?php

require_once ac_global_functions("rss.php");

$GLOBALS['activerss_items_found'] = 0;
$GLOBALS['activerss_cache'] = array();

function activerss_parse($campaign, $message, $html = false, $log = false, $action = 'send') {
	$str = $message[ ( $html ? 'html' : 'text' ) ];
	$cid = ( isset($campaign['id']) ? $campaign['id'] : 0 );
	$mid = ( isset($message['id'])  ? $message['id']  : 0 );
	$GLOBALS['activerss_items_found'] = 0;
	preg_match_all('/(%RSS-FEED\|URL:([^|]*)\|SHOW:(NEW|ALL)%)(.*?)(%RSS-FEED%)/s', $str, $rssmatches);
	$cnt = count($rssmatches[0]);
	$format = ( $html ? 'HTML' : 'text' );
	if ( $log ) campaign_sender_log("Found $cnt RSS blocks in $format message.");
	if ( $cnt == 0 ) return $str;
	// for every block
	foreach ( $rssmatches[0] as $k => $rssblock ) {
		$url = $rssmatches[2][$k];
		//$limit = $rssmatches[3][$k];
		$type = strtolower($rssmatches[3][$k]);
		if($campaign['type']=="activerss") $type = "new";
		$inner = $rssmatches[4][$k];
		$loop = $rssmatches[4][$k];
		// get it's RSS feed
		$url = str_replace('&amp;', '&', $url);
		list($url) = explode('   ', $url); // 2do!!! support for multiple feeds
		$limit = 1;
		// now loop through all loops in this block
		preg_match_all('/(%RSS-LOOP\|LIMIT:(\d+)%)(.*?)(%RSS-LOOP%)/s', $rssblock, $loopmatches);
		foreach ( $loopmatches[0] as $k => $loopblock ) {
			$lim = (int)$loopmatches[2][$k];
			if ( !$lim or $lim > $limit ) {
				$limit = $lim;
			}
			if ( !$lim ) break;
		}

		$rsshash = md5("$url, $limit, $type, $cid, $mid");
		if ( $log ) {
			campaign_sender_log("This RSS fetch got cache hash: $rsshash (url=$url;limit=$limit;type=$type;cid=$cid;mid=$mid)");
		}
		if ( isset($GLOBALS['activerss_cache'][$rsshash]) ) {
			$feed = $GLOBALS['activerss_cache'][$rsshash];
			$GLOBALS['activerss_items_found'] += $feed['found_count'];
		} else {
			ac_rss_useragent_set(activerss_useragent(/*$campaign['total_amt']*/));
			$GLOBALS['activerss_cache'][$rsshash] =
			$feed = activerss_fetch($url, $limit, $type, $cid, $mid, $action);
			if ( $feed['rss'] ) $GLOBALS['activerss_cache'][$rsshash] = $feed;
			ac_rss_useragent_unset();
		}

		$cnt = 0;
		if ( $feed['rss'] and isset($feed['rss']->item) ) $cnt = count($feed['rss']->item);
		if ( $log ) {
			campaign_sender_log("Found $cnt RSS items in feed $url");
			//campaign_sender_log(print_r(( $feed['rss'] and isset($feed['rss']->item) ) ? $feed['rss']->item : $feed, 1));
			//campaign_sender_log(print_r($feed['feed'],1));
			//campaign_sender_log($type);
		}
		if ( !$feed['rss'] ) {
			$str = str_replace($rssblock, '', $str);
			continue;
		}
		// now loop through all loops in this block
		foreach ( $loopmatches[0] as $k => $loopblock ) {
			$limit = $loopmatches[2][$k];
			$loop = $loopmatches[3][$k];
			// build a content for each loop
			$content = activerss_replace_items($feed, $loop, $limit, $html);
			// replace the original block with the generated contents
			$inner = str_replace($loopblock, $content, $inner);
		}
		// build a content outside of loops
		$content = activerss_replace($feed, $inner, '', $html);
		// replace the original block with the generated contents
		$str = str_replace($rssblock, $content, $str);
		// try to save links if doing this within a (tracked) campaign (that we're formally sending!)
		if ( $cid > 0 and $mid > 0 and $campaign['tracklinks'] != 'none' and $campaign['status'] == 2 ) {
			if ( $log ) {
				campaign_sender_log("Fetching links to track");
			}
			// fetch all links found
			$tmp = array(
				'id' => $mid,
				'format' => $campaign['tracklinks'],
				'text' => $content,
				'html' => $content,
			);
			$links = message_extract_links($tmp);
			// go thru all found links in this rss block
			if ( $log ) {
				campaign_sender_log("Saving tracked links");
			}
			foreach ( $links as $link ) {
				// check if already added
				$linkesc = ac_sql_escape($link['link']);
				$id = intval(ac_sql_select_one("
					SELECT
						id
					FROM
						#link
					WHERE
						link = '$linkesc'
					AND
						campaignid = '$cid'
					AND
						messageid = '$mid'
				"));
				if ( $id ) continue;
				// add link
				$insert = array(
					'id' => 0,
					'campaignid' => $cid,
					'messageid' => $mid,
					'link' => $link['link'],
					//'=name' => 'NULL',
					//'ref' => '',
					'tracked' => 1,
				);
				ac_sql_insert('#link', $insert);
			}
			if ( $log ) {
				campaign_sender_log("Tracked links saved.");
			}
		}
	}
	return $str;
}

function activerss_fetch($url, $limit = 0, $type = 'new', $cid = 0, $mid = 0, $action = 'send') {
	global $site;
	$tstamp = 0;
	// initialize counter for loop
	$counter = 0;
	// always fetch the feed without caching here
	$feed = ac_rss_fetch($url, 0);
	// if feed is fetched
	if ( $feed['rss'] ) {
		$urlEsc = ac_sql_escape($url);
		$conds = array();
		if ( $cid > 0 ) $conds[] = "`campaignid` = '$cid'";
		if ( $mid > 0 ) $conds[] = "`messageid` = '$mid'";
		$conds[] = "`url` = '$urlEsc'";
		$feedArr = ac_sql_select_row("SELECT * FROM #rssfeed WHERE" . implode(" AND ", $conds));
		if ( !$feedArr ) {
			// add it if new
			$feedArr = array(
				'id' => 0,
				'campaignid' => $cid,
				'messageid' => $mid,
				'url' => $url,
				'type' => $type,
				'=lastcheck' => 'NULL',
				'howmany' => $limit,
			);
			if ( $cid > 0 ) {
				ac_sql_insert('#rssfeed', $feedArr);
				$feedArr['id'] = ac_sql_insert_id();
			}
			unset($feedArr['=lastcheck']);
			$feedArr['lastcheck'] = null;
		} else {
			# Update it!

			$up = array(
				"type" => $type,
				"howmany" => $limit,
			);

			if ($cid > 0) {
				ac_sql_update("#rssfeed", $up, "id = '$feedArr[id]'");
			}

			$feedArr["type"] = $type;
			$feedArr["howmany"] = $limit;
			if ( $action != 'send' ) $feedArr['lastcheck'] = null;
		}

		// figure out last check timestamp
		if ( $feedArr['lastcheck'] ) $tstamp = (int)@strtotime($feedArr['lastcheck']);
		if ( !$tstamp or $tstamp == -1 ) $tstamp = 0;
		$feed['rss']->item = array();
		// if any items are fetched
		if ( isset($feed['rss']->items) ) {
			foreach ( $feed['rss']->items as $k => $v ) {
				// RSS1.0 has no times used (weird, huh?)
				if ( !isset($v['date_timestamp']) ) $v['date_timestamp'] = 0;
				// check if this item should be added to the array of items
				$nike = false;
				if ( $type == 'new' ) {
					// if lastcheck is respected
					// first check if published after last check
					if ( $v['date_timestamp'] >= $tstamp ) {
						// then check for how many should be included
						$nike = ( $feedArr['howmany'] == 0 or $counter < $feedArr['howmany'] );
					}
				} else {
					// if lastcheck is not respected
					// just check for how many should be included
					$nike = ( $feedArr['howmany'] == 0 or $counter < $feedArr['howmany'] );
				}
				// include item
				if ( $nike ) {
					// save date in sql format
					$unixtime = ( $v['date_timestamp'] > 0 ? $v['date_timestamp'] : time() );
					$v['sqldate'] =
					$v['date'] = date('Y-m-d H:i:s', $unixtime);
					$v['dateonly'] = strftime($site['dateformat'], $unixtime);
					$v['timeonly'] = strftime($site['timeformat'], $unixtime);
					$v['date'] =
					$v['datetime'] = strftime($site['datetimeformat'], $unixtime);
					// save this feed item
					$feed['rss']->items[$k] =
					$feed['rss']->item[] = $v;
					$counter++;
				}
			}
			reset($feed['rss']->items);
		}
		// if image is found
		if ( isset($feed['rss']->image) ) {
			// if image url is found
			if ( isset($feed['rss']->image['url']) ) {
				$props = array();
				$props[] = "src=\"{$feed['rss']->image['url']}\"";
				if ( isset($feed['rss']->image['width']) ) $props[] = "width=\"{$feed['rss']->image['width']}\"";
				if ( isset($feed['rss']->image['height']) ) $props[] = "height=\"{$feed['rss']->image['height']}\"";
				if ( isset($feed['rss']->image['description']) ) $props[] = "title=\"{$feed['rss']->image['description']}\"";
				if ( isset($feed['rss']->image['title']) ) $props[] = "alt=\"{$feed['rss']->image['title']}\"";
				$props = implode(" ", $props);
				$feed['rss']->image['tag'] = "<img $props />";
				// if link for image is found
				if ( isset($feed['rss']->image['link']) ) {
					$props = array();
					$props[] = "href=\"{$feed['rss']->image['link']}\"";
					$props[] = "target=\"_blank\"";
					if ( isset($feed['rss']->image['description']) ) $props[] = "title=\"{$feed['rss']->image['description']}\"";
					$props = implode(" ", $props);
					$feed['rss']->image['taglink'] = "<a $props>{$feed['rss']->image['tag']}</a>";
				}
			}
		}
	} else {
		if ( $cid and $mid and $action == 'send' ) {
			$cname = ac_sql_select_one("name", "#campaign", "id = '$cid'");
			$from_name = $site['site_name'];
			$admin = ac_admin_get_totally_unsafe(1);
			$from_mail = $admin['email'];
			$body = sprintf(_a("I was unable to send the campaign %s as the RSS feed that was included could not be reached or had an error. Please check your RSS feed and try sending again."), $cname);
			global $MAGPIE_ERROR;
			//if ( $MAGPIE_ERROR ) $body .= "\n\n" . sprintf(_a("Reason for failing: %s"), $MAGPIE_ERROR);
			$subject = sprintf(_a("%s Sending Failure"), $cname);
			$email = ac_sql_select_one("fromemail", "#message", "id = '$mid'");
			$to_name = _a("Campaign Sender");
			$options = array();
			ac_mail_send("text", $from_name, $from_mail, $body, $subject, $email, $to_name, $options);
			if ( $MAGPIE_ERROR ) $body .= "\n\n" . sprintf(_a("Reason for failing: %s"), $MAGPIE_ERROR);
		}
	}
	// save the found counter
	$GLOBALS['activerss_items_found'] += $counter;
	$feed['found_count'] = $counter;
	// save this feed for personalization
	$feedArr['lctstamp'] = $tstamp;
	$feed['feed'] = $feedArr;
	return $feed;
}

function activerss_replace($feed, $tpl, $filter = '', $html = true) {
	if ( $filter ) $filter = trim($filter, ':') . ':';
	preg_match_all('/%RSS:' . $filter . '([^%]*)%/', $tpl, $matches);
	if ( count($matches[0]) == 0 ) return $tpl;
	foreach ( $matches[1] as $k => $v ) {
		$val = activerss_tag($feed, 'RSS:' . $v, $html);
		$tpl = str_replace($matches[0][$k], $val, $tpl);
	}
	return $tpl;
}

function activerss_replace_items($feed, $tpl, $limit, $html = true) {
	$r = '';
	if ( count($feed['rss']->item) == 0 ) return $r;
	$filter = 'ITEM:';
	preg_match_all('/%RSS:ITEM:([^%]*)%/', $tpl, $matches);
	if ( count($matches[0]) == 0 ) return str_repeat($tpl, count($feed['rss']->item));
	$i = 0;
	foreach ( $feed['rss']->item as $key => $item ) {
		if ( $i < $limit or $limit == 0 ) {
			$str = $tpl;
			foreach ( $matches[1] as $k => $v ) {
				$val = activerss_tag($feed, 'RSS:ITEM:' . $key . ':' . $v, $html);
				$str = str_replace($matches[0][$k], $val, $str);
			}
			$r .= $str;
			//$r .= activerss_replace($feed, $tpl, $filter . $key . ':', $html);
		}
		$i++;
	}
	return $r;
}

function activerss_tag($feed, $tag, $html) {
	$arr = explode('|', $tag);
	if ( !isset($arr[1]) ) $arr[1] = 0;
	list($tag, $shorten) = $arr;
	$val = activerss_tag_value($feed, $tag);
	if ( !$html ) $val = trim(strip_tags($val));
	$stripTags = ( substr($shorten, 0, 1) != '*' );
	if ( !$stripTags ) {
		$shorten = substr($shorten, 1);
	}
	if ( $shorten = (int)$shorten ) {
		if ( $html and $stripTags ) $val = trim(strip_tags($val));
		$val = ac_str_shorten($val, (int)$shorten);
	}
	return $val;
}

function activerss_tag_value($item, $ourtag) {
	$tags = explode(':', $ourtag);
	$r = activerss_tag_value_recursive($item, $tags);
	// if returned other than a string
	if ( is_object($r) ) $r = get_object_vars($r);
	// get first string in array
	while ( is_array($r) ) {
		reset($r);
		$r = current($r);
		if ( is_object($r) ) $r = get_object_vars($r);
	}
	return (string)$r;
}

function activerss_tag_value_recursive($item, $tags) {
	// if object, convert to array
	if ( is_object($item) ) $item = get_object_vars($item);
	// if not an array, we reached the value
	if ( !is_array($item) ) return (string)$item;
	// if no more tags, we reached the value
	if ( count($tags) == 0 ) return $item; // return array here!
	// get current tag to find
	$tag = array_shift($tags);
	// if tag doesn't exist
	if ( !isset($item[$tag]) ) {
		// try uppercased versions
		$item = array_change_key_case($item, CASE_UPPER);
		$tag  = strtoupper($tag);
	}
	// if tag STILL doesn't exist
	if ( !isset($item[$tag]) ) {
		return '';
	}
	// found a tag, go deeper
	return activerss_tag_value_recursive($item[$tag], $tags);
}

function activerss_useragent($subscribers = 0) {
	//require(ac_admin('functions/versioning.php'));
	$name = _i18n('ActiveCampaign Email Marketing');
	$url  = _i18n('http://www.activecampaign.com/');
	//$ua = 'MagpieRSS/'. MAGPIE_VERSION . ' (+http://magpierss.sf.net';
	$ua = "$name (+$url";
	//$ua = "$name/$thisVersion (+$url";
	if ( $subscribers ) $ua .= "; $subscribers subscribers";
	if ( defined("MAGPIE_CACHE_ON") && !MAGPIE_CACHE_ON ) $ua .= '; No cache';
	$ua .= ')';
	return $ua;
}

function activerss_checkfeed($url) {
	$url = trim(ac_b64_decode($url));
	if ( !ac_str_is_url($url) ) {
		return ac_ajax_api_result(false, _a("RSS feed URL is not valid."));
	}

	//$feed = @ac_rss_fetch($url);
	$feed = @activerss_fetch($url, 0, 'all', 0, 0, 'test');
	// if feed is fetched
	if ( !$feed['rss'] ) {
		return ac_ajax_api_result(false, _a("URL provided is not a valid RSS feed."), $feed);
	}

	if ( isset($feed['rss']->item) && $feed['rss']->item ) {
		reset($feed['rss']->item);
		$item = current($feed['rss']->item);
	} elseif ( isset($feed['rss']->items) && $feed['rss']->items ) {
		reset($feed['rss']->items);
		$item = current($feed['rss']->items);
	} else {
		$item = array();
	}
	$samples = array(
		'channel' => $feed['rss']->channel,
		'item' => $item,
		'image' => isset($feed['rss']->image) && $feed['rss']->image ? $feed['rss']->image : array(),
	);

	$tags = array();
	foreach ( $samples as $section => $sample ) {
		$tags[$section] = array();
		$pers = activerss_gettags($sample);
		foreach ( $pers as $tag ) {
			$key = strtoupper("%RSS:$section:$tag%");
			$val = activerss_tag_value($sample, $tag);
			$tags[$section][] = array('key' => $key, 'val' => $val);
		}
	}

	$r = array(
		'items' => isset($feed['rss']->items) ? count($feed['rss']->items) : 0,
		'tags' => $tags,
		'url' => $feed['url'],
	);

	return ac_ajax_api_result(true, _a("RSS feed successfully fetched."), $r);
}

function activerss_gettags($row) {
	$r = array();
	foreach ( $row as $k => $v ) {
		$r[] = $k;
		if ( is_array($v) ) {
			foreach ( $v as $v1 => $v2 ) {
				$r[] = $k.':'.$v1;
			}
		}
	}
	return $r;
}

?>
