<?php

function mpma_macro_error($str) {
	echo nl2br("<span style='color:red;font-weight:bold;'>$str\n</span>");
}

function mpma_macro_queries($arr) {
	foreach ( $arr as $key => $qry ) {
		echo nl2br(strtoupper($key) . ":\n");
		ac_mpma_query($qry);
		echo ac_mpma_response();
	}
}

function _macro_campaign($id = 0) {
	$id = (int)$id;
	if ( !$id ) {
		return mpma_macro_error("ERROR: Campaign ID not provided.");
	}
	$sid = (int)ac_sql_select_one("id", "#campaign_count", "campaignid = '$id' ORDER BY id DESC");
	$queries = array(
		"CAMPAIGN INFO" => "SELECT * FROM #campaign         WHERE id = '$id'",
		"LISTS"         => "SELECT * FROM #campaign_list    WHERE campaignid = '$id'",
		"COUNTS"        => "SELECT * FROM #campaign_count   WHERE campaignid = '$id'",
		"MESSAGES"      => "SELECT * FROM #campaign_message WHERE campaignid = '$id'",
		"PROCESS"       => "SELECT * FROM #process          WHERE id = ( SELECT MAX(processid) FROM #campaign_count WHERE campaignid = '$id' )",
		"ERRORS"        => "
			SELECT
				id,
				tstamp,
				errnumber,
				errmessage,
				filename,
				url,
				linenum,
				session,
				userid,
				ip,
				host,
				referer
			FROM
				#trapperrlogs
			WHERE
				`url` LIKE CONCAT('%/process.php?id=', ( SELECT MAX(processid) FROM #campaign_count WHERE campaignid = '$id' ))
		",
		"TOTAL IN X"    => "SELECT COUNT(*) FROM #x$sid",
		"UNSENT IN X"   => "SELECT COUNT(*) FROM #x$sid WHERE sent = 0",
		"4WINNER IN X"  => "SELECT COUNT(*) FROM #x$sid WHERE messageid = 0",
	);
	mpma_macro_queries($queries);
}

function _macro_subscriberid($id = 0) {
	$id = (int)$id;
	if ( !$id ) {
		return mpma_macro_error("ERROR: Subscriber ID not provided.");
	}
	$queries = array(
		"SUBSCRIBER INFO" => "SELECT * FROM #subscriber           WHERE id           = '$id'",
		"LISTS"           => "SELECT * FROM #subscriber_list      WHERE subscriberid = '$id'",
		"FIELDS_NEW"      => "SELECT * FROM #field_value          WHERE relid        = '$id'",
		"FIELDS_OLD"      => "SELECT * FROM #list_field_value     WHERE relid        = '$id'",
		"SENT RESPONDERS" => "SELECT * FROM #subscriber_responder WHERE subscriberid = '$id'",
	);
	mpma_macro_queries($queries);
}

function _macro_subscriberhash($hash = '') {
	if ( !$hash ) {
		return mpma_macro_error("ERROR: Subscriber hash not provided.");
		return;
	}
	$esc = ac_sql_escape($hash);
	$queries = array(
		"SUBSCRIBER INFO" => "SELECT * FROM #subscriber           WHERE hash         = '$esc'",
		"LISTS"           => "SELECT * FROM #subscriber_list      WHERE subscriberid = ( SELECT id FROM #subscriber WHERE hash = '$esc' LIMIT 0, 1 )",
		"FIELDS_NEW"      => "SELECT * FROM #field_value          WHERE relid        = ( SELECT id FROM #subscriber WHERE hash = '$esc' LIMIT 0, 1 )",
		"FIELDS_OLD"      => "SELECT * FROM #list_field_value     WHERE relid        = ( SELECT id FROM #subscriber WHERE hash = '$esc' LIMIT 0, 1 )",
		"SENT RESPONDERS" => "SELECT * FROM #subscriber_responder WHERE subscriberid = ( SELECT id FROM #subscriber WHERE hash = '$esc' LIMIT 0, 1 )",
	);
	mpma_macro_queries($queries);
}

function _macro_subscriberemail($email = '') {
	if ( !$email or !ac_str_is_email($email) ) {
		return mpma_macro_error("ERROR: Subscriber email not provided.");
		return;
	}
	$esc = ac_sql_escape($email);
	$queries = array(
		"SUBSCRIBER INFO" => "SELECT * FROM #subscriber           WHERE email        = '$esc'",
		"LISTS"           => "SELECT * FROM #subscriber_list      WHERE subscriberid = ( SELECT id FROM #subscriber WHERE email = '$esc' LIMIT 0, 1 )",
		"FIELDS_NEW"      => "SELECT * FROM #field_value          WHERE relid        = ( SELECT id FROM #subscriber WHERE email = '$esc' LIMIT 0, 1 )",
		"FIELDS_OLD"      => "SELECT * FROM #list_field_value     WHERE relid        = ( SELECT id FROM #subscriber WHERE email = '$esc' LIMIT 0, 1 )",
		"SENT RESPONDERS" => "SELECT * FROM #subscriber_responder WHERE subscriberid = ( SELECT id FROM #subscriber WHERE email = '$esc' LIMIT 0, 1 )",
	);
	mpma_macro_queries($queries);
}

function _macro_campaign_undelete($id = 0) {
	$id = (int)$id;
	if ( !$id ) {
		return mpma_macro_error("ERROR: Campaign ID not provided.");
	}
	$queries = array(
		"ADDING"           => "INSERT INTO #campaign SELECT * FROM #campaign_deleted WHERE id = '$id'",
		"REMOVING"         => "DELETE FROM #campaign_deleted WHERE id = '$id';",
	);
	mpma_macro_queries($queries);
}

function _macro_filter($id = 0) {
	$id = (int)$id;
	if ( !$id ) {
		return mpma_macro_error("ERROR: Filter ID not provided.");
	}
	$queries = array(
		"FILTER INFO"     => "SELECT * FROM #filter            WHERE id       = '$id'",
		"FILTER GROUPS"   => "SELECT * FROM #filter_group      WHERE filterid = '$id'",
		"FILTER CONDS"    => "SELECT * FROM #filter_group_cond WHERE filterid = '$id'",
	);
	mpma_macro_queries($queries);
}

function _macro_form($id = 0) {
	$id = (int)$id;
	if ( !$id ) {
		return mpma_macro_error("ERROR: Form ID not provided.");
	}
	$queries = array(
		"FORM"          => "SELECT * FROM #form WHERE id = '$id'",
		"PARTS"         => "SELECT * FROM #form_part WHERE formid = '$id'",
		"LISTS"         => "SELECT * FROM #form_list WHERE formid = '$id'",
	);
	mpma_macro_queries($queries);
}

?>