<?php

// fetch the remote data
function socialsharing_data_fetch_read($campaignid, $messageid, $socialshare_url, $bitly_urls, $source = null) {
	$facebook = $twitter = array();
	switch ($source) {
		default :
		case "facebook" :
			// if it's not in em_bitly yet, that means no one has clicked on the share link yet
			if ( !isset($bitly_urls["facebook"]) || $bitly_urls["facebook"] == "" ) {
				require_once ac_global_functions("twit.php");
				$bitly_urls["facebook"] = ac_bitly($socialshare_url . "&ref=facebook");
				// if ac_bitly() fails, use the social share URL
				if (!$bitly_urls["facebook"]) $bitly_urls["facebook"] = $socialshare_url . "&ref=facebook";
				// check again if it exists
				$bitly_exists = ac_sql_select_one("=COUNT(*)", "#bitly", "campaignid = '$campaignid' AND messageid = '$messageid' AND ref = 'facebook'");
				if (!$bitly_exists) {
					// save it so lt.php doesn't have to (when someone clicks on the share link)
					$insert = array(
						"id" => 0,
						"campaignid" => $campaignid,
						"messageid" => $messageid,
						"ref" => "facebook",
						"bitly" => $bitly_urls["facebook"],
					);
					ac_sql_insert("#bitly", $insert);
				}
			}
			$facebook_data["facebook_bitly"] = $bitly_urls["facebook"];
			$shares = 0;
			$data_user = array();
			if ($GLOBALS["site"]["facebook_app_id"] != "" && $GLOBALS["site"]["facebook_app_secret"] != "") {
				$facebook = socialsharing_facebook_oauth_init();
				$facebook_session = socialsharing_facebook_oauth_getsession($facebook);
				$socialshare_url_seo = $GLOBALS["site"]["p_link"] . "/social/" . md5($campaignid) . "." . $messageid . "/like";
				$socialshare_url_facebook = $socialshare_url . "&ref=facebook";
			  $fql = "SELECT share_count, like_count, comment_count, total_count FROM link_stat WHERE url IN ('$bitly_urls[facebook]','$socialshare_url_facebook')";
				$param = array(
					"method" => "fql.query",
					"query" => $fql,
					"callback" => "",
				);
				$data_totals = $facebook->api($param);
				if (!$data_totals) {
					// I've seen it's an empty string sometimes; not sure why
					$data_totals = array();
				}
				elseif ( !is_array($data_totals[0]) ) {
					// I've seen it's an object sometimes; not sure why
					array_map("get_object_vars", $data_totals);
				}
				// not including "comment_count" right now - just want to add up "share_count" and "like_count"
				$share_count = 0;
				$like_count = 0;
				// we pass more than one link (bitly + web/social copy) so each is returned as its own separate array of totals
				foreach ($data_totals as $link_totals) {
					$share_count += $link_totals["share_count"];
					$like_count += $link_totals["like_count"];
				}
				$shares = $share_count + $like_count;
				//$data_user = $facebook->api("/search?q=" . urlencode($bitly_urls["facebook"]));
				//dbg($data_user);
			}
			$facebook_data["facebook_shares"] = $shares;
			$facebook_data["facebook_data"] = array();
		if ($source) break;
		case "twitter" :
			require_once ac_global_functions("twit.php");
			// if it's not in em_bitly yet, that means no one has clicked on the share link yet
			if ( !isset($bitly_urls["twitter"]) || $bitly_urls["twitter"] == "" ) {
				$bitly_urls["twitter"] = ac_bitly($socialshare_url . "&ref=twitter");
				// if ac_bitly() fails, use the social share URL
				if (!$bitly_urls["twitter"]) $bitly_urls["twitter"] = $socialshare_url . "&ref=twitter";
				// check again if it exists
				$bitly_exists = ac_sql_select_one("=COUNT(*)", "#bitly", "campaignid = '$campaignid' AND messageid = '$messageid' AND ref = 'twitter'");
				if (!$bitly_exists) {
					// save it so lt.php doesn't have to (when someone clicks on the share link)
					$insert = array(
						"id" => 0,
						"campaignid" => $campaignid,
						"messageid" => $messageid,
						"ref" => "twitter",
						"bitly" => $bitly_urls["twitter"],
					);
					ac_sql_insert("#bitly", $insert);
				}
			}
			$twitter_data["twitter_bitly"] = $bitly_urls["twitter"];
			$search = array(
				$bitly_urls["twitter"],
				$socialshare_url . "&ref=twitter"
			);
//dbg($search);
			$mentions = ac_twit_api_search($search);
			$mentions_total = 0;
//dbg($mentions);
			$data = array();
			if ($mentions && isset($mentions->results) && $mentions->results) {
				foreach ($mentions->results as $mention) {
					$mention = get_object_vars($mention);
//dbg($mention);
					$mention_link = "http://twitter.com/" . $mention["from_user"] . "/status/" . $mention["id_str"];
					$mention_author = array(
						"id" => $mention["from_user_id_str"],
						"user" => $mention["from_user"],
						"justname" => $mention["from_user_name"],
						"profile_image" => $mention["profile_image_url"],
					);

					$continue = false;
					if ( isset($mention["entities"]) ) {
						if ( isset($mention["entities"]->urls) ) {
							foreach ($mention["entities"]->urls as $url) {
								// look for a case-sensitive match of the bitly URL
								$bitly_match_sensitive = preg_match("|" . $bitly_urls["twitter"] . "|", $url->expanded_url);
								if ($bitly_match_sensitive) {
									// if we have found the exact bitly URL within the Twitter post content, then we know to proceed
									$continue = true;
								}
							}
						}
					}

					if ($continue) {
						$mentions_total++;
						$mention = array(
							"itemid" => $mention["id_str"],
							"published" => $mention["created_at"],
							"link" => $mention_link,
							"image" => $mention["profile_image_url"],
							"title" => $mention["text"],
							"content" => $mention["text"],
							"author" => $mention_author,
						);
						$data[] = $mention;
					}
				}
			}
			$twitter_data["twitter_mentions"] = $mentions_total; // limited by pagination
			$twitter_data["twitter_data"] = $data;
		if ($source) break;
	}
	$r = array_merge($facebook_data, $twitter_data);
//dbg($r);
	return $r;
}

// fetch the cached data
function socialsharing_data_cache_read($so, $campaignid, $source = null, $export = false) {
	if (!$so) $so = new AC_Select;
	if ($source) {
		// get the actual cached data to display in paginator
		if ($source != "all") $so->push("AND s.source = '$source'");
		$rows = socialsharing_select_array($so, null, $campaignid);
		require_once(ac_global_functions('process.php'));
		foreach ($rows as $k => $v) {
			if ($v["source"] == "twitter") {
				$rows[$k]["data"] = ac_str_unserialize($v["data"]);
			}
			if ($v["subscriberid"]) {
				$rows[$k]["subscriber"] = subscriber_select_row($v["subscriberid"]);
				$rows[$k]["subscriber"]["md5email"] = md5($rows[$k]["subscriber"]["email"]);
			}
		}
		$r["source"] = $source;
		$r["data"] = $rows;
		$r["total"] = count($rows);
		if ($export) {
			$export_rows = array();
			foreach ($rows as $item) {
				$export_row = array();
				$export_row["source"] = $item["source"];
				if ($item["source"] == "facebook") {
					$export_row["name"] = ( isset($item["subscriber"]["name"]) ) ? $item["subscriber"]["name"] : _a("Undefined");
					$export_row["content"] = $item["data"];
					$export_row["published"] = $item["cdate"];
				}
				elseif ($item["source"] == "twitter") {
					$export_row["name"] = $item["data"]["author"]["name"];
					$export_row["content"] = $item["data"]["content"];
					$export_row["published"] = $item["cdate"];
				}
				$export_rows[] = $export_row;
			}
			return $export_rows;
		}
	}
	else {
		// just get totals for the top part, "Facebook Shares" and "Twitter Mentions" toggle links
		$so->slist = array(
			"SUM(s.source = 'facebook') AS facebook_total",
			"SUM(s.source = 'twitter') AS twitter_total",
		);
		$r = ac_sql_select_row(socialsharing_select_query($so, $campaignid));
		$total_campaign_socialshare = ac_sql_select_one("SELECT socialshares FROM #campaign WHERE id = '$campaignid'");
		$total_cached_data = ac_sql_select_one("SELECT COUNT(*) FROM #socialshare WHERE campaignid = '$campaignid'");
		$total_socialshare_facebook_external = 0;
		if ($total_campaign_socialshare > $total_cached_data) {
			// find out how many pertain to facebook (shared both within, and outside of our interface)
			// the remaining must be facebook, since we obtain twitter directly from api, then cache
			$total_socialshare_facebook = $total_campaign_socialshare - $r["twitter_total"];
			// take remaining share count, and subtract the cached facebook total, and we are left with ONLY the count that is external facebook shares (outside of our UI)
			$total_socialshare_facebook_external = $total_socialshare_facebook - $r["facebook_total"];
		}
		// the below is all shares that occurred on Facebook, but not through our UI. so if I just copy the bitly link and paste into facebook to share
		$r["total_socialshare_facebook_external"] = $total_socialshare_facebook_external;
	}
	return $r;
}

// fetch the remote data, then cache it to database
function socialsharing_data_cache_write($campaignid, $messageid, $socialshare_url) {
	$bitly_urls = ac_sql_select_box_array("SELECT ref, bitly FROM #bitly WHERE campaignid = '$campaignid'");
	$data = socialsharing_select_totals($campaignid, $messageid, $socialshare_url, $bitly_urls);

	// adjust social report counts - this is after we fetch the remote data
	$total_remote = $total_socialshares = $data["facebook_total"] + $data["twitter_total"];
	$total_cached_data = (int)ac_sql_select_one("SELECT COUNT(*) FROM #socialshare WHERE campaignid = '$campaignid'");
	// we use $total_remote by default (total of all facebook and twitter shares)
	// unless for some reason total_remote is LESS THAN what we have cached, then use the cached total
	if ($total_remote < $total_cached_data) {
		$total_socialshares = $total_cached_data;
	}
	ac_sql_query("UPDATE #campaign SET socialshares = $total_socialshares WHERE id = '$campaignid'");
	ac_sql_query("UPDATE #campaign_message SET socialshares = $total_socialshares WHERE campaignid = '$campaignid' AND messageid = '$messageid'");

	$insert = array(
		"campaignid" => $campaignid,
	);
	// there currently is no facebook data being returned, other than counts - so it should never get in this foreach
	foreach ($data["facebook_data"] as $share) {
		$insert["source"] = "facebook";
		$insert["accountid"] = $share["itemid"];
		$insert["=cdate"] = "NOW()";
		$insert["pdate"] = $share["published"];
		$insert["data"] = serialize($share["data"]);
		$exists = ac_sql_select_row("SELECT * FROM #socialshare WHERE source = 'facebook' AND accountid = '$share[itemid]'");
		if (!$exists) {
			ac_sql_insert("#socialshare", $insert);
			$did = (int)ac_sql_insert_id();
			stats_activity_log('facebook', 0, $campaignid);
		}
	}
	//dbg( $data["twitter_data"] );
	foreach ($data["twitter_data"] as $mention) {
		$insert["source"] = "twitter";
		$insert["accountid"] = $mention["itemid"];
		$insert["=cdate"] = "NOW()";
		$insert["pdate"] = $mention["published"];
		$insert["data"] = serialize($mention);
		$exists = ac_sql_select_row("SELECT * FROM #socialshare WHERE source = 'twitter' AND accountid = '$mention[itemid]'");
		if (!$exists) {
			ac_sql_insert("#socialshare", $insert);
			$did = (int)ac_sql_insert_id();
			stats_activity_log('twitter', 0, $campaignid, $did);
		}
	}
	return $data;
}

function socialsharing_select_totals($campaignid, $messageid, $socialshare_url = null, $bitly_urls = array()) {
	$campaignid = intval($campaignid);
	if (!$campaignid) return ac_ajax_api_result(false, _a("Campaign ID cannot be 0."));
	$messageid  = intval($messageid);
	$table      = "#campaign";
	$cond       = "id = '$campaignid'";

	$site = ac_site_get();

	if ($messageid > 0) {
		$table = "#campaign_message";
		$cond  = "messageid = '$messageid' AND campaignid = '$campaignid'";
	}

	$total_amt = 0;
	$webcopy = "";
	$bitly_urls = array();
	if ($campaignid) {
		/*
		$campaign = ac_sql_select_row("
			SELECT
				total_amt,
				socialshares
			FROM
				$table
			WHERE
				$cond
		");
		*/
		$campaign = campaign_select_row($campaignid, true);

		// get the Facebook session access tokens (for each list associated with this campaign), the bitly URL, and social share URL
		$facebook_sessions = array();
		foreach ($campaign["lists"] as $list) {
			// get facebook sessions (stored as serialized array)
			if ($list["facebook_session"]) {
				$facebook_session = unserialize($list["facebook_session"]);
				if (is_object($facebook_session)) $facebook_session = get_object_vars($facebook_session);
				if (isset($facebook_session["access_token"]) && $facebook_session["access_token"]) {
					$facebook_sessions[ $list["id"] ] = $facebook_session["access_token"];
				}
			}
		}
		$campaign["facebook_access_tokens"] = $facebook_sessions;
		if ( isset($campaign["messages"]) && isset($campaign["messages"][0]) ) {
			$webcopy = $site["p_link"] . "/index.php?action=social&c=" . md5($campaign["id"]) . "." . $campaign["messages"][0]["id"];
		}
		$bitly_urls = ac_sql_select_box_array("SELECT ref, bitly FROM #bitly WHERE campaignid = '$campaign[id]'");
		$bitly_urls = $bitly_urls;
	}

	// fetch the remote data
	if ($socialshare_url) {
		$data = socialsharing_data_fetch_read($campaignid, $messageid, $socialshare_url, $bitly_urls);
		$r = array(
			"total_amt_sent" => $campaign["total_amt"],
			"facebook_total" => $data["facebook_shares"],
			"facebook_data" => $data["facebook_data"],
			"facebook_bitly" => $data["facebook_bitly"],
			"twitter_total" => $data["twitter_mentions"],
			"twitter_data" => $data["twitter_data"],
			"twitter_bitly" => $data["twitter_bitly"],
		);
	}
	else {
		// get the cached data - in this case we are just getting totals to display along the top of the "Social Sharing" campaign reports page
		$data = socialsharing_data_cache_read(null, $campaignid);
		$facebook_total = ($data["facebook_total"]) ? $data["facebook_total"] : 0;
		$twitter_total = ($data["twitter_total"]) ? $data["twitter_total"] : 0;
		$r = array(
			"total_amt_sent" => $campaign["total_amt"],
			"total_shares" => $campaign["socialshares"],
			"facebook_total" => $facebook_total,
			"twitter_total" => $twitter_total,
			"total_socialshare_facebook_external" => $data["total_socialshare_facebook_external"],
		);
	}

	// return this in array, so it's easy to find out what the values are (debugging)
	$r["webcopy_url"] = $webcopy;
	$r["bitly_urls"] = $bitly_urls;

	return $r;
}

function socialsharing_select_query(&$so, $campaignid = 0) {
	if ( !ac_admin_ismain() ) {
		$admin = ac_admin_get();
		if ( $admin['id'] != 1 ) {
			if ( !isset($so->permsAdded) ) {
				$so->permsAdded = 1;
				$so->push("AND s.campaignid IN (SELECT cl.campaignid FROM #campaign_list cl WHERE cl.listid IN ('" . implode("', '", $admin['lists']) . "'))");
			}
		}
	}

	if ($campaignid > 0)
		$so->push("AND s.campaignid = '$campaignid'");

	return $so->query("
		SELECT
			*
		FROM
			#socialshare s
		WHERE
			[...]
	");
}

function socialsharing_select_array($so = null, $ids = null, $campaignid = 0) {
	if ($so === null || !is_object($so))
		$so = new AC_Select;

	if ($ids !== null) {
		if ( !is_array($ids) ) $ids = explode(',', $ids);
		$tmp = array_diff(array_map("intval", $ids), array(0));
		$ids = implode("','", $tmp);
		$so->push("AND f.id IN ('$ids')");
	}
	return ac_sql_select_array(socialsharing_select_query($so, $campaignid));
}

function socialsharing_select_array_paginator($id, $sort, $offset, $limit, $filter, $campaignid = 0, $source = null) {
	$admin     = ac_admin_get();
	$so        = new AC_Select;

	$filter = intval($filter);
	if ($filter > 0) {
		$conds = ac_sql_select_one("SELECT conds FROM #section_filter WHERE id = '$filter' AND userid = '$admin[id]' AND sectionid = 'socialsharing'");
		$so->push($conds);
	}

	switch ($sort) {
		default:
		case "01":
			$so->orderby("cdate"); break;
		case "01D":
			$so->orderby("cdate DESC"); break;
	}

	$limit  = (int)$limit;
	$offset = (int)$offset;
	$so->limit("$offset, $limit");
	$data = socialsharing_data_cache_read($so, $campaignid, $source);
	$total = ac_sql_select_one("SELECT COUNT(*) FROM #socialshare WHERE campaignid = '$campaignid'");
	$rows = $data["data"];

	return array(
		"source"      => $source,
		"paginator"   => $id,
		"offset"      => $offset,
		"limit"       => $limit,
		"total"       => $total,
		"cnt"         => count($rows),
		"rows"        => $rows,
	);
}

// api
function socialsharing_select_list($campaignid, $messageid = array()) {
	$so = new AC_Select;
	if ($messageid) {
		$so->push( "AND s.campaignid IN (SELECT campaignid FROM #campaign_message WHERE messageid IN ('" . implode("', '", $messageid) . "'))" );
	}
	return socialsharing_select_array($so, null, $campaignid);
}

function socialsharing_filter_post() {
	$whitelist = array("s.cdate", "s.data");

	$ary = array(
		"userid" => $GLOBALS['admin']['id'],
		"sectionid" => "socialsharing",
		"conds" => "",
		"=tstamp" => "NOW()",
	);

	if (isset($_POST["qsearch"]) && !isset($_POST["content"])) {
		$_POST["content"] = $_POST["qsearch"];
	}

	if (isset($_POST["content"]) and $_POST['content'] != '') {
		$content = ac_sql_escape($_POST["content"], true);
		$conds = array();

		if (!isset($_POST["section"]) || !is_array($_POST["section"]))
			$_POST["section"] = $whitelist;

		foreach ($_POST["section"] as $sect) {
			if (!in_array($sect, $whitelist)) {
				continue;
			}
			$conds[] = "$sect LIKE '%$content%'";
		}

		$conds = implode(" OR ", $conds);
		$ary["conds"] = "AND ($conds) ";
	}

	if ( $ary['conds'] == '' ) return array("filterid" => 0);

	$conds_esc = ac_sql_escape($ary['conds']);
	$filterid = ac_sql_select_one("
		SELECT
			id
		FROM
			#section_filter
		WHERE
			userid = '$ary[userid]'
		AND
			sectionid = 'socialsharing'
		AND
			conds = '$conds_esc'
	");

	if (intval($filterid) > 0)
		return array("filterid" => $filterid);
	ac_sql_insert("#section_filter", $ary);
	return array("filterid" => ac_sql_insert_id());
}

function socialsharing_process_link($c, $m, $s, $url, $link = null) {
  $site = ac_site_get();
	if (!$link) {
		// $link will be empty when calling from spots that display social share icons that are NOT within the campaign message source
		// in this situation, they haven't gone through lt.php
		// $url at this point is the $webcopy URL, WITH "ref=whatever" on the end
		$link = array('link' => $url);
	}
	else {
		// coming from lt.php
		// first replace the hashes
		$link['link'] = str_replace('cmpgnhash', md5($c), $link['link']);
		$link['link'] = str_replace('cmpgnid', $c, $link['link']);
		$link['link'] = str_replace('currentmesg', $m, $link['link']);
		$link['link'] = str_replace('subscriberid', $s, $link['link']);
	}
  if ($s) $s = subscriber_exists($s, 0, 'hash');
	$facebook_like_link = false;
	// if it's the facebook "like" button that was clicked, we still need to obtain the facebook share link
	if ( ac_str_instr('&facebook=like', $link['link']) ) {
		$facebook_like_link = true;
		// convert it to the standard facebook share link, so we can get the bitly for it (for use in facebook "like" iframe)
		$link['link'] = str_replace('&facebook=like', '&ref=facebook', $link['link']);
	}
	// find ref
	// match any occurrence of "&ref=whatever" or "&referral=whatever"
	$param_str = preg_match("/[?&]+ref[a-z]*=[a-z]+/", $link['link'], $matches);
	if ( isset($matches[0]) ) list(,$ref) = explode('=', $matches[0]);
	if (!isset($ref) || !$ref) return;
	// capture social.php links - convert to webcopy URL, so bitly goes to right place
	if ( ac_str_instr('/social.php?c=', $link['link']) ) {
	  $link['link'] = $site['p_link'] . "/index.php?action=social&c=" . md5($c) . "." . $m . "&ref=" . $ref;
	}
	// find bitly
	$bitly = (int)ac_sql_select_one("=COUNT(*)", "#bitly", "campaignid = '$c' AND messageid = '$m' AND ref = '$ref'");
	if (!$bitly) {
		require_once(ac_global_functions('twit.php'));
		// get new bitly
		$bitly = ac_bitly($link['link']);
		// ac_bitly() can return false (might not connect to bitly server), so make sure there is SOME link here
		if (!$bitly) $bitly = $link["link"];
		// save it
		$insert = array(
			'id' => 0,
			'campaignid' => $c,
			'messageid' => $m,
			'ref' => $ref,
			'bitly' => $bitly,
		);
		ac_sql_insert("#bitly", $insert);
	}
	else {
		// grab the actual bitly URL from the table
		$bitly = ac_sql_select_one("bitly", "#bitly", "campaignid = '$c' AND messageid = '$m' AND ref = '$ref'");
	}

	// facebook "like" link does not get redirected to the external site - we remain on the web copy page (a modal pops up instead).
	// so further below, we don't assign a new $url - we leave that as is, and we change the $ref to 'facebook' (which we return),
	// so we know that a facebook "like" is the same thing as a facebook share, and is captured with subscriber actions, etc
	if ($facebook_like_link) $ref = "facebook_like";

	// get subject
	$subject = ac_sql_select_one("subject", "#message", "id = '$m'");
	//if ( !$subject ) $subject = $campaign['name'];
	$subjectesc = rawurlencode($subject);
	$shared_verbiage = "";

	// assign bitly URL instead
	$bitlyesc = rawurlencode($bitly);
	switch ( $ref ) {
		case 'facebook':
			$url = "http://www.facebook.com/share.php?u=$bitlyesc";
			if ( $subject ) $url .= "&t=$subjectesc";
			$shared_verbiage = "shared";
		case 'facebook_like':
			// don't re-assign $url in this case.
			// just change the $ref BACK to facebook ($ref is what we return at the end of this function),
			// so later on during subscriber_action_dispatch, the "like" is also treated as a facebook share
			$ref = 'facebook';
			if (!$shared_verbiage) $shared_verbiage = "liked";
			// $s can be 0 if coming from report_campaign.php context (and possibly other places) - in this case we just need the actual external URL's, and we're
			// not processing a subscriber's click. so if we just need the URL's, don't bother inserting into database
			if ($s) {
				require_once ac_admin("functions/subscriber.select.php");
				$subscriber = subscriber_select_row($s["id"]);
				// store this occurrence into #socialshare so we can display on reports
				$exists = ac_sql_select_row("SELECT * FROM #socialshare WHERE campaignid = '$c' AND subscriberid = '$subscriber[id]' AND source = 'facebook'");
				if (!$exists) {
					$insert["campaignid"] = $c;
					$insert["subscriberid"] = $subscriber["id"];
					$insert["accountid"] = "";
					$insert["source"] = "facebook";
					$insert["=cdate"] = "NOW()";
					$insert["=pdate"] = "NOW()";
					$insert["data"] = $subscriber["first_name"] . " " . $subscriber["last_name"] . " " . $shared_verbiage . " " . _a("this campaign on Facebook");
					$sql = ac_sql_insert("#socialshare", $insert);
					$did = (int)ac_sql_insert_id();
					stats_activity_log('facebook', $subscriber['id'], $c, $did);
				}
			}
			break;
		case 'twitter':
			$url = "http://twitter.com/share?text=" . _p('Currently reading') . "&url=$bitlyesc";
			/*
			if ($s) {
				require_once ac_admin("functions/subscriber.select.php");
				$subscriber = subscriber_select_row($s["id"]);
				// store this occurrence into #socialshare so we can display on reports
				$exists = ac_sql_select_row("SELECT * FROM #socialshare WHERE campaignid = '$c' AND subscriberid = '$susbscriber[id]' AND source = 'twitter'");
				if (!$exists) {
					$insert["campaignid"] = $c;
					$insert["subscriberid"] = $subscriber['id'];
					$insert["accountid"] = "";
					$insert["source"] = "twitter";
					$insert["=cdate"] = "NOW()";
					$insert["=pdate"] = "NOW()";
					$insert["data"] = "";
					$sql = ac_sql_insert("#socialshare", $insert);
					$did = (int)ac_sql_insert_id();
					stats_activity_log('twitter', $subscriber['id'], $c, $did);
				}
			}
			*/
			break;
		case 'digg':
			$url = "http://digg.com/submit?phase=2&url=$bitlyesc";
			if ( $subject ) $url .= "&title=$subjectesc";
			break;
		case 'delicious':
			$url = "http://del.icio.us/post?v=2&url=$bitlyesc";
			if ( $subject ) $url .= "&title=$subjectesc";
			break;
		case 'gplus':
			$url = "https://plus.google.com/share?url=$bitlyesc";
			//if ( $subject ) $url .= "&title=$subjectesc";
			break;
		case 'reddit':
			$url = "http://reddit.com/submit?url=$bitlyesc";
			if ( $subject ) $url .= "&title=$subjectesc";
			break;
		case 'stumbleupon':
			$url = "http://www.stumbleupon.com/submit?url=";
			$url .= rawurlencode($link["link"]);
			if ( $subject ) $url .= "&title=$subjectesc";
			break;
		default:
			// do nothing
	}
	return array($url, $link, $ref);
}

function socialsharing_facebook_oauth_init() {
	$site = ac_site_get();
	require_once ac_global_classes("facebook.php");
	$facebook = new Facebook( array("appId" => $site["facebook_app_id"], "secret" => $site["facebook_app_secret"], "cookie" => true) );
	return $facebook;
}

function socialsharing_facebook_oauth_getsession($init) {
	$session = null;
	// see if the cookie is set
	$session = $init->getSession();
	if ($session) {

	}
	return $session;
}

?>
