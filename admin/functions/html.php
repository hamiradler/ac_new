<?php

$GLOBALS["html_matched"] = array();

function html_savefix($source) {
	# Alter HTML in such a way as to be more friendly to email clients.

	global $html_matched;
	$rval = $source;

	if (strpos(strtolower($rval), "<body") === false) {
		$rval = "<body>" . $rval . "</body>";
	}

	# Strip out some tags.
	$rval = ac_str_strip_tag($rval, "noscript");
	$rval = ac_str_strip_tag($rval, "title");

	# float: left in images can cause problems in Microsoft Outlook 2007.
	$rval = preg_replace_callback('#<img [^>]+float:\s*left[^>]*>#im', 'html_fix_addalignleft', $rval);

	# float: right in images can cause problems in Microsoft Outlook 2007.
	$rval = preg_replace_callback('#<img [^>]+float:\s*right[^>]*>#im', 'html_fix_addalignright', $rval);

	# v/h space: spacing/margin in images can cause problems in Microsoft Outlook 2007.
	$rval = preg_replace_callback('#<img [^>]+margin-\w*:\s*\d+px;[^>]*>#im', 'html_fix_addspacing', $rval);

	if (false && strpos(strtolower($rval), "img { display: block }") === false) {
		# Make sure we have display: block in img tags (mostly for Gmail).
		# Only run this if we have never done so before.
		$rval = preg_replace_callback('#<body[^>]*>#im', 'html_fix_adddisplayblock', $rval);
	}

	return $rval;
}

function html_sendfix($source) {
	# Some last minute changes to HTML source that we don't necessarily want to commit to any saved version.
}

# Fix functions.
function html_fix_addalignleft($str) {
	if (preg_match('/align=[\'"]left/i', $str[0]))
		return $str[0];

	return preg_replace('/<img/i', "<img align='left'", $str[0]);
}

function html_fix_addalignright($str) {
	if (preg_match('/align=[\'"]right/i', $str[0]))
		return $str[0];

	return preg_replace('/<img/i', "<img align='right'", $str[0]);
}

function html_fix_adddisplayblock($str) {
	return "<style> img { display: block } </style>" . $str[0];
}

function html_fix_addspacing($str) {
	if (preg_match('/space=[\'"]/i', $str[0]))
		return $str[0];

	preg_match_all('/margin-(\w+):\s*(\d+)px;/mi', $str[0], $matches);

	$map = array();
	$val = array();
	foreach ( $matches[0] as $k => $v ) {
		//$map[$v] = array();
		$key = strtolower($matches[1][$k]);
		$map[$key] = $v;
		$val[$key] = $matches[2][$k];
	}
	if ( isset($val['top']) and isset($val['bottom']) and $val['top'] == $val['bottom'] ) {
		// vspace
		$str[0] = str_replace($map['top'], '', $str[0]);
		$str[0] = str_replace($map['bottom'], '', $str[0]);
		$str[0] = preg_replace('/<img/i', "<img vspace='$val[top]'", $str[0]);
	}
	if ( isset($val['left']) and isset($val['right']) and $val['left'] == $val['right'] ) {
		// vspace
		$str[0] = str_replace($map['left'], '', $str[0]);
		$str[0] = str_replace($map['right'], '', $str[0]);
		$str[0] = preg_replace('/<img/i', "<img hspace='$val[left]'", $str[0]);
	}

	return $str[0];
}

function html_oneperline($input) {
	// Takes any HTML elements and places one each on new line.
	$input = preg_replace_callback('#<([a-zA-Z]+)[^>]*>#im', 'html_oneperline_opencb', $input);
	$input = preg_replace_callback('#</([a-zA-Z]+)[^>]*>#im', 'html_oneperline_closecb', $input);
	return $input;
}

function html_oneperline_opencb($m) {
	return $m[0] . ($m[1] != "a" && $m[1] != "textarea" ? "\n" : "");
}

function html_oneperline_closecb($m) {
	return ($m[1] != "a" && $m[1] != "textarea" ? "\n" : "") . $m[0] . "\n";
}

function html_pprint($input) {
	// Naive pretty-print for HTML.
	$input = html_oneperline($input);
	$lines = explode("\n", $input);
	$rval = array();
	$ind = "  ";
	$inc = 0;
	$textarea = false;

	foreach ($lines as $line) {
		$line = trim($line);
		if (strlen($line) > 1) {
			if ($line[0] == "<") {
			   	if ($line[1] == "/") {
					if (ac_str_instr("textarea", $line))
						$textarea = false;
					$inc--;
					$rval[] = (($inc > 0) ? str_repeat("  ", $inc) : "") . $line;
					continue;
				} else {
					$rval[] = (($inc > 0) ? str_repeat("  ", $inc) : "") . $line;
					if (!ac_str_instr("<input", $line) && !ac_str_instr("</", $line))
						$inc++;
					if (ac_str_instr("<textarea", $line) && !ac_str_instr("</", $line))
						$textarea = true;
					continue;
				}
			}
		}

		if ($line != "") {
			$rval[] = (($inc > 0 && !$textarea) ? str_repeat("  ", $inc) : "") . $line;
			if (ac_str_instr("</textarea", $line))
				$textarea = false;
		} else {
			if ($textarea)
				$rval[] = $line;
		}
	}

	return implode("\n", $rval);
}

?>
