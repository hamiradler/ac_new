<?php

// load API example doc file content
function api_load() {
	$filename = ac_http_param("filename");

	// just load the file and return the content
	$file = ac_file_get( ac_base('docs/api-examples/' . $filename) );
	$file = str_replace('YOUR_USERNAME', $GLOBALS['admin']['username'], $file);
	$file = str_replace('http://yourdomain.com/path/to/12all', $GLOBALS['site']['p_link'], $file);
	$file = str_replace('ActiveCampaign Email Marketing', $GLOBALS['admin']['brand_site_name'], $file);
	//dbg($file);
	return $file;
}

function api_deprecated() {
	return ac_ajax_api_result(0, _a("This API call has been deprecated and does not exist anymore."));
}

function api_public($api_url, $api_key, $component, $method, $params = "", $post_data = array()) {
	// uses public API wrapper
	// test installs
	$test_urls = array("https://localhost", "https://matt.co.in");
	if (in_array($api_url, $test_urls)) $api_url .= "/hosted";
	// if developing locally
	if ($api_url == "https://localhost.api-us1.com" && $_SERVER["SERVER_NAME"] == "localhost") $api_url = "http://localhost/hosted";
	define("ACTIVECAMPAIGN_URL", $api_url);
	define("ACTIVECAMPAIGN_API_KEY", $api_key);
	require_once ac_global_classes("activecampaign-api-php/ActiveCampaign.class.php");
	$ac = new ActiveCampaign($api_url, $api_key);
//dbg($ac);
	// $method is something like "campaign_report_unopen_list" - we want to make it "campaign/report_unopen_list"
//dbg($method,1);
	//$method_pieces = explode("_", $method);
	//$component = $method_pieces[0];
	// join the rest of the array (minus key 0) into a string again
	//$method = implode("_", array_slice($method_pieces, 1));
	// $params could be something like: id=1
	if ($params) $method .= "?" . $params;
	$str = $component . "/" . $method;
//dbg($str);
	if (!$post_data) {
		// GET request
		$r = $ac->api($str);
	}
	else {
		// POST request
		$r = $ac->api($str, $post_data);
	}
	//$r = json_encode($r);
	return $r;
}

function api_public_post() {
	$api_url = $_POST["func_params"][0];
	$api_key = $_POST["func_params"][1];
	$component = $_POST["func_params"][2];
	$method = $_POST["func_params"][3];
	$params = urldecode($_POST["func_params"][4]);
	$post_data = array();
	foreach ($_POST as $k => $v) {
		if ($k != "func_params") {
			$post_data[$k] = $v;
		}
	}
	$r = api_public($api_url, $api_key, $component, $method, $params, $post_data);
	return $r;
}

function api_credentials_get() {
	$r = array("url" => $GLOBALS["site"]["p_link"], "key" => $GLOBALS["admin"]["apikey"]);
	return $r;
}

?>
