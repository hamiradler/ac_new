#!/usr/local/bin/php
<?php
if ( !defined('AC_CRON') ) define('AC_CRON', 1);

// require main include file
define('ACADMINCRON', true);
require_once(dirname(dirname(dirname(__FILE__))) . '/prepend.inc.php');
require_once(ac_global_functions('cron.php'));
require_once(ac_admin('functions/campaign.php'));

if ( !defined('AC_CRON') ) define('AC_CRON', 1);

// turning off some php limits
@ignore_user_abort(1);
@ini_set('max_execution_time', 950 * 60);
@set_time_limit(950 * 60);
$ml = ini_get('memory_limit');
if ( $ml != -1 and (int)$ml < 128 and substr($ml, -1) == 'M') @ini_set('memory_limit', '128M');
set_include_path('.');
@set_magic_quotes_runtime(0);

// admin permission reset (use admin=1!)
$admin = ac_admin_get_totally_unsafe(1);

// Preload the language file
ac_lang_get('admin');


$cron_run_id = ac_cron_monitor_start(basename(__FILE__, '.php')); // log cron start

campaign_responder(); // this initializes scheduled autoresponders

$cron_run_id = ac_cron_monitor_stop(); // log cron end

?>
