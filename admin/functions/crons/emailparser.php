#!/usr/local/bin/php
<?php
// require main include file
define('ACADMINCRON', true);
require_once(dirname(dirname(dirname(__FILE__))) . '/prepend.inc.php');
require_once(ac_global_functions('cron.php'));
require_once(ac_global_functions('process.php'));
require_once(ac_global_functions('ajax.php'));

if ( !defined('AC_CRON') ) define('AC_CRON', 1);

// turning off some php limits
@ignore_user_abort(1);
@ini_set('max_execution_time', 950 * 60);
@set_time_limit(950 * 60);
$ml = ini_get('memory_limit');
if ( $ml != -1 and (int)$ml < 128 and substr($ml, -1) == 'M') @ini_set('memory_limit', '128M');
set_include_path('.');
@set_magic_quotes_runtime(0);

// admin permission reset (use admin=1!)
$admin = ac_admin_get_totally_unsafe(1);

// Preload the language file
ac_lang_get('admin');



$id = (int)ac_http_param('id');
$debug = (bool)ac_http_param('debug');
$debug_comm = (bool)ac_http_param('debug_comm');

if ( !$id ) {
	$id = ( isset($_SERVER['argv'][1]) ? (int)$_SERVER['argv'][1] : 0 );
	if ( isset($_SERVER['argv'][2]) ) $debug = (bool)$_SERVER['argv'][2];
}


if ( $debug ) {
	echo "

	<style type='text/css'>

		body {
			font-family: Arial;
			font-size: 11px;
		}

	</style>

	";
	if ( !defined('AC_POP3_DEBUG') ) define('AC_POP3_DEBUG', $debug);
}

if ( $debug_comm ) {
	if ( !defined('AC_POP3_DEBUG_COMM') ) define('AC_POP3_DEBUG_COMM', $debug_comm);
}

// include these after setting a debugging constant
require_once(ac_admin('functions/emailaccount.php'));
require_once(ac_global_functions('pop3.php'));



if ( !$debug ) {
	$cron_run_id = ac_cron_monitor_start(basename(__FILE__, '.php')); // log cron start
}


emailaccount_process($id);

if ( !$debug ) {
	ac_cron_monitor_stop(); // log cron end
}

?>
