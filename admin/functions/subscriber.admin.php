<?php

require_once ac_global_functions("ajax.php");

function subscriber_update_ip($subid, $listid, $target, $ip = null) {
	if (!in_array($target, array("last", "sub", "unsub")))
		return;

	$subid = (int)$subid;
	$listid = (int)$listid;

	if ($ip === null)
		$ip = ( isset($_SERVER['REMOTE_ADDR']) ? ac_str_noipv6($_SERVER['REMOTE_ADDR']) : '127.0.0.1' );

	$up = array(
		'=ip4_' . $target => "INET_ATON('$ip')",
	);

	ac_sql_update("#subscriber_list", $up, "subscriberid = '$subid' AND listid = '$listid'");
}

function subscriber_insert_post_web() {
	if (!withinlimits('subscriber'))
		return ac_ajax_api_result(false, _a("You have reached your subscriber limit.  Please upgrade before adding more subscribers."));

	$lists = array();
	if ( isset($_POST['p']) ) {
		if (!is_array($_POST['p']))
			$_POST['p'] = array($_POST['p']);
		$lists = array_map('intval', $_POST['p']);
	} else {
		return ac_ajax_api_result(false, _a("You did not select any lists, or you did not submit a POST request."));
	}

	$status = (int)ac_http_param("status");
	$formid = (int)ac_http_param('form');
	$noresponders = (int)ac_http_param('noresponders');
	$sendoptin = (int)ac_http_param('sendoptin');
	$instantresponders = (int)ac_http_param('instantresponders');
	$lastmessage = (int)ac_http_param('lastmessage');

	if (!$formid) $formid = 0;

	$ary = array(
		'id' => 0,
		'email' => trim((string)ac_http_param('email')),
		'=cdate' => 'NOW()',
		//'=hash' => "MD5(CONCAT(id, email))",
	);

	$fullname  = trim((string)ac_http_param('name')); // vBulletin API call
	if ($fullname) {
		$fullname = explode(" ", $fullname);
		$firstname = array_shift($fullname);
		$lastname = implode(" ", $fullname);
	}
	else {
		$firstname = trim((string)ac_http_param('first_name'));
		$lastname = trim((string)ac_http_param('last_name'));
	}

	// check email
	if ( !ac_str_is_email($ary['email']) ) {
		return ac_ajax_api_result(false, _a("Subscriber Email Address is not valid."));
	}

	// duplicates check
	$update = false;
	$addcounter = 0;
	// try to find this email in the system
	$found = subscriber_exists($ary['email']);
	// if subscriber is in the system (any list)
	if ( $found ) {
		// then loop through provided lists
		foreach ( $lists as $l ) {
			// if email is in this list
			if ( subscriber_exists($ary['email'], $l) ) {
				// complain
				return ac_ajax_api_result(false, _a("You selected a list that does not allow duplicates. This email is in the system already, please edit that subscriber instead."));
			}
		}
	}
	// we should update if we found him, and not inserting him into all lists (then we would insert a brand new row)
	$update = ( $found and $addcounter < count($lists) );
	// if this subscriber should be updated rather than inserted, then run updater
	if ( $update ) {
		$id = (int)$found['id'];
	} else {
		/*
			INSERT NEW SUBSCRIBER
		*/
		$sql = ac_sql_insert("#subscriber", $ary);
		if ( !$sql ) {
			return ac_ajax_api_result(false, _a("Subscriber could not be added."));
		}
		$id = ac_sql_insert_id();

		// update same record with hash, now that we have the ID
		ac_sql_update_one('#subscriber', '=hash', 'MD5(CONCAT(id, email))', "`id` = '$id'");
	}

	// save custom fields
	if ( is_array(ac_http_param('field')) ) {
		ac_cfield_update_data(ac_http_param('field'), '#field_value', 'fieldid', array('relid' => $id));
	}

	$r = array(
		'subscriber_id' => $id,
		'sendlast_should' => 0,
		'sendlast_did' => 0,
	);

	// save lists
	$admin = ac_admin_get();
	foreach ( $lists as $l ) {
		$sdate_field = '=sdate';
		$sdate_value = 'NOW()';
		$responder = intval(!$noresponders);
		$ary2 = array(
			'id' => 0,
			'subscriberid' => $id,
			'listid' => $l,
			'formid' => 0,
			$sdate_field => $sdate_value,
			'=udate' => ( $status == 2 ? 'NOW()' : 'NULL' ),
			'status' => $status,
			'responder' => $responder,
			'sync' => 0,
			'=unsubreason' => 'NULL',
			'unsubcampaignid' => 0,
			'unsubmessageid' => 0,
			'=ip4_sub' => "INET_ATON('127.0.0.1')",
			'=ip4_last' => "INET_ATON('127.0.0.1')",
			'first_name' => $firstname,
			'last_name' => $lastname,
			'sourceid' => 3,
		);
		$sql = ac_sql_insert('#subscriber_list', $ary2);
		if ( !$sql ) {
			return ac_ajax_api_result(false, _a("Subscriber could not be added."));
		}

		$subscriber = subscriber_select_row($id);
		$list_send_last_message = (int)ac_sql_select_one("SELECT send_last_broadcast FROM #list WHERE id = '{$l}'");

		if ( $status == 2 ) { // UNSUBSCRIBED actions
			// nothing here yet...
			if ( $instantresponders ) {
				// (re)send instant autoresponders
				mail_responder_send($subscriber, $l, 'unsubscribe');
			}
		} elseif ( $status == 1 ) { // SUBSCRIBED actions
			if ( $instantresponders ) {
				// (re)send instant autoresponders
				mail_responder_send($subscriber, $l, 'subscribe');
			}
			if ($lastmessage || $list_send_last_message) {
				// (re)send last broadcast message
				$r['sendlast_should'] = 1;
				$r['sendlast_did'] += mail_campaign_send_last($subscriber, $l);
			}
		} else {//if ( $status == 0 ) { // UNCONFIRMED actions
			if ( $sendoptin ) {
				// (re)send opt in email
				form_send_message_exact($formid, $l, $subscriber);
			}
		}

		subscriber_action_dispatch("subscribe", $subscriber, $l, null, null);
	}

	cache_clear('subcnt');
	cache_clear("withinlimits_subscriber");
	return ac_ajax_api_added(_a("Subscriber"), $r);
}

function subscriber_insert_post($service = "") {
	$lists = array();

	// check for any integration-specific GET or POST values
	require_once ac_admin("functions/service.php");
	service_subscriber_integration("add", $service);

	if ( isset($_POST['p']) and is_array($_POST['p']) and count($_POST['p']) > 0 ) {
		$lists = array_map('intval', $_POST['p']);
		$sdates = (array)ac_http_param('sdate');
		$statuses = (array)ac_http_param('status');
		$formid = (int)ac_http_param('form');
		$noresponders = (array)ac_http_param('noresponders');
		$instantresponders = (array)ac_http_param('instantresponders');
		$lastmessage = (array)ac_http_param('lastmessage');
	} else {
		return ac_ajax_api_result(false, _a("You did not select any lists, or you did not submit a POST request."));
	}

	if (!$formid) $formid = 0;

	$ip4 = ac_http_param('ip4');
	if (!$ip4) $ip4 = '127.0.0.1';

	// make sure each list exists
	foreach ($lists as $l) {
	  $list = list_select_row($l);
	  if (!$list) return ac_ajax_api_result( false, sprintf(_a("List ID %s does not exist."), $l) );
	}

	$ary = array(
		'id' => 0,
		'email' => trim((string)ac_http_param('email')),
		'=cdate' => 'NOW()',
		//'=hash' => "MD5(CONCAT(id, email))",
	);

	$fullname  = trim((string)ac_http_param('name')); // vBulletin API call
	if ($fullname) {
		$fullname = explode(" ", $fullname);
		$firstname = array_shift($fullname);
		$lastname = implode(" ", $fullname);
	}
	else {
		$firstname = trim((string)ac_http_param('first_name'));
		$lastname = trim((string)ac_http_param('last_name'));
	}

	// check email
	if ( !ac_str_is_email($ary['email']) ) {
		return ac_ajax_api_result(false, _a("Subscriber Email Address is not valid."));
	}

	// duplicates check
	$update = false;
	$addcounter = 0;
	// try to find this email in the system
	$found = subscriber_exists($ary['email']);

	// if subscriber is in the system (any list)
	if ( $found ) {
		// then loop through provided lists
		foreach ( $lists as $l ) {
			// if email is in this list
			if ( $s = subscriber_exists($ary['email'], $l) ) {
				// complain
				return ac_ajax_api_result(false, sprintf(_a("You selected a list that does not allow duplicates. The email %s is in the system already, please edit that subscriber instead."), $ary['email']), array($s));
			}
			else {
				// found in the system, but not in this list
				// we won't be adding him to this list, so we won't update the counter
				// (so it switches to update if all good)
				//$addcounter++;
			}
		}
	}

	// we should update if we found him, and not inserting him into all lists (then we would insert a brand new row)
	$update = ( $found and $addcounter < count($lists) );
	// if this subscriber should be updated rather than inserted, then run updater
	if ( $update ) {
		$id = (int)$found['id'];
	} else {
		/*
			INSERT NEW SUBSCRIBER
		*/
		$sql = ac_sql_insert("#subscriber", $ary);
		if ( !$sql ) {
			return ac_ajax_api_result(false, _a("Subscriber could not be added."));
		}
		$id = (int)ac_sql_insert_id();

		// update same record with hash, now that we have the ID
		ac_sql_update_one('#subscriber', '=hash', 'MD5(CONCAT(id, email))', "`id` = '$id'");
	}

	// save custom fields
	if ( is_array(ac_http_param('field')) ) {
		$fields = ac_http_param('field');
		$fields_array = array();
		// make sure the data ID (if passed) corresponds to this subscriber ID and field ID
		foreach ($fields as $fieldid_dataid => $value) {
			$tmparr = explode(",", $fieldid_dataid);
			if ( !isset($tmparr[1]) ) $tmparr[1] = 0;
			// if it starts and ends with a percentage sign, it is a personalization tag
			// (you can pass a field ID, or pers tag, since both are unique)
			if ( preg_match("/^%/", trim($tmparr[0])) && preg_match("/%$/", trim($tmparr[0])) ) {
				$pers_tag = trim($tmparr[0], "%");
				// check if this pers tag actually exists for a field
				$field_row_id = (int)ac_sql_select_one("SELECT id FROM #field WHERE perstag = '$pers_tag'");
				if ($field_row_id) {
					$tmparr[0] = $field_row_id;
				}
				else {
					// invalid pers tag
					return ac_ajax_api_result(false, _a("Personalization tag") . " %" . $pers_tag . "% " . _a("cannot be found."));
				}
			}
			list($fieldid, $dataid) = array_map('intval', $tmparr);
			$where = array(
				"relid = '$id'",
				"fieldid = '$fieldid'",
			);
			// check for existing data row - if it's not there it will be added
			$data_row_id = (int)ac_sql_select_one("SELECT id FROM #field_value WHERE " . implode(" AND ", $where));
			$fieldid_dataid = implode(",", array($fieldid, $data_row_id));
			$fields_array[$fieldid_dataid] = $value;
		}
		ac_cfield_update_data($fields_array, '#field_value', 'fieldid', array('relid' => $id));
	}

	$r = array(
		'subscriber_id' => $id,
		'sendlast_should' => 0,
		'sendlast_did' => 0,
	);

	// save lists
	$admin = ac_admin_get();
	if ($formid) {
		$form = form_select_row($formid);
	}
	foreach ( $lists as $l ) {
		$list = list_select_row($l);
		$sdate_field = ( isset($sdates[$l]) ? 'sdate' : '=sdate' );
		$sdate_value = ( isset($sdates[$l]) ? $sdates[$l] : 'NOW()' );
		$status = ( isset($statuses[$l]) ? (int)$statuses[$l] : 1 );

		// We don't support unconfirmed statuses here.
		if ($status == 0)
			$status = 1;

		// Except if you have a form...
		if ($formid > 0 && isset($form) && (int)$form["sendoptin"])
			$status = 0;

		$responder = (int)!isset($noresponders[$l]);
		$notifies = array();
		$ary2 = array(
			'id' => 0,
			'subscriberid' => $id,
			'listid' => $l,
			'formid' => $formid,
			$sdate_field => $sdate_value,
			'=udate' => ( $status == 2 ? 'NOW()' : 'NULL' ),
			'status' => $status,
			'responder' => $responder,
			'sync' => 0,
			'=unsubreason' => 'NULL',
			'=ip4_sub' => "INET_ATON('$ip4')",
			'=ip4_last' => "INET_ATON('$ip4')",
			'unsubcampaignid' => 0,
			'unsubmessageid' => 0,
			'first_name' => $firstname,
			'last_name' => $lastname,
			'sourceid' => 4,
		);
		$sql = ac_sql_insert('#subscriber_list', $ary2);
		if ( !$sql ) {
			return ac_ajax_api_result(false, _a("Subscriber could not be added."));
		}

		$subscriber = subscriber_select_row($id);
		if ( $status == 2 ) { // UNSUBSCRIBED actions
			// nothing here yet...
			if ( isset($instantresponders[$l]) ) {
				// (re)send instant autoresponders
				mail_responder_send($subscriber, $l, 'unsubscribe');
			}

			// admin notifications
			if ( $list["unsubscription_notify"] ) {
				// Full list array gets passed
				$notifies[] = $list;
			}
			if ( count($notifies) > 0 ) mail_admin_send($subscriber, $notifies, 'unsubscribe');

		} elseif ( $status == 1 ) { // SUBSCRIBED actions
			if ( isset($instantresponders[$l]) ) {
				// (re)send instant autoresponders
				mail_responder_send($subscriber, $l, 'subscribe');
			}
			if ( isset($lastmessage[$l]) && (int)$lastmessage[$l] ) {
				// (re)send last broadcast message
				$r['sendlast_should'] = 1;
				$r['sendlast_did'] += mail_campaign_send_last($subscriber, $l);
			}

			// admin notifications
			if ( $list["subscription_notify"] ) {
				// Full list array gets passed
				$notifies[] = $list;
			}
			if ( count($notifies) > 0 ) mail_admin_send($subscriber, $notifies, 'subscribe');

		}
		subscriber_action_dispatch("subscribe", $subscriber, $l, null, null);
	}

	if ($formid > 0) {
		// If we shouldn't be sending, we'll catch that in the function.
		if (count($lists) > 0) {
			$curlist = current($lists);
			form_send_message_exact($formid, $curlist["id"], $subscriber);
		}
	}

	if ( isset($GLOBALS['_hosted_account']) ) {
		require(dirname(dirname(__FILE__)) . '/manage/subscriber.add.inc.php');
	}

	cache_clear('subcnt');
	cache_clear("withinlimits_subscriber");
	return ac_ajax_api_added(_a("Subscriber"), $r);
}

function subscriber_update_post() {
  $overwrite = 1;
  if ( $_GET['p'] && isset($_GET['p'][0]) ) {
    // flag to overwrite everything (even if POST parameter is not passed - this is default behavior), or only used passed POST parameters
    // mainly for API (subscriber_edit)
    $overwrite = (int)$_GET['p'][0];
  }

	if ( $overwrite && (!isset($_POST['p']) || !is_array($_POST['p']) || count($_POST['p']) == 0) ) {
	  return ac_ajax_api_result(false, _a("You did not select any lists."));
	}

	if ( isset($_POST['p']) ) {
	  $lists = array_map('intval', $_POST['p']);
	}
	else {
	  $lists = array();
	}

	$statuses = (array)ac_http_param('status');
	$formid = (int)ac_http_param('form');
	$noresponders = (array)ac_http_param('noresponders');
	$sendoptout = (array)ac_http_param('sendoptout');
	$instantresponders = (array)ac_http_param('instantresponders');
	$lastmessage = (array)ac_http_param('lastmessage');
	$unsubreason = (array)ac_http_param('unsubreason');

	if (!$formid) $formid = 0;

	$ary = array();

	// only if email IS provided
	if ( ac_http_param_exists('email') ) {
		$ary = array(
			'email' => trim((string)ac_http_param('email')),
			'=hash' => "MD5(CONCAT(id, email))",
		);
		// check email
		if ( !ac_str_is_email($ary['email']) ) {
			return ac_ajax_api_result(false, _a("Subscriber Email Address is not valid."));
		}
	}

	$fullname  = trim((string)ac_http_param('name')); // vBulletin API call
	if ($fullname) {
		$fullname = explode(" ", $fullname);
		$firstname = array_shift($fullname);
		$lastname = implode(" ", $fullname);
	}
	else {
		$firstname = trim((string)ac_http_param('first_name'));
		$firstname_list = (array)ac_http_param('first_name_list');
		$lastname = trim((string)ac_http_param('last_name'));
		$lastname_list = (array)ac_http_param('last_name_list');
	}

	$id = (int)ac_http_param("id");
	if ( !$id ) {
		return ac_ajax_api_result(false, _a("Subscriber not provided."));
	}

	$s = subscriber_select_row($id);
	if (!$s) {
	  return ac_ajax_api_result(false, _a("Invalid subscriber ID") . ": " . $id);
	}

	if ($ary) {
	  $sql = ac_sql_update("#subscriber", $ary, "id = '$id'");

    if ( !$sql ) {
      return ac_ajax_api_result(false, _a("Subscriber could not be updated."));
    }
	}

	// save custom fields
	if ( is_array(ac_http_param('field')) ) {
		$fields = ac_http_param('field');
		$fields_array = array();
		// make sure the data ID (if passed) corresponds to this subscriber ID and field ID
		foreach ($fields as $fieldid_dataid => $value) {
			$tmparr = explode(",", $fieldid_dataid);
			if ( !isset($tmparr[1]) ) $tmparr[1] = 0;
			// if it starts and ends with a percentage sign, it is a personalization tag
			// (you can pass a field ID, or pers tag, since both are unique)
			if ( preg_match("/^%/", trim($tmparr[0])) && preg_match("/%$/", trim($tmparr[0])) ) {
				$pers_tag = trim($tmparr[0], "%");
				// check if this pers tag actually exists for a field
				$field_row_id = (int)ac_sql_select_one("SELECT id FROM #field WHERE perstag = '$pers_tag'");
				if ($field_row_id) {
					$tmparr[0] = $field_row_id;
				}
				else {
					// invalid pers tag
					return ac_ajax_api_result(false, _a("Personalization tag") . " %" . $pers_tag . "% " . _a("cannot be found."));
				}
			}
			list($fieldid, $dataid) = array_map('intval', $tmparr);
			$where = array(
				"relid = '$id'",
				"fieldid = '$fieldid'",
			);
			// check for existing data row - if it's not there it will be added
			$data_row_id = (int)ac_sql_select_one("SELECT id FROM #field_value WHERE " . implode(" AND ", $where));
			$fieldid_dataid = implode(",", array($fieldid, $data_row_id));
			$fields_array[$fieldid_dataid] = $value;
		}
		ac_cfield_update_data($fields_array, '#field_value', 'fieldid', array('relid' => $id));
	}

	// delete old that are now deselected
	if ($s) {
		foreach ( $s['lists'] as $k => $v ) {
		  if ($overwrite) {
  			if ( !in_array($k, $lists) ) {
  				ac_sql_delete('#subscriber_list', "subscriberid = '$id' AND listid = '$k'");
  			}
		  }
		}
	}

	# Update their cache records.
	//filter_cache_subscriber($id, true);

	$r = array(
		'sendlast_should' => 0,
		'sendlast_did' => 0,
	);

	$admin = ac_admin_get();
	if ($formid) {
		$form = form_select_row($formid);
	}

	$form_send_opt = true;

	// save list relations
	foreach ($lists as $l) {

	  $list_exists = ac_sql_select_one('=COUNT(*)', '#list', "id = '$l'");
	  if (!$list_exists) return ac_ajax_api_result(false, _a("List ID") . " " . $l . " " . _a("does not exist."));

	  $status = ( isset($statuses[$l]) ? (int)$statuses[$l] : 1 );

	  if ($status == 0)
		  $status = 1;

		$oldstatus = (int)ac_sql_select_one('status', "em_subscriber_list", "subscriberid = '$id' AND listid = '$l'");
		$oldformid = (int)ac_sql_select_one('formid', "em_subscriber_list", "subscriberid = '$id' AND listid = '$l'");

	  $responder = (int)!isset($noresponders[$l]);
	  $firstname = ( isset($firstname_list[$l]) ? (string)$firstname_list[$l] : $firstname );
	  $lastname = ( isset($lastname_list[$l]) ? (string)$lastname_list[$l] : $lastname );
	  $exists = ac_sql_select_one('=COUNT(*)', '#subscriber_list', "subscriberid = '$id' AND listid = '$l'");
	  $adding = $editing = 0;
	  if ( $exists ) {

		  // editing this subscriber/list relation

	  	$editing = 1;

	  	// if they are editing with the same form ID for a subscriber who has not confirmed yet, leave them in Unconfirmed status for this list
	  	if ($formid && $formid == $oldformid && $oldstatus == 0 && isset($form) && (int)$form["sendoptin"]) {
	  		$status = 0;
	  		$form_send_opt = false;
	  	}
	  	elseif ($status == 1 && isset($form) && (int)$form["sendoptin"]) {
	  		// editing existing list relation through a form that has opt-in required, don't re-send the confirmation email.
	  		// typically happens when using subscriber_sync API method with the "form" parameter.
	  		$form_send_opt = false;
	  	}

			$ary2 = array(
				'status' => $status,
				'responder' => $responder,
				'=udate' => ( $status == 2 ? 'NOW()' : 'NULL' ),
				'first_name' => $firstname,
				'last_name' => $lastname,
			);
			if ( $status != 2 ) {
				$ary2['=unsubreason'] = 'NULL';
				$ary2['=unsubcampaignid'] = 0;
				$ary2['=unsubmessageid'] = 0;
			}
			elseif ($status == 2) {
				$ary2['unsubreason'] = isset($unsubreason[$l]) ? $unsubreason[$l] : '';
			}

			if (!$overwrite) {
			  // only update fields that were passed with POST (API)
			  $post_keys = array_keys($_POST);
			  $ary2_keys = array_keys($ary2);
			  $ary2_new = array();
			  foreach ($post_keys as $field) {
			    if ($field != 'id' && $field != 'p') {
			      // only include fields/values that are in POST array
			      if ( isset($ary2[$field]) ) $ary2_new[$field] = $ary2[$field];
			    }
			  }
			  foreach ($ary2 as $k => $v) {
			    // save all keys that start with '='
			    if ( preg_match('/^=/', $k) ) {
			      $ary2_new[$k] = $v;
			    }
			  }
			  $ary2 = $ary2_new;
			}

			//dbg($ary2);
			$sql = ac_sql_update('#subscriber_list', $ary2, "subscriberid = '$id' AND listid = '$l'");
		}
		else {

		  // adding this subscriber/list relation

		  $adding = 1;

			if ($formid && isset($form) && (int)$form["sendoptin"])
				$status = 0;

			$ary2 = array(
				'id' => 0,
				'subscriberid' => $id,
				'listid' => $l,
				'formid' => $formid,
				'=sdate' => 'NOW()',
				'=udate' => ( $status == 2 ? 'NOW()' : 'NULL' ),
				'status' => $status,
				'responder' => $responder,
				'sync' => 0,
				'first_name' => $firstname,
				'last_name' => $lastname,
				'sourceid' => 3,
			);
			if ( $status != 2 ) {
				$ary2['=unsubreason'] = 'NULL';
				$ary2['=unsubcampaignid'] = 0;
				$ary2['=unsubmessageid'] = 0;
			}
			//if unsusbcribing someone but sending them opt-out msg first, make sure to keep their status as 'active' for now
			//if ($status == '2' && $sendoptoutlist) $ary2['status'] = 1;

			if (!$overwrite) {
			  // only update fields that were passed with POST (API)
			  $post_keys = array_keys($_POST);
			  $ary2_keys = array_keys($ary2);
			  $ary2_new = array();
			  foreach ($post_keys as $field) {
			    if ($field != 'p') {
			      // only include fields/values that are in POST array
			      if ( isset($ary2[$field]) ) $ary2_new[$field] = $ary2[$field];
			    }
			  }
			  foreach ($ary2 as $k => $v) {
			    // save all keys that start with '=', and other required fields/values
			    // typically we retain all non-variable values - anything hard-coded when we declare the array
			    if ( preg_match('/^=/', $k) || $k == 'subscriberid' || $k == 'listid' || $k == 'formid' || $k == 'sync' || $k == 'sourceid' ) {
			      $ary2_new[$k] = $v;
			    }
			  }
			  $ary2 = $ary2_new;
			}

			$sql = ac_sql_insert('#subscriber_list', $ary2);
			$sid = ( $sql ? ac_sql_insert_id() : 0 );
		}
		if ( !$sql ) {
			return ac_ajax_api_result(false, _a("Subscriber could not be added."));
		}
		$subscriber = subscriber_select_row($id);
		$list_send_last_message = (int)ac_sql_select_one("SELECT send_last_broadcast FROM #list WHERE id = '{$l}'");
		$subact     = "subscribe";
		if ( $status == 2 ) { // UNSUBSCRIBED actions
			$subact = "unsubscribe";
			if ( isset($instantresponders[$l]) ) {
				// (re)send instant autoresponders
				mail_responder_send($subscriber, $l, 'unsubscribe');
			}
		} elseif ( $status == 1 ) { // SUBSCRIBED actions
			if ( isset($instantresponders[$l]) ) {
				// (re)send instant autoresponders
				mail_responder_send($subscriber, $l, 'subscribe');
			}
			if (isset($lastmessage[$l]) || ($adding && $list_send_last_message)) {
				// (re)send last broadcast message
				$r['sendlast_should'] = 1;
				$r['sendlast_did'] += mail_campaign_send_last($subscriber, $l);
			}
		}
		subscriber_action_dispatch($subact, $subscriber, $l, null, null);

		if ($formid && $form_send_opt) {
			// If we shouldn't be sending, we'll catch that in the function.
			if (count($lists) > 0) {
				$curlist = current($lists);
				form_send_message_exact($formid, $curlist["id"], $subscriber);
			}
		}

	}

	return ac_ajax_api_updated(_a("Subscriber"), $r);
}

function subscriber_sync($service) {
	// public api: add or edit a subscriber without having to pass separate calls

	// try to add first
	$add = subscriber_insert_post($service);
	// ADD will work as normal, up until it finds the first list the subscriber is part of already
	if (!(int)$add["succeeded"] && isset($add[0]["id"])) {
		// at this point, try to EDIT (using all of the same lists - subscriber_update_post() starts from the *beginning* of the list (p) array)
		// $add[0] is the result of subscriber_exists (should be the subscriber record/array)
		$_POST["id"] = (int)$add[0]["id"];
		if (isset($_POST["instantresponders"])) {
			foreach ($_POST["instantresponders"] as $listid => $v) {
				// set instant responders to OFF, if editing
				$_POST["instantresponders"][$listid] = 0;
			}
		}
		$_GET["p"][0] = 0; // set overwrite to OFF (update only data passed with the API call)
		$edit = subscriber_update_post($service);
		if (!(int)$edit["succeeded"]) {
			// edit failed.
		}
		return $edit;
	}
	return $add;
}

function subscriber_delete_post() {
	$id = intval(ac_http_param("id"));
	$listids = ac_http_param_forcearray("listids");
	$listid = (int)ac_http_param("listid");

	if ($listid) $listids[] = $listid;
	$listids = array_unique($listids);

	foreach ($listids as $lid)
		subscriber_softdelete($id, $lid);

	return ac_ajax_api_result(true, _a("Subscriber deleted"));
}

function subscriber_delete($id, $listids = null, $countdelete = true) {
	$id        = intval($id);
	$admin     = $GLOBALS["admin"];
	$admincond = '';

	if (!withindeletelimits()) {
		return ac_ajax_api_result(false, _a("You cannot delete any more subscribers in this billing period"), array("pastlimit" => 1));
	}

	if ($listids !== null && is_array($listids)) {
		# soft delete: only delete those list relations given in $listids that we have access to.
		$listids   = array_intersect(array_diff(array_map('intval', $listids), array(0)), $admin["lists"]);
		$liststr   = implode("','", $listids);
		$admincond = "AND listid IN ('$liststr')";
	} else {
		# hard delete: grab every list relation that we can.
		if ( !ac_admin_ismain() ) {
			$liststr   = implode("','", $admin["lists"]);
			$admincond = "AND listid IN ('$liststr')";
		}
	}

	ac_sql_delete('#subscriber_list', "subscriberid = '$id' $admincond");
	ac_sql_delete('#subscriber_responder', "subscriberid = '$id' $admincond");
	if ( ac_sql_select_one('=COUNT(*)', '#subscriber_list', "subscriberid = '$id'") == 0 ) {
		ac_sql_delete('#subscriber', "id = '$id'");
		ac_sql_delete('#field_value', "relid = '$id'");

		if (isset($GLOBALS["_hosted_account"]) && $countdelete) {
			ac_sql_query("UPDATE #backend SET deletedsubs = deletedsubs + 1");
		}
	}

	if ( !function_exists('ac_ajax_api_deleted') ) return true;

	if (!isset($GLOBALS['subscriber_batch'])) {
		cache_clear('subcnt');
		cache_clear("withinlimits_subscriber");
	}

	return ac_ajax_api_deleted(_a("Subscriber"));
}

function subscriber_delete_multi_post() {
	$ids      = strval(ac_http_param("ids"));
	$filterid = (int)ac_http_param("filter");
	$listids = ac_http_param_forcearray("listids");
	$listid = (int)ac_http_param("listid");

	if ($listid) $listids[] = $listid;
	$listids = array_unique($listids);

	$delete = subscriber_delete_multi($ids, $listids, $filterid);
	if ($delete["succeeded"] && $ids == "_all") $delete["message"] = _a("All subscribers deleted");
	return $delete;
}

function subscriber_delete_multi($ids, $listids = null, $filter = 0) {
	@set_time_limit(950 * 60);

	$admin = $GLOBALS["admin"];

	if ($listids !== null && is_array($listids)) {
		# soft delete: only delete those list relations given in $listids that we have access to.
		$listids = array_intersect(array_diff(array_map('intval', $listids), array(0)), $admin["lists"]);
	} else {
		# hard delete: grab every list relation that we can.
		$listids = $admin["lists"];
	}

	if ( $ids == '_all' ) {
		$tmp = array();
		$so = new AC_Select();
		$so->slist = array('s.id');
		$so->remove = false;
		$filter = intval($filter);
		if ($filter > 0) {
			$admin = ac_admin_get();
			$conds = ac_sql_select_one("SELECT conds FROM #section_filter WHERE id = '$filter' AND userid = '$admin[id]' AND sectionid = 'subscriber'");
			$so->push($conds);
		} else {
			$so->push("AND l.status = 1"); // subscribed = DEFAULT
		}
		$listids_str = implode("','", $listids);
		$so->push("AND l.listid IN ('{$listids_str}')");
		$all = subscriber_select_array($so);
		foreach ( $all as $v ) {
			$tmp[] = $v['id'];
		}
	} else {
		$tmp = array_map("intval", explode(",", $ids));
	}
	foreach ( $tmp as $id ) {
		foreach ($listids as $lid) {
			subscriber_softdelete($id, $lid);
		}
	}
	return ac_ajax_api_result(true, _a("Subscribers deleted"));
}

function subscriber_bounce_reset($id, $what) {
	$admin = ac_admin_get();
	if ( !$admin['pg_subscriber_edit'] ) {
		return ac_ajax_api_result(false, _a("You do not have permission to reset subscriber bounces."));
	}
	$id = (int)$id;
	$subscriber = subscriber_select_row($id);
	if ( !$subscriber ) {
		return ac_ajax_api_result(false, _a("Subscriber not found."));
	}
	$email = ac_sql_escape($subscriber['email']);
	$update = array();
	// reset soft bounces
	if ( $what != 'hard' ) {
		$update['bounced_soft'] = 0;
		subscriber_bounce_lowercounts($id, $email, "soft");
		ac_sql_delete('#bounce_data', "( `subscriberid` = '$id' OR `email` = '$email' ) AND `type` = 'soft'");
	}
	// reset hard bounces
	if ( $what != 'soft' ) {
		$update['bounced_hard'] = 0;
		subscriber_bounce_lowercounts($id, $email, "hard");
		ac_sql_delete('#bounce_data', "( `subscriberid` = '$id' OR `email` = '$email' ) AND `type` = 'hard'");
		$affected = mysql_affected_rows();
	}
	$r = ac_sql_update('#subscriber', $update, "`id` = '$id'");
	if ( $r ) {
		return ac_ajax_api_result(true, _a("Subscriber bounces reset."), array('what' => $what));
	}
	return ac_ajax_api_result(false, _a("Subscriber bounces were not reset."), array('what' => $what));
}

function subscriber_remove_batch() {
	//
}

function subscriber_remove_all() {}

function subscriber_list_in($subscriber, $listid) {
	$where = "`subscriberid` = '$subscriber[id]' AND `listid` = '$listid'";
	$found = (int)ac_sql_select_one('=COUNT(*)', '#subscriber_list', $where);
	return (bool)( $found > 0 );
}

function subscriber_list_add($subscriber, $listid) {
	if ( !subscriber_list_in($subscriber, $listid) ) {
		# Find any other lists they might be in.
		$oldlists = ac_sql_select_list("SELECT listid FROM #subscriber_list WHERE subscriberid = '$subscriber[id]'");
		$oldliststr = implode("','", $oldlists);

		# Now find any fields that we may be sharing with those old lists...
		$oldfields = ac_sql_select_list("SELECT fieldid FROM #field_rel WHERE relid IN ('$oldliststr')");
		$newfields = ac_sql_select_list("SELECT fieldid FROM #field_rel WHERE relid = '$listid'");

		$fields = array_intersect($oldfields, $newfields);
		$fieldstr = implode("','", $fields);

		$insert = array(
			'id' => 0,
			'subscriberid' => $subscriber['id'],
			'listid' => $listid,
			'formid' => 0,
			'=sdate' => 'NOW()',
			'=udate' => 'NULL',
			'status' => 1,
			'responder' => 1,
			'sync' => 0,
			'=unsubreason' => 'NULL',
			'unsubcampaignid' => 0,
			'unsubmessageid' => 0,
			'first_name' => $subscriber['first_name'],
			'last_name' => $subscriber['last_name'],
			'sourceid' => 7,
		);
		ac_sql_insert('#subscriber_list', $insert);

		$fvals = ac_sql_query("SELECT * FROM #field_value WHERE fieldid IN ('$fieldstr')");
		while ($row = ac_sql_fetch_assoc($fvals)) {
			$ins = array(
				"relid" => $listid,
				"fieldid" => $row["fieldid"],
				"val" => $row["val"],
			);

			ac_sql_insert("#field_value", $ins);
		}

		// admin notifications
		$list = array();
		$list = list_select_row($listid);
		if ( $list["subscription_notify"] ) {
			// Full list array gets passed
			$notifies[] = $list;
		}
		if ( count($notifies) > 0 ) mail_admin_send($subscriber, $notifies, 'subscribe');

		//instant autoresponders
		mail_responder_send($subscriber, $listid, 'subscribe');

		//Send most recent campaign (if that option is enabled for the list)
		$list_send_last_message = $list['send_last_broadcast'];
		if ($list_send_last_message) {
			mail_campaign_send_last($subscriber, $listid);
		}


	}
}

function subscriber_list_remove($subscriber, $listid) {
	if ( subscriber_list_in($subscriber, $listid) ) {
		//ac_sql_delete('#subscriber_list', "`subscriberid` = '$subscriber[id]' AND `listid` = '$listid'");
		subscriber_softdelete($subscriber['id'], $listid);
	}
}

function subscriber_softdelete($id, $listid) {
	$id     = intval($id);
	$listid = intval($listid);
	$delete = false;

	// Mark this guy for deletion.  Check here instead of after the following
	// DELETE queries to avoid a possible race condition.
	$c = (int)ac_sql_select_one("SELECT COUNT(*) FROM #subscriber_list WHERE subscriberid = '$id'");
	if ($c < 2)
		$delete = true;

	ac_sql_query("
		DELETE FROM
			#subscriber_list
		WHERE
			subscriberid = '$id'
		AND
			listid = '$listid'
	");

	ac_sql_query("
		DELETE FROM
			#subscriber_responder
		WHERE
			subscriberid = '$id'
		AND
			listid = '$listid'
	");

	if ($delete)
		subscriber_delete($id);
}

function subscriber_update_info($subscriber, $field, $value) {
	$custom = preg_match('/^\d+$/', $field);
	if ( $custom ) {
		// update custom field if exists, otherwise set it
		$where = "`relid` = '$subscriber[id]' AND `fieldid` = '$field'";
		$dataid = (int)ac_sql_select_one('id', '#field_value', $where);
		if ( $dataid > 0 ) {
			ac_sql_update_one('#field_value', 'val', $value, $where);
		} else {
			$insert = array(
				'id' => 0,
				'relid' => $subscriber['id'],
				'fieldid' => $field,
				'val' => $value,
			);
			ac_sql_insert('#field_value', $insert);
		}
	} else {
		// update regular field (if exists)
		if ( in_array($field, array_keys($subscriber)) ) {
			if ( in_array($field, array('first_name', 'last_name')) ) {
				ac_sql_update_one('#subscriber_list', $field, $value, "`subscriberid` = '$subscriber[id]'");
			} else {
				ac_sql_update_one('#subscriber', $field, $value, "`id` = '$subscriber[id]'");
			}
			if ( $field == 'email' ) {
				ac_sql_update_one('#subscriber', '=hash', 'MD5(CONCAT(id, email))', "`id` = '$subscriber[id]'");
			}
		}
	}
}

function subscriber_update_email($subscriberid, $email) {
	$subscriberid = (int)$subscriberid;
	if ( !$subscriberid ) {
		return ac_ajax_api_result(false, _a("Subscriber not provided."));
	}
	$subscriber = subscriber_select_row($subscriberid);
	if ( !$subscriber ) {
		return ac_ajax_api_result(false, _a("Subscriber not found."));
	}

	// now check if any other subscriber has this email address already
	$emailesc = ac_sql_escape($email);
	$found = (int)ac_sql_select_one("id", "#subscriber", "email = '$emailesc' AND id != '$subscriberid'");
	if ( $found ) {
		return ac_ajax_api_result(false, _a("This email address is already used by another subscriber."), array('id' => $found));
	}

	$sql = ac_sql_update_one("#subscriber", "email", $email, "id = '$subscriberid'");
	if ( !$sql ) {
		return ac_ajax_api_result(false, _a("New email address for this subscriber could not be saved."));
	}

	return ac_ajax_api_result(true, _a("Email address has been updated for this subscriber."));
}

function subscriber_responder_log($subscriberid, $listid, $campaignid, $messageid) {
	$insert = array(
		'id' => 0,
		'subscriberid' => (int)$subscriberid,
		'listid' => (int)$listid,
		'campaignid' => (int)$campaignid,
		'messageid' => (int)$messageid,
		'=sdate' => 'NOW()',
	);
	ac_sql_insert('#subscriber_responder', $insert);
}

function subscriber_add_valid() {
	if ( !isset($GLOBALS['admin_subscribers_count']) ) {
		$GLOBALS['admin_subscribers_count'] = limit_count($GLOBALS['admin'], 'subscriber');
	}
	$valid = withinlimits('subscriber', $GLOBALS['admin_subscribers_count'] + 1, $GLOBALS['admin']);
	return $valid;
}

function subscriber_add_increment() {
	if (isset($GLOBALS["admin_subscribers_count"]))
		$GLOBALS["admin_subscribers_count"]++;
}

function subscriber_visible($subid) {
	$admin = ac_admin_get();
	$liststr = implode("','", $admin["lists"]);
	$subid = (int)$subid;

	$c = (int)ac_sql_select_one("SELECT COUNT(*) FROM #subscriber_list WHERE listid IN ('$liststr') AND subscriberid = '$subid'");
	return $c > 0;
}

function subscriber_optin_post() {
	$id = intval(ac_http_param("id"));
	$optid = intval(ac_http_param("optid"));
	if ( !$optid ) $optid = 1;
	return subscriber_optin($id, $optid);
}

function subscriber_optin($id, $optid) {
	require_once(ac_admin('functions/mail.php'));
	$id        = intval($id);
	$optid     = intval($optid);
	$admin     = ac_admin_get();
	$admincond = '';

	# grab every list relation that we can.
	if ( !ac_admin_ismain() ) {
		$liststr   = implode("','", $admin["lists"]);
		$admincond = "AND listid IN ('$liststr')";
	}

	// get subscriber
	$subscriber = subscriber_select_row($id);
	$listids = array();
	$list = null;
	$formid = 0;

	if (!isset($_SESSION["nla"])) {
		$sql = ac_sql_query("SELECT * FROM #subscriber_list WHERE status = 0 AND subscriberid = '$id' $admincond");
		while ( $row = ac_sql_fetch_assoc($sql) ) {
			$listids[] = $row['listid'];
			if ( !$list ) {
				$list = list_select_row($row["listid"]);
				$formid = (int)$row['formid'];
			}
		}
	} else {
		$nl      = (int)$_SESSION["nla"];
		$row     = ac_sql_select_row("SELECT * FROM #subscriber_list WHERE status = 0 AND subscriberid = '$id' AND listid = '$nl'");
		$listids = array($row["listid"]);
		$list    = list_select_row($row["listid"]);
		$formid  = (int)$row['formid'];
	}

	if ( !$list ) return false;

	if (!$formid)
		return false;

	// send optin
	form_send_message_exact($formid, $list["id"], $subscriber);

	if ( !function_exists('ac_ajax_api_result') ) return true;

	return ac_ajax_api_result(true, _a("Email Reminder sent."));
}

function subscriber_optin_multi_post() {
	$ids      = strval(ac_http_param("ids"));
	$optid    = (int)ac_http_param("optid");
	$filterid = (int)ac_http_param("filter");

	if ( !$optid ) $optid = 1;

	return subscriber_optin_multi($ids, $optid, $filterid);
}

function subscriber_optin_multi($ids, $optid, $filter = 0) {
	@set_time_limit(950 * 60);
	if ( $ids == '_all' ) {
		$tmp = array();
		$so = new AC_Select();
		$so->slist = array('s.id');
		$so->remove = false;
		$filter = intval($filter);
		if ($filter > 0) {
			$admin = ac_admin_get();
			$conds = ac_sql_select_one("SELECT conds FROM #section_filter WHERE id = '$filter' AND userid = '$admin[id]' AND sectionid = 'subscriber'");
			$so->push($conds);
		} else {
			$so->push("AND l.status = 0"); // unconfirmed = DEFAULT
		}
		$all = subscriber_select_array($so);
		foreach ( $all as $v ) {
			$tmp[] = $v['id'];
		}
	} else {
		$tmp = array_map("intval", explode(",", $ids));
	}
	foreach ( $tmp as $id ) {
		$r = subscriber_optin($id, $optid);
	}
	return $r;
}

$GLOBALS["subscriber_domaincodes"] = array(
	// AT&T
	"ameritech.net" => 1,
	"att.net" => 1,
	"bellsouth.net" => 1,
	"flash.net" => 1,
	"nvbell.net" => 1,
	"pacbell.net" => 1,
	"prodigy.net" => 1,
	"sbcglobal.com" => 1,
	"sbcglobal.net" => 1,
	"snet.net" => 1,
	"swbell.net" => 1,
	"wans.net" => 1,

	// AOL
	"aim.com" => 2,
	"cs.com" => 2,
	"netscape.net" => 2,
	"aol" => 2,
);

function subscriber_delayed($addr) {
	global $subscriber_domaincodes;

	$expl = explode("@", $addr);

	# ?!
	if (count($expl) < 2)
		return true;

	$domain = $expl[1];

	if (!isset($subscriber_domaincodes[$domain])) {
		// No exact match.  Try to find a partial match.
		$parts = explode(".", $domain);

		// Weird.
		if (count($parts) < 2)
			return false;

		// Not even a partial match.
		if (!isset($subscriber_domaincodes[$parts[0]]))
			return false;

		// If we get here, there was a partial match, and we should pretend that $domain is that.
		$domain = $parts[0];
	}

	$code = (int)$subscriber_domaincodes[$domain];
	$c = (int)ac_sql_select_one("SELECT * FROM #delay WHERE code = '$code'");

	# If we found the code in the table, then we're delayed.
	return $c > 0;
}

?>
