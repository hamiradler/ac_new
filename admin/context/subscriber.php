<?php

require_once ac_admin("functions/subscriber.php");
require_once ac_global_classes("select.php");
require_once ac_global_classes("pagination.php");
class subscriber_context extends ACP_Page {

	function subscriber_context() {
		$this->pageTitle = _a("Subscribers");
		$this->ACP_Page();
	}

	function process(&$smarty) {

		$this->setTemplateData($smarty);

		if (!permission("pg_subscriber_add") && !permission("pg_subscriber_edit") && !permission("pg_subscriber_delete")) {
			ac_smarty_noaccess($smarty);
			return;
		}

		if ( list_get_cnt() == 0 ) {
			$smarty->assign('content_template', 'nolists_subscriber.htm');
			return;
		}

		$smarty->assign("content_template", "subscriber.htm");
		$smarty->assign("side_content_template", "side.subscriber.htm");

		$so = new AC_Select;

		// subscriber search
		$query = trim((string)ac_http_param('q'));
		if ( $query ) $_POST["qsearch"] = $query;

		// list filter
		if ( isset($_GET['listid']) && (int)$_GET['listid'] ) $_POST['listid'] = (int)$_GET['listid'];
		$filterArray = subscriber_filter_post();
		$filter = $filterArray['filterid'];
		$filter_content = ""; // used to pre-populate search box

		if (ac_http_param("filterid")) {
			if (ac_http_param("search")) {
				$filter = (int)ac_http_param("search");
				if (ac_http_param_exists("content")) $filter_content = urldecode( ac_http_param("content") );
			} else {
				$filterArray = subscriber_filter_segment(ac_http_param("filterid"));
				$filter = $filterArray["filterid"];
			}
			$segmentid = intval(ac_http_param("filterid"));
			$smarty->assign("segmentname", ac_sql_select_one("SELECT name FROM #filter WHERE id = '$segmentid'"));
			$smarty->assign("segmentid", $segmentid);
		}

		$smarty->assign("filter_content", $filter_content);

		if ($filter > 0) {
			$conds = ac_sql_select_one("SELECT conds FROM #section_filter WHERE id = '$filter' AND userid = '{$this->admin['id']}' AND sectionid = 'subscriber'");
			$so->push($conds);
		}

		$smarty->assign("filterid", $filter);
		$smarty->assign("listfilter", ( isset($_SESSION['nla']) ? $_SESSION['nla'] : null ));
		$smarty->assign("statfilter", ( isset($_SESSION['12all_subscriber_status']) ? $_SESSION['12all_subscriber_status'] : 1 ));

		$listid = isset($_SESSION['nla']) ? (int)$_SESSION['nla'] : 0;

		if ($listid > 0) {
			$optins = (int)ac_sql_select_one("
				SELECT
					COUNT(*)
				FROM
					#list
				WHERE
					optinmessageid > 0
				AND
					id = '$listid'
			");
			$smarty->assign("optins", $optins);
		}

		// get count
		$so->count('DISTINCT(l.subscriberid)');
		$total = (int)ac_sql_select_one(subscriber_select_query($so));
		$count = $total;

		$paginator = new Pagination($total, $count, $this->admin['subscribers_per_page'], 0, 'main.php?action=subscriber');
		$paginator->allowLimitChange = true;
		$paginator->ajaxAction = 'subscriber.subscriber_select_array_paginator';
		$smarty->assign('paginator', $paginator);

		$sections = array(
			array("col" => "s.email", "label" => _a("Email")),
			array("col" => "l.first_name", "label" => _a("First Name")),
			array("col" => "l.last_name", "label" => _a("Last Name")),
		);
		$smarty->assign("search_sections", $sections);

		$fields_listfilter = ( isset($_SESSION['nla']) ) ? $_SESSION['nla'] : $GLOBALS["admin"]["lists"];

		$fields = list_get_fields($fields_listfilter, true); // no list id's, but global
		$smarty->assign("fields", $fields);

		$lists = list_get_all(false, true, null);
		foreach ( $lists as $k => $v ) {
			$lists[$k]['existingresponders'] = (int)ac_sql_select_one("
				SELECT
					COUNT(*)
				FROM
					#campaign c,
					#campaign_list l
				WHERE
					c.id = l.campaignid
				AND
					l.listid = '$v[id]'
				AND
					c.status != 0
				AND
					c.sdate < NOW()
				AND
					c.type = 'responder'
			");
			$lists[$k]['existingcampaigns'] = (int)ac_sql_select_one("
				SELECT
					COUNT(*)
				FROM
					#campaign c,
					#campaign_list l
				WHERE
					c.id = l.campaignid
				AND
					l.listid = '$v[id]'
				AND
					c.status != 0
				AND
					c.sdate < NOW()
				AND
					c.type IN ('single', 'recurring', 'activerss', 'split', 'text')
			");
		}
		$listsCnt = count($lists);
		$smarty->assign('subscriberLists', $lists);
		$smarty->assign('subscriberListsCnt', $listsCnt);

	}
}

?>
