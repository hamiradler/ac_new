<?php

require_once ac_admin("functions/campaign.php");
require_once ac_global_functions("gravatar.php");
require_once ac_global_classes("select.php");
require_once ac_global_classes("pagination.php");
class report_context extends ACP_Page {

	function report_context() {
		$this->pageTitle = _a("Reports");
		$this->sideTemplate = "side.report.htm";
		$this->ACP_Page();
	}

	function process(&$smarty) {
		$this->setTemplateData($smarty);
		$smarty->assign("content_template", "report.htm");



		if (!$this->admin["pg_reports_campaign"]) {
			$smarty->assign('side_content_template', '');
			$smarty->assign('content_template', 'noaccess.htm');
			return;
		}
		elseif ( list_get_cnt() == 0 ) {
			$smarty->assign('side_content_template', '');
			$smarty->assign('content_template', 'nolists.htm');
			return;
		}

		$so = new AC_Select;
		$so->push("AND c.status != 0");

		// list filter
		$filterArray = campaign_filter_post();
		$filter = $filterArray['filterid'];
		if ($filter > 0) {
			$conds = ac_sql_select_one("SELECT conds FROM #section_filter WHERE id = '$filter' AND userid = '{$this->admin['id']}' AND sectionid = 'campaign'");
			$so->push($conds);
		}
		$smarty->assign("filterid", $filter);
		$smarty->assign("listfilter", ( isset($_SESSION['nla']) ? $_SESSION['nla'] : null ));

		$so->count();
		$total = (int)ac_sql_select_one(campaign_select_query($so));
		$count = $total;

		$paginator = new Pagination($total, $count, 10, 0, 'main.php?action=report');
		//$paginator->allowLimitChange = true;
		$paginator->ajaxAction = 'campaign.campaign_select_array_paginator';
		$smarty->assign('paginator', $paginator);


		$so = new AC_Select();
		$so->count();
		$campaignscnt = (int)ac_sql_select_one(campaign_select_query($so));
		$smarty->assign('campaignscnt', $campaignscnt);


		$so = new AC_Select();
		$so->count('DISTINCT(l.subscriberid)');
		$subs = (int)ac_sql_select_one(subscriber_select_query($so));
		$smarty->assign('subscriberscnt', $subs);


		$campaign_statuses = campaign_statuses();
		$smarty->assign("campaign_statuses", $campaign_statuses);

		$types = campaign_types();
		$smarty->assign("types", $types);

		$recur_intervals = campaign_recur_intervals();
		$smarty->assign("recur_intervals", $recur_intervals);


		campaign_sidemenu($smarty, 'report');

		$admin = ac_admin_get();
		$lists_str = implode("','", $admin["lists"]);

		$query = "
			SELECT
				s.*,
				MD5(LOWER(s.email)) AS emailhash,
				SUM(l.times) AS activity
			FROM
				#subscriber s
			LEFT JOIN
				#link_data l
			ON
				s.id = l.subscriberid
			WHERE
				s.gravatar = 2
			AND
				(SELECT COUNT(*) FROM #subscriber_list sl WHERE sl.subscriberid = s.id AND sl.listid IN ('{$lists_str}')) > 0
			GROUP BY s.id
			ORDER BY activity DESC
			LIMIT 25
		";
		$masonry = array();
		$sql = ac_sql_query($query);
		$num = mysql_num_rows($sql);
		$i = 0;
		while ( $row = mysql_fetch_assoc($sql) ) {
			$row['size'] = $i > $num / 2 ? '50' : '75';
			$row['screenshot'] = gravatar_url($row['emailhash'], $row['size']);
			$masonry[] = $row;
			$i++;
		}
		shuffle($masonry);
		$smarty->assign("masonry", $masonry);

	}
}

?>
