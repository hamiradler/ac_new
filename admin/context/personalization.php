<?php

require_once ac_admin("functions/personalization.php");
require_once ac_global_classes("select.php");
require_once ac_global_classes("pagination.php");
class personalization_context extends ACP_Page {

	function personalization_context() {
		$this->pageTitle = _a("Sender Sersonalization");
		$this->ACP_Page();
	}

	function process(&$smarty) {

		$this->setTemplateData($smarty);
		$smarty->assign("ac_load_editor", "1");

		if (!$this->admin["pg_template_add"] && !$this->admin["pg_template_edit"] && !$this->admin["pg_template_delete"]) {
			$smarty->assign('content_template', 'noaccess.htm');
			return;
		}

		if ( list_get_cnt() == 0 ) {
			$smarty->assign('content_template', 'nolists.htm');
			return;
		}
		$smarty->assign("side_content_template", "side.campaign.htm");
		$smarty->assign("content_template", "personalization.htm");

		campaign_sidemenu($smarty, 'personalization');

		$so = new AC_Select;

		// list filter
		$filterArray = personalization_filter_post();
		$filter = $filterArray['filterid'];
		if ($filter > 0) {
			$conds = ac_sql_select_one("SELECT conds FROM #section_filter WHERE id = '$filter' AND userid = '{$this->admin['id']}' AND sectionid = 'personalization'");
			$so->push($conds);
		}
		$smarty->assign("filterid", $filter);
		$smarty->assign("listfilter", ( isset($_SESSION['nla']) ? $_SESSION['nla'] : null ));

		// get count
		$so->count();
		$total = (int)ac_sql_select_one(personalization_select_query($so));
		$count = $total;

		$paginator = new Pagination($total, $count, 20, 0, 'main.php?action=personalization');
		$paginator->allowLimitChange = true;
		$paginator->ajaxAction = 'personalization.personalization_select_array_paginator';
		$smarty->assign('paginator', $paginator);

		$sections = array(
			array("col" => "tag", "label" => _a("Personalization Tag")),
			array("col" => "name", "label" => _a("Personalization Name")),
			array("col" => "format", "label" => _a("Format")),
			array("col" => "content", "label" => _a("Content")),
		);
		$smarty->assign("search_sections", $sections);

		$fields = list_get_fields(array(), true); // no list id's, but global
		$smarty->assign("fields", $fields);
	}
}

?>
