<?php

require_once ac_admin("functions/campaign.php");
require_once ac_admin("functions/subscriber.php");
require_once ac_global_classes("select.php");
require_once ac_global_classes("pagination.php");

class campaign_new_summary_context extends ACP_Page {

	function campaign_new_summary_context() {
		$this->pageTitle = _a("Create a New Campaign");
		//$this->sideTemplate = "side.message.htm";
		$this->ACP_Page();
	}

	function process(&$smarty) {
		$this->setTemplateData($smarty);
		$admin = ac_admin_get();

		if (!$this->admin["pg_message_add"] && !$this->admin["pg_message_edit"]) {
			$smarty->assign('content_template', 'noaccess.htm');
			return;
		}

		$smarty->assign("content_template", "campaign_new_summary.htm");

		$campaignid = (int)ac_http_param("id");

		if ($campaignid < 1)
			ac_http_redirect("main.php");

		campaign_save_markpos("summary", $campaignid);

		$isEdit = false;
		$showAllMessages = false;

		ac_smarty_submitted($smarty, $this);
		if ( isset($_SESSION["campaign_save_result"][$campaignid]) ) {
			$smarty->assign("formSubmitted", true);
			$smarty->assign("submitResult", $_SESSION["campaign_save_result"][$campaignid]);
			unset($_SESSION["campaign_save_result"][$campaignid]);
		}

		$row = campaign_select_row($campaignid);
		if ( $row ) {
			// use this campaign
			$campaign = $row;
			// campaign info
			if ( in_array($row['status'], array(0, 1, 3, 6, 7)) and !ac_http_param('use') ) { // if not sending or completed
				// statuses that can be reused are : draft, scheduled, (while sending?) paused, stopped
				$campaign['id'] = $row['id']; // edit this campaign allowed
				$campaign['status'] = $row['status']; // reuse the same status
				if ( $row['status'] != 0 ) $isEdit = true;
			} else {
				ac_http_redirect("main.php?action=campaign_new&id=$campaignid");
			}

			if ( !$campaign['lists'] ) {
				ac_http_redirect("main.php?action=campaign_new_list&id=$campaignid");
			}
			if ( !$campaign['messages'] ) {
				$cp = $campaign['type'] == 'text' ? 'text' : 'template';
				ac_http_redirect("main.php?action=campaign_new_$cp&id=$campaignid");
			}
		} else {
			ac_http_redirect("main.php?action=campaign_new");
		}

		$count = campaign_subscribers_fetch(ac_array_extract($campaign["lists"], "id"), $campaign["filterid"], 1, 0, 0, $campaign);
		$smarty->assign("count", $count);

		# List names
		$listids = ac_sql_select_list("SELECT listid FROM #campaign_list WHERE campaignid = '$campaignid'");
		$liststr = implode("','", $listids);
		$listnames = ac_sql_select_list("SELECT name FROM #list WHERE id IN ('$liststr') ORDER BY name");
		$smarty->assign("listnames", implode(", ", $listnames));

		# Figure out of we need to allow Google Analytics
		$showgread = 0;
		$showglink = 0;
		foreach ($listids as $listid) {
			$res = ac_sql_select_row("SELECT p_use_analytics_read, p_use_analytics_link FROM #list WHERE id = '$listid' AND analytics_ua != ''");

			if ($res) {
				if ($res["p_use_analytics_read"] && !$showgread)
					$showgread = 1;

				if ($res["p_use_analytics_link"] && !$showglink)
					$showglink = 1;
			}

			if ($showgread && $showglink)
				break;
		}

		$smarty->assign("showgread", $showgread);
		$smarty->assign("showglink", $showglink);

		// Check if we are supposed to use social sharing funcs
		$pass = function_exists('curl_init') && function_exists('hash_hmac') && (int)PHP_VERSION > 4;
		$pass_twitter = false;
		$pass_facebook = false;
		if ( $pass ) {
			$pass_twitter = (int)ac_sql_select_one("=COUNT(*)", "#list", "id IN ('$liststr') AND twitter_token != '' AND twitter_token_secret != ''");
			$pass_facebook = (int)ac_sql_select_one("=COUNT(*)", "#list", "id IN ('$liststr') AND facebook_session IS NOT NULL AND facebook_session != ''");
			if ( !$pass_twitter ) $campaign['tweet'] = 0;
			if ( !$pass_facebook ) $campaign['facebook'] = 0;
		} else {
			$campaign['tweet'] = $campaign['facebook'] = 0;
		}
		$smarty->assign('isShareable', $pass);
		$smarty->assign("isTweetable", $pass_twitter);
		$smarty->assign("isFacebookable", $pass_facebook);

		// can we offer archiving
		$isForPublic = (bool)ac_sql_select_one("=COUNT(*)", "#list", "id IN ('$liststr') AND private = 0");
		if ( !$isForPublic ) $campaign['public'] = 0;
		$smarty->assign("isForPublic", $isForPublic);

		# Segment name
		$smarty->assign("segmentname", "");
		if ($campaign["filterid"] > 0)
			$smarty->assign("segmentname", (string)ac_sql_select_one("SELECT name FROM #filter WHERE id = '$campaign[filterid]'"));

		# Subscriber total
		$count = 0;
		if ($campaign["filterid"] > 0) {
			$sql = filter_compile($campaign["filterid"]);
			$so  = new AC_Select;
			$so->push("AND l.listid IN ('$liststr')");
			$so->push("AND l.status = 1");

			if ($sql != "")
				$so->push("AND $sql");

			$so->count('DISTINCT(l.subscriberid)');
			$count = (int)ac_sql_select_one(subscriber_select_query($so));
		} else {
			$so  = new AC_Select;
			$so->push("AND l.listid IN ('$liststr')");
			$so->push("AND l.status = 1");

			$so->count('DISTINCT(l.subscriberid)');
			$count = (int)ac_sql_select_one(subscriber_select_query($so));
		}
		$smarty->assign("subtotal", $count);

		# Messages + Links
		$messagelist = ac_sql_select_list("SELECT messageid FROM #campaign_message WHERE campaignid = '$campaignid'");
		$messagestr = implode("','", $messagelist);
		$messages = ac_sql_select_array("SELECT id, subject, htmlfetch, textfetch, html, text FROM #message WHERE id IN ('$messagestr')");

		$linkcount = 0;
		$smarty->assign("hasfetch", 0);
		$smarty->assign("hasrss", 0);

		foreach ($messages as $k => $msg) {
			if (in_array($msg["htmlfetch"], array("send", "cust")) || in_array($msg["textfetch"], array("send", "cust")))
				$smarty->assign("hasfetch", 1);

			if (ac_str_instr("%RSS-FEED%", $msg["html"]) || ac_str_instr("%RSS-FEED%", $msg["text"]))
				$smarty->assign("hasrss", 1);

			$messages[$k]["links"] = ac_sql_select_array("SELECT * FROM #link WHERE campaignid = '$campaignid' AND messageid = '$msg[id]' AND link != 'open'");
			foreach ($messages[$k]["links"] as $l => $v) {
				$linkcount++;
				$actionid = $messages[$k]["links"][$l]["actionid"] = (int)ac_sql_select_one("SELECT id FROM #subscriber_action WHERE linkid = '$v[id]'");
				$messages[$k]["links"][$l]["actioncount"] = (int)ac_sql_select_one("SELECT COUNT(*) FROM #subscriber_action_part WHERE actionid = '$actionid'");
			}
		}

		$smarty->assign("messages", $messages);
		$smarty->assign("linkcount", $linkcount);

		# All the other junk we need for subscriber actions.  Campaigns, (full) lists and fields.
		$lcampaigns = ac_sql_select_array("SELECT DISTINCT c.id, c.name FROM #campaign c, #campaign_list l WHERE c.id = l.campaignid AND l.listid IN ('$liststr') ORDER BY c.id DESC LIMIT 100");
		$smarty->assign("campaigns", $lcampaigns);

		$admin = ac_admin_get();
		$adminliststr = implode("','", $admin["lists"]);

		$llists = ac_sql_select_array("SELECT id, name FROM #list WHERE id IN ('$adminliststr') ORDER BY name LIMIT 100");
		$smarty->assign("lists", $llists);
		$lfields = ac_sql_select_array("SELECT DISTINCT f.id, f.title FROM #field f, #field_rel r WHERE f.id = r.fieldid AND r.relid IN ('0', '$liststr')");
		$smarty->assign("fields", $lfields);
		$dfields = ac_sql_select_list("SELECT DISTINCT f.id FROM #field f, #field_rel r WHERE f.id = r.fieldid AND r.relid IN ('0', '$liststr') AND f.type = 'date'");
		$smarty->assign("datefields", $dfields);

		# Figure out our read-action id (possibly 0 if we have none).
		$readactionid = (int)ac_sql_select_one("SELECT id FROM #subscriber_action WHERE campaignid = '$campaignid' AND type = 'read'");
		$smarty->assign("readactionid", $readactionid);
		$smarty->assign("readactioncount", (int)ac_sql_select_one("SELECT COUNT(*) FROM #subscriber_action_part WHERE actionid = '$readactionid'"));

		# Our time zone offset.
		$smarty->assign("tzoffset", tz_gmtoffset($admin["local_zoneid"]));

		# To avoid having a huge list of options for the schedule taking up space in the template, here are some hours/minutes arrays.
		$hours = array();
		$minutes = array();

		for ($i = 0; $i < 24; $i++)
			$hours[] = sprintf("%02d", $i);

		for ($i = 0; $i < 60; $i++)
			$minutes[] = sprintf("%02d", $i);

		$smarty->assign("hours", $hours);
		$smarty->assign("minutes", $minutes);

		if ($campaign["schedule"] && $campaign["sdate"] != "") {
			# Fake like the default times are the ones in the campaign.
			$time = strtotime($campaign["sdate"]);
			if ($time !== false && $time > 0) {
				if ( !in_array($campaign["type"], array("responder", "reminder")) && $time < time() ) {
					$time = time();
					$campaign["schedule"] = 0;
				}
				$smarty->assign("currenthour", date("H", $time));
				$smarty->assign("currentminute", date("i", $time));
				$smarty->assign("currentdate", date("Y/m/d", $time));
			} else {
				# The current hour, minute, date.
				$rval = ac_sql_select_row("SELECT NOW() AS tstamp", array("tstamp"));
				$time = strtotime($rval["tstamp"]);
				$smarty->assign("currenthour", date("H", $time));
				$smarty->assign("currentminute", date("i", $time));
				$smarty->assign("currentdate", date("Y/m/d", $time));
				$campaign["schedule"] = 0;
			}
		} else {
			# The current hour, minute, date.
			$rval = ac_sql_select_row("SELECT NOW() AS tstamp", array("tstamp"));
			$time = strtotime($rval["tstamp"]);
			$smarty->assign("currenthour", date("H", $time));
			$smarty->assign("currentminute", date("i", $time));
			$smarty->assign("currentdate", date("Y/m/d", $time));
			$campaign["schedule"] = 0;
		}

		# Figure out the default reminder string.
		$reminder_example = campaign_reminder_compile($campaign["reminder_field"], $campaign["reminder_offset_sign"], $campaign["reminder_offset"], $campaign["reminder_offset_type"]);
		$smarty->assign("reminder_example", $reminder_example);

		// assign all presets
		$smarty->assign('campaignid', $campaignid);
		$smarty->assign('campaign', $campaign);
		$smarty->assign("isEdit", $isEdit);
		$smarty->assign("showAllMessages", $showAllMessages);

		# Last ditch check; too many subscribers?
		$pastlimit = campaign_subscribers($campaignid, $campaign["filterid"]);
		$smarty->assign("pastlimit", $pastlimit);

		// default debugging
		$debugging = $campaign['mailer_log_file'];
		// custom debugging
		if ( ac_http_param_exists('debug') ) {
			$debugging = (int)ac_http_param('debug');
		}
		$smarty->assign("debugging", $debugging);
		$smarty->assign('isDemo', isset($GLOBALS['demoMode']));

		$recur_intervals = campaign_recur_intervals();
		$smarty->assign("recur_intervals", $recur_intervals);
	}

	function formProcess(&$smarty) {
		campaign_save();
		campaign_save_after();

		if ($GLOBALS["campaign_save_id"] > 0)
			ac_http_redirect("main.php?action=campaign_new_summary&id=$GLOBALS[campaign_save_id]");

	}
}

?>
