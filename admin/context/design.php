<?php

require_once ac_admin("functions/design.php");
require_once ac_global_classes("select.php");
require_once ac_global_classes("pagination.php");
class design_context extends ACP_Page {

	function design_context() {
		$this->pageTitle = _a("Design Settings");
		$this->ACP_Page();
	}

	function process(&$smarty) {
		$this->setTemplateData($smarty);
		$smarty->assign("ac_load_editor", "1");

		if (!in_array('3', $this->admin['groups'])) {
			$smarty->assign('content_template', 'noaccess.htm');
			return;
		}

		$smarty->assign("content_template", "design.htm");

		$so = new AC_Select;
		$so->count();
		$total = (int)ac_sql_select_one(design_select_query($so));
		$count = $total;

		$paginator = new Pagination($total, $count, 20, 0, 'main.php?action=design');
		$paginator->allowLimitChange = true;
		$paginator->ajaxAction = 'design.design_select_array_paginator';
		$smarty->assign('paginator', $paginator);

		$sections = array(
			array("col" => "title", "label" => _a("Group Name")),
			array("col" => "descript", "label" => _a("Group Description")),
			array("col" => "site_name", "label" => _a("Site Name")),
			array("col" => "header_text", "label" => _a("Text Header")),
			array("col" => "header_html", "label" => _a("HTML Header")),
			array("col" => "footer_text", "label" => _a("Text Footer")),
			array("col" => "footer_html", "label" => _a("HTML Footer")),
		);
		$smarty->assign("search_sections", $sections);


		// default html templates
		$admin_template_htm = ac_file_get(ac_admin('templates/main.tpl'));
		$smarty->assign("admin_template_htm", $admin_template_htm);
		$public_template_htm = ac_file_get(ac_base('templates/main.tpl'));
		$smarty->assign("public_template_htm", $public_template_htm);

		$smarty->assign("siteroot", ac_site_inroot());

	}
}

?>
