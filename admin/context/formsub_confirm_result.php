<?php

require_once ac_global_classes("select.php");
require_once ac_global_classes("pagination.php");
require_once ac_admin("functions/form.php");

class formsub_confirm_result_context extends ACP_Page {
	function formsub_confirm_result_context() {
		$this->pageTitle = _a("Edit Form");
		//$this->sideTemplate = "side.message.htm";
		$this->ACP_Page();
	}

	function process(&$smarty) {
		$this->setTemplateData($smarty);

		ac_smarty_submitted($smarty, $this);

		$admin = ac_admin_get();
		$listid = (int)ac_http_param("listid");
		$smarty->assign("ac_load_editor", "1");

		if (!$listid) {
			$listid = current($admin["lists"]);
			if (!$listid) ac_http_redirect("main.php?action=form");
			ac_http_redirect("main.php?action=formsub_confirm_result&listid=$listid");
		}

		$form = form_select_row_listid($listid, FORM_SUBSCRIBE_CONFIRM_RESULT);

		if (!$form) {
			if (!$this->admin["pg_form_edit"]) {
				$smarty->assign('content_template', 'noaccess.htm');
				return;
			}

			$listname = (string)ac_sql_select_one("SELECT name FROM #list WHERE id = '$listid'");
			$name = sprintf(_a("%s Unsubscribe"), $listname);

			$ins = array(
				"name" => $name,
				"theme" => "simple-blue",
				"target" => FORM_UNSUBSCRIBE,
			);

			ac_sql_insert("#form", $ins);
			$id = (int)ac_sql_insert_id();

			# Set up list relation
			$ins = array(
				"formid" => $id,
				"listid" => $listid,
			);

			ac_sql_insert("#form_list", $ins);
			ac_http_redirect("main.php?action=formsub_confirm_result&listid=$listid");
		}

		$id = $form["id"];

		ac_smarty_submitted($smarty, $this);

		$lists = ac_sql_select_list("SELECT listid FROM #form_list WHERE formid = '$form[id]'");
		$liststr = implode("','", $lists);

		// Now get the actual lists we need for the dropdown...
		$adminlists = implode("','", $admin["lists"]);
		$lists = ac_sql_select_array("SELECT id, name FROM #list WHERE id IN ('$adminlists') ORDER BY name");

		# See if we have any parts... add some automatically if not.
		$parts = ac_sql_select_array("SELECT id, fieldid, builtin FROM #form_part WHERE formid = '$id'");

		if (count($parts) == 0) {
			$ins = array(
				"formid" => $id,
				"builtin" => "email",
				"header" => _a("Email"),
				"ordernum" => 0,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "unsubscribe",
				"header" => _a("Unsubscribe"),
				"ordernum" => 1,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "subscribe",
				"content" => _a("Submit"),
				"ordernum" => 2,
			);

			ac_sql_insert("#form_part", $ins);
			$parts = ac_sql_select_array("SELECT id, fieldid, builtin FROM #form_part WHERE formid = '$id'");
		}

		# Grab any themes.
		$dp = @opendir(ac_admin("templates/form-themes"));
		if (!$dp)
			ac_http_redirect("main.php");

		$themes = array();
		while ($file = @readdir($dp)) {
			if ($file == "." || $file == "..")
				continue;

			$path = "templates/form-themes/$file";
			$themes[] = array(
				"preview" => sprintf("%s/preview.gif", $path),
				"style" => sprintf("%s/style.css", $path),
				"name" => $file,
				"selected" => ($file == $form["theme"]) ? 1 : 0,
			);
		}

		if (form_theme_safe($form["theme"]))
			$path = dirname(dirname(__FILE__)) . "/templates/form-themes/$form[theme]";

		if (file_exists($path . "/header.htm"))
			$smarty->assign("header", @file_get_contents($path . "/header.htm"));

		if (file_exists($path . "/header.htm"))
			$smarty->assign("footer", @file_get_contents($path . "/footer.htm"));

		$smarty->assign("form", $form);
		$smarty->assign("lists", $lists);
		$smarty->assign("listid", $listid);
		$smarty->assign("themes", $themes);
		$smarty->assign("showtheme", (int)ac_http_param("theme"));
		$smarty->assign("content_template", "formsub_confirm_result.htm");
	}

	function formProcess(&$smarty) {
		if (ac_http_param("redirecting") == 1) {
			$formid = (int)ac_http_param("formid");
			$listid = (int)ac_http_param("listid");
			$url = (string)ac_http_param("redirecturl");

			if (!$formid)
				ac_http_redirect("main.php?action=form#list-01-0-0");

			$up = array(
				"redirecturl" => $url,
			);

			ac_sql_update("#form", $up, "id = '$formid'");
			ac_http_redirect("main.php?action=formsub_confirm_result&listid=$listid");
		}
		if (ac_http_param("imageupload") == 1) {
			$partid = (int)ac_http_param("partid");
			$formid = (int)ac_http_param("id");
			$listid = (int)ac_http_param("listid");
			$imagealt = (string)ac_http_param("imagealt");

			if ($formid == 0)
				ac_http_redirect("main.php?action=form");

			if ($partid == 0 || !isset($_FILES['imagefile']) || !isset($_FILES['imagefile']['tmp_name']))
				ac_http_redirect("main.php?action=formsub_confirm_result&listid=$listid");

			$content = @file_get_contents($_FILES['imagefile']['tmp_name']);
			$type = $_FILES['imagefile']['type'];
			$size = (int)$_FILES['imagefile']['size'];

			$up = array(
				"imagedata" => $content,
				"imagetype" => $type,
				"imagesize" => $size,
				"imagealt" => $imagealt,
				"imagealign" => (string)ac_http_param("imagealign"),
			);

			if ($up["imagedata"] == "") {
				unset($up["imagedata"]);
				unset($up["imagetype"]);
				unset($up["imagesize"]);
			}

			if (isset($up["imagedata"]) && function_exists('getimagesize')) {
				$rval = @getimagesize($_FILES['imagefile']['tmp_name']);
				$up["imagewidth"] = $rval[0];
				$up["imageheight"] = $rval[1];
			}

			ac_sql_update("#form_part", $up, "id = '$partid'");
			ac_http_redirect("main.php?action=formsub_confirm_result&listid=$listid");
		}
	}
}

?>
