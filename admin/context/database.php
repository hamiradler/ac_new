<?php

require_once ac_admin("functions/database.php");
require_once ac_global_classes("select.php");
require_once ac_global_classes("pagination.php");
require_once ac_global_functions("file.php");

class database_context extends ACP_Page {
	function database_context() {
		$this->pageTitle = _a("Database Utilities");
		//$this->sideTemplate = "side.settings.htm";
		$this->ACP_Page();
		$this->admin = $GLOBALS["admin"];
	}

	function process(&$smarty) {

		$smarty->assign("content_template", "database.htm");
		$this->setTemplateData($smarty);

		ac_smarty_submitted($smarty, $this);

		if ($this->admin["id"] == 1 && ac_http_param("backup")) {
			$gz = ac_http_param("gz");
			$file = sprintf("backup-%s.", date("Ymd-His"));
			ac_http_header_attach($file . ($gz ? 'gz' : 'sql'));	# Assumes appliation/octet-stream
			database_backup($gz);
			exit;
		}
	}

	function formProcess(&$smarty) {
		if ($this->admin["id"] == 1 && isset($_FILES["restore"])) {
#			if ($_FILES["restore"]["size"] > 5000000) {
			if ($_FILES["restore"]["size"] > 30000) {
				ac_smarty_message($smarty, _a("Database backup file to restore is too large.  Consult with your MySQL administrator about restoring from the command line."));
				return;
			}
			$sql = ac_file_get($_FILES["restore"]["tmp_name"]);
			ac_sql_restore($sql, true, 'comment');
		}
	}
}

?>
