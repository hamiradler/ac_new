<?php

require_once ac_global_classes("select.php");
require_once ac_global_classes("pagination.php");
require_once ac_admin("functions/form.php");
require_once ac_admin("functions/message.php");

class form_optin_context extends ACP_Page {
	function form_optin_context() {
		$this->pageTitle = _a("Form Opt-In");
		$this->ACP_Page();
	}

	function process(&$smarty) {
		$this->setTemplateData($smarty);
		$smarty->assign("ac_load_editor", "1");

		if (!ac_http_param("id")) {
			ac_http_redirect("main.php?action=form#list-01-0-0");
		}

		$id = (int)ac_http_param("id");
		$form = form_select_row($id);

		if (!$form)
			ac_http_redirect("main.php?action=form#list-01-0-0");

		$message = message_select_row($form["messageid"]);

		$lists = ac_sql_select_list("SELECT listid FROM #form_list WHERE formid = '$id'");
		$liststr = implode("','", $lists);
		$fields = ac_cfield_select_nodata_rel("#field", "#field_rel", "r.relid IN ('0', '$liststr')");

		$smarty->assign("form", $form);
		$smarty->assign("message", $message);
		$smarty->assign("fields", $fields);
		$smarty->assign("content_template", "form_optin.htm");

		ac_smarty_submitted($smarty, $this);
	}

	function formProcess(&$smarty) {
		form_optin_save();

		$id = (int)ac_http_param("id");
		switch (ac_http_param("next")) {
			default:
			case "list":
				ac_http_redirect("main.php?action=form#list-01-0-0");
				exit;

			case "settings":
				ac_http_redirect("main.php?action=form_settings&id=$id");
				exit;

			case "edit":
				ac_http_redirect("main.php?action=form_edit&id=$id");
				exit;

			case "theme":
				ac_http_redirect("main.php?action=form_edit&id=$id&theme=1");
				exit;

			case "optin":
				ac_http_redirect("main.php?action=form_optin&id=$id");
				exit;

			case "code":
				ac_http_redirect("main.php?action=form_code&id=$id");
				exit;
		}
	}
}

?>
