<?php

require_once ac_global_classes("select.php");
require_once ac_global_classes("pagination.php");
require_once ac_admin("functions/form.php");
require_once ac_admin("functions/message.php");
require_once ac_admin("functions/list.php");

class formdetails_request_email_context extends ACP_Page {
	function formdetails_request_email_context() {
		$this->pageTitle = _a("Unsubscribe Confirmation Form");
		$this->ACP_Page();
	}

	function process(&$smarty) {
		$this->setTemplateData($smarty);

		$admin = ac_admin_get();
		$listid = (int)ac_http_param("listid");
		$smarty->assign("ac_load_editor", "1");
		if (!$listid) {
			$listid = current($admin["lists"]);
			if (!$listid) ac_http_redirect("main.php?action=form");
			ac_http_redirect("main.php?action=formdetails_request_email&listid=$listid");
		}

		$form = form_select_row_listid($listid, FORM_DETAILS_REQUEST);

		if (!$form) {
			ac_http_redirect("main.php?action=form#list-01-0-0");
		}

		$message = message_select_row($form["messageid"]);
		$fields = ac_cfield_select_nodata_rel("#field", "#field_rel", "r.relid IN ('0', '$listid')");

		$adminlists = implode("','", $admin["lists"]);
		$lists = ac_sql_select_array("SELECT id, name FROM #list WHERE id IN ('$adminlists') ORDER BY name");

		$smarty->assign("lists", $lists);
		$smarty->assign("form", $form);
		$smarty->assign("list", list_select_row($listid));
		$smarty->assign("listid", $listid);
		$smarty->assign("message", $message);
		$smarty->assign("fields", $fields);
		$smarty->assign("content_template", "formdetails_request_email.htm");

		ac_smarty_submitted($smarty, $this);
	}

	function formProcess(&$smarty) {
		$id = (int)ac_http_param("id");
		$listid = (int)ac_http_param("listid");
		$messageid = (int)ac_http_param("messageid");
		$manage = (int)ac_http_param("managetext");

		$up = array(
			"managetext" => $manage,
		);

		ac_sql_update("#form", $up, "id = '$id'");

		$up = array(
			"fromname" => ac_http_param("fromname"),
			"fromemail" => ac_http_param("fromemail"),
			"reply2" => ac_http_param("reply2"),
			"subject" => ac_http_param("subject"),
			"html" => ac_http_param("html"),
			"text" => ac_http_param("text"),
		);

		# If they're not managing their own text version, do a quick convert on the html.
		if (!$manage) {
			$up["text"] = ac_htmltext_convert($up["html"]);
		}

		ac_sql_update("#message", $up, "id = '$messageid'");

		// Update the list field for the optout conf.

		$up = array(
			"optoutconf" => (int)ac_http_param("optoutconf"),
		);

		ac_sql_update("#list", $up, "id = '$listid'");
		ac_http_redirect("main.php?action=formdetails_request_email&listid=$listid");
	}
}

?>
