#!/usr/local/bin/php
<?php
// require main include file
define('ACADMINCRON', true);
require_once(dirname(__FILE__) . '/prepend.inc.php');
require_once(ac_global_functions('process.php'));
require_once(ac_global_functions('mime.php'));


// turning off some php limits
@ignore_user_abort(1);
@ini_set('max_execution_time', 950 * 60);
@set_time_limit(950 * 60);
$ml = ini_get('memory_limit');
if ( $ml != -1 and (int)$ml < 128 and substr($ml, -1) == 'M') @ini_set('memory_limit', '128M');
set_include_path('.');
@set_magic_quotes_runtime(0);


$debug = (bool)ac_http_param('debug');
//$test = (bool)ac_http_param('test');


if ( $debug ) {
	if ( !defined('AC_POP3_DEBUG') ) define('AC_POP3_DEBUG', $debug);
}

require_once(ac_admin('functions/emailaccount.php'));
require_once(ac_global_functions('pop3.php'));



// Preload the language file
ac_lang_get('admin');

$email = ac_php_stdin();

emailaccount_parse(ac_mail_extract($email), $email);

?>
