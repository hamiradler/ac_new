ALTER TABLE `em_sync` ADD `updateexisting` TINYINT( 1 ) DEFAULT '1' NOT NULL AFTER `lastmessage` ;


ALTER TABLE `em_subscriber_list` ADD `first_name` VARCHAR( 250 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ;
ALTER TABLE `em_subscriber_list` ADD `last_name` VARCHAR( 250 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ;

ALTER TABLE `em_link_data` DROP INDEX `linkid`