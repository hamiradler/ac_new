ALTER TABLE `em_bounce_data` ADD KEY `subscriberid` (`subscriberid`), ADD KEY `type` (`type`) ;

|ALTER TABLE `em_link_data` DROP KEY `tstamp`;
ALTER TABLE `em_link_data` ADD KEY `campaignid` (`campaignid`);

ALTER TABLE `em_campaign` ADD KEY `type` (`type`);
ALTER TABLE `em_subscriber` ADD KEY `gravatar` (`gravatar`);
