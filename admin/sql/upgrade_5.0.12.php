<?php

require_once dirname(dirname(__FILE__)) . "/functions/em.php";

$done = true;
spit(_a('Converting subscriber-responder relations (introducing lists into the equation): '), 'em', 1);
$sql = ac_sql_query("
	SELECT
		r.id,
		(
			SELECT
				c.listid
			FROM
				#campaign_list c,
				#subscriber_list s
			WHERE
				r.campaignid = c.campaignid
			AND
				r.subscriberid = s.subscriberid
			AND
				c.listid = s.listid
			LIMIT 0, 1
		) AS listid
	FROM
		#subscriber_responder r
");
while ( $row = ac_sql_fetch_assoc($sql) ) {
	$lid = (int)$row['listid'];
	spit('. ', ( $lid > 0 ? 'strong' : '' ));
	if ( $lid > 0 ) {
		$done = ac_sql_update_one('#subscriber_responder', 'listid', $lid, "id = '$row[id]'");
		if ( !$done ) break;
	}
}
if ( !$done ) {
	spit(_a('Error'), 'strong|error', 1);
	error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
	return;
} else {
	spit(_a('Done'), 'strong|done', 1);
}

?>
