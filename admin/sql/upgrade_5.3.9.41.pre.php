<?php

spit(_a("Converting template screenshots: "), 'em');

ac_sql_delete("#screenshot", "target = 'template'");

$sql = ac_sql_query("SELECT id, preview_mime, preview_data FROM `#template` WHERE preview_data != ''");

while ( $row = mysql_fetch_assoc($sql) ) {
	$shot = new Screenshot;
	$shot->target = "template";
	$shot->targetid = $row["id"];
	$shot->mime = $row["preview_mime"];
	$shot->data = $row["preview_data"];
	$shot->insert();
}

spit(_a('Done'), 'strong|done', 1);

?>
