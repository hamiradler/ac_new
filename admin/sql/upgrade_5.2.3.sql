|ALTER TABLE `em_backend`  ADD COLUMN `facebook_app_id` VARCHAR(128) NOT NULL DEFAULT '' AFTER `twitter_consumer_secret`;
|ALTER TABLE `em_backend`  ADD COLUMN `facebook_app_secret` VARCHAR(128) NOT NULL DEFAULT '' AFTER `facebook_app_id`;
|UPDATE `em_backend` SET `maxcampaignid` = (SELECT MAX(`id`) FROM `em_campaign`);
