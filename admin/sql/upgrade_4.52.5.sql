ALTER TABLE `12all_lists` ADD `default_reply_to` VARCHAR( 250 ) NULL;
ALTER TABLE `12all_lists` ADD `charset` VARCHAR( 250 ) NOT NULL DEFAULT 'utf-8';
ALTER TABLE `12all_lists` ADD `encoding` VARCHAR( 250 ) NOT NULL DEFAULT '8bit';
ALTER TABLE `12all_lists` ADD `a_duplicate_s` INT( 1 ) NOT NULL DEFAULT '0';
UPDATE `12all_messages` SET type = 'mime' WHERE type = '';