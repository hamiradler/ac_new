<?php

// Force users to have only one group.  Always use the "first" group that they 
// are assigned to.  Force admin to be a member of the Admin user group.
$rs = ac_sql_query("SELECT id FROM #user");
while ($row = ac_sql_fetch_assoc($rs)) {
	$_rs = ac_sql_query("SELECT id FROM #user_group WHERE userid = '$row[id]'");
	$skip = true;

	if ($row["id"] == 1) {
		continue;	// Skip for now... admin hasn't had multi-group capabilities in a long time.

		//ac_sql_delete("#user_group", "userid = '1'");
		//$ins = array(
		//	"userid" => 1,
		//	"groupid" => 3,
		//);
		//ac_sql_insert("#user_group", $ins);
	} else {
		while ($_row = ac_sql_fetch_assoc($_rs)) {
			if ($skip) {
				$skip = false;
				continue;
			}

			ac_sql_delete("#user_group", "id = '$_row[id]'");
		}
	}
}

?>
