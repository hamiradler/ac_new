<?php

require_once dirname(dirname(__FILE__)) . "/functions/em.php";

spit(_a('Converting links with corrupted link tracking attributes: '), 'em');
$done = ac_sql_query("
	UPDATE
		#link
	SET
		`link` = REPLACE(`link`, '&amp;', '&')
");
if ( !$done ) {
	spit(_a('Error'), 'strong|error', 1);
	error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
	return;
} else {
	spit(_a('Done'), 'strong|done', 1);
}

?>
