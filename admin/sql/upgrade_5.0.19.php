<?php

require_once dirname(dirname(__FILE__)) . "/functions/em.php";

spit(_a('Compiling bounce statistics: '), 'em');
$rs = ac_sql_query("SELECT id, campaignid, messageid FROM #campaign_message");
while ($row = ac_sql_fetch_assoc($rs)) {
	$ary = ac_sql_select_row("
		SELECT
			(
				SELECT
					COUNT(*)
				FROM
					#bounce_data
				WHERE
					campaignid = '$row[campaignid]'
				AND
					messageid = '$row[messageid]'
				AND
					type = 'hard'
			) AS hardbounces,
			(
				SELECT
					COUNT(*)
				FROM
					#bounce_data
				WHERE
					campaignid = '$row[campaignid]'
				AND
					messageid = '$row[messageid]'
				AND
					type = 'soft'
			) AS softbounces
	");
	ac_sql_query("
		UPDATE
			#campaign_message
		SET
			hardbounces = '$ary[hardbounces]',
			softbounces = '$ary[softbounces]'
		WHERE
			id = '$row[id]'
	");
}
spit(_a('Done'), 'strong|done', 1);

?>
