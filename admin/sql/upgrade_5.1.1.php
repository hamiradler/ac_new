<?php

require_once dirname(dirname(__FILE__)) . "/functions/em.php";
require_once ac_global_functions("tz.php");

spit(_a('Updating site time offset: '), 'em');
$rs = ac_sql_query("SELECT local_zoneid FROM #backend WHERE id = '1'");
if ($row = ac_sql_fetch_assoc($rs)) {
	$up               = array();
	$offset           = tz_offset($row["local_zoneid"]);
	$up["t_offset_o"] = ($offset >= 0 ? "+" : "-");
	$up["t_offset"]   = tz_hours($offset);
	$up["t_offset_min"] = tz_minutes($offset, $up["t_offset"]);

	ac_sql_update("#backend", $up, "id = '1'");
}
spit(_a('Done'), 'strong|done', 1);
spit(_a('Updating user time offsets: '), 'em');
$rs = ac_sql_query("SELECT id, local_zoneid FROM em_user");
while ($row = ac_sql_fetch_assoc($rs)) {
	$up               = array();
	$offset           = tz_offset($row["local_zoneid"]);
	$up["t_offset_o"] = ($offset >= 0 ? "+" : "-");
	$up["t_offset"]   = tz_hours($offset);
	$up["t_offset_min"] = tz_minutes($offset, $up["t_offset"]);

	ac_sql_update("#user", $up, "id = '$row[id]'");
}
spit(_a('Done'), 'strong|done', 1);

?>
