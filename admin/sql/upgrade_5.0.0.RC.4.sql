ALTER TABLE `em_message` CHANGE `html` `html` LONGTEXT NULL;
ALTER TABLE `em_message` CHANGE `text` `text` LONGTEXT NULL;
ALTER TABLE `em_subscriber` ADD KEY `cdate` (`cdate`);
ALTER TABLE `em_subscriber_list` ADD KEY `sdate` (`sdate`);
ALTER TABLE `em_subscriber_list` ADD KEY `udate` (`udate`);
