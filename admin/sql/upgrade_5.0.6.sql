ALTER TABLE `em_loginsource` ADD `binddn` VARCHAR( 250 ) NOT NULL DEFAULT '';
ALTER TABLE `em_loginsource` ADD `bindpw` VARCHAR( 250 ) NOT NULL DEFAULT '';
ALTER TABLE `em_loginsource` ADD `userattr` VARCHAR( 250 ) NOT NULL DEFAULT '';
ALTER TABLE `em_subscriber_import` ADD `code` INT( 10 ) NOT NULL DEFAULT '0' AFTER `res`;
UPDATE `em_trapperr` SET `value` = 'em_trapperrlogs' WHERE `id` = 'db_table';
