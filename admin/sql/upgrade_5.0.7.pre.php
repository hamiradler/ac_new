<?php

spit(_a('Converting Blocked Emails/IPs into a per-list Exclusion List: '), 'em');
$sql = ac_sql_query("SELECT b.listid, b.phrase FROM #block b, #list l WHERE b.listid = l.id");
$done = true;
if ( $sql ) {
	while ( $row = ac_sql_fetch_assoc($sql) ) {
		$phraseEsc = ac_sql_escape($row['phrase']);
		$id = (int)ac_sql_select_one('id', '#exclusion', "email = '$phraseEsc'");
		if ( !$id ) {
			$insert = array(
				'id' => 0,
				'email' => $row['phrase'],
			);
			$done = ac_sql_insert('#exclusion', $insert);
			if ( !$done ) break;
			$id = ac_sql_insert_id();
		}
		$found = (int)ac_sql_select_one('=COUNT(*)', '#exclusion_list', "exclusionid = '$id' AND listid IN ('$row[listid]', '0')");
		if ( !$found ) {
			$insert = array(
				'id' => 0,
				'exclusionid' => $id,
				'listid' => $row['listid'],
				'sync' => 0,
			);
			$done = ac_sql_insert('#exclusion_list', $insert);
			if ( !$done ) break;
		}
	}
}
if ( !$done ) {
	spit(_a('Error'), 'strong|error', 1);
	error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
	return;
} else {
	spit(_a('Done'), 'strong|done', 1);
}

spit(_a('Fixing bounce counts for subscribers: '), 'em');
// first reset ALL subscriber rows to 0
$update = array(
	'bounced_hard' => 0,
	'bounced_soft' => 0,
	'=bounced_date' => 'NULL',
);
$done = ac_sql_update('#subscriber', $update);
if ( !$done ) {
	spit(_a('Error'), 'strong|error', 1);
	error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
	return;
}
// now fetch all bounce data
$sql = ac_sql_query("SELECT subscriberid, type, tstamp FROM #bounce_data ORDER BY tstamp");
while ( $row = ac_sql_fetch_assoc($sql) ) {
	// and update all subscribers
	$update = array(
		'=bounced_' . $row['type'] => "bounced_$row[type] + 1",
		'bounced_date' => $row['tstamp']
	);
	$done = ac_sql_update('#subscriber', $update, "id = '$row[subscriberid]'");
}
if ( !$done ) {
	spit(_a('Error'), 'strong|error', 1);
	error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
	return;
} else {
	spit(_a('Done'), 'strong|done', 1);
}

?>
