UPDATE `12all_admin_p` SET `dflt_a_rq` = 1 WHERE `dflt_a_rq` != '0' AND `dflt_a_rq` != '1';
UPDATE `12all_admin_p` SET `dflt_a_sc` = 1 WHERE `dflt_a_sc` != '0' AND `dflt_a_sc` != '1';
UPDATE `12all_admin_p` SET `dflt_a_priv` = 0 WHERE `dflt_a_priv` != '0' AND `dflt_a_priv` != '1';
ALTER TABLE `12all_admin_p` CHANGE `dflt_a_rq` `dflt_a_rq` TINYINT( 1 ) NOT NULL DEFAULT '1';
ALTER TABLE `12all_admin_p` CHANGE `dflt_a_sc` `dflt_a_sc` TINYINT( 1 ) NOT NULL DEFAULT '1';
ALTER TABLE `12all_admin_p` CHANGE `dflt_a_priv` `dflt_a_priv` TINYINT( 1 ) NOT NULL DEFAULT '0';
ALTER TABLE `12all_logs` ADD `comment` text NULL;