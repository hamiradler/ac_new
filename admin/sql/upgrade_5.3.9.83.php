<?php

require_once(dirname(dirname(__FILE__)) . "/functions/em.php");

$template = ac_sql_select_row("SELECT `id`, `content` FROM #template WHERE `name` = 'Bookkeepr'");

if ($template) {
	if ( preg_match("/align=\"left\">$/", $template["content"]) ) {
		// if content ends in align="left"> , delete it
		$delete = ac_sql_query("DELETE FROM #template WHERE id = $template[id] LIMIT 1");
		$delete = ac_sql_query("DELETE FROM #template_list WHERE templateid = $template[id]");
		$delete = ac_sql_query("DELETE FROM #template_tag WHERE templateid = $template[id]");
		// re-import it
		$templates_import = import_files("template", "xml", array("bookkeepr.xml"));
	}
	elseif ( preg_match("/<\/center>$/", $template["content"]) ) {
		// if content ends in </center> , then they likely edited the template already (the editor tries to complete the HTML)
		$new_content = $template["content"] . "</body></html>";
		$ary = array(
			"content" => $new_content,
		);
		$done = ac_sql_update("#template", $ary, "id = '$template[id]'");
	}
}

?>