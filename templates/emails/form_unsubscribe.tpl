<body>
	<div style="font-size: 12px; font-family: Arial, Helvetica;"><strong>Hi,</strong></div>
	<div style="font-size: 12px; font-family: Arial, Helvetica;"><b><br></b></div>
	<div style="font-size: 12px; font-family: Arial, Helvetica;"><b>This is a confirmation email letting you know about your subscription update with our mailing list %LISTNAME%</b></div>
	<div style="padding: 15px; font-size: 12px; background: #F2FFD8; border: 3px solid #E4F4C3; margin-bottom: 0px; margin-top: 15px; font-family: Arial, Helvetica;">You have been unsubscribed and will not receive any further emails.</div>
</body>
