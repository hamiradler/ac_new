<?php

define('ACPUBLIC', true);
if ( !isset($_GET['useauth']) ) define('ACP_USER_NOAUTH', true);

// require main include file
require_once('admin/prepend.inc.php');
require_once ac_admin("functions/message.php");
require_once ac_admin("functions/campaign.php");
require_once ac_admin("functions/socialsharing.php");
require_once(ac_global_functions('ajax.php'));

// Preload the language file
ac_lang_get( !isset($_GET['useauth']) ? 'admin' : 'public' );

if ( !ac_http_param_exists('c') || !ac_http_param_exists('m') || !ac_http_param_exists('s') ) exit();

$c = (int)ac_http_param('c');
$m = (int)ac_http_param('m');
$hash = trim((string)ac_http_param('s'));
// stumbleupon uses "referral"
$ref = ( ac_http_param_exists('referral') ) ? ac_http_param('referral') : ac_http_param('ref');
$email = '_t.e.s.t_@example.com';
if ( $hash != '' ) {
	$subscriber = subscriber_exists($hash, 0, 'hash');
	if ( $subscriber ) {
		$email = $subscriber['email'];
	}
}

$url = (!empty($_SERVER['HTTPS'])) ? "https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] : "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];

$query = "SELECT * FROM #campaign WHERE `id` = '$c'";
$campaign = ac_sql_select_row($query);
if (!$campaign) {
	ac_flush(_p('Unable to redirect you to this link. Please try again or contact your list admin.'));
	exit;
}

list($url, $link, $socmedia) = socialsharing_process_link($c, $m, $hash, $url, null);

ac_http_redirect($url, $stop = false);

//subscriber_action_dispatch("social", $subscriber, null, $campaign, null, $socmedia);

?>