<?php

require_once dirname(__FILE__) . "/admin/prepend.inc.php";
require_once ac_admin("functions/form.php");

header("Content-Type: text/javascript");

$a = (string)ac_http_param("a");

if (!$a)
	exit;

$a = ac_sql_escape($a);
$formid = (int)ac_sql_select_one("SELECT id FROM #form WHERE MD5(CONCAT('Oy=Mv qFkk577S:zrzB~', id, 'AhdZDzt_urt[|eVK4:9&')) = '$a'");

if (!$formid)
	exit;

$GLOBALS["form_compile_view"] = "working";

$form = form_select_row($formid);
$form["parts"] = form_compile($form["id"]);

echo "document.write('";
echo "<style>";
$css = @file_get_contents(dirname(__FILE__) . "/admin/templates/form-themes/$form[theme]/style.css");
echo escape($css);
echo "</style>";

echo "<form action=\"$site[p_link]/proc.php\" method=\"post\" id=\"_form_$formid\" accept-charset=\"utf-8\" enctype=\"multipart/form-data\"";

if (isset($form["incga"]) && $form["incga"])
	echo "onsubmit=\"_acUTM(this); return true;\"";

echo ">";

foreach ($form["parts"] as $part) {
	echo escape($part["html"]);
}
echo "</form>";
echo "');";

function escape($str) {
	$str = str_replace("'", "\\'", $str);
	$str = str_replace("\n", "\\n", $str);
	$str = str_replace("\r", "", $str);
	$str = str_replace("\t", "\\t", $str);

	return $str;
}

function erf() {
	die("');");
}

?>
