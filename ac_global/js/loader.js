// loader.js

function ac_loader_add(id, base) {
    var elem = document.getElementById(id);


    if (elem !== null) {
        var img = document.createElement("img");
        img.src = base + "media/loader.gif";
        img.id  = id + "_loader";

        ac_dom_remove_children(elem);
        elem.appendChild(img);
    }
}

function ac_loader_rem(id) {
    var elem = document.getElementById(id);
    var img  = document.getElementById(id + "_loader");

    if (elem !== null && img !== null) {
        elem.removeChild(img);
    }
}

function ac_loader_show(txt) {
	// cleanup previous
	if ( ac_error_visible() ) ac_error_hide();
	if ( ac_result_visible() ) ac_result_hide();
	if ( txt == '' ) {
		if ( ac_loader_visible() ) ac_loader_hide();
		return;
	} else if ( !txt ) {
		$('ac_loading_text').innerHTML = nl2br(jsLoading);
	} else {
		$('ac_loading_text').innerHTML = nl2br(txt);
	}
	$('ac_loading_bar').className = 'ac_block';
	if ( typeof(ismobile) != "undefined" && ismobile ) {
		$('ac_admin_container').style.display = 'none';
	}
}

function ac_loader_hide() {
	$('ac_loading_bar').className = 'ac_hidden';
	if ( typeof(ismobile) != "undefined" && ismobile ) {
		$('ac_admin_container').style.display = 'inline';
	}
}

function ac_loader_visible() {
	return $('ac_loading_bar').className == 'ac_block';
}

function ac_loader_flip() {
	ac_dom_toggle_class('ac_loading_bar', 'ac_hidden', 'ac_block');
}
