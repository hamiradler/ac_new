var cron_log_str_cant_find   = '{"Cron Log not found."|alang|js}';
var cron_log_str_row         = '{"Process started at %s and finished at %s."|alang|js}';
var cron_log_str_stalled     = '{"an unknown time (possible timeout or error)"|alang|js}';
var cron_log_str_errors      = '{"Errors: "|alang|js}';

{literal}
function cron_log(id) {
	ac_ui_api_call(jsLoading);
	ac_ajax_call_cb("api.php", "cron!ac_cron_log", cron_log_cb, id);
}

function cron_log_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();
	if ( !ary.cnt ) {
		ac_error_show(cron_log_str_cant_find);
		ac_ui_anchor_set(cron_list_anchor());
		return;
	}
	ary.cnt = parseInt(ary.cnt, 10);

	// set count
	$('log_count').innerHTML = ary.cnt;

	// set list
	ac_dom_remove_children($('log_list'));
	$('log_list' ).className = ( ary.cnt >  0 ? 'ac_block' : 'ac_hidden' );
	$('log_empty').className = ( ary.cnt == 0 ? 'ac_block' : 'ac_hidden' );
	if ( ary.cnt > 0 ) {
		for ( var i = 0; i < ary.cnt; i++ ) {
			var row = ary.log[i];
			var enddate = ( row.edate ? sql2date(row.edate).format(datetimeformat) : cron_log_str_stalled );
			// add errors if exist
			if ( row.errors && row.errors != '' ) {
				enddate += cron_log_str_errors + row.errors;
			}
			$('log_list').appendChild(
				Builder.node(
					'li',
					{ className: 'cron_log_row' },
					[
						Builder._text(sprintf(cron_log_str_row, sql2date(row.sdate).format(datetimeformat), enddate))
					]
				)
			);
		}
	}

	//ac_dom_toggle_display("log", "block");
	ac_dom_display_block("log");
}
{/literal}
