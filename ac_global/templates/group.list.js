{literal}
var group_table = new ACTable();
var group_list_sort = "01";
var group_list_offset = "0";
var group_list_filter = "0";
var group_list_sort_discerned = false;

group_table.setcol(0, function(row) {
	if (row.id > 3)
		return Builder.node("input", { type: "checkbox", name: "multi[]", value: row.id, onclick: "ac_form_check_selection_none(this, $('acSelectAllCheckbox'), $('selectXPageAllBox'))" });
	else
		return Builder.node("span");
});

group_table.setcol(1, function(row) {
	var edit = Builder.node("a", { href: sprintf("#form-%d", row.id) }, jsEdit);
	var dele = Builder.node("a", { href: sprintf("#delete-%d", row.id) }, jsDelete);

	// Check permissions

	var ary = [];

	if (typeof group_can_update == "function" && group_can_update()) {
		ary.push(edit);
		ary.push(" ");
	}

	if ( typeof ac_group_row_options == 'function' ) ary = ac_group_row_options(ary, row);

	if (typeof group_can_delete == "function" && group_can_delete() && row.id > 3)
		ary.push(dele);

	return Builder.node("div", { className: "ac_table_row_options" }, ary);
});

group_table.setcol(2, function(row) {
	if (typeof row.a_istrial != "undefined")
		row.title += " " + row.a_istrial;
	return row.title;
});

function group_list_anchor() {
	return sprintf("list-%s-%s-%s", group_list_sort, group_list_offset, group_list_filter);
}

function group_list_tabelize(rows, offset) {
	if (rows.length < 1) {
		// We may have some trs left if we just deleted the last row.
		ac_dom_remove_children($("list_table"));

		$("list_noresults").className = "ac_table_rowgroup";
		ac_ui_api_callback();
		return;
	}
	$("list_noresults").className = "ac_hidden";
	$("loadingBar").className = "ac_hidden";
	$("acSelectAllCheckbox").checked = false;
	$("selectXPageAllBox").className = 'ac_hidden';
	ac_paginator_tabelize(group_table, "list_table", rows, offset);

	// We also need to fill up the list of potential groups to move users to when
	// deleting

	ac_dom_remove_children($("delete_alt"));
	$("delete_alt").appendChild(Builder.node("option", { value: "0" }, jsNoGroup));

	for (var i = 0; i < rows.length; i++)
		$("delete_alt").appendChild(Builder.node("option", { value: rows[i].id }, rows[i].title));

	$("delete_alt").getElementsByTagName("option")[0].selected = true;
}

// This function should only be run through a paginator (e.g., paginators[n].paginate(offset))
function group_list_paginate(offset) {
	if (!ac_loader_visible() && !ac_result_visible() && !ac_error_visible())
		ac_ui_api_call(jsLoading);

	if (group_list_filter > 0)
		$("list_clear").style.display = "inline";
	else
		$("list_clear").style.display = "none";

	group_list_offset = parseInt(offset, 10);

	ac_ui_anchor_set(group_list_anchor());
	ac_ajax_call_cb(this.ajaxURL, this.ajaxAction, paginateCB, this.id, group_list_sort, group_list_offset, group_list_filter);

	$("list").className = "ac_block";
}

function group_list_clear() {
	group_list_sort = "01";
	group_list_offset = "0";
	group_list_filter = "0";
	$("list_search").value = "";
	group_search_defaults();
	ac_ui_anchor_set(group_list_anchor());
}

function group_list_search() {
	var post = ac_form_post($("list"));
	ac_ajax_post_cb("api.php", "group!ac_group_filter_post", group_list_search_cb, post);
}

function group_list_search_cb(xml) {
	var ary = ac_dom_read_node(xml, null);

	group_list_filter = ary.filterid;
	ac_ui_anchor_set(group_list_anchor());
}

function group_list_chsort(newSortId) {
	var sortlen = group_list_sort.length;
	var oldSortId = ( group_list_sort.substr(sortlen-1, 1) == 'D' ? group_list_sort.substr(0, 2) : group_list_sort );
	var oldSortObj = $('list_sorter' + oldSortId);
	var sortObj = $('list_sorter' + newSortId);
	// if sort column didn't change (only direction [asc|desc] did)
	if ( oldSortId == newSortId ) {
		// switching asc/desc
		if ( group_list_sort.substr(sortlen-1, 1) == 'D' ) {
			// was DESC
			newSortId = group_list_sort.substr(0, 2);
			sortObj.className = 'ac_sort_asc';
		} else {
			// was ASC
			newSortId = group_list_sort + 'D';
			sortObj.className = 'ac_sort_desc';
		}
	} else {
		// remove old group_list_sort
		if ( oldSortObj ) oldSortObj.className = 'ac_sort_other';
		// set sort field
		sortObj.className = 'ac_sort_asc';
	}
	group_list_sort = newSortId;
	ac_ui_api_call(jsSorting);
	ac_ui_anchor_set(group_list_anchor());
	return false;
}

function group_list_discern_sortclass() {
	if (group_list_sort_discerned)
		return;

	var elems = $("list_head").getElementsByTagName("a");

	for (var i = 0; i < elems.length; i++) {
		var str = sprintf("list_sorter%s", group_list_sort.substring(0, 2));

		if (elems[i].id == str) {
			if (group_list_sort.match(/D$/))
				elems[i].className = "ac_sort_desc";
			else
				elems[i].className = "ac_sort_asc";
		} else {
			elems[i].className = "ac_sort_other";
		}
	}

	group_list_sort_discerned = true;
}

{/literal}
