<?php

require_once ac_admin("functions/form.php");

class form_context extends ACP_Page {
	function form_context() {
		$this->pageTitle = _p("Subscribe");
		parent::ACP_Page();
	}

	function process(&$smarty) {

		$this->setTemplateData($smarty);
		$smarty->assign("content_template", "form.htm");

		if (!$this->site["general_public"] && $_SERVER['REQUEST_METHOD'] != 'GET') {
			ac_smarty_redirect($smarty, $this->site["p_link"] . "/admin/");
		}


		// get form id
		$id = (int)ac_http_param('id');
		if ( !$id ) $id = 1;

		// get form object
		$form = form_select_row($id);
		if ( !$form and $id != 1 ) {
			$form = form_select_row($id = 1);
		}
		if ( !$form ) {
			$allforms = form_select_array();
			if ( $allforms ) {
				$form = current($allforms);
				$id = $form["id"];
			}
		}

		if ( !$form ) {
			ac_smarty_redirect($smarty, $this->site["p_link"] . "/admin/");
		}

		// get source character set
		$_charset = (string)ac_http_param('_charset');
		if ( !$_charset ) $_charset = _i18n('utf-8');

		// generate the actual subscription form
		$GLOBALS["form_compile_view"] = "working";
		$form['parts'] = form_compile($id);
		$form['offerlists'] = false;
		$form['offerunsub'] = false;
		foreach ( $form['parts'] as $part ) {
			if ( $part['builtin'] == 'listselector' ) {
				$form['offerlists'] = true;
			} elseif ( $part['builtin'] == 'unsubscribe' ) {
				$form['offerunsub'] = true;
			}
		}

		// get lists
		//$form['lists'] = ac_sql_select_box_array("SELECT l.id, l.name FROM #form_list f, #list l WHERE f.formid = '$id' AND f.listid = l.id");
		$form['lists'] = ac_sql_select_list("SELECT listid FROM #form_list WHERE formid = '$id'");
		if ( !$form['lists'] ) {
			// select all lists here or something?
			die("Unknown problem occurred -- please contact customer support for help.");
		}
		$listsstr = implode("', '", $form['lists']);

		$form['offerchset'] = ( $_charset != _i18n('utf-8') );
		//$form['offerchset'] = false;
		if ( $_charset != _i18n('utf-8') ) {
			$code = str_replace(
				'<input type="hidden" name="_charset" value="' . _i18n('utf-8') . '" />',
				'<input type="hidden" name="_charset" value="' . $_charset . '" />',
				$code
			);
		}
		$smarty->assign("_charset", $_charset);

		$cond = "
			id IN ('$listsstr')
			AND
			(
				( analytics_ua != '' AND p_use_analytics_read = 1 )
			OR
				( analytics_domains != '' AND analytics_source != '' AND p_use_analytics_link = 1 )
			)
		";
		$form['incga'] = 0;//ac_sql_select_one("=COUNT(*)", "#list", $cond); // enable later when we start saving it

		$smarty->assign("form", $form);

		// subscriber
		$hash = trim((string)ac_http_param('s'));
		$subscriber = subscriber_exists($hash, 0, "hash");

		// subscribe codes
		$mode = ac_http_param('action');//ac_http_param('mode');
		$lists = ac_http_param('lists');
		$codes = ac_http_param('codes');

		$smarty->assign("subscriber", $subscriber);
		$smarty->assign("act", "sub");

		$extra = array(
			"act" => "sub",
			"s" => $subscriber["hash"],
			"c" => (int)ac_http_param("c"),
			"m" => (int)ac_http_param("m"),
		);
		$smarty->assign("html", $x = html_pprint(form_html($form, $extra)));
	}
}

?>
