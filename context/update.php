<?php

require_once ac_admin("functions/form.php");

class update_context extends ACP_Page {
	function update_context() {
		$this->pageTitle = _p("Subscribe");
		parent::ACP_Page();
	}

	function process(&$smarty) {

		$this->setTemplateData($smarty);
		$smarty->assign("content_template", "form.htm");

		if (!$this->site["general_public"] && $_SERVER['REQUEST_METHOD'] != 'GET') {
			ac_smarty_redirect($smarty, $this->site["p_link"] . "/admin/");
		}

		// get form id
		$listid = (int)ac_http_param('lists');

		if (!$listid)
			$listid = (int)ac_http_param("listid");

		if (!$listid)
			ac_http_redirect(ac_site_plink());

		$hash = ac_http_param("s");
		$sub = subscriber_exists($hash, array($listid), 'hash');

		// get form object
		$form = form_select_row_listid($listid, FORM_DETAILS);
		if ( !$form ) {
			ac_http_redirect("index.php");
		}

		$id = $form["id"];

		// get source character set
		$_charset = (string)ac_http_param('_charset');
		if ( !$_charset ) $_charset = _i18n('utf-8');

		// get lists
		$form['lists'] = ac_sql_select_list("SELECT listid FROM #form_list WHERE formid = '$id'");
		if ( !$form['lists'] ) {
			// select all lists here or something?
			die("Unknown problem occurred -- please contact customer support for help.");
		}
		$listsstr = implode("', '", $form['lists']);

		// Grab some fields for this subscriber.
		$fieldids = ac_sql_select_list("SELECT fieldid FROM #field_rel WHERE relid IN ('$listsstr')");
		$fieldstr = implode("','", $fieldids);
		$rs = ac_sql_query("SELECT fieldid, val FROM #field_value WHERE relid = '$sub[id]' AND fieldid IN ('$fieldstr')");

		while ($row = ac_sql_fetch_assoc($rs)) {
			$sub["fields"][$row["fieldid"]] = $row["val"];
		}

		// generate the actual subscription form
		$GLOBALS["form_compile_view"] = "working";
		$form['parts'] = form_compile($id, $sub);
		$form['offerlists'] = false;
		$form['offerunsub'] = false;
		foreach ( $form['parts'] as $part ) {
			if ( $part['builtin'] == 'listselector' ) {
				$form['offerlists'] = true;
			} elseif ( $part['builtin'] == 'unsubscribe' ) {
				$form['offerunsub'] = true;
			}
		}

		$form['offerchset'] = ( $_charset != _i18n('utf-8') );
		//$form['offerchset'] = false;
		if ( $_charset != _i18n('utf-8') ) {
			$code = str_replace(
				'<input type="hidden" name="_charset" value="' . _i18n('utf-8') . '" />',
				'<input type="hidden" name="_charset" value="' . $_charset . '" />',
				$code
			);
		}
		$smarty->assign("_charset", $_charset);

		$cond = "
			id IN ('$listsstr')
			AND
			(
				( analytics_ua != '' AND p_use_analytics_read = 1 )
			OR
				( analytics_domains != '' AND analytics_source != '' AND p_use_analytics_link = 1 )
			)
		";
		$form['incga'] = 0;//ac_sql_select_one("=COUNT(*)", "#list", $cond); // enable later when we start saving it

		$smarty->assign("form", $form);

		// subscriber
		$hash = trim((string)ac_http_param('s'));
		$subscriber = subscriber_exists($hash, 0, "hash");

		// subscribe codes
		$mode = ac_http_param('action');//ac_http_param('mode');
		$lists = ac_http_param('lists');
		$codes = ac_http_param('codes');

		$smarty->assign("subscriber", $subscriber);
		$smarty->assign("act", "update");

		$extra = array(
			"act" => "update",
			"s" => $subscriber["hash"],
			"c" => (int)ac_http_param("c"),
			"m" => (int)ac_http_param("m"),
		);
		$smarty->assign("html", $x = html_pprint(form_html($form, $extra)));
	}
}

?>
