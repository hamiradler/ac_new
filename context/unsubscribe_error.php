<?php

require_once ac_admin("functions/form.php");

class unsubscribe_error_context extends ACP_Page {
	function unsubscribe_error_context() {
		$this->pageTitle = _p("Subscribe");
		parent::ACP_Page();
	}

	function process(&$smarty) {
		$this->setTemplateData($smarty);
		$smarty->assign("content_template", "form.htm");

		if (!$this->site["general_public"] && $_SERVER['REQUEST_METHOD'] != 'GET') {
			ac_smarty_redirect($smarty, $this->site["p_link"] . "/admin/");
		}

		$errors = "";
		if (isset($_SESSION["unsubscribe_error_message"])) {
			$errors = $_SESSION["unsubscribe_error_message"];
			$smarty->assign("error_message", $errors);
			unset($_SESSION["unsubscribe_error_message"]);
		}

		// get form id
		$listid = (int)ac_http_param('listid');

		if (!$listid)
			ac_http_redirect(ac_site_plink());

		// get form object
		$form = form_select_row_listid($listid, FORM_SUBSCRIBE_ERROR);
		if ( !$form ) {
			ac_http_redirect("index.php");
		}

		if ($form["redirecturl"] != "")
			ac_http_redirect($form["redirecturl"]);

		$id = $form["id"];

		// get source character set
		$_charset = (string)ac_http_param('_charset');
		if ( !$_charset ) $_charset = _i18n('utf-8');

		// subscriber
		$hash = trim((string)ac_http_param('s'));
		$subscriber = subscriber_exists($hash, $listid, "hash");

		if ( $subscriber ) {
			$subscriber["error_list"] = $listid;
			$subscriber["errors"] = $errors;
		}

		// generate the actual subscription form
		$GLOBALS["form_compile_view"] = "working";
		$form['parts'] = form_compile($id, $subscriber);
		$form['offerlists'] = false;
		$form['offerunsub'] = false;
		foreach ( $form['parts'] as $part ) {
			if ( $part['builtin'] == 'listselector' ) {
				$form['offerlists'] = true;
			} elseif ( $part['builtin'] == 'unsubscribe' ) {
				$form['offerunsub'] = true;
			}
		}

		// get lists
		//$form['lists'] = ac_sql_select_box_array("SELECT l.id, l.name FROM #form_list f, #list l WHERE f.formid = '$id' AND f.listid = l.id");
		$form['lists'] = ac_sql_select_list("SELECT listid FROM #form_list WHERE formid = '$id'");
		if ( !$form['lists'] ) {
			// select all lists here or something?
			die("Unknown problem occurred -- please contact customer support for help.");
		}
		$listsstr = implode("', '", $form['lists']);

		$form['offerchset'] = ( $_charset != _i18n('utf-8') );
		//$form['offerchset'] = false;
		if ( $_charset != _i18n('utf-8') ) {
			$code = str_replace(
				'<input type="hidden" name="_charset" value="' . _i18n('utf-8') . '" />',
				'<input type="hidden" name="_charset" value="' . $_charset . '" />',
				$code
			);
		}
		$smarty->assign("_charset", $_charset);

		$cond = "
			id IN ('$listsstr')
			AND
			(
				( analytics_ua != '' AND p_use_analytics_read = 1 )
			OR
				( analytics_domains != '' AND analytics_source != '' AND p_use_analytics_link = 1 )
			)
		";
		$form['incga'] = 0;//ac_sql_select_one("=COUNT(*)", "#list", $cond); // enable later when we start saving it

		$smarty->assign("form", $form);

		// subscribe codes
		$mode = ac_http_param('action');//ac_http_param('mode');
		$lists = ac_http_param('lists');
		$codes = ac_http_param('codes');

		ac_smarty_submitted($smarty, $this);
		$smarty->assign("subscriber", $subscriber);
		$smarty->assign("act", "unsub_error");
		$extra = array(
			"act" => "unsub_error",
			"s" => isset($subscriber["hash"]) ? $subscriber["hash"] : "",
			"c" => (int)ac_http_param("c"),
			"m" => (int)ac_http_param("m"),
		);
		$smarty->assign("html", $x = html_pprint(form_html($form, $extra)));
	}
}

?>
