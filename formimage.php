<?php

require_once dirname(__FILE__) . "/admin/prepend.inc.php";
require_once ac_global_functions("http.php");

$partid = (int)ac_http_param("partid");
$part = ac_sql_select_row("SELECT * FROM #form_part WHERE id = '$partid' AND builtin = 'image' AND imagesize > 0");

if ($part) {
	header("Content-Type: $part[imagetype]");
	header("Content-Length: $part[imagesize]");
	echo $part["imagedata"];
	exit;
}

?>
